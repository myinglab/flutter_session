#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "IngAppSecurityPlugin.h"

FOUNDATION_EXPORT double ing_app_securityVersionNumber;
FOUNDATION_EXPORT const unsigned char ing_app_securityVersionString[];

