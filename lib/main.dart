import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter_appcenter_bundle/flutter_appcenter_bundle.dart';
import 'package:iversioning/data/web/iversioning_json.dart';
import 'package:iversioning/iversioning.dart';
import 'package:nesternship_flut/bloc/user_bloc.dart';
import 'package:nesternship_flut/data/database/app_database.dart';
import 'package:nesternship_flut/datalayer/data_factory.dart';
import 'package:nesternship_flut/ui/screen/landing_screen.dart';
import 'package:nesternship_flut/ui/screen/main_screen.dart';
import 'package:nesternship_flut/ui/screen/splash_screen.dart';
import 'package:nesternship_flut/ui/style/app_colors.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_auth/nui_auth.dart';
import 'package:nui_core/busevent/nui_busevent.dart';
import 'package:nui_core/code/nui_code_util.dart';
import 'package:nui_core/ui/component/nui_app.dart';
import 'package:nui_core/ui/nui_display_util.dart';
import 'package:nui_core/ui/nui_sharedpref_util.dart';
import 'package:nui_database/nui_database.dart';
import 'package:nui_error_handler/nui_error_handler.dart';
import 'package:nui_media/nui_media.dart';
import 'package:nui_push/nui_push.dart';
import 'package:nui_web/nui_web.dart';
import 'ui/screen/landing_screen.dart';

final errorHandler = () => NUIErrorHandlerBuilder()
        .module("app")
        .appVersion(appConfig().versionName)
        .showErrorOnEncounter(false)
        .redirect((context, actionCode, errorMapping) {
      switch (actionCode) {
        case "404":
          {
            //Todo something here
            break;
          }
        case "401":
          {
            //Todo something here
            break;
          }
        case "1001":
          {
            //Todo something here
            break;
          }
      }
    });

//final logging = () => NUILogBuilder()
//    .appVersion(appConfig().versionName)
//    .appCenterAndroidKey(configField<String>("appCenterKeyAndroid"))
//    .appId(appConfig().appId);

const String APP_MODULE = "Nesternship";

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown
  ]).then((_) {
    runNUIAppWithErrorHandler(
        NUIApp(
          buildType: BuildType.DEV,
          title: "Nesternship",
          themeData: (context) => theme(context),
          overlayStyle: (context) => NUIOverlayStyle(darkStatusBarIcon: false),
          preAppTask: (context) async {

            await NUIBusEventBuilder().build();

            await AppCenter.startAsync(
              appSecretAndroid: '1c77eba8-ae1c-4b66-8704-e8244c406428',
              appSecretIOS: '00c55e74-0106-4291-8894-f5ad8b75e0ac',
              enableAnalytics: true, // Defaults to true
              enableCrashes: true, // Defaults to true
              enableDistribute: true, // Defaults to false
              usePrivateDistributeTrack: false, // Defaults to false
              disableAutomaticCheckForUpdate: false, // Defaults to false
            );

            final versioning = IVersioningBuilder()
                .appVersion(appConfig().versionName)
                .baseUrl(appConfig().baseURL)
                .apiKey(configField<String>("apiKey"))
                .dialogTheme(IDialogTheme(accentColor: AppColors.AccentBright.value.toRadixString(16), backgroundColor: AppColors.BackgroundWhite.value.toRadixString(16), textOnAccentColor: AppColors.TextWhite.value.toRadixString(16), textOnBackgroundColor: AppColors.TextDarkGray.value.toRadixString(16), textHighlightColor: AppColors.AccentBright.value.toRadixString(16)))
                .build();

            //Build shared preference
            final pref = NUISharedPrefBuilder().setInitials({
              "appName": "Nesternship",
            }).build();

            //TODO: Setup Google Login for XCode
            final auth = NUIAuthBuilder()
                .appVersion(appConfig().versionName)
                .baseUrl(appConfig().baseURL)
                .build();

            final media = NUIMediaBuilder().build();
            await DataFactory.instance.getManipulator(RepoType.sqlitedb);

            //TODO: Setup OneSignal for XCode https://documentation.onesignal.com/docs/flutter-sdk-setup
            final push = NUIPushBuilder()
                .oneSignalAppId("75d5afbb-61b0-4400-9fd9-ea15e14b2285")
                .notificationOpenedListener((notification) {
              logNUI("Main", "Notification opened : ${notification.header}");
            })
                .notificationReceivedListener((notification) {
              logNUI("Main", "Notification received : ${notification.header}");
            })
                .build();

            //Build http web instance
            final web = NUIWebBuilder()
                .headers(() => {
              "Authorization":
              "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiI2NiIsInVzZXIiOnsicm9sZSI6IlJPTEVfVVNFUiIsImlkIjoiNjYifSwiaWF0IjoxNTk1MjIxMjczLCJleHAiOjE1OTUzMDc2NzN9.edhC0tJm5E_1huvFuF9FJQPBoEiAzjvQBXIFegkdKkxbOJc0UjA1Aa2rbshgqFP3RbY9OuaY3qB2kLebeX3cFA",
              "X-DeviceOs": "web"
            })
                .module(APP_MODULE)
                .baseURL(appConfig().baseURL)
                .errorHandler(errorHandler)
                .statusMessages({
              404: "Oops, this page you're looking for is not found",
              500: "Oops, seems like something went wrong"
            }).build();

            //Build database
            final db = NUIDatabaseBuilder()
                .readyCallback((success, message) {
              logNUI("Main", "Open database with success : $success");
            })
                .model(AppDatabase())
                .build();

            await db;
            await pref;
            await push;

            logNUI("Main", "Finish loading builders");
            return true;
          },
          homeScreen: (context) async => !UserBloc().isLoggedIn() ?  LandingScreen() : MainScreen(),
          loadingScreen: (context) => SplashScreen(),
        ),
        errorHandler);
  });

}

