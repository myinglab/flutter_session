import 'package:nui_core/nui_core.dart';

class ApiSetting {
//  static const SESSION_TOKEN_NAME = "token";
//  static bool dataEncrypt = false;
//  static List<String>
//      sslPinningFingerPrints; //["64 CD 58 02 7F 6D BC 6D 80 1C 6B EF DA 4F 31 DD 1B 92 2F E9 7C 45 62 6C 78 01 1A 10 61 2C 1F DE"];
//  static String serverUrl = "http://inglabcloud.hopto.org:6080/nesternship/";
//  static String dbName = "nesternship";
//  static String token = "";

  static String getServerUrl(){
    return configField<String>("baseURL");
  }

  static String getSessionTokenName(){
    return configField<String>("sessionTokenName");
  }

  static String getDbName(){
    return configField<String>("dbName");
  }

  static bool getDataEncrypt(){
    return configField<bool>("dataEncrypt");
  }
}
