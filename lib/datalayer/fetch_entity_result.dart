import 'package:ingdao/dataflow/control/dfresponse.dart';

class FetchEntityResult<T> {
  DfError error;
  List<T> entities;

  FetchEntityResult({this.entities, this.error});
}
