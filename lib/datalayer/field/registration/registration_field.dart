import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/repository/dfbaserepository.dart';
import 'package:nesternship_flut/datalayer/db/registration_db.dart';
import 'package:nesternship_flut/datalayer/entity/registration/registration_request.dart';

import '../../data_factory.dart';

abstract class RegistrationField {
  DfBaseField fmemberEmail;
  DfBaseField fmemberName;
  DfBaseField fuserId;
  DfBaseField favatar;
  DfBaseField fdateOfBirth;
  DfBaseField fgender;
  DfBaseField foriginCountry;
  DfBaseField fnestleCountry;

  static RegistrationField createFieldBasedOnRepo(DfBaseRepository repo) {
    switch (repo.model.name) {
      case REPO_NAME_SQLITEDB:
        return RegistrationDb();
      case REPO_NAME_WEB:
        return RegistrationRequest();
    }
    return null;
  }
}