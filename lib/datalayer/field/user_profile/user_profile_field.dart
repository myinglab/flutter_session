import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/repository/dfbaserepository.dart';
import 'package:nesternship_flut/datalayer/db/registration_db.dart';
import 'package:nesternship_flut/datalayer/db/user_profile_db.dart';
import 'package:nesternship_flut/datalayer/entity/registration/registration_request.dart';
import 'package:nesternship_flut/datalayer/entity/user_profile/user_profile_request.dart';

import '../../data_factory.dart';

abstract class UserProfileField {
  DfBaseField fmemberEmail;
  DfBaseField fmemberName;
  DfBaseField fId;
  DfBaseField fuserId;
  DfBaseField favatar;
  DfBaseField fdateOfBirth;
  DfBaseField fgender;
  DfBaseField foriginCountry;
  DfBaseField fnestleCountry;

  static UserProfileField createFieldBasedOnRepo(DfBaseRepository repo) {
    switch (repo.model.name) {
      case REPO_NAME_SQLITEDB:
        return UserProfileDb();
      case REPO_NAME_WEB:
        return GetUserProfileRequest();
    }
    return null;
  }
}
