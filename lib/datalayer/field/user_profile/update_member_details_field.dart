import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/repository/dfbaserepository.dart';
import 'package:nesternship_flut/datalayer/db/update_member_details_db.dart';
import 'package:nesternship_flut/datalayer/entity/user_profile/update_member_details_request.dart';
import '../../data_factory.dart';
abstract class UpdateMemberDetailsField {

  DfBaseField fId;
  DfBaseField fMemberEmail;
  DfBaseField fMemberName;
  DfBaseField fUserId;
  DfBaseField fAvatar;
  DfBaseField fDateOfBirth;
  DfBaseField fGender;
  DfBaseField fOriginCountry;
  DfBaseField fNestleCountry;

  static UpdateMemberDetailsField createFieldBasedOnRepo(DfBaseRepository repo) {
    switch (repo.model.name) {
      case REPO_NAME_SQLITEDB:
       return UpdateMemberProfileDb();
      case REPO_NAME_WEB:
       return UpdateMemberDetailsRequest();
    }
    return null;
  }
}