import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/repository/dfbaserepository.dart';
import 'package:nesternship_flut/datalayer/entity/category/category_list_response.dart';

import '../../data_factory.dart';

abstract class CategoryListField {
  DfBaseField fCourseList;

  static CategoryListField createFieldBasedOnRepo(DfBaseRepository repo) {
    switch (repo.model.name) {
      case REPO_NAME_WEB:
        return CategoryListResponse();
    }
    return null;
  }
}
