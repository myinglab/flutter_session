import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/repository/dfbaserepository.dart';
import 'package:nesternship_flut/datalayer/entity/category/category_response.dart';
import '../../data_factory.dart';

abstract class CategoryField {
  DfBaseField fId;
  DfBaseField fTitle;
  DfBaseField fSequence;
  DfBaseField fCreatedDate;
  DfBaseField fModifiedDate;

  static CategoryField createFieldBasedOnRepo(DfBaseRepository repo) {
    switch (repo.model.name) {
      case REPO_NAME_WEB:
        return CategoryResponse();
    }
    return null;
  }
}
