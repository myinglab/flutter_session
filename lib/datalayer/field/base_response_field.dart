import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/repository/dfbaserepository.dart';
import 'package:nesternship_flut/datalayer/entity/base_response.dart';

import '../data_factory.dart';

abstract class BaseResponseField {
  DfBaseField fMessage;
  DfBaseField fStatus;
  DfBaseField fErrorCode;
  DfBaseField fData;

  static BaseResponseFieldcreateFieldBasedOnRepo(DfBaseRepository repo) {
    switch (repo.model.name) {
      // case REPO_NAME_SQLITEDB :
      //   return TankTimesheet();
      case REPO_NAME_WEB:
        return BaseResponse();
    }
    return null;
  }
}
