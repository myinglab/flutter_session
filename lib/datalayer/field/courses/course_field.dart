import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/repository/dfbaserepository.dart';
import 'package:nesternship_flut/datalayer/db/course_db.dart';
import 'package:nesternship_flut/datalayer/entity/course/course_response.dart';
import '../../data_factory.dart';

abstract class CourseField {
  DfBaseField fId;
  DfBaseField fType;
  DfBaseField fTitle;
  DfBaseField fDescription;
  DfBaseField fThumbnailUrl;
  DfBaseField fSourceUrl;
  DfBaseField fVideoDuration;
  DfBaseField fAvailableInCountries;
  DfBaseField fCategoryId;
  DfBaseField fMandatoryFlag;
  DfBaseField fDashboardDisplay;
  DfBaseField fSeminarStartDate;
  DfBaseField fSeminarEndDate;
  DfBaseField fCreatedDate;
  DfBaseField fModifiedDate;

  static CourseField createFieldBasedOnRepo(DfBaseRepository repo) {
    switch (repo.model.name) {
      case REPO_NAME_SQLITEDB:
        return CourseDb();
      case REPO_NAME_WEB:
        return CourseResponse();
    }
    return null;
  }
}
