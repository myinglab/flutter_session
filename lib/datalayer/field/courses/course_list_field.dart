import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/repository/dfbaserepository.dart';
import 'package:nesternship_flut/datalayer/entity/course/course_list_response.dart';

import '../../data_factory.dart';

abstract class CourseListField {
  DfBaseField fCourseList;

  static CourseListField createFieldBasedOnRepo(DfBaseRepository repo) {
    switch (repo.model.name) {
      case REPO_NAME_WEB:
        return CourseListResponse();
    }
    return null;
  }
}
