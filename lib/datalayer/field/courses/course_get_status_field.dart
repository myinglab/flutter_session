import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/repository/dfbaserepository.dart';
import 'package:nesternship_flut/datalayer/data_factory.dart';
import 'package:nesternship_flut/datalayer/entity/course/course_get_status_response.dart';

abstract class CourseGetStatusField {
  DfBaseField fCourseCompleted;

  static CourseGetStatusField createFieldBasedOnRepo(DfBaseRepository repo) {
    switch (repo.model.name) {
      case REPO_NAME_WEB:
        return CourseGetStatusResponse();
    }
    return null;
  }
}
