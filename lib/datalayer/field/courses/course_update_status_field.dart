import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/repository/dfbaserepository.dart';
import 'package:nesternship_flut/datalayer/data_factory.dart';
import 'package:nesternship_flut/datalayer/entity/course/course_update_status_request.dart';

abstract class CourseUpdateStatusField {
  DfBaseField fCourseId;
  DfBaseField fUserId;
  DfBaseField fCourseCompleted;

  static CourseUpdateStatusField createFieldBasedOnRepo(DfBaseRepository repo) {
    switch (repo.model.name) {
      case REPO_NAME_WEB:
        return CourseUpdateStatusRequest();
    }
    return null;
  }
}
