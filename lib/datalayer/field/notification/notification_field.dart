import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/repository/dfbaserepository.dart';
import 'package:nesternship_flut/datalayer/db/notification_db.dart';
import 'package:nesternship_flut/datalayer/entity/notification/notification_count_request.dart';

import '../../data_factory.dart';

abstract class NotificationField {
  DfBaseField fuserId;
  DfBaseField fread;
  DfBaseField fisRead;
  DfBaseField fnotificationContentId;
  DfBaseField fnotificationCount;
  DfBaseField fnotificationId;

  static NotificationField createFieldBasedOnRepo(DfBaseRepository repo) {
    switch (repo.model.name) {
      case REPO_NAME_SQLITEDB:
        return NotificationDb();
      case REPO_NAME_WEB:
        return GetNotificationCountRequest();
    }
    return null;
  }
}
