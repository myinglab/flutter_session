import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/repository/dfbaserepository.dart';
import 'package:nesternship_flut/datalayer/entity/notification/notification_list_request.dart';

import '../../data_factory.dart';

abstract class NotificationListField {
  DfBaseField fNotificationList;

  static NotificationListField createFieldBasedOnRepo(DfBaseRepository repo) {
    switch (repo.model.name) {
      case REPO_NAME_WEB:
        return GetNotificationListRequest();
    }
    return null;
  }
}