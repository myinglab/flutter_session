import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/repository/dfbaserepository.dart';
import 'package:nesternship_flut/datalayer/entity/notification/notification_list_information_response.dart';

import '../../data_factory.dart';

abstract class NotificationListBodyField {
  DfBaseField fNotificationId;
  DfBaseField fUserId;
  DfBaseField fNotificationContentId;
  DfBaseField fNotificationTitle;
  DfBaseField fNotificationDesc;
  DfBaseField fNotificationDateTime;
  DfBaseField fBannerUrl;
  DfBaseField fIsRead;

  static NotificationListBodyField createFieldBasedOnRepo(DfBaseRepository repo) {
    switch (repo.model.name) {
      case REPO_NAME_WEB:return NotificationListInfoResponse();
    }
    return null;
  }
}