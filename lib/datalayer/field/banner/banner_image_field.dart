import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/repository/dfbaserepository.dart';
import 'package:nesternship_flut/datalayer/entity/banner/banner_image_response.dart';

import '../../data_factory.dart';

abstract class BannerImageField {
  DfBaseField fBannerId;
  DfBaseField fBannerUrl;
  DfBaseField fActionUrl;
  DfBaseField fBannerTitle;
  DfBaseField fBannerDescription;
  DfBaseField fSequence;
  DfBaseField fStatus;

  static BannerImageField createFieldBasedOnRepo(DfBaseRepository repo) {
    switch (repo.model.name) {
      case REPO_NAME_WEB: return BannerImageResponse();
    }
    return null;
  }
}