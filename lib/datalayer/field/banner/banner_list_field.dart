import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/repository/dfbaserepository.dart';
import 'package:nesternship_flut/datalayer/entity/banner/banner_image_list_request.dart';

import '../../data_factory.dart';

abstract class BannerImageListField {
  DfBaseField fBannerImageList;

  static BannerImageListField createFieldBasedOnRepo(DfBaseRepository repo) {
    switch (repo.model.name) {
      case REPO_NAME_WEB:
       return GetBannerImagesRequest();
    }
    return null;
  }
}