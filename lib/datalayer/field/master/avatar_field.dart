import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/repository/dfbaserepository.dart';
import 'package:nesternship_flut/datalayer/db/country_db.dart';
import 'package:nesternship_flut/datalayer/entity/master/avatar_response.dart';
import 'package:nesternship_flut/datalayer/entity/master/country_response.dart';

import '../../data_factory.dart';

abstract class AvatarField {
  DfBaseField fId;
  DfBaseField fAvatar;
  DfBaseField fBinaryData;
  DfBaseField fStatus;

  static AvatarField createFieldBasedOnRepo(DfBaseRepository repo) {
    switch (repo.model.name) {
      case REPO_NAME_WEB:
        return AvatarResponse();
    }
    return null;
  }
}
