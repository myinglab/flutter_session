import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/repository/dfbaserepository.dart';
import 'package:nesternship_flut/datalayer/db/country_db.dart';
import 'package:nesternship_flut/datalayer/entity/master/country_response.dart';

import '../../data_factory.dart';

abstract class CountryField {
  DfBaseField fCountryId;
  DfBaseField fCountryName;
  DfBaseField fCountryCode;
  DfBaseField fIsNestleCountry;

  static CountryField createFieldBasedOnRepo(DfBaseRepository repo) {
    switch (repo.model.name) {
      case REPO_NAME_SQLITEDB:
        return CountryDb();
      case REPO_NAME_WEB:
        return CountryResponse();
    }
    return null;
  }
}
