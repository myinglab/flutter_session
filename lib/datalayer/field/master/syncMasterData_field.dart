import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/repository/dfbaserepository.dart';
import 'package:nesternship_flut/datalayer/db/sync_master_data_db.dart';
import 'package:nesternship_flut/datalayer/entity/master/sync_master_data_request.dart';

import '../../data_factory.dart';

abstract class SyncMasterDataField {
  DfBaseField fCountryList;
  DfBaseField fLastSyncDate;
  DfBaseField fAvatarList;

  static SyncMasterDataField createFieldBasedOnRepo(DfBaseRepository repo) {
    switch (repo.model.name) {
      case REPO_NAME_SQLITEDB:
        return SyncMasterDataDb();
      case REPO_NAME_WEB:
        return SyncMasterDataRequest();
    }
    return null;
  }
}