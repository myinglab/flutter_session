import 'package:ingdao/ingdao.dart';
import 'package:ingdao/dataflow/model/dfwebmodel.dart';
import 'package:ingdao/dataflow/repository/dfwebrepository.dart';
import 'package:ingdao/dataflow/repository/dfsyncrepository.dart';
import 'package:ingdao/dataflow/model/dfdbmodel.dart';
import 'package:nui_web/nui_web.dart';
import 'package:ingdao/dataflow/repository/dfdbrepository.dart';
import 'package:nesternship_flut/datalayer/db/country_db.dart';
import 'package:nesternship_flut/datalayer/entity/category/category_list_response.dart';
import 'package:nesternship_flut/datalayer/entity/category/category_request.dart';
import 'package:nesternship_flut/datalayer/entity/category/category_response.dart';
import 'package:nesternship_flut/datalayer/entity/course/course_get_status_request.dart';
import 'package:nesternship_flut/datalayer/entity/course/course_get_status_response.dart';
import 'package:nesternship_flut/datalayer/entity/course/course_list_response.dart';
import 'package:nesternship_flut/datalayer/entity/course/course_request.dart';
import 'package:nesternship_flut/datalayer/entity/course/course_response.dart';
import 'package:nesternship_flut/datalayer/entity/course/course_search_request.dart';
import 'package:nesternship_flut/datalayer/entity/course/course_update_status_request.dart';
import 'package:nesternship_flut/datalayer/entity/master/avatar_response.dart';
import 'package:nesternship_flut/datalayer/entity/master/country_response.dart';
import 'package:nesternship_flut/datalayer/entity/master/sync_master_data_request.dart';
import 'package:nesternship_flut/datalayer/entity/master/sync_master_data_response.dart';
import 'package:nesternship_flut/datalayer/entity/notification/notification_create_request.dart';
import 'package:nesternship_flut/datalayer/entity/notification/notification_create_response.dart';
import 'package:nesternship_flut/datalayer/entity/notification/notification_list_information_response.dart';
import 'package:nesternship_flut/datalayer/entity/notification/notification_list_request.dart';
import 'package:nesternship_flut/datalayer/entity/notification/notification_list_response.dart';
import 'package:nesternship_flut/datalayer/entity/notification/notification_delete_request.dart';
import 'package:nesternship_flut/datalayer/entity/notification/notification_delete_response.dart';
import 'package:nesternship_flut/datalayer/entity/notification/notification_list_clear_request.dart';
import 'package:nesternship_flut/datalayer/entity/notification/notification_list_clear_response.dart';
import 'package:nesternship_flut/datalayer/entity/user_profile/set_member_inactive_request.dart';
import 'package:nesternship_flut/datalayer/entity/notification/notification_count_response.dart';
import 'package:nesternship_flut/datalayer/entity/notification/set_notification_read_request.dart';
import 'package:nesternship_flut/datalayer/entity/notification/set_notification_read_response.dart';
import 'package:nesternship_flut/datalayer/entity/user_profile/user_profile_request.dart';
import 'package:nesternship_flut/datalayer/entity/user_profile/user_profile_response.dart';
import 'package:nesternship_flut/datalayer/entity/user_profile/update_member_details_request.dart';
import 'package:nesternship_flut/datalayer/entity/user_profile/update_member_details_response.dart';
import 'package:nesternship_flut/datalayer/entity/banner/banner_image_response.dart';
import 'package:nesternship_flut/datalayer/entity/banner/banner_image_list_request.dart';
import 'package:nesternship_flut/datalayer/entity/banner/banner_image_list_response.dart';
import 'package:nui_auth/nui_auth.dart';
import 'package:nui_core/nui_core.dart';

import 'api_setting.dart';
import 'db/registration_db.dart';
import 'entity/base_response.dart';

import 'entity/course/course_by_country_request.dart';
import 'entity/notification/notification_count_request.dart';

import 'entity/registration/check_duplicate_email_request.dart';
import 'entity/registration/registration_request.dart';
import 'entity/registration/registration_response.dart';

const REPO_NAME_SQLITEDB = "sqlitedb";
const REPO_NAME_WEB = "web";
const REPO_NAME_ONLINE_SYNC = "onlinesync";
const CODE_NO_INTERNET = 2001;

enum RepoType { sqlitedb, web, onlinesync }


extension RepositoryTypeExtension on RepoType {
  String get name {
    switch (this) {
      case RepoType.sqlitedb:
        return REPO_NAME_SQLITEDB;
      case RepoType.web:
        return REPO_NAME_WEB;
      case RepoType.onlinesync:
        return REPO_NAME_ONLINE_SYNC;
      default:
        return null;
    }
  }

  String desp() {
    return this.name;
  }
}

class DataFactory {
  DataFactory._();
  static final instance = DataFactory._();
  static Map<String, DfFactory> _storedFactory = Map();

  Future<DfFactory> _initWebRepo(RepoType type) async {
    DfFactory dfFactory = DfFactory();
    Map<String, String> headers = Map<String, String>();

    headers["Content-Type"] = "application/json";
    headers["X-DeviceOs"] = "android";
    headers["Version"] = configField<String>("versionName");

    final accessToken = NUIAuth.get().getAccessToken();
    if (accessToken != null) {
      final bearerToken = accessToken.token;
      headers["Authorization"] = "Bearer $bearerToken";
    }

    //Web
    SslPinningCri sslPinningCri; //SslPinningCri(shakeys: appSetting.sslPinningFingerPrints, shatype: SslPinningCriShaType.SHA256);

    logNUI("DataFactory", "Internet connection interceptor");
    final internetInterceptor =  InternetCheckingInterceptor(noInternetConnection: (){

    });

    DfWebModel modelWeb = DfWebModel(
        name: RepoType.web.name,
        encrypt: ApiSetting.getDataEncrypt(),
        interceptors: [
          internetInterceptor,
          NUIAuthWebInterceptor(onTokenExpired: () async {
            logNUI("DataFactory", "Token has expired and failed to refresh, go back to login screen");
            NUIBusEvent.get().send(NUIBusEventData<String>(
                data: "Session expired",
                sender: "Web interceptor",
                code: "401")
            );
          }),
        ],
        entities: [
          RegistrationRequest(),
          BaseResponse(),
          CheckDuplicateEmailRequest(),
          RegistrationResponse(),
          GetUserProfileRequest(),
          GetUserProfileResponse(),
          UpdateMemberDetailsRequest(),
          UpdateMemberDetailsResponse(),
          SyncMasterDataResponse(),
          SyncMasterDataRequest(),
          CountryResponse(),
          SetMemberInactiveRequest(),
          GetNotificationCountRequest(),
          NotificationCountResponse(),
          SetNotificationReadRequest(),
          SetNotificationReadResponse(),
          CreateNotificationRequest(),
          CreateNotificationResponse(),
          NotificationListResponse(),
          NotificationListInfoResponse(),
          GetNotificationListRequest(),
          DeleteNotificationRequest(),
          DeleteNotificationResponse(),
          ClearNotificationListRequest(),
          ClearNotificationListResponse(),
          AvatarResponse(),
          BannerImageListResponse(),
          BannerImageResponse(),
          GetBannerImagesRequest(),
          CourseRequest(),
          CourseListResponse(),
          CourseResponse(),
          CategoryRequest(),
          CategoryListResponse(),
          CategoryResponse(),
          CourseSearchRequest(),
          CourseUpdateStatusRequest(),
          CourseGetStatusRequest(),
          CourseGetStatusResponse(),
          CourseByCountryRequest()
        ],
        serverUrl: ApiSetting.getServerUrl(),
        commonHeaders: headers,
        sslPinningCri: sslPinningCri);

    DfWebRepository repWeb = DfWebRepository(model: modelWeb);
    dfFactory.registerRepository(RepoType.web.name, repWeb);
    await dfFactory.activateRepository(type.name);

    return dfFactory;
  }

  Future<DfFactory> _initDBRepo(RepoType type) async {
    logNUI("DataFactory", "Initiating db repo");
    DfFactory dfFactory = DfFactory();
    //Database
    DfDbModel modelDb = DfDbModel(
        name: RepoType.sqlitedb.name,
        dbName: ApiSetting.getDbName(),
        encrypt: ApiSetting.getDataEncrypt(),
        entities: [CountryDb()]);
    DfDbRepository repDb = DfDbRepository(model: modelDb);
    dfFactory.registerRepository(RepoType.sqlitedb.name, repDb);
    await dfFactory.activateRepository(type.name);
    return dfFactory;
  }

  Future<DfFactory> _getFactoryByType(RepoType type) async {
    switch (type) {
      case RepoType.sqlitedb:
        {
          logNUI("DataFactory", "Has db repo cache : ${_storedFactory[type.name] != null}");
          final factory = _storedFactory[type.name] ?? await _initDBRepo(type);
          _storedFactory[type.name] = factory;
          return factory;
        }
      case RepoType.web:
        {
          final factory = await _initWebRepo(type);
          _storedFactory[type.name] = factory;
          return factory;
        }
      default:
        {
          return null;
        }
    }
  }

//  Future<DfFactory> _initSyncRepo(RepoType type) async{
//    DfFactory dfFactory = DfFactory();
//
//    //Online Sync
//    DfSyncModel modelSync = DfSyncModel(
//        name: RepoType.onlinesync.name,
//        retryIntervalInSecond: 15 * 60,
//        dbRepository: repDb,
//        webRepository: repWeb);
//    DfSyncRepository repSync = DfSyncRepository(model: modelSync);
//    dfFactory.registerRepository(RepoType.onlinesync.name, repSync);
//
//    //Activate
//    await dfFactory.activateRepository(type.name);
//    return dfFactory;
//  }

  Future<DfFactory> getManipulator(RepoType type) async {
    return _getFactoryByType(type);
  }
}

class InternetCheckingInterceptor extends NUIWebInterceptor{
  InternetCheckingInterceptor({this.noInternetConnection});
  Function noInternetConnection;

  @override
  Future<NUIWebInterceptorResponse> intercept(String url, NUIWebMethod method, dynamic, Map<String, String> headers, Map<String, dynamic> body) async{
    logNUI("DataFactory", "Intercepting to check on internet connectivity");
    final hasInternet = await NUIConnectionUtil.hasConnection(lookUp: false);
    logNUI("DataFactory", "Intercepting to check on internet connectivity, has internet : $hasInternet");
    if (hasInternet == true) {
      return NUIWebInterceptorResponse(success: true, message: "Has internet connection", interceptResponse: false);
    }
    else{
      if(noInternetConnection != null) noInternetConnection();
      logNUI("DataFactory", "No internet connection found when calling url: $url");
      return NUIWebInterceptorResponse(success: false, message: "No internet connection", interceptResponse: true, code: CODE_NO_INTERNET);
    }
  }

}