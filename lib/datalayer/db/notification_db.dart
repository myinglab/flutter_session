import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/entity/dfdbentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/field/dfdbfield.dart';
import 'package:ingdao/dataflow/field/dffieldtype.dart';
import 'package:nesternship_flut/datalayer/field/notification/notification_field.dart';
import 'package:nesternship_flut/datalayer/field/registration/registration_field.dart';
import 'package:object_mapper/src/mapper.dart';

class NotificationDb extends DfDbEntity with NotificationField {
  NotificationDb(
      {String userId,
      String read,
      String notificationContentId,
      String notificationCount,
      List<int> notificationId,
      bool isRead})
      : super(name: "notification") {
    fuserId = DfDbField(
        entity: this,
        name: "userId",
        primaryKey: true,
        joinKey: false,
        fieldType: DfFieldType.text(),
        value: userId);
    fread = DfDbField(
        entity: this,
        name: "read",
        primaryKey: false,
        joinKey: false,
        fieldType: DfFieldType.text(),
        value: read);
    fnotificationContentId = DfDbField(
        entity: this,
        name: "notificationContentId",
        primaryKey: false,
        joinKey: true,
        fieldType: DfFieldType.text(),
        value: notificationContentId);
    fnotificationCount = DfDbField(
        entity: this,
        name: "Notification Count",
        primaryKey: false,
        joinKey: true,
        fieldType: DfFieldType.text(),
        value: notificationContentId);
    fnotificationId = DfDbField(
        entity: this,
        name: "NotificationId",
        primaryKey: false,
        joinKey: true,
        fieldType: DfFieldType.text(),
        value: notificationId);
    fisRead = DfDbField(
        entity: this,
        name: "isRead",
        primaryKey: false,
        joinKey: false,
        fieldType: DfFieldType.boolean(),
        value: isRead);
  }

  //Define the sqilite table fields available.
  //Define the fields will map back to the sqlite query executed reuslt: Map<String,dynamic>
  @override
  List<DfDbField> fields() {
    return [fuserId, fread, fnotificationContentId, fnotificationCount];
  }

  @override
  DfBaseEntity<DfBaseField> newInstance() {
    return NotificationDb();
  }

  @override
  DfBaseEntity<DfDbField> jsonToObject(
      DfQueryPurpose queryPurpose, Mapper map) {
    return map.toObject<NotificationDb>();
  }

  @override
  void customFieldJsonMap(Mapper map, List<DfDbField> fields) {}

  @override
  DfBaseEntity<DfDbField> parseExecRawJsonResultToEntity(
      DfQueryPurpose queryPurpose, Mapper map) {
    // TODO: implement parseExecRawJsonResultToEntity
    throw UnimplementedError();
  }
}
