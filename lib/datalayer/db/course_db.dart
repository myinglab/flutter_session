import 'package:flutter/material.dart';
import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/entity/dfdbentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/field/dfdbfield.dart';
import 'package:ingdao/dataflow/field/dffieldtype.dart';
import 'package:nesternship_flut/datalayer/field/courses/course_field.dart';
import 'package:object_mapper/src/mapper.dart';

class CourseDb extends DfDbEntity with CourseField {
  String id;
  String type;
  String title;
  String description;
  String thumbnailUrl;
  String sourceUrl;
  String videoDuration;
  String availableInCountries;
  String categoryId;
  bool mandatoryFlag;
  bool dashboardDisplay;
  DateTime seminarStartDate;
  DateTime seminarEndDate;
  DateTime createdDate;
  DateTime modifiedDate;

  CourseDb(
      {int pId,
      String pType,
      String pTitle,
      String pDescription,
      String pThumbnailUrl,
      String pSourceUrl,
      String pAvailableInCountries,
      String pCategoryId,
      bool pMandatoryFlag,
      String pVideoDuration,
      bool pDashboardDisplay,
      DateTime pSeminarStartDate,
      DateTime pSeminarEndDate,
      DateTime pCreatedDate,
      DateTime pModifiedDate})
      : super(name: "course") {
    fId = DfDbField(
        entity: this,
        name: "id",
        fieldType: DfFieldType.integer(),
        primaryKey: true,
        joinKey: false,
        getObjectValue: () => id,
        setObjectValue: (v) => id = v,
        value: pId);
    fTitle = DfDbField(
        entity: this,
        name: "title",
        fieldType: DfFieldType.text(),
        primaryKey: false,
        joinKey: false,
        getObjectValue: () => title,
        setObjectValue: (v) => title = v,
        value: pTitle);
    fType = DfDbField(
        entity: this,
        name: "type",
        fieldType: DfFieldType.text(),
        primaryKey: false,
        joinKey: false,
        getObjectValue: () => type,
        setObjectValue: (v) => type = v,
        value: pType);
    fDescription = DfDbField(
        entity: this,
        name: "description",
        fieldType: DfFieldType.text(),
        primaryKey: false,
        joinKey: false,
        getObjectValue: () => description,
        setObjectValue: (v) => description = v,
        value: pDescription);
    fThumbnailUrl = DfDbField(
        entity: this,
        name: "thumbnailUrl",
        fieldType: DfFieldType.text(),
        primaryKey: false,
        joinKey: false,
        getObjectValue: () => thumbnailUrl,
        setObjectValue: (v) => thumbnailUrl = v,
        value: pThumbnailUrl);
    fSourceUrl = DfDbField(
        entity: this,
        name: "sourceUrl",
        fieldType: DfFieldType.text(),
        primaryKey: false,
        joinKey: false,
        getObjectValue: () => sourceUrl,
        setObjectValue: (v) => sourceUrl = v,
        value: pSourceUrl);
    fAvailableInCountries = DfDbField(
        entity: this,
        name: "availableInCountries",
        fieldType: DfFieldType.text(),
        primaryKey: false,
        joinKey: false,
        getObjectValue: () => availableInCountries,
        setObjectValue: (v) => availableInCountries = v,
        value: pAvailableInCountries);
    fCategoryId = DfDbField(
        entity: this,
        name: "categoryId",
        fieldType: DfFieldType.text(),
        primaryKey: false,
        joinKey: false,
        getObjectValue: () => categoryId,
        setObjectValue: (v) => categoryId = v,
        value: pCategoryId);
    fMandatoryFlag = DfDbField(
        entity: this,
        name: "mandatoryFlag",
        fieldType: DfFieldType.boolean(),
        primaryKey: false,
        joinKey: false,
        getObjectValue: () => mandatoryFlag,
        setObjectValue: (v) => mandatoryFlag = v,
        value: pMandatoryFlag);
    fVideoDuration = DfDbField(
        entity: this,
        name: "videoDuration",
        fieldType: DfFieldType.text(),
        primaryKey: false,
        joinKey: false,
        getObjectValue: () => videoDuration,
        setObjectValue: (v) => videoDuration = v,
        value: pVideoDuration);
    fDashboardDisplay = DfDbField(
        entity: this,
        name: "dashboardDisplay",
        fieldType: DfFieldType.boolean(),
        primaryKey: false,
        joinKey: false,
        getObjectValue: () => dashboardDisplay,
        setObjectValue: (v) => dashboardDisplay = v,
        value: pDashboardDisplay);
    fSeminarStartDate = DfDbField(
        entity: this,
        name: "seminarStartDate",
        fieldType: DfFieldType.datetime(),
        primaryKey: false,
        joinKey: false,
        getObjectValue: () => seminarStartDate,
        setObjectValue: (v) => seminarStartDate = v,
        value: pSeminarStartDate);
    fSeminarEndDate = DfDbField(
        entity: this,
        name: "seminarEndDate",
        fieldType: DfFieldType.datetime(),
        primaryKey: false,
        joinKey: false,
        getObjectValue: () => seminarEndDate,
        setObjectValue: (v) => seminarEndDate = v,
        value: pSeminarEndDate);
    fCreatedDate = DfDbField(
        entity: this,
        name: "createdDate",
        fieldType: DfFieldType.datetime(),
        primaryKey: false,
        joinKey: false,
        getObjectValue: () => createdDate,
        setObjectValue: (v) => createdDate = v,
        value: pCreatedDate);
    fModifiedDate = DfDbField(
        entity: this,
        name: "modifiedDate",
        fieldType: DfFieldType.datetime(),
        primaryKey: false,
        joinKey: false,
        getObjectValue: () => modifiedDate,
        setObjectValue: (v) => modifiedDate = v,
        value: pModifiedDate);
  }

  @override
  void customFieldJsonMap(Mapper map, List<DfDbField> fields) {
    // TODO: implement customFieldJsonMap
  }

  @override
  List<DfDbField> fields() {
    // TODO: implement fields
    return [
      fId,
      fType,
      fTitle,
      fDescription,
      fThumbnailUrl,
      fSourceUrl,
      fVideoDuration,
      fAvailableInCountries,
      fCategoryId,
      fMandatoryFlag,
      fDashboardDisplay,
      fSeminarStartDate,
      fSeminarEndDate,
      fCreatedDate,
      fModifiedDate
    ];
  }

  @override
  DfBaseEntity<DfBaseField> newInstance() {
    // TODO: implement newInstance
    return CourseDb();
  }

  @override
  DfBaseEntity<DfDbField> parseExecRawJsonResultToEntity(
      DfQueryPurpose queryPurpose, Mapper map) {
    // TODO: implement parseExecRawJsonResultToEntity
    if (queryPurpose == DfQueryPurpose.FETCH) {
      return map.toObject<CourseDb>();
    }
    return null;
  }

  @override
  DfBaseEntity<DfDbField> jsonToObject(
      DfQueryPurpose queryPurpose, Mapper map) {
    return map.toObject<CourseDb>();
  }
}
