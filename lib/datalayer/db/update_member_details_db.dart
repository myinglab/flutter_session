import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/entity/dfdbentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/field/dfdbfield.dart';
import 'package:ingdao/dataflow/field/dffieldtype.dart';
import 'package:nesternship_flut/datalayer/field/user_profile/update_member_details_field.dart';
import 'package:object_mapper/src/mapper.dart';

class UpdateMemberProfileDb extends DfDbEntity with UpdateMemberDetailsField{
  UpdateMemberProfileDb(
      {String id,
        String memberEmail,
        String memberName,
        String userId,
        String avatar,
        String dateOfBirth,
        String gender,
        String originCountry,
        String nestleCountry})
      : super(name: "updateMemberDetails") {
    fId = DfDbField(
        entity: this,
        name: "memberId",
        primaryKey: true,
        joinKey: false,
        fieldType: DfFieldType.text(),
        value: id);
    fMemberEmail = DfDbField(
        entity: this,
        name: "memberName",
        primaryKey: false,
        joinKey: false,
        fieldType: DfFieldType.text(),
        value: memberEmail);
    fMemberName = DfDbField(
        entity: this,
        name: "memberName",
        primaryKey: false,
        joinKey: false,
        fieldType: DfFieldType.text(),
        value: memberName);
    fUserId = DfDbField(
        entity: this,
        name: "userId",
        primaryKey: true,
        joinKey: false,
        fieldType: DfFieldType.text(),
        value: userId);
    fAvatar = DfDbField(
        entity: this,
        name: "avatar",
        primaryKey: false,
        joinKey: false,
        fieldType: DfFieldType.text(),
        value: avatar);
    fDateOfBirth = DfDbField(
        entity: this,
        name: "dateOfBirth",
        primaryKey: false,
        joinKey: false,
        fieldType: DfFieldType.text(),
        value: dateOfBirth);
    fGender = DfDbField(
        entity: this,
        name: "gender",
        primaryKey: false,
        joinKey: false,
        fieldType: DfFieldType.text(),
        value: gender);
    fOriginCountry = DfDbField(
        entity: this,
        name: "originCountry",
        primaryKey: false,
        joinKey: false,
        fieldType: DfFieldType.text(),
        value: originCountry);
    fNestleCountry = DfDbField(
        entity: this,
        name: "nestleCountry",
        primaryKey: false,
        joinKey: false,
        fieldType: DfFieldType.text(),
        value: nestleCountry);
  }

  //Define the sqilite table fields available.
  //Define the fields will map back to the sqlite query executed reuslt: Map<String,dynamic>
  @override
  List<DfDbField> fields() {
    return [
      fId,
      fMemberEmail,
      fMemberName,
      fUserId,
      fAvatar,
      fDateOfBirth,
      fGender,
      fOriginCountry,
      fNestleCountry
    ];
  }

  @override
  DfBaseEntity<DfBaseField> newInstance() {
    return UpdateMemberProfileDb();
  }

  @override
  DfBaseEntity<DfDbField> jsonToObject(
      DfQueryPurpose queryPurpose, Mapper map) {
    return map.toObject<UpdateMemberProfileDb>();
  }


  @override
  void customFieldJsonMap(Mapper map, List<DfDbField> fields) {}

  @override
  DfBaseEntity<DfDbField> parseExecRawJsonResultToEntity(DfQueryPurpose queryPurpose, Mapper map) {
    // TODO: implement parseExecRawJsonResultToEntity
    throw UnimplementedError();
  }
}

