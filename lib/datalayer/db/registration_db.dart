import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/entity/dfdbentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/field/dfdbfield.dart';
import 'package:ingdao/dataflow/field/dffieldtype.dart';
import 'package:nesternship_flut/datalayer/field/registration/registration_field.dart';
import 'package:object_mapper/src/mapper.dart';

class RegistrationDb extends DfDbEntity with RegistrationField {
  RegistrationDb(
      {String memberEmail,
      String memberName,
      String userId,
      String avatar,
      String dateOfBirth,
      String gender,
      String originCountry,
      String nestleCountry})
      : super(name: "registration") {
    fmemberEmail = DfDbField(
        entity: this,
        name: "memberEmail",
        primaryKey: false,
        joinKey: false,
        fieldType: DfFieldType.text(),
        value: memberEmail);
    fmemberName = DfDbField(
        entity: this,
        name: "memberName",
        primaryKey: false,
        joinKey: false,
        fieldType: DfFieldType.text(),
        value: memberName);
    fuserId = DfDbField(
        entity: this,
        name: "userId",
        primaryKey: true,
        joinKey: false,
        fieldType: DfFieldType.text(),
        value: userId);
    favatar = DfDbField(
        entity: this,
        name: "avatar",
        primaryKey: false,
        joinKey: false,
        fieldType: DfFieldType.text(),
        value: avatar);
    fdateOfBirth = DfDbField(
        entity: this,
        name: "dateOfBirth",
        primaryKey: false,
        joinKey: false,
        fieldType: DfFieldType.text(),
        value: dateOfBirth);
    fgender = DfDbField(
        entity: this,
        name: "gender",
        primaryKey: false,
        joinKey: false,
        fieldType: DfFieldType.text(),
        value: gender);
    foriginCountry = DfDbField(
        entity: this,
        name: "originCountry",
        primaryKey: false,
        joinKey: false,
        fieldType: DfFieldType.text(),
        value: originCountry);
    fnestleCountry = DfDbField(
        entity: this,
        name: "nestleCountry",
        primaryKey: false,
        joinKey: false,
        fieldType: DfFieldType.text(),
        value: nestleCountry);
  }

  //Define the sqilite table fields available.
  //Define the fields will map back to the sqlite query executed reuslt: Map<String,dynamic>
  @override
  List<DfDbField> fields() {
    return [
      fmemberEmail,
      fmemberName,
      fuserId,
      favatar,
      fdateOfBirth,
      fgender,
      foriginCountry,
      fnestleCountry
    ];
  }

  @override
  DfBaseEntity<DfBaseField> newInstance() {
    return RegistrationDb();
  }

  @override
  DfBaseEntity<DfDbField> jsonToObject(
      DfQueryPurpose queryPurpose, Mapper map) {
    return map.toObject<RegistrationDb>();
  }

  @override
  void customFieldJsonMap(Mapper map, List<DfDbField> fields) {}

  @override
  DfBaseEntity<DfDbField> parseExecRawJsonResultToEntity(
      DfQueryPurpose queryPurpose, Mapper map) {
    // TODO: implement parseExecRawJsonResultToEntity
    throw UnimplementedError();
  }
}
