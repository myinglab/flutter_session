import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/entity/dfdbentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/field/dfdbfield.dart';
import 'package:ingdao/dataflow/field/dffieldtype.dart';
import 'package:nesternship_flut/data/model/country_model.dart';
import 'package:nesternship_flut/datalayer/entity/base_response.dart';
import 'package:nesternship_flut/datalayer/entity/master/country_response.dart';
import 'package:nesternship_flut/datalayer/field/master/country_field.dart';
import 'package:nesternship_flut/datalayer/field/registration/registration_field.dart';
import 'package:object_mapper/src/mapper.dart';

class CountryDb extends DfDbEntity with CountryField {
  int countryId;
  String countryCode;
  String countryName;
  bool isNestleCountry;

  CountryDb({int pCountryId, String pCountryCode, String pCountryName, bool pIsNestleCountry})
      : super(name: "country") {

    countryId = pCountryId;
    countryName = pCountryName;
    countryCode = pCountryCode;

    fCountryId = DfDbField(
        entity: this,
        name: "countryId",
        primaryKey: true,
        joinKey: false,
        fieldType: DfFieldType.integer(),
        getObjectValue: () => countryId,
        setObjectValue: (v) => countryId = v,
        value: pCountryId);
    fCountryCode = DfDbField(
        entity: this,
        name: "countryCode",
        primaryKey: true,
        joinKey: false,
        fieldType: DfFieldType.text(),
        getObjectValue: () => countryCode,
        setObjectValue: (v) => countryCode = v,
        value: pCountryCode);
    fCountryName = DfDbField(
        entity: this,
        name: "countryName",
        primaryKey: false,
        joinKey: false,
        fieldType: DfFieldType.text(),
        getObjectValue: () => countryName,
        setObjectValue: (v) => countryName = v,
        value: pCountryName);
    fIsNestleCountry = DfDbField(
        entity: this,
        name: "isNestleCountry",
        primaryKey: false,
        joinKey: false,
        fieldType: DfFieldType.boolean(),
        getObjectValue: () => isNestleCountry,
        setObjectValue: (v) => isNestleCountry = v,
        value: pIsNestleCountry);
  }

  //Define the sqilite table fields available.
  //Define the fields will map back to the sqlite query executed reuslt: Map<String,dynamic>
  @override
  List<DfDbField> fields() {
    return [fCountryId, fCountryCode, fCountryName, fIsNestleCountry];
  }

  @override
  DfBaseEntity<DfBaseField> newInstance() {
    return CountryDb();
  }

  @override
  DfBaseEntity<DfDbField> jsonToObject(
      DfQueryPurpose queryPurpose, Mapper map) {
    return map.toObject<CountryDb>();
  }

  @override
  void customFieldJsonMap(Mapper map, List<DfDbField> fields) {}

  @override
  DfBaseEntity<DfDbField> parseExecRawJsonResultToEntity(
      DfQueryPurpose queryPurpose, Mapper map) {
    // TODO: implement parseExecRawJsonResultToEntity
    if (queryPurpose == DfQueryPurpose.FETCH) {
      return map.toObject<CountryDb>();
    }
    return null;
  }
}
