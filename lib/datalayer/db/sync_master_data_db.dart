import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/entity/dfdbentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/field/dfdbfield.dart';
import 'package:ingdao/dataflow/field/dffieldtype.dart';
import 'package:nesternship_flut/datalayer/field/master/syncMasterData_field.dart';
import 'package:object_mapper/src/mapper.dart';

class SyncMasterDataDb extends DfDbEntity with SyncMasterDataField {
  SyncMasterDataDb (
      {String lastSyncDate}) : super(name: "master") {
    fLastSyncDate = DfDbField(
        entity: this,
        name: "memberEmail",
        primaryKey: false,
        joinKey: false,
        fieldType: DfFieldType.datetime(),
        value: lastSyncDate);
  }

  //Define the sqilite table fields available.
  //Define the fields will map back to the sqlite query executed reuslt: Map<String,dynamic>
  @override
  List<DfDbField> fields() {
    return [
      fLastSyncDate
    ];
  }

  @override
  DfBaseEntity<DfBaseField> newInstance() {
    return SyncMasterDataDb();
  }

  @override
  DfBaseEntity<DfDbField> jsonToObject(
      DfQueryPurpose queryPurpose, Mapper map) {
    return map.toObject<SyncMasterDataDb>();
  }

  @override
  void customFieldJsonMap(Mapper map, List<DfDbField> fields) {}

  @override
  DfBaseEntity<DfDbField> parseExecRawJsonResultToEntity(
      DfQueryPurpose queryPurpose, Mapper map) {
    // TODO: implement parseExecRawJsonResultToEntity
    throw UnimplementedError();
  }
}