import 'package:meta/meta.dart';
import 'package:nesternship_flut/datalayer/field/registration/registration_field.dart';

class RegistrationBean {
  String memberEmail;
  String memberName;
  String userId;
  String avatar;
  DateTime dateOfBirth;
  String gender;
  String originCountry;
  String nestleCountry;

  RegistrationBean(
      {this.memberEmail,
      this.memberName,
      this.userId,
      this.avatar,
      this.dateOfBirth,
      this.gender,
      this.originCountry,
      this.nestleCountry});

  RegistrationBean.from({@required RegistrationField field}) {
    memberEmail = field.fmemberEmail.valueAsString();
    memberName = field.fmemberName.valueAsString();
    userId = field.fuserId.valueAsString();
    avatar = field.favatar.valueAsString();
    dateOfBirth = field.fdateOfBirth.valueAsDateTime();
    gender = field.fgender.valueAsString();
    originCountry = field.foriginCountry.valueAsString();
    nestleCountry = field.fnestleCountry.valueAsString();
  }

  setTo({@required RegistrationField field}) {
    field.fmemberEmail.value = memberEmail;
    field.fmemberName.value = memberName;
    field.fuserId.value = userId;
    field.favatar.value = avatar;
    field.fdateOfBirth.value = dateOfBirth;
    field.fgender.value = gender;
    field.foriginCountry.value = originCountry;
    field.fnestleCountry.value = nestleCountry;
  }
}
