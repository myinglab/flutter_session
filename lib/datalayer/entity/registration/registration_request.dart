import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/control/dfqueryorderby.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/entity/dfwebentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/field/dfwebfield.dart';
import 'package:ingdao/dataflow/field/dffieldtype.dart';
import 'package:nesternship_flut/datalayer/entity/registration/registration_response.dart';
import 'package:nesternship_flut/datalayer/field/registration/registration_field.dart';
import 'package:object_mapper/src/mapper.dart';

import '../base_response.dart';

class RegistrationRequest extends DfWebEntity with RegistrationField {
  String memberEmail;
  String memberName;
  String userId;
  String avatar;
  String dateOfBirth;
  String gender;
  String originCountry;
  String nestleCountry;

  RegistrationRequest(
      {String memberEmail,
      String memberName,
      String userId,
      String avatar,
      String dateOfBirth,
      String gender,
      String originCountry,
      String nestleCountry})
      : super(contextPath: "member/registration") {

    this.memberEmail = memberEmail;
    this.memberName = memberName;
    this.userId = userId;
    this.avatar = avatar;
    this.dateOfBirth = dateOfBirth;
    this.gender = gender;
    this.originCountry = originCountry;
    this.nestleCountry = nestleCountry;

    fmemberEmail = DfWebField(
        entity: this,
        name: "memberEmail",
        fieldType: DfFieldType.text(),
        getObjectValue: () => this.memberEmail,
        setObjectValue: (v) => this.memberEmail = v,
        value: memberEmail);
    fmemberName = DfWebField(
        entity: this,
        name: "memberName",
        fieldType: DfFieldType.text(),
        getObjectValue: () => this.memberName,
        setObjectValue: (v) => this.memberName = v,
        value: memberName);
    fuserId = DfWebField(
        entity: this,
        name: "userId",
        fieldType: DfFieldType.text(),
        getObjectValue: () => this.userId,
        setObjectValue: (v) => this.userId = v,
        value: userId);
    favatar = DfWebField(
        entity: this,
        name: "avatar",
        fieldType: DfFieldType.text(),
        getObjectValue: () => this.avatar,
        setObjectValue: (v) => this.avatar = v,
        value: avatar);
    fdateOfBirth = DfWebField(
        entity: this,
        name: "dateOfBirth",
        fieldType: DfFieldType.text(),
        getObjectValue: () => this.dateOfBirth,
        setObjectValue: (v) => this.dateOfBirth = v,
        value: dateOfBirth);
    fgender = DfWebField(
        entity: this,
        name: "gender",
        fieldType: DfFieldType.text(),
        getObjectValue: () => this.gender,
        setObjectValue: (v) => this.gender = v,
        value: gender);
    foriginCountry = DfWebField(
        entity: this,
        name: "originCountry",
        fieldType: DfFieldType.text(),
        getObjectValue: () => this.originCountry,
        setObjectValue: (v) => this.originCountry = v,
        value: originCountry);
    fnestleCountry = DfWebField(
        entity: this,
        name: "nestleCountry",
        fieldType: DfFieldType.text(),
        getObjectValue: () => this.nestleCountry,
        setObjectValue: (v) => this.nestleCountry = v,
        value: nestleCountry);
  }

  //Define what are the fields will map back the result from http response Map<String,dynamic>
  @override
  List<DfWebField> fields() {
    return [
      fmemberEmail,
      fmemberName,
      fuserId,
      favatar,
      fdateOfBirth,
      fgender,
      foriginCountry,
      fnestleCountry
    ];
  }

  @override
  DfWebReqParam convertToWebReqParam(DfQuery query) {
    DfWebReqParam param = DfWebReqParam();
    // param.headers[ApiSetting.SESSION_TOKEN_NAME] = ApiSetting.token;

    if (query.purpose == DfQueryPurpose.ADD) {
      final dict = query.extraReq.toDict();
      param.reqBody = dict;

      return param;
    }
    return null;
  }

  @override
  DfBaseEntity<DfBaseField> newInstance() {
    return RegistrationRequest();
  }

  @override
  void customFieldJsonMap(Mapper map, List<DfWebField> fields) {}

  @override
  DfBaseEntity<DfWebField> parseExecRawJsonResultToEntity(
      DfQueryPurpose purpose, Mapper map) {
    if (purpose == DfQueryPurpose.ADD) {
      BaseResponse.dataType = RegistrationResponse;
      return map.toObject<BaseResponse>();
    }
    return null;
  }
}
