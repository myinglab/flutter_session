class RegistrationDetail{
  RegistrationDetail({this.email, this.password, this.name, this.gender, this.dateOfBirth, this.originCountry, this.internCountry});
  String email;
  String password;
  String name;
  String gender;
  String dateOfBirth;
  String originCountry;
  String internCountry;

  @override
  String toString(){
    return "email: $email, password: $password, name: $name, gender: $gender, dateOfBirth: $dateOfBirth, originCountry: $originCountry, internCountry: $internCountry";
  }
}