import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/control/dfqueryorderby.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/entity/dfwebentity.dart';
import 'package:ingdao/dataflow/entity/dfwebresponseentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/field/dfwebfield.dart';
import 'package:ingdao/dataflow/field/dffieldtype.dart';
import 'package:nesternship_flut/datalayer/field/registration/registration_field.dart';
import 'package:object_mapper/src/mapper.dart';

class RegistrationResponse extends DfWebResponseEntity with RegistrationField {
  String memberEmail;

  RegistrationResponse({String pMemberEmail}) {
    fmemberEmail = DfWebField(
        entity: this,
        name: "memberEmail",
        fieldType: DfFieldType.text(),
        getObjectValue: () => memberEmail,
        setObjectValue: (v) => memberEmail = v,
        value: pMemberEmail);
  }

  @override
  List<DfWebField> fields() {
    return [fmemberEmail];
  }

  @override
  DfBaseEntity<DfBaseField> newInstance() {
    return RegistrationResponse();
  }

  @override
  void customFieldJsonMap(Mapper map, List<DfBaseField> fields) {}
}
