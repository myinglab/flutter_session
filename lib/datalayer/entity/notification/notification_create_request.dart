import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/control/dfqueryorderby.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/entity/dfwebentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/field/dfwebfield.dart';
import 'package:ingdao/dataflow/field/dffieldtype.dart';
import 'package:nesternship_flut/datalayer/entity/notification/notification_create_response.dart';
import 'package:nesternship_flut/datalayer/entity/registration/registration_response.dart';
import 'package:nesternship_flut/datalayer/field/notification/notification_field.dart';
import 'package:object_mapper/src/mapper.dart';

import '../base_response.dart';

class CreateNotificationRequest extends DfWebEntity with NotificationField {
  String notificationContentId;
  String read;
  int userId;

  CreateNotificationRequest(
      {String notificationContentId, String read, int userId})
      : super(contextPath: "notification/create") {
    fnotificationContentId = DfWebField(
        entity: this,
        name: "notificationContentId",
        fieldType: DfFieldType.text(),
        getObjectValue: () => this.notificationContentId,
        setObjectValue: (v) => this.notificationContentId = v,
        value: notificationContentId);
    fread = DfWebField(
        entity: this,
        name: "read",
        fieldType: DfFieldType.text(),
        getObjectValue: () => this.read,
        setObjectValue: (v) => this.read = v,
        value: read);
    fuserId = DfWebField(
        entity: this,
        name: "userId",
        fieldType: DfFieldType.integer(),
        getObjectValue: () => this.userId,
        setObjectValue: (v) => this.userId = v,
        value: userId);
  }

  //Define what are the fields will map back the result from http response Map<String,dynamic>
  @override
  List<DfWebField> fields() {
    return [fnotificationContentId, fread, fuserId];
  }

  @override
  DfWebReqParam convertToWebReqParam(DfQuery query) {
    DfWebReqParam param = DfWebReqParam();
    // param.headers[ApiSetting.SESSION_TOKEN_NAME] = ApiSetting.token;

    if (query.purpose == DfQueryPurpose.ADD) {
      final dict = query.extraReq.toDict();
      param.reqBody = dict;

      return param;
    }
    return null;
  }

  @override
  DfBaseEntity<DfBaseField> newInstance() {
    return CreateNotificationRequest();
  }

  @override
  void customFieldJsonMap(Mapper map, List<DfWebField> fields) {}

  @override
  DfBaseEntity<DfWebField> parseExecRawJsonResultToEntity(
      DfQueryPurpose purpose, Mapper map) {
    if (purpose == DfQueryPurpose.ADD) {
      BaseResponse.dataType = CreateNotificationResponse;
      return map.toObject<BaseResponse>();
    }
    return null;
  }
}
