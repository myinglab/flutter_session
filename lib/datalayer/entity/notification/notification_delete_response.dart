import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/entity/dfwebresponseentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/field/dffieldtype.dart';
import 'package:ingdao/dataflow/field/dfwebfield.dart';
import 'package:nesternship_flut/datalayer/field/notification/notification_field.dart';
import 'package:object_mapper/src/mapper.dart';

class DeleteNotificationResponse extends DfWebResponseEntity
    with NotificationField {
  int notificationId;
  DeleteNotificationResponse({int pNotificationId}) {
    fnotificationId = DfWebField(
        entity: this,
        name: "Notification Id",
        fieldType: DfFieldType.integer(),
        getObjectValue: () => notificationId,
        setObjectValue: (v) => notificationId = v,
        value: pNotificationId);
  }

  @override
  List<DfWebField> fields() {
    return [fnotificationId];
  }

  @override
  DfBaseEntity<DfBaseField> newInstance() {
    return DeleteNotificationResponse();
  }

  @override
  void customFieldJsonMap(Mapper map, List<DfBaseField> fields) {}
}
