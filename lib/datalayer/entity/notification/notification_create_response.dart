import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/entity/dfwebresponseentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/field/dfwebfield.dart';
import 'package:nesternship_flut/datalayer/field/notification/notification_field.dart';
import 'package:object_mapper/src/mapper.dart';

class CreateNotificationResponse extends DfWebResponseEntity
    with NotificationField {
  CreateNotificationResponse() {}

  @override
  List<DfWebField> fields() {
    return [];
  }

  @override
  DfBaseEntity<DfBaseField> newInstance() {
    return CreateNotificationResponse();
  }

  @override
  void customFieldJsonMap(Mapper map, List<DfBaseField> fields) {}
}
