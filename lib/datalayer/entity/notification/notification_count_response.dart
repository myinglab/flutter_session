import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/control/dfqueryorderby.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/entity/dfwebentity.dart';
import 'package:ingdao/dataflow/entity/dfwebresponseentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/field/dfwebfield.dart';
import 'package:ingdao/dataflow/field/dffieldtype.dart';
import 'package:nesternship_flut/datalayer/field/notification/notification_field.dart';
import 'package:nesternship_flut/datalayer/field/registration/registration_field.dart';
import 'package:object_mapper/src/mapper.dart';

class NotificationCountResponse extends DfWebResponseEntity
    with NotificationField {
  int notificationCount;

  NotificationCountResponse({int pNotificationCount}) {
    fnotificationCount = DfWebField(
        entity: this,
        name: "Notification Count",
        fieldType: DfFieldType.integer(),
        getObjectValue: () => notificationCount,
        setObjectValue: (v) => notificationCount = v,
        value: pNotificationCount);
  }

  @override
  List<DfWebField> fields() {
    return [fnotificationCount];
  }

  @override
  DfBaseEntity<DfBaseField> newInstance() {
    return NotificationCountResponse();
  }

  @override
  void customFieldJsonMap(Mapper map, List<DfBaseField> fields) {}
}
