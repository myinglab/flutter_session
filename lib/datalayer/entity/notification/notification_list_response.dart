import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/control/dfqueryorderby.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/entity/dfwebentity.dart';
import 'package:ingdao/dataflow/entity/dfwebresponseentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/field/dfwebfield.dart';
import 'package:ingdao/dataflow/field/dffieldtype.dart';
import 'package:nesternship_flut/datalayer/entity/notification/notification_list_information_response.dart';
import 'package:nesternship_flut/datalayer/field/notification/notification_list_field.dart';
import 'package:object_mapper/src/mapper.dart';

class NotificationListResponse extends DfWebResponseEntity with NotificationListField {
  List<NotificationListInfoResponse> notificationList;

  NotificationListResponse({pNotificationList}) {
    fNotificationList = DfWebField(
        entity: this,
        name: "NotificationList",
        fieldType: DfFieldType.custom(),
        getObjectValue: () => notificationList,
        setObjectValue: (v) => notificationList = v,
        value: pNotificationList);
  }

  @override
  List<DfWebField> fields() {
    return [
      fNotificationList
    ];
  }

  @override
  DfBaseEntity<DfBaseField> newInstance() {
    return NotificationListResponse();
  }

  @override
  void customFieldJsonMap(Mapper map, List<DfBaseField> fields) {
    for (var field in fields) {
      if (field == fNotificationList) {
        map<NotificationListInfoResponse>(field.name, field.getObjectValue(), (v) => field.setObjectValue(v), field.getTransform());
      }
    }
  }
}