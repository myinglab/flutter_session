import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/control/dfqueryorderby.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/entity/dfwebentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/field/dfwebfield.dart';
import 'package:ingdao/dataflow/field/dffieldtype.dart';
import 'package:nesternship_flut/datalayer/entity/notification/set_notification_read_response.dart';
import 'package:nesternship_flut/datalayer/entity/user_profile/update_member_details_response.dart';
import 'package:nesternship_flut/datalayer/field/notification/notification_field.dart';
import 'package:object_mapper/src/mapper.dart';

import '../base_response.dart';

class SetNotificationReadRequest extends DfWebEntity {
  SetNotificationReadRequest()
      : super(contextPath: "notification/setNotificationRead") {}

  //Define what are the fields will map back the result from http response Map<String,dynamic>
  @override
  List<DfWebField> fields() {
    return [];
  }

  @override
  DfWebReqParam convertToWebReqParam(DfQuery query) {
    DfWebReqParam param = DfWebReqParam();

    if (query.purpose == DfQueryPurpose.ADD) {
      final reqParam = query.extraReq.dict;
      final notificationIds = reqParam["notificationIds"] as List<int>;
      final isRead = reqParam["isRead"] as bool;

      param.reqBody = {"notificationId": notificationIds, "isRead": isRead};
      return param;
    }
    return null;
  }

  @override
  DfBaseEntity<DfBaseField> newInstance() {
    return SetNotificationReadRequest();
  }

  @override
  void customFieldJsonMap(Mapper map, List<DfWebField> fields) {}

  @override
  DfBaseEntity<DfWebField> parseExecRawJsonResultToEntity(
      DfQueryPurpose purpose, Mapper map) {
    if (purpose == DfQueryPurpose.ADD) {
      BaseResponse.dataType = SetNotificationReadResponse;
      return map.toObject<BaseResponse>();
    }
    return null;
  }
}
