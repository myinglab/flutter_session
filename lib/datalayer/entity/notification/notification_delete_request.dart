import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/control/dfqueryorderby.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/entity/dfwebentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/field/dfwebfield.dart';
import 'package:ingdao/dataflow/field/dffieldtype.dart';
import 'package:nesternship_flut/datalayer/entity/notification/notification_create_response.dart';
import 'package:nesternship_flut/datalayer/entity/notification/notification_delete_response.dart';
import 'package:nesternship_flut/datalayer/entity/registration/registration_response.dart';
import 'package:nesternship_flut/datalayer/field/notification/notification_field.dart';
import 'package:nui_core/code/nui_code_util.dart';
import 'package:object_mapper/src/mapper.dart';

import '../base_response.dart';

class DeleteNotificationRequest extends DfWebEntity with NotificationField {
  DeleteNotificationRequest()
      : super(contextPath: 'notification/deleteNotification') {}

  //Define what are the fields will map back the result from http response Map<String,dynamic>
  @override
  List<DfWebField> fields() {
    return [];
  }

  @override
  DfWebReqParam convertToWebReqParam(DfQuery query) {
    DfWebReqParam param = DfWebReqParam();
    // param.headers[ApiSetting.SESSION_TOKEN_NAME] = ApiSetting.token;

    if (query.purpose == DfQueryPurpose.DELETE) {
      param.reqParams = {
        'notificationId': query.extraReq.dict['notificationId'].toString()
        //'notificationId': '15'
      };
      return param;
    }
    return null;
  }

  @override
  DfBaseEntity<DfBaseField> newInstance() {
    return DeleteNotificationRequest();
  }

  @override
  void customFieldJsonMap(Mapper map, List<DfWebField> fields) {}

  @override
  DfBaseEntity<DfWebField> parseExecRawJsonResultToEntity(
      DfQueryPurpose purpose, Mapper map) {
    if (purpose == DfQueryPurpose.DELETE) {
      BaseResponse.dataType = DeleteNotificationResponse;
      return map.toObject<BaseResponse>();
    }
    return null;
  }
}
