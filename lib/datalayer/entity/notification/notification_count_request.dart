import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/control/dfqueryorderby.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/entity/dfwebentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/field/dfwebfield.dart';
import 'package:ingdao/dataflow/field/dffieldtype.dart';
import 'package:nesternship_flut/datalayer/entity/registration/registration_response.dart';
import 'package:nesternship_flut/datalayer/entity/user_profile/user_profile_response.dart';
import 'package:nesternship_flut/datalayer/field/notification/notification_field.dart';
import 'package:nesternship_flut/datalayer/field/registration/registration_field.dart';
import 'package:nesternship_flut/datalayer/field/user_profile/user_profile_field.dart';
import 'package:nui_core/code/nui_code_util.dart';
import 'package:object_mapper/src/mapper.dart';

import '../base_response.dart';
import 'notification_count_response.dart';

class GetNotificationCountRequest extends DfWebEntity with NotificationField {
  GetNotificationCountRequest()
      : super(contextPath: "notification/getNotificationCount") {}

  //Define what are the fields will map back the result from http response Map<String,dynamic>
  @override
  List<DfWebField> fields() {
    return [];
  }

  @override
  DfWebReqParam convertToWebReqParam(DfQuery query) {
    DfWebReqParam param = DfWebReqParam();
    // param.headers[ApiSetting.SESSION_TOKEN_NAME] = ApiSetting.token;

    if (query.purpose == DfQueryPurpose.FETCH) {
      param.reqParams["userId"] = query.extraReq.dict['userId'].toString();
      return param;
    }
    return null;
  }

  @override
  DfBaseEntity<DfBaseField> newInstance() {
    return GetNotificationCountRequest();
  }

  @override
  void customFieldJsonMap(Mapper map, List<DfWebField> fields) {}

  @override
  DfBaseEntity<DfWebField> parseExecRawJsonResultToEntity(
      DfQueryPurpose purpose, Mapper map) {
    if (purpose == DfQueryPurpose.FETCH) {
      BaseResponse.dataType = NotificationCountResponse;
      return map.toObject<BaseResponse>();
    }
    return null;
  }
}
