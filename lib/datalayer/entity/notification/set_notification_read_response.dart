import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/control/dfqueryorderby.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/entity/dfwebentity.dart';
import 'package:ingdao/dataflow/entity/dfwebresponseentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/field/dfwebfield.dart';
import 'package:ingdao/dataflow/field/dffieldtype.dart';
import 'package:nesternship_flut/datalayer/field/notification/notification_field.dart';
import 'package:object_mapper/src/mapper.dart';

class SetNotificationReadResponse extends DfWebResponseEntity
    with NotificationField {
  bool isRead;

  SetNotificationReadResponse({bool pIsRead}) {
    fisRead = DfWebField(
        entity: this,
        name: "isRead",
        fieldType: DfFieldType.boolean(),
        getObjectValue: () => isRead,
        setObjectValue: (v) => isRead = v,
        value: pIsRead);
  }

  @override
  List<DfWebField> fields() {
    return [fisRead];
  }

  @override
  DfBaseEntity<DfBaseField> newInstance() {
    return SetNotificationReadResponse();
  }

  @override
  void customFieldJsonMap(Mapper map, List<DfBaseField> fields) {}
}
