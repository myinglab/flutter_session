import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/control/dfqueryorderby.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/entity/dfwebentity.dart';
import 'package:ingdao/dataflow/entity/dfwebresponseentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/field/dfwebfield.dart';
import 'package:ingdao/dataflow/field/dffieldtype.dart';
import 'package:nesternship_flut/datalayer/field/notification/notification_list_body_field.dart';
import 'package:object_mapper/src/mapper.dart';

class NotificationListInfoResponse extends DfWebResponseEntity with NotificationListBodyField{
  int notificationId;
  int userId;
  int notificationContentId;
  String notificationTitle;
  String notificationDesc;
  DateTime notificationDateTime;
  String bannerUrl;
  bool isRead;

  NotificationListInfoResponse({
    int pNotificationId,
    int pUserId,
    int pNotificationContentId,
    String pNotificationTitle,
    String pNotificationDesc,
    DateTime pNotificationDateTime,
    String pBannerUrl,
    bool pIsRead
  }) {
    fNotificationId = DfWebField(
        entity: this,
        name: "notificationId",
        fieldType: DfFieldType.integer(),
        getObjectValue: () => notificationId,
        setObjectValue: (v) => notificationId = v,
        value: pNotificationId);
    fUserId  = DfWebField(
        entity: this,
        name: "userId",
        fieldType: DfFieldType.text(),
        getObjectValue: () =>userId,
        setObjectValue: (v) => userId = v,
        value: pUserId);
    fNotificationContentId  = DfWebField(
        entity: this,
        name: "notificationContentId",
        fieldType: DfFieldType.text(),
        getObjectValue: () => notificationContentId,
        setObjectValue: (v) => notificationContentId = v,
        value: pNotificationContentId);
    fNotificationTitle  = DfWebField(
        entity: this,
        name: "notificationTitle",
        fieldType: DfFieldType.text(),
        getObjectValue: () => notificationTitle,
        setObjectValue: (v) => notificationTitle = v,
        value: pNotificationTitle);
    fNotificationDesc  = DfWebField(
        entity: this,
        name: "notificationDesc",
        fieldType: DfFieldType.text(),
        getObjectValue: () => notificationDesc,
        setObjectValue: (v) => notificationDesc = v,
        value: pNotificationDesc);
    fNotificationDateTime  = DfWebField(
        entity: this,
        name: "notificationDateTime",
        fieldType: DfFieldType.datetime(),
        getObjectValue: () => notificationDateTime,
        setObjectValue: (v) => notificationDateTime = v,
        value: pNotificationDateTime);
    fBannerUrl  = DfWebField(
        entity: this,
        name: "bannerUrl",
        fieldType: DfFieldType.text(),
        getObjectValue: () => bannerUrl,
        setObjectValue: (v) => bannerUrl = v,
        value: pBannerUrl);
    fIsRead  = DfWebField(
        entity: this,
        name: "isRead",
        fieldType: DfFieldType.boolean(),
        getObjectValue: () => isRead,
        setObjectValue: (v) => isRead = v,
        value: pIsRead);
  }

  @override
  List<DfWebField> fields() {
    return [
      fNotificationId,
      fUserId,
      fNotificationContentId,
      fNotificationTitle,
      fNotificationDesc,
      fNotificationDateTime,
      fBannerUrl,
      fIsRead
    ];
  }

  @override
  DfBaseEntity<DfBaseField> newInstance() {
    return NotificationListInfoResponse();
  }

  @override
  void customFieldJsonMap(Mapper map, List<DfBaseField> fields) {}
}