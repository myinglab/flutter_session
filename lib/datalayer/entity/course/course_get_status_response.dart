import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/entity/dfwebresponseentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/field/dffieldtype.dart';
import 'package:ingdao/dataflow/field/dfwebfield.dart';
import 'package:nesternship_flut/datalayer/field/courses/course_get_status_field.dart';
import 'package:object_mapper/src/mapper.dart';

class CourseGetStatusResponse extends DfWebResponseEntity
    with CourseGetStatusField {
  bool courseCompleted;

  CourseGetStatusResponse({bool pCourseCompleted}) {
    fCourseCompleted = DfWebField(
        entity: this,
        name: "courseCompleted",
        fieldType: DfFieldType.boolean(),
        getObjectValue: () => courseCompleted,
        setObjectValue: (v) => courseCompleted = v,
        value: pCourseCompleted);
  }

  @override
  void customFieldJsonMap(Mapper map, List<DfWebField> fields) {
    // TODO: implement customFieldJsonMap
  }

  @override
  List<DfWebField> fields() {
    // TODO: implement fields
    return [fCourseCompleted];
  }

  @override
  DfBaseEntity<DfBaseField> newInstance() {
    // TODO: implement newInstance
    return CourseGetStatusResponse();
  }
}
