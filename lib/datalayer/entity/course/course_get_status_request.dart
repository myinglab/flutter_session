import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/entity/dfwebentity.dart';
import 'package:nesternship_flut/datalayer/entity/base_response.dart';
import 'package:nesternship_flut/datalayer/entity/course/course_get_status_response.dart';
import 'package:object_mapper/src/mapper.dart';
import 'package:ingdao/dataflow/field/dfwebfield.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';

class CourseGetStatusRequest extends DfWebEntity {
  CourseGetStatusRequest() : super(contextPath: "courses/getReadStatus") {}

  @override
  DfWebReqParam convertToWebReqParam(DfQuery query) {
    // TODO: implement convertToWebReqParam
    DfWebReqParam param = DfWebReqParam();

    if (query.purpose == DfQueryPurpose.FETCH) {
      param.reqParams["userId"] = query.extraReq.dict['userId'].toString();
      param.reqParams["courseId"] = query.extraReq.dict['courseId'].toString();
      return param;
    }
    return null;
  }

  @override
  void customFieldJsonMap(Mapper map, List<DfWebField> fields) {
    // TODO: implement customFieldJsonMap
  }

  @override
  List<DfWebField> fields() {
    // TODO: implement fields
    return [];
  }

  @override
  DfBaseEntity<DfBaseField> newInstance() {
    // TODO: implement newInstance
    return CourseGetStatusRequest();
  }

  @override
  DfBaseEntity<DfWebField> parseExecRawJsonResultToEntity(
      DfQueryPurpose queryPurpose, Mapper map) {
    // TODO: implement parseExecRawJsonResultToEntity
    if (queryPurpose == DfQueryPurpose.FETCH) {
      BaseResponse.dataType = CourseGetStatusResponse;
      return map.toObject<BaseResponse>();
    }
    return null;
  }
}
