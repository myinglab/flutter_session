import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/control/dfqueryorderby.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/entity/dfwebentity.dart';
import 'package:ingdao/dataflow/entity/dfwebresponseentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/field/dfwebfield.dart';
import 'package:ingdao/dataflow/field/dffieldtype.dart';
import 'package:nesternship_flut/datalayer/field/courses/course_field.dart';
import 'package:object_mapper/src/mapper.dart';

class CourseResponse extends DfWebResponseEntity with CourseField {
  int id;
  String type;
  String title;
  String description;
  String thumbnailUrl;
  String sourceUrl;
  String videoDuration;
  String availableInCountries;
  int categoryId;
  bool mandatoryFlag;
  bool dashboardDisplay;
  String seminarStartDate;
  String seminarEndDate;
  String createdDate;
  String modifiedDate;

  CourseResponse(
      {int pId,
      String pType,
      String pTitle,
      String pDescription,
      String pThumbnailUrl,
      String pSourceUrl,
      String pAvailableInCountries,
      int pCategoryId,
      bool pMandatoryFlag,
      String pVideoDuration,
      bool pDashboardDisplay,
      String pSeminarStartDate,
      String pSeminarEndDate,
      String pCreatedDate,
      String pModifiedDate}) {
    fId = DfWebField(
        entity: this,
        name: "id",
        fieldType: DfFieldType.integer(),
        getObjectValue: () => id,
        setObjectValue: (v) => id = v,
        value: pId);
    fType = DfWebField(
        entity: this,
        name: "type",
        fieldType: DfFieldType.text(),
        getObjectValue: () => type,
        setObjectValue: (v) => type = v,
        value: pType);
    fTitle = DfWebField(
        entity: this,
        name: "title",
        fieldType: DfFieldType.text(),
        getObjectValue: () => title,
        setObjectValue: (v) => title = v,
        value: pTitle);
    fDescription = DfWebField(
        entity: this,
        name: "description",
        fieldType: DfFieldType.text(),
        getObjectValue: () => description,
        setObjectValue: (v) => description = v,
        value: pDescription);
    fThumbnailUrl = DfWebField(
        entity: this,
        name: "thumbnailUrl",
        fieldType: DfFieldType.text(),
        getObjectValue: () => thumbnailUrl,
        setObjectValue: (v) => thumbnailUrl = v,
        value: pThumbnailUrl);
    fSourceUrl = DfWebField(
        entity: this,
        name: "sourceUrl",
        fieldType: DfFieldType.text(),
        getObjectValue: () => sourceUrl,
        setObjectValue: (v) => sourceUrl = v,
        value: pSourceUrl);
    fAvailableInCountries = DfWebField(
        entity: this,
        name: "availableInCountries",
        fieldType: DfFieldType.text(),
        getObjectValue: () => availableInCountries,
        setObjectValue: (v) => availableInCountries = v,
        value: pAvailableInCountries);
    fCategoryId = DfWebField(
        entity: this,
        name: "categoryId",
        fieldType: DfFieldType.integer(),
        getObjectValue: () => categoryId,
        setObjectValue: (v) => categoryId = v,
        value: pCategoryId);
    fMandatoryFlag = DfWebField(
        entity: this,
        name: "mandatoryFlag",
        fieldType: DfFieldType.boolean(),
        getObjectValue: () => mandatoryFlag,
        setObjectValue: (v) => mandatoryFlag = v,
        value: pMandatoryFlag);
    fVideoDuration = DfWebField(
        entity: this,
        name: "videoDuration",
        fieldType: DfFieldType.text(),
        getObjectValue: () => videoDuration,
        setObjectValue: (v) => videoDuration = v,
        value: pVideoDuration);
    fDashboardDisplay = DfWebField(
        entity: this,
        name: "dashboardDisplay",
        fieldType: DfFieldType.text(),
        getObjectValue: () => dashboardDisplay,
        setObjectValue: (v) => dashboardDisplay = v,
        value: pDashboardDisplay);
    fSeminarStartDate = DfWebField(
        entity: this,
        name: "seminarStartDate",
        fieldType: DfFieldType.text(),
        getObjectValue: () => seminarStartDate,
        setObjectValue: (v) => seminarStartDate = v,
        value: pSeminarStartDate);
    fSeminarEndDate = DfWebField(
        entity: this,
        name: "seminarEndDate",
        fieldType: DfFieldType.text(),
        getObjectValue: () => seminarEndDate,
        setObjectValue: (v) => seminarEndDate = v,
        value: pSeminarEndDate);
    fCreatedDate = DfWebField(
        entity: this,
        name: "createdDate",
        fieldType: DfFieldType.text(),
        getObjectValue: () => createdDate,
        setObjectValue: (v) => createdDate = v,
        value: pCreatedDate);
    fModifiedDate = DfWebField(
        entity: this,
        name: "modifiedDate",
        fieldType: DfFieldType.text(),
        getObjectValue: () => modifiedDate,
        setObjectValue: (v) => modifiedDate = v,
        value: pModifiedDate);
  }

  @override
  void customFieldJsonMap(Mapper map, List<DfWebField> fields) {
    // TODO: implement customFieldJsonMap
  }

  @override
  List<DfWebField> fields() {
    // TODO: implement fields
    return [
      fId,
      fType,
      fTitle,
      fDescription,
      fThumbnailUrl,
      fSourceUrl,
      fVideoDuration,
      fAvailableInCountries,
      fCategoryId,
      fMandatoryFlag,
      fDashboardDisplay,
      fSeminarStartDate,
      fSeminarEndDate,
      fCreatedDate,
      fModifiedDate
    ];
  }

  @override
  DfBaseEntity<DfBaseField> newInstance() {
    // TODO: implement newInstance
    return CourseResponse();
  }
}
