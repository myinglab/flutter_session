import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/entity/dfwebentity.dart';
import 'package:nesternship_flut/datalayer/entity/base_response.dart';
import 'package:nesternship_flut/datalayer/entity/course/course_list_response.dart';
import 'package:object_mapper/src/mapper.dart';
import 'package:ingdao/dataflow/field/dfwebfield.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';

class CourseSearchRequest extends DfWebEntity {
  CourseSearchRequest() : super(contextPath: "courses/search") {}

  @override
  DfWebReqParam convertToWebReqParam(DfQuery query) {
    // TODO: implement convertToWebReqParam
    DfWebReqParam param = DfWebReqParam();

    if (query.purpose == DfQueryPurpose.FETCH) {
      param.reqParams["courseName"] =
          query.extraReq.dict['courseName'].toString();
      param.reqParams["nestleCountryId"] =
          query.extraReq.dict['nestleCountryId'].toString();
      param.reqParams["page"] = query.extraReq.dict['page'].toString();
      return param;
    }
    return null;
  }

  @override
  void customFieldJsonMap(Mapper map, List<DfWebField> fields) {
    // TODO: implement customFieldJsonMap
  }

  @override
  List<DfWebField> fields() {
    // TODO: implement fields
    return [];
  }

  @override
  DfBaseEntity<DfBaseField> newInstance() {
    // TODO: implement newInstance
    return CourseSearchRequest();
  }

  @override
  DfBaseEntity<DfWebField> parseExecRawJsonResultToEntity(
      DfQueryPurpose queryPurpose, Mapper map) {
    // TODO: implement parseExecRawJsonResultToEntity
    if (queryPurpose == DfQueryPurpose.FETCH) {
      BaseResponse.dataType = CourseListResponse;
      return map.toObject<BaseResponse>();
    }
    return null;
  }
}
