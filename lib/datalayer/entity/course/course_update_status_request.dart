import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/entity/dfwebentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/field/dffieldtype.dart';
import 'package:ingdao/dataflow/field/dfwebfield.dart';
import 'package:nesternship_flut/datalayer/entity/base_response.dart';
import 'package:nesternship_flut/datalayer/field/courses/course_update_status_field.dart';
import 'package:nui_core/code/nui_code_util.dart';
import 'package:object_mapper/src/mapper.dart';

class CourseUpdateStatusRequest extends DfWebEntity
    with CourseUpdateStatusField {
  String courseId;
  String userId;
  bool courseCompleted;

  CourseUpdateStatusRequest(
      {String pCourseId, String pUserId, bool pCourseCompleted})
      : super(contextPath: "courses/updateStatus") {
    this.courseId = pCourseId;
    this.userId = pUserId;
    this.courseCompleted = pCourseCompleted;

    fCourseId = DfWebField(
        entity: this,
        name: "courseId",
        fieldType: DfFieldType.text(),
        getObjectValue: () => this.courseId,
        setObjectValue: (v) => this.courseId = v,
        value: pCourseId);
    fUserId = DfWebField(
        entity: this,
        name: "userId",
        fieldType: DfFieldType.text(),
        getObjectValue: () => this.userId,
        setObjectValue: (v) => this.userId = v,
        value: pUserId);
    fCourseCompleted = DfWebField(
        entity: this,
        name: "courseCompleted",
        fieldType: DfFieldType.text(),
        getObjectValue: () => this.courseCompleted,
        setObjectValue: (v) => this.courseCompleted = v,
        value: pCourseCompleted);
  }

  @override
  DfWebReqParam convertToWebReqParam(DfQuery query) {
    // TODO: implement convertToWebReqParam
    DfWebReqParam param = DfWebReqParam();
    if (query.purpose == DfQueryPurpose.ADD) {
      final dict = query.extraReq.entity.toJson();
      param.reqBody = dict;

      return param;
    }
    return null;
  }

  @override
  void customFieldJsonMap(Mapper map, List<DfWebField> fields) {
    // TODO: implement customFieldJsonMap
  }

  @override
  List<DfWebField> fields() {
    // TODO: implement fields
    return [fCourseId, fUserId, fCourseCompleted];
  }

  @override
  DfBaseEntity<DfBaseField> newInstance() {
    // TODO: implement newInstance
    return CourseUpdateStatusRequest();
  }

  @override
  DfBaseEntity<DfWebField> parseExecRawJsonResultToEntity(
      DfQueryPurpose queryPurpose, Mapper map) {
    // TODO: implement parseExecRawJsonResultToEntity
    if (queryPurpose == DfQueryPurpose.ADD) {
      return map.toObject<BaseResponse>();
    }
    return null;
  }
}
