import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/control/dfqueryorderby.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/entity/dfwebentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/field/dfwebfield.dart';
import 'package:ingdao/dataflow/field/dffieldtype.dart';
import 'package:nesternship_flut/datalayer/entity/banner/banner_image_list_response.dart';
import 'package:nesternship_flut/datalayer/field/banner/banner_list_field.dart';
import 'package:nui_core/code/nui_code_util.dart';
import 'package:object_mapper/src/mapper.dart';

import '../base_response.dart';


class GetBannerImagesRequest extends DfWebEntity with BannerImageListField{
  GetBannerImagesRequest()
      : super(contextPath: "banner/getBannerImages") {}

  //Define what are the fields will map back the result from http response Map<String,dynamic>
  @override
  List<DfWebField> fields() {
    return [];
  }

  @override
  DfWebReqParam convertToWebReqParam(DfQuery query) {
    DfWebReqParam param = DfWebReqParam();
    // param.headers[ApiSetting.SESSION_TOKEN_NAME] = ApiSetting.token;
    if (query.purpose == DfQueryPurpose.FETCH) {
      param.reqParams["userId"] = query.extraReq.dict['userId'].toString();
      return param;
    }
    return null;
  }

  @override
  DfBaseEntity<DfBaseField> newInstance() {
    return GetBannerImagesRequest();
  }

  @override
  void customFieldJsonMap(Mapper map, List<DfWebField> fields) {}

  @override
  DfBaseEntity<DfWebField> parseExecRawJsonResultToEntity(
      DfQueryPurpose purpose, Mapper map) {
    if (purpose == DfQueryPurpose.FETCH) {
      BaseResponse.dataType = BannerImageListResponse;
      return map.toObject<BaseResponse>();
    }
    return null;
  }
}
