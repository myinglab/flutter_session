import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/control/dfqueryorderby.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/entity/dfwebentity.dart';
import 'package:ingdao/dataflow/entity/dfwebresponseentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/field/dfwebfield.dart';
import 'package:ingdao/dataflow/field/dffieldtype.dart';
import 'package:nesternship_flut/datalayer/field/banner/banner_list_field.dart';
import 'package:nesternship_flut/datalayer/entity/banner/banner_image_response.dart';
import 'package:object_mapper/src/mapper.dart';

class BannerImageListResponse extends DfWebResponseEntity with BannerImageListField{
  List<BannerImageResponse> bannerImageList;

  BannerImageListResponse({pBannerImage}) {
    fBannerImageList = DfWebField(
        entity: this,
        name: "BannerImages",
        fieldType: DfFieldType.custom(),
        getObjectValue: () => bannerImageList,
        setObjectValue: (v) => bannerImageList = v,
        value: pBannerImage);
  }

  @override
  List<DfWebField> fields() {
    return [fBannerImageList];
  }

  @override
  DfBaseEntity<DfBaseField> newInstance() {
    return BannerImageListResponse();
  }

  @override
  void customFieldJsonMap(Mapper map, List<DfBaseField> fields) {
    for (var field in fields) {
      if (field == fBannerImageList) {
        map<BannerImageResponse>(field.name, field.getObjectValue(), (v) => field.setObjectValue(v), field.getTransform());
      }
    }
  }
}