import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/control/dfqueryorderby.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/entity/dfwebentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/field/dfwebfield.dart';
import 'package:ingdao/dataflow/field/dffieldtype.dart';
import 'package:ingdao/dataflow/entity/dfwebresponseentity.dart';
import 'package:nesternship_flut/datalayer/field/banner/banner_image_field.dart';
import 'package:object_mapper/src/mapper.dart';

class BannerImageResponse extends DfWebResponseEntity with BannerImageField {
  int bannerId;
  String bannerUrl;
  String actionUrl;
  String bannerTitle;
  String bannerDescription;
  int sequence;
  int status;

  BannerImageResponse({
    pBannerId,
    pBannerUrl,
    pActionUrl,
    pBannerTitle,
    pBannerDescription,
    pSequence,
    pStatus}){
    fBannerId = DfWebField(
        entity: this,
        name: "id",
        fieldType: DfFieldType.integer(),
        getObjectValue: () => bannerId,
        setObjectValue: (v) => bannerId = v,
        value: pBannerId);
    fBannerUrl =  DfWebField(
        entity: this,
        name: "bannerUrl",
        fieldType: DfFieldType.text(),
        getObjectValue: () => bannerUrl,
        setObjectValue: (v) => bannerUrl = v,
        value: pBannerUrl);
    fActionUrl =  DfWebField(
        entity: this,
        name: "actionUrl",
        fieldType: DfFieldType.text(),
        getObjectValue: () => actionUrl,
        setObjectValue: (v) => actionUrl = v,
        value: pActionUrl);
    fBannerTitle =  DfWebField(
        entity: this,
        name: "title",
        fieldType: DfFieldType.text(),
        getObjectValue: () => bannerTitle,
        setObjectValue: (v) => bannerTitle = v,
        value: pBannerTitle);
    fBannerDescription =  DfWebField(
        entity: this,
        name: "description",
        fieldType: DfFieldType.text(),
        getObjectValue: () => bannerDescription,
        setObjectValue: (v) => bannerDescription = v,
        value: pBannerDescription);
    fSequence =  DfWebField(
        entity: this,
        name: "sequence",
        fieldType: DfFieldType.integer(),
        getObjectValue: () => sequence,
        setObjectValue: (v) => sequence= v,
        value: pSequence);
    fStatus =  DfWebField(
        entity: this,
        name: "status",
        fieldType: DfFieldType.integer(),
        getObjectValue: () => status,
        setObjectValue: (v) => status= v,
        value: pStatus);
  }

  @override
  List<DfWebField> fields() {
    return [
      fBannerId,
      fBannerUrl,
      fActionUrl,
      fBannerTitle,
      fBannerDescription,
      fSequence,
      fStatus
    ];
  }

  @override
  DfBaseEntity<DfBaseField> newInstance() {
    return  BannerImageResponse();
  }

  @override
  void customFieldJsonMap(Mapper map, List<DfWebField> fields) {}
}