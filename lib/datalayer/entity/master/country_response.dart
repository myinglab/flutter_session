import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/control/dfqueryorderby.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/entity/dfwebentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/field/dfwebfield.dart';
import 'package:ingdao/dataflow/field/dffieldtype.dart';
import 'package:ingdao/dataflow/entity/dfwebresponseentity.dart';
import 'package:nesternship_flut/datalayer/field/master/country_field.dart';
import 'package:object_mapper/src/mapper.dart';

class CountryResponse extends DfWebResponseEntity with CountryField {
  int countryId;
  String countryName;
  String countryCode;
  bool isNestleCountry;

  CountryResponse({pCountryId, pCountryName, pCountryCode ,pIsNestleCountry}) {
    fCountryId = DfWebField(
        entity: this,
        name: "countryId",
        fieldType: DfFieldType.integer(),
        getObjectValue: () => countryId,
        setObjectValue: (v) => countryId = v,
        value: pCountryId);
    fCountryName = DfWebField(
        entity: this,
        name: "countryName",
        fieldType: DfFieldType.text(),
        getObjectValue: () => countryName,
        setObjectValue: (v) => countryName = v,
        value: pCountryName);
    fCountryCode = DfWebField(
        entity: this,
        name: "countryCode",
        fieldType: DfFieldType.text(),
        getObjectValue: () => countryCode,
        setObjectValue: (v) => countryCode = v,
        value: pCountryCode);
    fIsNestleCountry = DfWebField(
        entity: this,
        name: "isNestleCountry",
        fieldType: DfFieldType.boolean(),
        getObjectValue: () => isNestleCountry,
        setObjectValue: (v) => isNestleCountry = v,
        value: pIsNestleCountry);
  }

  @override
  List<DfWebField> fields() {
    return [fCountryId, fCountryCode, fCountryName, fCountryCode ,fIsNestleCountry];
  }

  @override
  void customFieldJsonMap(Mapper map, List<DfWebField> fields) {}

  @override
  DfBaseEntity<DfBaseField> newInstance() {
    return CountryResponse();
  }
}
