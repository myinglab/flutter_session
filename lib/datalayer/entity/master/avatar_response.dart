import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/control/dfqueryorderby.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/entity/dfwebentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/field/dfwebfield.dart';
import 'package:ingdao/dataflow/field/dffieldtype.dart';
import 'package:ingdao/dataflow/entity/dfwebresponseentity.dart';
import 'package:nesternship_flut/datalayer/field/master/avatar_field.dart';
import 'package:nesternship_flut/datalayer/field/master/country_field.dart';
import 'package:object_mapper/src/mapper.dart';

class AvatarResponse extends DfWebResponseEntity with AvatarField {
  int id;
  String avatar;
  String binaryData;
  String status;

  AvatarResponse({pid, pavatar, pbinaryData, pstatus}) {
    fId = DfWebField(
        entity: this,
        name: "id",
        fieldType: DfFieldType.text(),
        getObjectValue: () => id,
        setObjectValue: (v) => id = v,
        value: pid);
    fAvatar = DfWebField(
        entity: this,
        name: "avatar",
        fieldType: DfFieldType.text(),
        getObjectValue: () => avatar,
        setObjectValue: (v) => avatar = v,
        value: pavatar);
    fBinaryData = DfWebField(
        entity: this,
        name: "binaryData",
        fieldType: DfFieldType.boolean(),
        getObjectValue: () => binaryData,
        setObjectValue: (v) => binaryData = v,
        value: pbinaryData);
    fStatus = DfWebField(
        entity: this,
        name: "status",
        fieldType: DfFieldType.boolean(),
        getObjectValue: () => status,
        setObjectValue: (v) => status = v,
        value: pstatus);
  }

  @override
  List<DfWebField> fields() {
    return [fId, fAvatar, fBinaryData, fStatus];
  }

  @override
  void customFieldJsonMap(Mapper map, List<DfWebField> fields) {}

  @override
  DfBaseEntity<DfBaseField> newInstance() {
    return AvatarResponse();
  }
}
