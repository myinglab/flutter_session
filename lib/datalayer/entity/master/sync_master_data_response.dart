import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/control/dfqueryorderby.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/entity/dfwebentity.dart';
import 'package:ingdao/dataflow/entity/dfwebresponseentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/field/dfwebfield.dart';
import 'package:ingdao/dataflow/field/dffieldtype.dart';
import 'package:nesternship_flut/datalayer/entity/master/avatar_response.dart';
import 'package:nesternship_flut/datalayer/field/master/syncMasterData_field.dart';
import 'package:nesternship_flut/datalayer/entity/master/country_response.dart';
import 'package:object_mapper/src/mapper.dart';

class SyncMasterDataResponse extends DfWebResponseEntity with  SyncMasterDataField {
  String lastSyncDate;
  List<CountryResponse> listOfCountries;
  List<AvatarResponse> listOfAvatars;
  SyncMasterDataResponse({ pListOfCountries ,pLastSyncDate, pListOfAvatars}){
    fCountryList =  DfWebField(entity: this, name: "Countries", fieldType: DfFieldType.custom(), getObjectValue: () => listOfCountries, setObjectValue: (v) => listOfCountries = v, value: pListOfCountries);
    fAvatarList =  DfWebField(entity: this, name: "Avatars", fieldType: DfFieldType.custom(), getObjectValue: () => listOfAvatars, setObjectValue: (v) => listOfAvatars = v, value: pListOfAvatars);
    fLastSyncDate = DfWebField(entity: this, name: "lastSyncData", fieldType: DfFieldType.datetime(), getObjectValue: () => lastSyncDate, setObjectValue: (v) => lastSyncDate = v, value: pLastSyncDate);
  }

  @override
  List<DfWebField> fields() {
    return [
      fCountryList,
      fAvatarList,
      fLastSyncDate
    ];
  }

  @override
  DfBaseEntity<DfBaseField> newInstance() {
    return SyncMasterDataResponse();
  }

  @override
  void customFieldJsonMap(Mapper map, List<DfBaseField> fields){
    for (var field in fields) {
      if (field == fCountryList) {
        map<CountryResponse>(field.name, field.getObjectValue(), (v) => field.setObjectValue(v), field.getTransform());
      }else if (field == fAvatarList) {
        map<AvatarResponse>(field.name, field.getObjectValue(), (v) => field.setObjectValue(v), field.getTransform());
      }
    }
  }
}




