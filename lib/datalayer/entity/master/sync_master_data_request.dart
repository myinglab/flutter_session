import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/control/dfqueryorderby.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/entity/dfwebentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/field/dfwebfield.dart';
import 'package:ingdao/dataflow/field/dffieldtype.dart';
import 'package:nesternship_flut/datalayer/entity/master/sync_master_data_response.dart';
import 'package:nesternship_flut/datalayer/field/master/syncMasterData_field.dart';
import 'package:object_mapper/src/mapper.dart';

import '../base_response.dart';

class SyncMasterDataRequest extends DfWebEntity with SyncMasterDataField{
  String lastSyncDate;

  SyncMasterDataRequest(
      {String lastSyncDate}) : super(contextPath: "master/syncMasterData") {
    fLastSyncDate = DfWebField(
        entity: this,
        name: "lastSyncDate",
        fieldType: DfFieldType.text(),
        getObjectValue: () => this.lastSyncDate,
        setObjectValue: (v) => this.lastSyncDate = v,
        value: lastSyncDate);
  }

  //Define what are the fields will map back the result from http response Map<String,dynamic>
  @override
  List<DfWebField> fields() {
    return [
      fLastSyncDate
    ];
  }

  @override
  DfWebReqParam convertToWebReqParam(DfQuery query) {
    DfWebReqParam param = DfWebReqParam();
    // param.headers[ApiSetting.SESSION_TOKEN_NAME] = ApiSetting.token;

    if (query.purpose == DfQueryPurpose.ADD) {
      final dict = query.extraReq.toDict();
      param.reqBody = dict;

      return param;
    }
    return null;
  }

  @override
  DfBaseEntity<DfBaseField> newInstance() {
    return SyncMasterDataRequest();
  }

  @override
  void customFieldJsonMap(Mapper map, List<DfWebField> fields) {}

  @override
  DfBaseEntity<DfWebField> parseExecRawJsonResultToEntity(
      DfQueryPurpose purpose, Mapper map) {
    if (purpose == DfQueryPurpose.ADD) {
      BaseResponse.dataType = SyncMasterDataResponse;
      return map.toObject<BaseResponse>();
    }
    return null;
  }
}
