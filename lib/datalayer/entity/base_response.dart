import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/control/dfqueryorderby.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/entity/dfwebentity.dart';
import 'package:ingdao/dataflow/entity/dfwebresponseentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/field/dfwebfield.dart';
import 'package:ingdao/dataflow/field/dffieldtype.dart';
import 'package:nesternship_flut/datalayer/entity/category/category_list_response.dart';
import 'package:nesternship_flut/datalayer/entity/course/course_get_status_response.dart';
import 'package:nesternship_flut/datalayer/entity/course/course_search_request.dart';
import 'package:nesternship_flut/datalayer/entity/notification/notification_count_response.dart';
import 'package:nesternship_flut/datalayer/entity/notification/notification_list_information_response.dart';
import 'package:nesternship_flut/datalayer/entity/notification/notification_delete_response.dart';
import 'package:nesternship_flut/datalayer/entity/notification/notification_list_response.dart';
import 'package:nesternship_flut/datalayer/entity/registration/registration_response.dart';
import 'package:nesternship_flut/datalayer/entity/notification/set_notification_read_response.dart';
import 'package:nesternship_flut/datalayer/entity/banner/banner_image_list_response.dart';
import 'package:nesternship_flut/datalayer/entity/user_profile/update_member_details_response.dart';
import 'package:nesternship_flut/datalayer/entity/user_profile/user_profile_response.dart';
import 'package:nesternship_flut/datalayer/field/base_response_field.dart';
import 'package:object_mapper/src/mapper.dart';
import 'course/course_list_response.dart';
import 'master/sync_master_data_response.dart';

class BaseResponse extends DfWebResponseEntity with BaseResponseField {
  String message;
  bool status;
  String errorCode;
  dynamic data;
  static Type dataType;

  BaseResponse() {
    fMessage = DfWebField(
        entity: this,
        name: "message",
        fieldType: DfFieldType.text(),
        getObjectValue: () => message,
        setObjectValue: (v) => message = v,
        value: message);
    fStatus = DfWebField(
        entity: this,
        name: "status",
        fieldType: DfFieldType.boolean(),
        getObjectValue: () => status,
        setObjectValue: (v) => status = v,
        value: message);
    fErrorCode = DfWebField(
        entity: this,
        name: "errorCode",
        fieldType: DfFieldType.text(),
        getObjectValue: () => errorCode,
        setObjectValue: (v) => errorCode = v,
        value: message);
    fData = DfWebField(
        entity: this,
        name: "data",
        fieldType: DfFieldType.custom(),
        getObjectValue: () => data,
        setObjectValue: (v) => data = v,
        value: data);
  }

  @override
  List<DfWebField> fields() {
    return [fMessage, fStatus, fErrorCode, fData];
  }

  @override
  DfBaseEntity<DfBaseField> newInstance() {
    return BaseResponse();
  }

  @override
  void customFieldJsonMap(Mapper map, List<DfWebField> fields) {
    for (var field in fields) {
      if (field == fData) {
        if (dataType == RegistrationResponse)
          map<RegistrationResponse>(field.name, field.getObjectValue(),
              (v) => field.setObjectValue(v), field.getTransform());
        else if (dataType == DeleteNotificationResponse)
          map<DeleteNotificationResponse>(field.name, field.getObjectValue(),
              (v) => field.setObjectValue(v), field.getTransform());
        else if (dataType == SyncMasterDataResponse) {
          map<SyncMasterDataResponse>(field.name, field.getObjectValue(),
              (v) => field.setObjectValue(v), field.getTransform());
        } else if (dataType == NotificationListInfoResponse) {
          map<NotificationListInfoResponse>(field.name, field.getObjectValue(),
              (v) => field.setObjectValue(v), field.getTransform());
        } else if (dataType == NotificationListResponse) {
          map<NotificationListResponse>(field.name, field.getObjectValue(),
                  (v) => field.setObjectValue(v), field.getTransform());
        } else if (dataType == NotificationCountResponse) {
          map<NotificationCountResponse>(field.name, field.getObjectValue(),
              (v) => field.setObjectValue(v), field.getTransform());
        } else if (dataType == SetNotificationReadResponse) {
          map<SetNotificationReadResponse>(field.name, field.getObjectValue(),
              (v) => field.setObjectValue(v), field.getTransform());
        } else if (dataType == BannerImageListResponse) {
          map<BannerImageListResponse>(field.name, field.getObjectValue(),
              (v) => field.setObjectValue(v), field.getTransform());
        } else if (dataType == UpdateMemberDetailsResponse) {
          map<UpdateMemberDetailsResponse>(field.name, field.getObjectValue(),
              (v) => field.setObjectValue(v), field.getTransform());
        } else if (dataType == GetUserProfileResponse) {
          map<GetUserProfileResponse>(field.name, field.getObjectValue(),
              (v) => field.setObjectValue(v), field.getTransform());
        } else if (dataType == CourseListResponse) {
          map<CourseListResponse>(field.name, field.getObjectValue(),
              (v) => field.setObjectValue(v), field.getTransform());
        } else if (dataType == CategoryListResponse) {
          map<CategoryListResponse>(field.name, field.getObjectValue(),
              (v) => field.setObjectValue(v), field.getTransform());
        } else if (dataType == CourseSearchRequest) {
          map<CourseSearchRequest>(field.name, field.getObjectValue(),
              (v) => field.setObjectValue(v), field.getTransform());
        } else if (dataType == CourseGetStatusResponse) {
          map<CourseGetStatusResponse>(field.name, field.getObjectValue(),
              (v) => field.setObjectValue(v), field.getTransform());
        }
      }
    }
  }
}
