import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/control/dfqueryorderby.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/entity/dfwebentity.dart';
import 'package:ingdao/dataflow/entity/dfwebresponseentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/field/dfwebfield.dart';
import 'package:ingdao/dataflow/field/dffieldtype.dart';
import 'package:nesternship_flut/datalayer/entity/category/category_response.dart';
import 'package:nesternship_flut/datalayer/field/category/category_list_field.dart';
import 'package:object_mapper/src/mapper.dart';

class CategoryListResponse extends DfWebResponseEntity with CategoryListField {
  List<CategoryResponse> courseList;

  CategoryListResponse({pCourseList}) {
    fCourseList = DfWebField(
        entity: this,
        name: "courseList",
        fieldType: DfFieldType.custom(),
        getObjectValue: () => courseList,
        setObjectValue: (v) => courseList = v,
        value: pCourseList);
  }

  @override
  List<DfWebField> fields() {
    // TODO: implement fields
    return [fCourseList];
  }

  @override
  DfBaseEntity<DfBaseField> newInstance() {
    // TODO: implement newInstance
    return CategoryListResponse();
  }

  @override
  void customFieldJsonMap(Mapper map, List<DfWebField> fields) {
    // TODO: implement customFieldJsonMap
    for (var field in fields) {
      if (field == fCourseList) {
        map<CategoryResponse>(field.name, field.getObjectValue,
            (v) => field.setObjectValue(v), field.getTransform());
      }
    }
  }
}
