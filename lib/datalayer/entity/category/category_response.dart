import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/control/dfqueryorderby.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/entity/dfwebentity.dart';
import 'package:ingdao/dataflow/entity/dfwebresponseentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/field/dfwebfield.dart';
import 'package:ingdao/dataflow/field/dffieldtype.dart';
import 'package:nesternship_flut/datalayer/field/category/category_field.dart';
import 'package:object_mapper/src/mapper.dart';

class CategoryResponse extends DfWebResponseEntity with CategoryField {
  int id;
  String title;
  int sequence;
  DateTime createdDate;
  DateTime modifiedDate;

  CategoryResponse(
      {int pId,
      String pTitle,
      int pSequence,
      DateTime pCreatedDate,
      DateTime pModifiedDate}) {
    fId = DfWebField(
        entity: this,
        name: "id",
        fieldType: DfFieldType.integer(),
        getObjectValue: () => id,
        setObjectValue: (v) => id = v,
        value: pId);
    fTitle = DfWebField(
        entity: this,
        name: "title",
        fieldType: DfFieldType.text(),
        getObjectValue: () => title,
        setObjectValue: (v) => title = v,
        value: pTitle);
    fSequence = DfWebField(
        entity: this,
        name: "sequence",
        fieldType: DfFieldType.integer(),
        getObjectValue: () => sequence,
        setObjectValue: (v) => sequence = v,
        value: pSequence);
    fCreatedDate = DfWebField(
        entity: this,
        name: "createdDate",
        fieldType: DfFieldType.datetime(),
        getObjectValue: () => createdDate,
        setObjectValue: (v) => createdDate = v,
        value: pCreatedDate);
    fModifiedDate = DfWebField(
        entity: this,
        name: "modifiedDate",
        fieldType: DfFieldType.datetime(),
        getObjectValue: () => modifiedDate,
        setObjectValue: (v) => modifiedDate = v,
        value: pModifiedDate);
  }

  @override
  void customFieldJsonMap(Mapper map, List<DfWebField> fields) {
    // TODO: implement customFieldJsonMap
  }

  @override
  List<DfWebField> fields() {
    // TODO: implement fields
    return [fId, fTitle, fSequence, fCreatedDate, fModifiedDate];
  }

  @override
  DfBaseEntity<DfBaseField> newInstance() {
    // TODO: implement newInstance
    return CategoryResponse();
  }
}
