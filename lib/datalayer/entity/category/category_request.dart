import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/control/dfqueryorderby.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/entity/dfwebentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/field/dfwebfield.dart';
import 'package:ingdao/dataflow/field/dffieldtype.dart';
import 'package:nesternship_flut/datalayer/entity/category/category_list_response.dart';
import 'package:object_mapper/src/mapper.dart';

import '../base_response.dart';

class CategoryRequest extends DfWebEntity {
  CategoryRequest() : super(contextPath: "category/categoriesByCountry") {}

  @override
  DfWebReqParam convertToWebReqParam(DfQuery query) {
    // TODO: implement convertToWebReqParam
    DfWebReqParam param = DfWebReqParam();

    if (query.purpose == DfQueryPurpose.FETCH) {
      param.reqParams["countryId"] =
          query.extraReq.dict['countryId'].toString();
      return param;
    }
    return null;
  }

  @override
  void customFieldJsonMap(Mapper map, List<DfWebField> fields) {
    // TODO: implement customFieldJsonMap
  }

  @override
  List<DfWebField> fields() {
    // TODO: implement fields
    return [];
  }

  @override
  DfBaseEntity<DfBaseField> newInstance() {
    // TODO: implement newInstance
    return CategoryRequest();
  }

  @override
  DfBaseEntity<DfWebField> parseExecRawJsonResultToEntity(
      DfQueryPurpose queryPurpose, Mapper map) {
    // TODO: implement parseExecRawJsonResultToEntity
    if (queryPurpose == DfQueryPurpose.FETCH) {
      BaseResponse.dataType = CategoryListResponse;
      return map.toObject<BaseResponse>();
    }
    return null;
  }
}
