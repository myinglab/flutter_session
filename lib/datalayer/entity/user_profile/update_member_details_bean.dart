import 'package:meta/meta.dart';
import 'package:nesternship_flut/data/model/user_profile.dart';

class UpdateMemberDetailsBean {
  String id;
  String memberEmail;
  String memberName;
  String userId;
  String avatar;
  String dateOfBirth;
  String gender;
  String originCountry;
  String nestleCountry;

  UpdateMemberDetailsBean(
      {this.id,
        this.memberEmail,
        this.memberName ,
        this.userId,
        this.avatar,
        this.dateOfBirth,
        this.gender,
        this.originCountry,
        this.nestleCountry});

  UpdateMemberDetailsBean.from({@required UserProfile field}) {
    id = field.id;
    memberEmail = field.memberEmail;
    memberName = field.memberName;
    userId = field.userId;
    avatar = field.avatar;
    dateOfBirth = field.dateOfBirth;
    gender = field.gender;
    originCountry = field.originCountry;
    nestleCountry = field.nestleCountry;
  }

  void setMemberName(String userName){
    this.memberName = userName;
  }
  
  void setDateOfBirth(String userDateOfBirth){
    this.dateOfBirth = userDateOfBirth;
  }
  
  void setGender(String userGender){
    this.gender = userGender;
  }

  void setOriginCountry(String userCountryName){
    this.originCountry = userCountryName;
  }
  
  void setNestleCountry(String userNestleCountry){
    this.nestleCountry =  userNestleCountry;
  }
}
