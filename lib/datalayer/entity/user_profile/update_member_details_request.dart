import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/control/dfqueryorderby.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/entity/dfwebentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/field/dfwebfield.dart';
import 'package:ingdao/dataflow/field/dffieldtype.dart';
import 'package:nesternship_flut/datalayer/entity/user_profile/update_member_details_response.dart';
import 'package:nesternship_flut/datalayer/field/user_profile/update_member_details_field.dart';
import 'package:object_mapper/src/mapper.dart';

import '../base_response.dart';

class UpdateMemberDetailsRequest extends DfWebEntity with UpdateMemberDetailsField {
  String id;
  String memberEmail;
  String memberName;
  String userId;
  String avatar;
  String dateOfBirth;
  String gender;
  String originCountry;
  String nestleCountry;

  UpdateMemberDetailsRequest(
      {String pId,
        String pMemberEmail,
        String pMemberName,
        String pUserId,
        String pAvatar,
        String pDateOfBirth,
        String pGender,
        String pOriginCountry,
        String pNestleCountry})
      : super(contextPath: "member/updateMemberDetails") {
    fId = DfWebField(
        entity: this,
        name: "id",
        fieldType: DfFieldType.text(),
        getObjectValue: () => this.id,
        setObjectValue: (v) => this.id = v,
        value: pId);
    fMemberEmail = DfWebField(
        entity: this,
        name: "memberEmail",
        fieldType: DfFieldType.text(),
        getObjectValue: () => this.memberEmail,
        setObjectValue: (v) => this.memberEmail = v,
        value: pMemberEmail);
    fMemberName = DfWebField(
        entity: this,
        name: "memberName",
        fieldType: DfFieldType.text(),
        getObjectValue: () => this.memberName,
        setObjectValue: (v) => this.memberName = v,
        value: pMemberName);
    fUserId= DfWebField(
        entity: this,
        name: "userId",
        fieldType: DfFieldType.text(),
        getObjectValue: () => this.userId,
        setObjectValue: (v) => this.userId = v,
        value: pUserId);
    fAvatar = DfWebField(
        entity: this,
        name: "avatar",
        fieldType: DfFieldType.text(),
        getObjectValue: () => this.avatar,
        setObjectValue: (v) => this.avatar = v,
        value: pAvatar);
    fDateOfBirth = DfWebField(
        entity: this,
        name: "dateOfBirth",
        fieldType: DfFieldType.text(),
        getObjectValue: () => this.dateOfBirth,
        setObjectValue: (v) => this.dateOfBirth = v,
        value: pDateOfBirth);
    fGender = DfWebField(
        entity: this,
        name: "gender",
        fieldType: DfFieldType.text(),
        getObjectValue: () => this.gender,
        setObjectValue: (v) => this.gender = v,
        value: pGender);
    fOriginCountry = DfWebField(
        entity: this,
        name: "originCountry",
        fieldType: DfFieldType.text(),
        getObjectValue: () => this.originCountry,
        setObjectValue: (v) => this.originCountry = v,
        value: pOriginCountry);
    fNestleCountry = DfWebField(
        entity: this,
        name: "nestleCountry",
        fieldType: DfFieldType.text(),
        getObjectValue: () => this.nestleCountry,
        setObjectValue: (v) => this.nestleCountry = v,
        value: pNestleCountry);
  }

  //Define what are the fields will map back the result from http response Map<String,dynamic>
  @override
  List<DfWebField> fields() {
    return [
      fId,
      fMemberEmail,
      fMemberName,
      fUserId,
      fAvatar,
      fDateOfBirth,
      fGender,
      fOriginCountry,
      fNestleCountry
    ];
  }

  @override
  DfWebReqParam convertToWebReqParam(DfQuery query) {
    DfWebReqParam param = DfWebReqParam();
    // param.headers[ApiSetting.SESSION_TOKEN_NAME] = ApiSetting.token;

    if (query.purpose == DfQueryPurpose.UPDATE_WHOLE_ENTITY_ATTRS) {
      final requestEntity = query.extraReq.entity;
      param.reqBody = requestEntity.toJson();
      return param;
    }
    return null;
  }

  @override
  DfBaseEntity<DfBaseField> newInstance() {
    return  UpdateMemberDetailsRequest();
  }

  @override
  void customFieldJsonMap(Mapper map, List<DfWebField> fields) {

  }

  @override
  DfBaseEntity<DfWebField> parseExecRawJsonResultToEntity(DfQueryPurpose purpose, Mapper map) {
    if (purpose == DfQueryPurpose.UPDATE_WHOLE_ENTITY_ATTRS) {
      BaseResponse.dataType = UpdateMemberDetailsResponse;
      return map.toObject<BaseResponse>();
    }
    return null;
  }
}

