import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/control/dfqueryorderby.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/entity/dfwebentity.dart';
import 'package:ingdao/dataflow/entity/dfwebresponseentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/field/dfwebfield.dart';
import 'package:ingdao/dataflow/field/dffieldtype.dart';
import 'package:nesternship_flut/datalayer/field/registration/registration_field.dart';
import 'package:nesternship_flut/datalayer/field/user_profile/user_profile_field.dart';
import 'package:object_mapper/src/mapper.dart';

class GetUserProfileResponse extends DfWebResponseEntity with UserProfileField {
  String memberEmail;
  String memberName;
  int id;
  int userId;
  String avatar;
  String dateOfBirth;
  String gender;
  String originCountry;
  String nestleCountry;

  GetUserProfileResponse(
      {String pMemberEmail,
      String pMemberName,
      int pId,
      int pUserId,
      String pAvatar,
      String pDateOfBirth,
      String pGender,
      String pOriginCountry,
      String pNestleCountry}) {
    fgender = DfWebField(
        entity: this,
        name: "gender",
        fieldType: DfFieldType.text(),
        getObjectValue: () => this.gender,
        setObjectValue: (v) => this.gender = v,
        value: pGender);
    foriginCountry = DfWebField(
        entity: this,
        name: "originCountry",
        fieldType: DfFieldType.text(),
        getObjectValue: () => this.originCountry,
        setObjectValue: (v) => this.originCountry = v,
        value: pOriginCountry);
    fmemberName = DfWebField(
        entity: this,
        name: "memberName",
        fieldType: DfFieldType.text(),
        getObjectValue: () => this.memberName,
        setObjectValue: (v) => this.memberName = v,
        value: pMemberName);
    fnestleCountry = DfWebField(
        entity: this,
        name: "nestleCountry",
        fieldType: DfFieldType.text(),
        getObjectValue: () => this.nestleCountry,
        setObjectValue: (v) => this.nestleCountry = v,
        value: pNestleCountry);
    fdateOfBirth = DfWebField(
        entity: this,
        name: "dateOfBirth",
        fieldType: DfFieldType.text(),
        getObjectValue: () => this.dateOfBirth,
        setObjectValue: (v) => this.dateOfBirth = v,
        value: pDateOfBirth);
    fId = DfWebField(
        entity: this,
        name: "id",
        fieldType: DfFieldType.integer(),
        getObjectValue: () => this.id,
        setObjectValue: (v) => this.id = v,
        value: pId);
    favatar = DfWebField(
        entity: this,
        name: "avatar",
        fieldType: DfFieldType.text(),
        getObjectValue: () => this.avatar,
        setObjectValue: (v) => this.avatar = v,
        value: pAvatar);
    fuserId = DfWebField(
        entity: this,
        name: "userId",
        fieldType: DfFieldType.integer(),
        getObjectValue: () => this.userId,
        setObjectValue: (v) => this.userId = v,
        value: pUserId);
    fmemberEmail = DfWebField(
        entity: this,
        name: "memberEmail",
        fieldType: DfFieldType.text(),
        getObjectValue: () => this.memberEmail,
        setObjectValue: (v) => this.memberEmail = v,
        value: pMemberEmail);
  }

  @override
  List<DfWebField> fields() {
    return [
      fgender,
      foriginCountry,
      fmemberName,
      fnestleCountry,
      fdateOfBirth,
      fId,
      favatar,
      fuserId,
      fmemberEmail,
    ];
  }

  @override
  DfBaseEntity<DfBaseField> newInstance() {
    return GetUserProfileResponse();
  }

  @override
  void customFieldJsonMap(Mapper map, List<DfBaseField> fields) {}
}
