import 'package:flutter/foundation.dart';
import 'package:ingdao/dataflow/control/dfquerycriteria.dart';
import 'package:ingdao/dataflow/control/dfresponse.dart';
import 'package:ingdao/dataflow/service/dfservice.dart';
import 'package:ingdao/ingdao.dart';
import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/control/dfqueryentityjoin.dart';
import 'package:ingdao/dataflow/repository/dfbaserepository.dart';
import 'package:nesternship_flut/datalayer/data_factory.dart';
import 'package:nesternship_flut/datalayer/entity/base_response.dart';
import 'package:nesternship_flut/datalayer/entity/course/course_by_country_request.dart';
import 'package:nesternship_flut/datalayer/entity/course/course_get_status_request.dart';
import 'package:nesternship_flut/datalayer/entity/course/course_get_status_response.dart';
import 'package:nesternship_flut/datalayer/entity/course/course_list_response.dart';
import 'package:nesternship_flut/datalayer/entity/course/course_request.dart';
import 'package:nesternship_flut/datalayer/entity/course/course_search_request.dart';
import 'package:nesternship_flut/datalayer/entity/course/course_update_status_request.dart';
import 'package:nui_core/code/nui_code_util.dart';
import 'package:nui_core/model/interfaces.dart';

class CourseListService extends DfService {
  @override
  startInit() {}

  Future<DataResult> getCourseListApi(
      String userId, int categoryId, int countryId,
      {int page: 1}) async {
    DfFactory webFactory =
        await DataFactory.instance.getManipulator(RepoType.web);

    DfBaseRepository repository = webFactory.currentRepository;

    DfExtraReq extraReq = DfExtraReq();
    extraReq.dict = {
      'userId': userId,
      'categoryId': categoryId,
      'countryId': countryId,
      'page': page ?? 1
    };

    DfQuery query = DfQuery(
        purpose: DfQueryPurpose.FETCH,
        queryFields: [],
        extraReq: extraReq,
        entityJoin: DfQueryEntityJoin(entities: [CourseRequest()], joins: []));

    DfResponse respList = await webFactory.fetch(query, repository: repository);
    BaseResponse result = respList.renderResultToEntity();
    final CourseListResponse response = result.data;
    logNUI("CourseListService", "Courses Size : ${response.courseList.length}");
    logNUI("CourseListService", "Courses List : ${response.courseList}");
    return DataResult(
        success: result.status,
        message: result.message,
        data: response.courseList);
  }

  Future<DataResult> getCourseSearchListApi(
      String userId, int categoryId, int countryId,
      {int page: 1, String keyword}) async {
    DfFactory webFactory =
        await DataFactory.instance.getManipulator(RepoType.web);

    DfBaseRepository repository = webFactory.currentRepository;

    DfExtraReq extraReq = DfExtraReq();
    extraReq.dict = {
      'courseName': keyword ?? "",
      'nestleCountryId': countryId ?? "",
      'page': page ?? 1,
      'userId': userId,
      'categoryId': categoryId,
    };

    DfQuery query = DfQuery(
        purpose: DfQueryPurpose.FETCH,
        queryFields: [],
        extraReq: extraReq,
        entityJoin:
            DfQueryEntityJoin(entities: [CourseSearchRequest()], joins: []));

    DfResponse respList = await webFactory.fetch(query, repository: repository);
    BaseResponse result = respList.renderResultToEntity();
    final CourseListResponse response = result.data;
    logNUI("CourseListService", "Courses Size : ${response.courseList.length}");
    logNUI("CourseListService", "Courses List : ${response.courseList}");
    return DataResult(
        success: result.status,
        message: result.message,
        data: response.courseList);
  }


  Future<DataResult> updateCourseStatus(
      CourseUpdateStatusRequest request) async {
    DfFactory webFactory =
    await DataFactory.instance.getManipulator(RepoType.web);

    DfBaseRepository repository = webFactory.currentRepository;

    DfExtraReq extraReq = DfExtraReq();
    extraReq.entity = request;

    DfQuery query = DfQuery(
        purpose: DfQueryPurpose.ADD,
        queryFields: [],
        extraReq: extraReq,
        entityJoin: DfQueryEntityJoin(entities: [request], joins: []));
    List<DfResponse> respList =
    await webFactory.save([query], repository: repository);
    BaseResponse result = respList[0].renderResultToEntity();
    return DataResult(
        success: result.status, message: result.message, data: null);
  }

  Future<DataResult> getCourseReadStatus(String userId, String courseId) async {
    DfFactory webFactory =
    await DataFactory.instance.getManipulator(RepoType.web);
    DfBaseRepository repository = webFactory.currentRepository;

    DfExtraReq extraReq = DfExtraReq();
    extraReq.dict = {
    'userId': userId,
    'courseId': courseId,
    };

    DfQuery query = DfQuery(
        purpose: DfQueryPurpose.FETCH,
        queryFields: [],
        extraReq: extraReq,
        entityJoin: DfQueryEntityJoin(entities: [CourseGetStatusRequest()], joins: []));

    DfResponse respList = await webFactory.fetch(query, repository: repository);
    BaseResponse result = respList.renderResultToEntity();
    final CourseGetStatusResponse response = result.data;
    logNUI("CourseReadStatusService",
    "Courses GetReadStatus Api : ${result.status}");
    logNUI("CourseReadStatusService",
    "Courses Read Status : ${response.courseCompleted}");
    return DataResult(
    success: result.status, message: result.message, data: response);
  }


  Future<DataResult> getCourseByCountryAPI (int countryId, {int page: 1}) async {
    DfFactory webFactory = await DataFactory.instance.getManipulator(RepoType.web);

    DfBaseRepository repository = webFactory.currentRepository;

    DfExtraReq extraReq = DfExtraReq();
    extraReq.dict = {
      'nestleCountryId': countryId,
      'page': page ?? 1,
    };

    DfQuery query = DfQuery(
        purpose: DfQueryPurpose.FETCH,
        queryFields: [],
        extraReq: extraReq,
        entityJoin:
        DfQueryEntityJoin(entities: [CourseByCountryRequest()], joins: []));

    DfResponse respList = await webFactory.fetch(query, repository: repository);
    BaseResponse result = respList.renderResultToEntity();
    final CourseListResponse response = result.data;
    return DataResult(
        success: result.status,
        message: result.message,
        data: response.courseList);
  }
}
