import 'package:ingdao/dataflow/control/dfquerycriteria.dart';
import 'package:ingdao/dataflow/control/dfresponse.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/service/dfservice.dart';
import 'package:ingdao/ingdao.dart';
import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/control/dfqueryentityjoin.dart';
import 'package:ingdao/dataflow/repository/dfbaserepository.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:nesternship_flut/datalayer/data_factory.dart';
import 'package:nesternship_flut/datalayer/entity/banner/banner_image_list_request.dart';
import 'package:nesternship_flut/datalayer/entity/banner/banner_image_list_response.dart';
import 'package:nesternship_flut/datalayer/entity/banner/banner_image_response.dart';
import 'package:nesternship_flut/datalayer/entity/base_response.dart';

import 'package:nui_core/code/nui_code_util.dart';

class BannerImageListService extends DfService {
  @override
  startInit() {}

  Future<List<BannerImageResponse>> getBannerImagesApi(String userId) async {
    DfFactory webFactory =
    await DataFactory.instance.getManipulator(RepoType.web);

    DfBaseRepository repository = webFactory.currentRepository;

    DfExtraReq extraReq = DfExtraReq();
    extraReq.dict = {'userId': userId};

    DfQuery query = DfQuery(
        purpose: DfQueryPurpose.FETCH,
        queryFields: [],
        extraReq: extraReq,
        paging: null,
        orderBy: null,
        criteriaGroups: null,
        //tbc function
        entityJoin: DfQueryEntityJoin(
            entities: [GetBannerImagesRequest()], joins: []));

    print("result try");
    DfResponse respList = await webFactory.fetch(query, repository: repository);
    BaseResponse result = respList.renderResultToEntity();

    if(result.status == true){
      final BannerImageListResponse response = result.data;
      logNUI("BannerImageService", "Banner Image Size: ${response.bannerImageList.length}");
      return response.bannerImageList;
    }
    logNUI('test printing result', result.message);
    logNUI('test printing result', result.errorCode);
    logNUI('test printing result', result.status.toString());
  }
}