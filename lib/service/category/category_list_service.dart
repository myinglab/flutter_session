import 'package:flutter/foundation.dart';
import 'package:ingdao/dataflow/control/dfquerycriteria.dart';
import 'package:ingdao/dataflow/control/dfresponse.dart';
import 'package:ingdao/dataflow/service/dfservice.dart';
import 'package:ingdao/ingdao.dart';
import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/control/dfqueryentityjoin.dart';
import 'package:ingdao/dataflow/repository/dfbaserepository.dart';
import 'package:nesternship_flut/datalayer/data_factory.dart';
import 'package:nesternship_flut/datalayer/entity/base_response.dart';
import 'package:nesternship_flut/datalayer/entity/category/category_list_response.dart';
import 'package:nesternship_flut/datalayer/entity/category/category_request.dart';
import 'package:nui_core/code/nui_code_util.dart';
import 'package:nui_core/model/interfaces.dart';

class CategoryListService extends DfService {
  @override
  startInit() {}

  Future<DataResult> getCategoryListApi(int countryId) async {
    DfFactory webFactory =
        await DataFactory.instance.getManipulator(RepoType.web);

    DfBaseRepository repository = webFactory.currentRepository;

    DfExtraReq extraReq = DfExtraReq();
    extraReq.dict = {'countryId': countryId};

    DfQuery query = DfQuery(
        purpose: DfQueryPurpose.FETCH,
        queryFields: [],
        extraReq: extraReq,
        entityJoin:
            DfQueryEntityJoin(entities: [CategoryRequest()], joins: []));

    DfResponse respList = await webFactory.fetch(query, repository: repository);
    BaseResponse result = respList.renderResultToEntity();
    final CategoryListResponse response = result.data;
    logNUI(
        "CategoryListService", "Courses Size : ${response.courseList.length}");
    logNUI("CategoryListService", "Category Size : ${response.courseList}");
    return DataResult(
        success: result.status == true,
        message: result.message,
        data: response.courseList);
  }
}
