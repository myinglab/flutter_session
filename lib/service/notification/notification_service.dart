import 'package:ingdao/dataflow/control/dfquerycriteria.dart';
import 'package:ingdao/dataflow/control/dfresponse.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/service/dfservice.dart';
import 'package:ingdao/ingdao.dart';
import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/control/dfqueryentityjoin.dart';
import 'package:ingdao/dataflow/repository/dfbaserepository.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:nesternship_flut/datalayer/data_factory.dart';
import 'package:nesternship_flut/datalayer/entity/base_response.dart';
import 'package:nesternship_flut/datalayer/entity/notification/notification_count_request.dart';
import 'package:nesternship_flut/datalayer/entity/notification/notification_list_clear_request.dart';

import 'package:nesternship_flut/datalayer/entity/notification/set_notification_read_request.dart';

import 'package:nesternship_flut/datalayer/entity/notification/notification_create_request.dart';
import 'package:nesternship_flut/datalayer/entity/notification/notification_delete_request.dart';

import 'package:nesternship_flut/datalayer/entity/registration/registration_request.dart';
import 'package:nesternship_flut/datalayer/entity/user_profile/user_profile_request.dart';
import 'package:nui_core/code/nui_code_util.dart';

class NotificationService extends DfService {
  @override
  startInit() {}

  Future<int> getNotificationCountApi(String userId) async {
    DfFactory webFactory =
        await DataFactory.instance.getManipulator(RepoType.web);

    DfBaseRepository repository = webFactory.currentRepository;

    DfExtraReq extraReq = DfExtraReq();
    extraReq.dict = {'userId': userId};

    DfQuery query = DfQuery(
        purpose: DfQueryPurpose.FETCH,
        queryFields: [],
        extraReq: extraReq,
        paging: null,
        orderBy: null,
        criteriaGroups: null,
        //tbc function
        entityJoin: DfQueryEntityJoin(
            entities: [GetNotificationCountRequest()], joins: []));

    print("result try");
    DfResponse respList = await webFactory.fetch(query, repository: repository);
    BaseResponse result = respList.renderResultToEntity();
    logNUI('test printing result', result.message);
    logNUI('test printing result', result.errorCode);
    logNUI('test printing result', result.status.toString());
    logNUI('test printing result', result.data.toString());
    logNUI('test printing result', result.data.notificationCount.toString());
    return result.data.notificationCount;
  }

  Future<bool> setNotificationReadApi(SetNotificationReadRequest request, int notifId) async {
    DfFactory webFactory =
        await DataFactory.instance.getManipulator(RepoType.web);

    DfBaseRepository repository = webFactory.currentRepository;

    DfExtraReq extraReq = DfExtraReq();
    extraReq.dict = {
      "notificationIds": [notifId],
      "isRead": true
    };

    DfQuery query = DfQuery(
        purpose: DfQueryPurpose.ADD,
        queryFields: [],
        extraReq: extraReq,
        paging: null,
        orderBy: null,
        criteriaGroups: null,
        //tbc function

        entityJoin: DfQueryEntityJoin(
            entities: [SetNotificationReadRequest()], joins: []));

    print("result try");
    List<DfResponse> respList =
        await webFactory.save([query], repository: repository);
    BaseResponse result = respList[0].renderResultToEntity();

    logNUI('test printing result', result.message);
    logNUI('test printing result', result.errorCode);
    logNUI('test printing result', result.status.toString());
    logNUI('test printing result', result.data.toString());
    logNUI('test printing result', result.data.isRead.toString());
    return result.data.isRead;
  }

  void createNotificationApi(CreateNotificationRequest request) async {
    DfFactory webFactory =
        await DataFactory.instance.getManipulator(RepoType.web);

    DfBaseRepository repository = webFactory.currentRepository;

    request.notificationContentId = "2";
    request.read = "false";
    request.userId = 2;

    DfExtraReq extraReq = DfExtraReq();
    extraReq.entity = request;

    DfQuery query = DfQuery(
        purpose: DfQueryPurpose.ADD,
        queryFields: [],
        extraReq: extraReq,
        paging: null,
        orderBy: null,
        criteriaGroups: null,
        //tbc function
        entityJoin: DfQueryEntityJoin(entities: [request], joins: []));

    print("result try");
    List<DfResponse> respList =
        await webFactory.save([query], repository: repository);
    BaseResponse result = respList[0].renderResultToEntity();

    logNUI('test printing result', result.message);
    logNUI('test printing result', result.errorCode);
    logNUI('test printing result', result.status.toString());
  }

  Future<void> deleteNotificationApi(String notificationId) async {
    DfFactory webFactory = await DataFactory.instance.getManipulator(RepoType.web);

    DfBaseRepository repository = webFactory.currentRepository;

    DfExtraReq extraReq = DfExtraReq();
    extraReq.dict = {"notificationId": notificationId};

    DfQuery query = DfQuery(
        purpose: DfQueryPurpose.DELETE,
        queryFields: [],
        extraReq: extraReq,
        paging: null,
        orderBy: null,
        criteriaGroups: null,
        //tbc function
        entityJoin: DfQueryEntityJoin(
            entities: [DeleteNotificationRequest()], joins: []));

    print("result try");
    List<DfResponse> respList = await webFactory.delete([query], repository: repository);
    BaseResponse result = respList[0].renderResultToEntity();

    logNUI('test printing result', result.message);
    logNUI('test printing result', result.errorCode);
    logNUI('test printing result', result.status.toString());

    return;
  }

  Future<bool> clearNotificationListApi(String userId) async {
    DfFactory webFactory = await DataFactory.instance.getManipulator(RepoType.web);

    DfBaseRepository repository = webFactory.currentRepository;

    DfExtraReq extraReq = DfExtraReq();
    extraReq.dict = {"userId": userId};

    DfQuery query = DfQuery(
        purpose: DfQueryPurpose.DELETE,
        queryFields: [],
        extraReq: extraReq,
        paging: null,
        orderBy: null,
        criteriaGroups: null,
        //tbc function
        entityJoin: DfQueryEntityJoin(
            entities: [ClearNotificationListRequest()], joins: []));

    print("result try");
    List<DfResponse> respList = await webFactory.delete([query], repository: repository);
    BaseResponse result = respList[0].renderResultToEntity();

    logNUI('test printing result', result.message);
    logNUI('test printing result', result.errorCode);
    logNUI('test printing result', result.status.toString());

    return result.status == true;
  }
}
