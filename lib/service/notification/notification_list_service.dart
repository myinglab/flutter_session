import 'dart:convert';

import 'package:ingdao/dataflow/control/dfquerycriteria.dart';
import 'package:ingdao/dataflow/control/dfresponse.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/service/dfservice.dart';
import 'package:ingdao/ingdao.dart';
import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/control/dfqueryentityjoin.dart';
import 'package:ingdao/dataflow/repository/dfbaserepository.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:nesternship_flut/bloc/notification_bloc.dart';
import 'package:nesternship_flut/datalayer/data_factory.dart';
import 'package:nesternship_flut/datalayer/entity/base_response.dart';
import 'package:nesternship_flut/datalayer/entity/notification/notification_list_information_response.dart';
import 'package:nesternship_flut/datalayer/entity/notification/notification_list_request.dart';
import 'package:nesternship_flut/datalayer/entity/notification/notification_list_response.dart';
import 'package:nui_core/code/nui_code_util.dart';

class NotificationListService extends DfService {
  @override
  startInit() {}

   Future<List<NotificationListInfoResponse>> getNotificationListApi(int page, String userId) async {
    DfFactory webFactory =
    await DataFactory.instance.getManipulator(RepoType.web);

    DfBaseRepository repository = webFactory.currentRepository;

    DfExtraReq extraReq = DfExtraReq();
    extraReq.dict = { 'page': page ,'userId': userId};

    DfQuery query = DfQuery(
        purpose: DfQueryPurpose.FETCH,
        queryFields: [],
        extraReq: extraReq,
        paging: null,
        orderBy: null,
        criteriaGroups: null,
        //tbc function
        entityJoin: DfQueryEntityJoin(
            entities: [GetNotificationListRequest()], joins: []));

    print("result try");
    DfResponse respList = await webFactory.fetch(query, repository: repository);
    BaseResponse result = respList.renderResultToEntity();
    logNUI("NotificationListService", "Base response : ${jsonEncode(result)}");
    if(result.status == true){
      final NotificationListResponse response = result.data;
      logNUI("SyncMasterService", "Notification Size : ${response.notificationList?.length}");
      return response.notificationList ?? [];
    }
    logNUI('test printing result', result.message);
    logNUI('test printing result', result.errorCode);
    logNUI('test printing result', result.status.toString());

  }
}