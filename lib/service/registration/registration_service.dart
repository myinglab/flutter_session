import 'package:ingdao/dataflow/control/dfquerycriteria.dart';
import 'package:ingdao/dataflow/control/dfresponse.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/service/dfservice.dart';
import 'package:ingdao/ingdao.dart';
import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/control/dfqueryentityjoin.dart';
import 'package:ingdao/dataflow/repository/dfbaserepository.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:nesternship_flut/datalayer/data_factory.dart';
import 'package:nesternship_flut/datalayer/entity/base_response.dart';
import 'package:nesternship_flut/datalayer/entity/registration/check_duplicate_email_request.dart';
import 'package:nesternship_flut/datalayer/entity/registration/registration_request.dart';
import 'package:nesternship_flut/datalayer/entity/registration/registration_response.dart';
import 'package:nui_core/code/nui_code_util.dart';
import 'package:nui_core/data/nui_data_util.dart';
import 'package:nui_core/model/interfaces.dart';

class RegistrationService extends DfService {
  @override
  startInit() {}

  Future<DataResult> sendRegisterApi(RegistrationRequest request) async {
    DfFactory webFactory =
        await DataFactory.instance.getManipulator(RepoType.web);

    DfBaseRepository repository = webFactory.currentRepository;

    DfExtraReq extraReq = DfExtraReq();
    extraReq.entity = request;

    DfQuery query = DfQuery(
        purpose: DfQueryPurpose.ADD,
        queryFields: [],
        extraReq: extraReq,
        paging: null,
        orderBy: null,
        criteriaGroups: null,
        //tbc function
        entityJoin: DfQueryEntityJoin(entities: [request], joins: []));

    List<DfResponse> respList =
    await webFactory.save([query], repository: repository);
    BaseResponse result = respList[0].renderResultToEntity();

    return DataResult(success: result.status == true, message: result.message);
  }

  Future<DataResult> sendCheckEmailApi(CheckDuplicateEmailRequest request, String email) async {
    DfFactory webFactory = await DataFactory.instance.getManipulator(RepoType.web);

    DfBaseRepository repository = webFactory.currentRepository;

    DfExtraReq extraReq = DfExtraReq();
    extraReq.dict = {
      "email": email
    };
    extraReq.entity = request;

    DfQuery query = DfQuery(
        purpose: DfQueryPurpose.FETCH,
        queryFields: [],
        extraReq: extraReq,
        paging: null,
        orderBy: null,
        criteriaGroups: null,
        //tbc function
        entityJoin: DfQueryEntityJoin(entities: [request], joins: []));

    print("result try");
    DfResponse respList = await webFactory.fetch(query, repository: repository);
    BaseResponse result = respList.renderResultToEntity();
    if (result == null) {
      print(respList.error.message);
      return DataResult(success: false, message: respList.error.message);
    } else {
      logNUI('test printing result', result.message);
      logNUI('test printing result', result.errorCode);
      logNUI('test printing result', result.status.toString());
      logNUI('test printing result', result.data.memberEmail);

      final memberEmail = result.data as RegistrationResponse;
      return DataResult(success: isNullOrEmpty(memberEmail.memberEmail), message: result.message);
    }

  }
}
