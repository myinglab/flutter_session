import 'package:ingdao/dataflow/control/dfresponse.dart';
import 'package:nui_web/nui_web.dart';

abstract class BaseService{

  static Future<bool> get hasInternetConnection async{
    return await NUIConnectionUtil.hasConnection(lookUp: false);
  }

  bool hasNoInternetConnection(DfResponse response){
    return response.error != null && response.error.code == 8102;
  }

}