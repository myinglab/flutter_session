import 'package:ingdao/dataflow/control/dfquerycriteria.dart';
import 'package:ingdao/dataflow/control/dfquerycriteriagroup.dart';
import 'package:ingdao/dataflow/control/dfquerycriteriagroups.dart';
import 'package:ingdao/dataflow/control/dfqueryorderby.dart';
import 'package:ingdao/dataflow/control/dfquerypaging.dart';
import 'package:ingdao/dataflow/control/dfresponse.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/invlog/logsetting.dart';
import 'package:ingdao/dataflow/service/dfservice.dart';
import 'package:ingdao/ingdao.dart';
import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/control/dfqueryentityjoin.dart';
import 'package:ingdao/dataflow/repository/dfbaserepository.dart';
import 'package:nesternship_flut/datalayer/data_factory.dart';
import 'package:nesternship_flut/datalayer/entity/base_response.dart';
import 'package:nesternship_flut/datalayer/entity/master/country_response.dart';
import 'package:nesternship_flut/datalayer/entity/master/sync_master_data_request.dart';
import 'package:nesternship_flut/datalayer/entity/master/sync_master_data_response.dart';
import 'package:nesternship_flut/datalayer/db/country_db.dart';
import 'package:nesternship_flut/datalayer/fetch_entity_result.dart';
import 'package:nesternship_flut/datalayer/field/master/country_field.dart';
import 'package:nui_core/code/nui_code_util.dart';

class SyncMasterDataService extends DfService {
  @override
  startInit() {}

  Future<void> sendSyncMasterDataApi(SyncMasterDataRequest request) async {
    DfFactory webFactory = await DataFactory.instance.getManipulator(RepoType.web);

    DfBaseRepository repository = webFactory.currentRepository;

    request.lastSyncDate = "";

    DfExtraReq extraReq = DfExtraReq();
    extraReq.entity = request;

    DfQuery query = DfQuery(
        purpose: DfQueryPurpose.ADD,
        queryFields: [],
        extraReq: extraReq,
        paging: null,
        orderBy: null,
        criteriaGroups: null,
        //tbc function
        entityJoin: DfQueryEntityJoin(entities: [request], joins: []));

    print("result try");
    List<DfResponse> respList =
        await webFactory.save([query], repository: repository);
    BaseResponse result = respList[0].renderResultToEntity();

    if (result.status == true) {
      final SyncMasterDataResponse response = result.data;
      logNUI("SyncMasterService", "Country size : ${response.listOfCountries.length}");
    }
    logNUI('test printing result', result.message);
    logNUI('test printing result', result.errorCode);
    logNUI('test printing result', result.status.toString());

    List<CountryDb> listOfCountries = <CountryDb>[];

    result.data.listOfCountries.forEach((country) {
      logNUI("SyncMasterDataService", "Country from web, id: ${country.countryId}, name: ${country.countryName}, code: ${country.countryCode}");
      CountryDb countryDb = CountryDb();
      countryDb.countryId = country.countryId;
      countryDb.countryCode = country.countryCode;
      countryDb.countryName = country.countryName;
      countryDb.isNestleCountry = country.isNestleCountry;
      listOfCountries.add(countryDb);
    });

    _saveCountries(listOfCountries);
  }

  Future<void> _saveCountries(List<CountryDb> entries) async {
    DfFactory dbFactory = await DataFactory.instance.getManipulator(RepoType.sqlitedb);
    DfBaseRepository repository = dbFactory.currentRepository;
    DfBaseEntity countryEntity = repository.model.entities[0];

    bool isNew = false;

    FetchEntityResult<CountryDb> countriesFromDb = await fetchCountries(isNestCountry: false);
    if (countriesFromDb.entities.length == 0) {
      isNew = true;
    } else {
      logNUI("test", "${countriesFromDb.entities.length}");
    }

    await dbFactory.truncate(countryEntity);
    if (isNew) {
      List<DfQuery> countrySaveQueries = <DfQuery>[];
      entries.forEach((entry) {
        logNUI("SyncMasterDataService", "Saving country id: ${entry.countryId}, code: ${entry.countryCode}, name: ${entry.countryName}");
        DfQuery query = DfQuery(
            purpose: DfQueryPurpose.ADD,
            queryFields: entry.toMap(),
            entityJoin: DfQueryEntityJoin(entities: [countryEntity]));
        countrySaveQueries.add(query);
      });
      await dbFactory.save(countrySaveQueries);
      logNUI("getting countries from database", "database empty");
      logNUI("getting countries from database", "adding data...");
    } else {
      List<DfQuery> countrySaveQueries = List<DfQuery>();
      entries.forEach((entry) {
        DfQuery query = DfQuery(
            purpose: DfQueryPurpose.UPDATE_SOME_ENTITY_ATTRS,
            queryFields: entry.toMap(),
            entityJoin: DfQueryEntityJoin(entities: [countryEntity]));
        countrySaveQueries.add(query);
      });

      await dbFactory.save(countrySaveQueries);
      logNUI("getting countries from database", "updating data");
    }
  }

  //fetch all countries from db
  Future<FetchEntityResult<CountryDb>> fetchCountries({bool isNestCountry}) async {
    FetchEntityResult<DfBaseEntity> dbResult = await _fetchCountries(isNesternshipCountry: isNestCountry);
    FetchEntityResult<CountryDb> finalResult = FetchEntityResult<CountryDb>();
    finalResult.error = dbResult.error;
    if (dbResult.entities != null) {
      finalResult.entities = <CountryDb>[];
      dbResult.entities.forEach((ent) {
        finalResult.entities.add(ent as CountryDb);
      });
    }
    return finalResult;
  }

  // fetch country based on country code provided
  Future<FetchEntityResult<CountryDb>> fetchCountryByCode({String countryCode}) async {
    FetchEntityResult<DfBaseEntity> dbResult = await  _fetchCountryByCountryCode(countryCode: countryCode);
    FetchEntityResult<CountryDb> finalResult = FetchEntityResult<CountryDb>();
    finalResult.error = dbResult.error;
    if (dbResult.entities != null) {
      finalResult.entities = <CountryDb>[];
      dbResult.entities.forEach((ent) {
        finalResult.entities.add(ent as CountryDb);
      });
    }
    return finalResult;
  }

  Future<FetchEntityResult<CountryDb>> fetchCountryByName({String countryName}) async {
    FetchEntityResult<DfBaseEntity> dbResult = await  _fetchCountryByCountryName(countryName: countryName);
    FetchEntityResult<CountryDb> finalResult = FetchEntityResult<CountryDb>();
    finalResult.error = dbResult.error;
    if (dbResult.entities != null) {
      finalResult.entities = <CountryDb>[];
      dbResult.entities.forEach((ent) {
        finalResult.entities.add(ent as CountryDb);
      });
    }
    return finalResult;
  }


  // dbresult function for fetching all countries from db
  Future<FetchEntityResult<DfBaseEntity>> _fetchCountries({bool isNesternshipCountry}) async {
    DfFactory factory =
    await DataFactory.instance.getManipulator(RepoType.sqlitedb);
    DfBaseRepository repository = factory.currentRepository;
    DfBaseEntity countryEntity = repository.model.entities[0];
    CountryField countryField = CountryField.createFieldBasedOnRepo(repository);

    List<DfBaseField> selFields = countryEntity.fields();

    List<DfQueryCriteria> criterias = [];
    if (isNesternshipCountry) {
      //database stores true as 1, false as 0
      int isNesternship = 1;
      criterias.add(DfQueryCriteria(field: countryField.fIsNestleCountry, oper: OperType.eq, values: [isNesternship] ));
    }

    DfQueryOrderBy orderBy = DfQueryOrderBy(
        field: countryField.fCountryName, orderByType: OrderByType.asc);
    // Paging paging = Paging(pageNumber: 0, perPageCount: 0);

    DfQuery query = DfQuery(
      purpose: DfQueryPurpose.FETCH,
      queryFields: selFields,
      entityJoin: DfQueryEntityJoin(
          entities: [countryEntity], joins: []), //JoinType.innerJoin
      criteriaGroups:  DfQueryCriteriaGroups(
          queryGroups: [DfQueryCriteriaGroup(
              criterias:criterias,
              andOrType: AndOrType.and)],
          queryStr: "") ,
      orderBy: [orderBy],
      // paging: paging
    );

    DfResponse resp = await factory.fetch(query);

    List<DfBaseEntity> finalList;

    if (resp.error == null) {
      finalList = resp.renderResultToEntityList();
    }
    return FetchEntityResult<DfBaseEntity>(entities: finalList);
  }


  //db result fetch country based on country code provided
  //need to check with neoh if its correct
  Future<FetchEntityResult<DfBaseEntity>> _fetchCountryByCountryCode({String countryCode}) async {
    DfFactory factory = await DataFactory.instance.getManipulator(RepoType.sqlitedb);
    DfBaseRepository repository = factory.currentRepository;
    DfBaseEntity countryEntity = repository.model.entities[0];
    CountryField countryField = CountryField.createFieldBasedOnRepo(repository);

    List<DfBaseField> selFields = countryEntity.fields();

    List<DfQueryCriteria> criterias = [];
    if (countryCode != null && countryCode.length > 0) {
      criterias.add(DfQueryCriteria(field: countryField.fCountryCode, oper: OperType.eq, values: [countryCode]));
    }

    DfQueryOrderBy orderBy = DfQueryOrderBy(
        field: countryField.fCountryName, orderByType: OrderByType.asc);
    DfQuery query = DfQuery(
      purpose: DfQueryPurpose.FETCH,
      queryFields: selFields,
      entityJoin: DfQueryEntityJoin(entities: [countryEntity],
          joins: []),
      criteriaGroups: DfQueryCriteriaGroups(
          queryGroups: [DfQueryCriteriaGroup(
              criterias:criterias,
              andOrType: AndOrType.and)],
          queryStr: "") ,
      //JoinType.innerJoin
      //queryStr can be something like "(g and g) or g"  - g represent a DfQueryCriteriaGroup object
      orderBy: [orderBy],
      // paging: paging
    );

    DfResponse resp = await factory.fetch(query);

    List<DfBaseEntity> finalList;

    if (resp.error == null) {
      finalList = resp.renderResultToEntityList();
    }
    return FetchEntityResult<DfBaseEntity>(entities: finalList);
  }

  Future<FetchEntityResult<DfBaseEntity>> _fetchCountryByCountryName({String countryName}) async {
    DfFactory factory = await DataFactory.instance.getManipulator(RepoType.sqlitedb);
    DfBaseRepository repository = factory.currentRepository;
    DfBaseEntity countryEntity = repository.model.entities[0];
    CountryField countryField = CountryField.createFieldBasedOnRepo(repository);

    List<DfBaseField> selFields = countryEntity.fields();

    List<DfQueryCriteria> criterias = [];
    if (countryName!= null && countryName.length > 0) {
      criterias.add(DfQueryCriteria(field: countryField.fCountryName, oper: OperType.eq, values: [countryName]));
    }

    DfQueryOrderBy orderBy = DfQueryOrderBy(
        field: countryField.fCountryName, orderByType: OrderByType.asc);
    DfQuery query = DfQuery(
      purpose: DfQueryPurpose.FETCH,
      queryFields: selFields,
      entityJoin: DfQueryEntityJoin(entities: [countryEntity],
          joins: []),
      criteriaGroups: DfQueryCriteriaGroups(
          queryGroups: [DfQueryCriteriaGroup(
              criterias:criterias,
              andOrType: AndOrType.and)],
          queryStr: "") ,
      //JoinType.innerJoin
      //queryStr can be something like "(g and g) or g"  - g represent a DfQueryCriteriaGroup object
      orderBy: [orderBy],
      // paging: paging
    );

    DfResponse resp = await factory.fetch(query);

    List<DfBaseEntity> finalList;

    if (resp.error == null) {
      finalList = resp.renderResultToEntityList();
    }
    return FetchEntityResult<DfBaseEntity>(entities: finalList);
  }
}
