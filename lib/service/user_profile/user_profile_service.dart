import 'package:ingdao/dataflow/control/dfquerycriteria.dart';
import 'package:ingdao/dataflow/control/dfresponse.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/service/dfservice.dart';
import 'package:ingdao/ingdao.dart';
import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/control/dfqueryentityjoin.dart';
import 'package:ingdao/dataflow/repository/dfbaserepository.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:nesternship_flut/datalayer/data_factory.dart';
import 'package:nesternship_flut/datalayer/entity/base_response.dart';
import 'package:nesternship_flut/service/base_service.dart';
import 'package:nesternship_flut/datalayer/entity/registration/registration_request.dart';
import 'package:nesternship_flut/datalayer/entity/user_profile/set_member_inactive_request.dart';
import 'package:nesternship_flut/datalayer/entity/user_profile/user_profile_request.dart';
import 'package:nesternship_flut/datalayer/entity/user_profile/user_profile_response.dart';
import 'package:nui_core/code/nui_code_util.dart';


class UserProfileService extends DfService with BaseService{
  @override
  startInit() {}

  Future<GetUserProfileResponse> getUserProfileApi(String userId) async {
    DfFactory webFactory = await DataFactory.instance.getManipulator(RepoType.web);

    DfBaseRepository repository = webFactory.currentRepository;

    DfExtraReq extraReq = DfExtraReq();
    extraReq.dict = {'userId': userId};

    DfQuery query = DfQuery(
        purpose: DfQueryPurpose.FETCH,
        queryFields: [],
        extraReq: extraReq,
        paging: null,
        orderBy: null,
        criteriaGroups: null,
        //tbc function
        entityJoin: DfQueryEntityJoin(entities: [GetUserProfileRequest()], joins: []));

    print("result try");

    try{
      DfResponse respList = await webFactory.fetch(query, repository: repository);
      logNUI("UserProfileService", "Response code: ${respList.error?.code}");
      BaseResponse result = respList.renderResultToEntity();
      final  GetUserProfileResponse response = result.data;
      logNUI('Get User Profile Response', response.userId.toString());

      logNUI('test printing result', result.message);
      logNUI('test printing result', result.errorCode);
      logNUI('test printing result', result.status.toString());

      //  logNUI('test printing result', result.data);
      return response;
    }catch(e){
      logNUI("UserProfileService", "Get user profile failed with error: $e");
      return null;
    }
  }

  void setMemberInactiveApi(String userId) async {
    DfFactory webFactory =
        await DataFactory.instance.getManipulator(RepoType.web);

    DfBaseRepository repository = webFactory.currentRepository;

    DfExtraReq extraReq = DfExtraReq();
    extraReq.dict = {'userId': userId};

    DfQuery query = DfQuery(
        purpose: DfQueryPurpose.UPDATE_WHOLE_ENTITY_ATTRS,
        queryFields: [],
        extraReq: extraReq,
        paging: null,
        orderBy: null,
        criteriaGroups: null,
        //tbc function
        entityJoin: DfQueryEntityJoin(
            entities: [SetMemberInactiveRequest()], joins: []));

    print("result try");
    List<DfResponse> respList =
        await webFactory.save([query], repository: repository);
    BaseResponse result = respList[0].renderResultToEntity();
    logNUI('test printing result', result.message);
    logNUI('test printing result', result.errorCode);
    logNUI('test printing result', result.status.toString());
    // logNUI('test printing result', result.data.memberEmail);
  }
}
