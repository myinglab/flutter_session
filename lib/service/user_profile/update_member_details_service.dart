import 'package:ingdao/dataflow/control/dfquerycriteria.dart';
import 'package:ingdao/dataflow/control/dfresponse.dart';
import 'package:ingdao/dataflow/field/dfbasefield.dart';
import 'package:ingdao/dataflow/service/dfservice.dart';
import 'package:ingdao/ingdao.dart';
import 'package:ingdao/dataflow/control/dfquery.dart';
import 'package:ingdao/dataflow/control/dfqueryentityjoin.dart';
import 'package:ingdao/dataflow/repository/dfbaserepository.dart';
import 'package:ingdao/dataflow/entity/dfbaseentity.dart';
import 'package:nesternship_flut/datalayer/data_factory.dart';
import 'package:nesternship_flut/datalayer/entity/base_response.dart';
import 'package:nesternship_flut/datalayer/entity/user_profile/update_member_details_request.dart';
import 'package:nesternship_flut/data/model/user_profile.dart';
import 'package:nui_core/code/nui_code_util.dart';
import 'package:nui_core/model/interfaces.dart';

class UpdateMemberDetailsService extends DfService {
  @override
  startInit() {}

  Future<DataResult> sendUpdateMemberDetailApi(UpdateMemberDetailsRequest request, UserProfile userProfile) async {
    DfFactory webFactory = await DataFactory.instance.getManipulator(RepoType.web);
    DfBaseRepository repository = webFactory.currentRepository;

    request.id = userProfile.id;
    request.memberEmail = userProfile.memberEmail;
    request.memberName = userProfile.memberName;
    request.userId = userProfile.userId;
    request.avatar = userProfile.avatar;
    request.dateOfBirth = userProfile.dateOfBirth;
    request.gender = userProfile.gender;
    request.originCountry = userProfile.originCountry;
    request.nestleCountry = userProfile.nestleCountry;

    DfExtraReq extraReq = DfExtraReq();
    extraReq.entity = request;

    DfQuery query = DfQuery(
        purpose: DfQueryPurpose.UPDATE_WHOLE_ENTITY_ATTRS,
        queryFields: [],
        extraReq: extraReq,
        paging: null,
        orderBy: null,
        criteriaGroups: null,
        //tbc function
        entityJoin: DfQueryEntityJoin(entities: [request], joins: []));

    print("result try");
    List<DfResponse> respList = await webFactory.save([query], repository: repository);
    BaseResponse result = respList[0].renderResultToEntity();

    logNUI('test printing result', result.message);
    logNUI('test printing result', result.errorCode);
    logNUI('test printing result', result.status.toString());
    logNUI('test printing result', result.data.memberEmail);

    return DataResult(success: respList[0].error == null && result.status == true, message: result.message);
  }
}