import 'package:nesternship_flut/datalayer/db/country_db.dart';
import 'package:nesternship_flut/datalayer/entity/master/sync_master_data_request.dart';
import 'package:nesternship_flut/datalayer/fetch_entity_result.dart';
import 'package:nesternship_flut/service/master/sync_master_data_service.dart';

class MasterBloc {
  Future<SyncMasterDataService> getSyncMasterDataService() async {
    SyncMasterDataService masterDataService = SyncMasterDataService();
    await masterDataService.startInit();
    return masterDataService;
  }

  Future<void> syncMasterData() async {
    SyncMasterDataRequest masterDataRequest = SyncMasterDataRequest();
    final service = await getSyncMasterDataService();
    service.sendSyncMasterDataApi(masterDataRequest);
  }

  // fetch all countries from db and display for the CountrySelectionScreen()
  Future<FetchEntityResult<CountryDb>> fetchCountries(bool isNesternshipCountry) async {
    final service = await getSyncMasterDataService();
    return service.fetchCountries(isNestCountry: isNesternshipCountry);
  }

  // fetch countries based on country code provided for User Profile Screen
  Future<FetchEntityResult<CountryDb>> fetchCountryName(String countryCode) async {
    final service = await getSyncMasterDataService();
    final userOriginCountry = await service.fetchCountryByCode(countryCode: countryCode);
    return userOriginCountry;
  }

  Future<FetchEntityResult<CountryDb>> fetchCountryCode(String countryName) async {
    final service = await getSyncMasterDataService();
    final userOriginCountry = await service.fetchCountryByName(countryName: countryName);
    return userOriginCountry;
  }

}
