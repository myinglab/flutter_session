import 'package:nesternship_flut/datalayer/entity/banner/banner_image_response.dart';
import 'package:nesternship_flut/datalayer/entity/banner/banner_image_list_request.dart';
import 'package:nesternship_flut/service/banner/banner_image_list_service.dart';

class BannerBloc{

  Future<BannerImageListService> getBannerListService() async {
    BannerImageListService bannerImageListService = BannerImageListService();
    await bannerImageListService.startInit();
    return bannerImageListService;
  }

  Future<List<BannerImageResponse>> getBannerImagesList(String userId) async {
    final service = await getBannerListService();
    return await service.getBannerImagesApi(userId);
  }


}
