import 'package:flutter/cupertino.dart';
import 'package:nesternship_flut/bloc/user_bloc.dart';
import 'package:nesternship_flut/data/model/user_profile.dart';
import 'package:nesternship_flut/datalayer/db/country_db.dart';
import 'package:nesternship_flut/datalayer/entity/registration/registration_detail.dart';
import 'package:nesternship_flut/datalayer/entity/user_profile/user_profile_request.dart';
import 'package:nesternship_flut/datalayer/entity/user_profile/user_profile_response.dart';
import 'package:nesternship_flut/service/user_profile/user_profile_service.dart';
import 'package:nesternship_flut/datalayer/entity/user_profile/update_member_details_request.dart';
import 'package:nesternship_flut/datalayer/entity/user_profile/update_member_details_bean.dart';
import 'package:nesternship_flut/service/user_profile/update_member_details_service.dart';
import 'package:nesternship_flut/bloc/master_bloc.dart';
import 'package:nesternship_flut/ui/custom/confirm_cancel_dialog.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_auth/domain/nui_user_info.dart';
import 'package:nui_auth/nui_auth.dart';
import 'package:nui_core/code/nui_code_util.dart';
import 'package:nui_core/datetime/nui_dt_util.dart';
import 'package:nui_core/model/interfaces.dart';
import 'package:nui_core/nui_core.dart';
import 'package:nui_core/string/nui_string_util.dart';
import 'package:permission_handler/permission_handler.dart';

class ProfileBloc {
  /*UserProfile getDummyUserProfile() {
    return UserProfile.getDummy();
  }*/
  MasterBloc masterBloc = MasterBloc();
  static CountryDb _nesternshipCountry;

  UserProfile getUserProfileFromResponse(GetUserProfileResponse resp){
    final UserProfile userProfile = UserProfile(
      id: resp.id.toString(),
      memberEmail: resp.memberEmail,
      memberName: resp.memberName,
      userId: resp.userId.toString(),
      gender: resp.gender,
      dateOfBirth: resp.dateOfBirth,
      nestleCountry: resp.nestleCountry,
      originCountry: resp.originCountry,
      avatar: resp.avatar
    );
    return userProfile;
  }

  Widget getUserAvatar(UserProfile profile){
    switch(profile.gender?.toLowerCase()){
      case "m":
      case "male": return Image.asset("assets/images/male.png");
      case "f":
      case "female": return Image.asset("assets/images/female.png");
      default: return Image.asset("assets/images/default_avatar.jpg");
    }
  }

  Future<CountryDb> getUserNesternshipCountry() async{
    if(_nesternshipCountry != null) return _nesternshipCountry;

    final userId = NUIAuth.get().getUserId();
    final service = await getUserProfileService();
    GetUserProfileResponse profileResponse = await service.getUserProfileApi(userId);

    logNUI("ProfileBloc", "Got profiles response for country code");

    final nestleCountryCode = await masterBloc.fetchCountryName(profileResponse.nestleCountry);
    for(var element in nestleCountryCode.entities){
      logNUI("ProfileBloc", "Matched country : ${element.countryName}, id : ${element.countryId}");
      _nesternshipCountry = element;
      return element;
    }

    //Something wrong here, shouldnt be null
    logNUI("ProfileBloc", "Something went wrong in getting user's nesternship country");
    return null;
  }

  Future<UserProfile> getUserProfile(String userId) async {
    String originCountry;
    String nestleCountry;
    String dateOfBirth;
    String gender;
    final service = await getUserProfileService();
    GetUserProfileResponse profileResponse = await service.getUserProfileApi(userId);

    switch(profileResponse.gender){
      case "F":
        gender = "Female";
        break;
      case "M":
        gender = "Male";
        break;
      default:
        gender = "";  // assign null if user have skip step 3
        break;
    }

    if(profileResponse.dateOfBirth != null){
      dateOfBirth= profileResponse.dateOfBirth;
    }else{
      dateOfBirth = "";  // assign null if user have skip step 4
    }

    final nestleCountryCode = await masterBloc.fetchCountryName(profileResponse.nestleCountry);
    nestleCountryCode.entities.forEach((element) {
      if (element.countryCode == profileResponse.nestleCountry) {
        nestleCountry = element.countryName;
      }
    });

    if(profileResponse.originCountry != null){
      final originCountryCode = await masterBloc.fetchCountryName(profileResponse.originCountry);
      originCountryCode.entities.forEach((element){
        if(element.countryCode == profileResponse.originCountry){
              originCountry = element.countryName;
        }
      });
    }else{
      originCountry = "";  // assign null if user have skip step 5
    }


    try {
      final infoResult = await NUIAuth.get().updateUserInfo(
          userInfo: NUIUserInfo(
              gender: profileResponse.gender,
              dateOfBirth: profileResponse.dateOfBirth,
              others: {
                "originCountry": originCountry,
                "internCountry": nestleCountry
              }
          ));
    }catch(e, s){
      logNUI("ProfileBloc", "Failed to update user info to IAUth with error : $e, stack trace: $s");
    }

    final UserProfile userProfile = UserProfile(
      id:  profileResponse.id.toString(),
      memberEmail: profileResponse.memberEmail,
      memberName: profileResponse.memberName,
      userId: profileResponse.userId.toString(),
      gender: gender,
      dateOfBirth: dateOfBirth,
      nestleCountry: nestleCountry,
      originCountry: originCountry,
      avatar: profileResponse.avatar,
    );
    return userProfile;
  }

  Future<UserProfileService> getUserProfileService() async {
    UserProfileService userProfileService = UserProfileService();
    await userProfileService.startInit();
    return userProfileService;
  }

  Future<UpdateMemberDetailsService> updateMemberDetailsService() async {
    UpdateMemberDetailsService updateMemberService = UpdateMemberDetailsService();
    await updateMemberService.startInit();
    return updateMemberService;
  }

  Future<DataResult> registerUserProfileDetails(String userId, RegistrationDetail registrationDetail) async{
    final updateBean = UpdateMemberDetailsBean(
      memberEmail: registrationDetail.email,
      userId: userId,
      memberName: registrationDetail.name,
      dateOfBirth: registrationDetail.dateOfBirth,
      gender: registrationDetail.gender,
      nestleCountry: registrationDetail.internCountry,
      originCountry: registrationDetail.originCountry
    );
    return updateMemberDetails(updateBean);
  }

  void showUpdateProfileConfirmDialog(BuildContext context, UpdateMemberDetailsBean profileBean, {Function doneCallback}) async{
    ConfirmCancelDialogController controller;
    controller = ConfirmCancelDialogController(
        onLoadingInitiated: () async {
          final result = await updateMemberDetails(profileBean);
          controller.stopLoading();
          if(result.success == true){
            controller.updateTitle(text: "Your profile has been updated!", confirmButton: "All done", confirmDismiss: true);
          }
          else{
            controller.updateTitle(text: "Seems like something went wrong ... Try again?", confirmButton: "Try Again", confirmDismiss: false);
          }
        },
        onConfirmClickListener: (){
          controller.initiateLoading();
        },
        onCancelClickListener: (){
          controller.dismiss();
        },
        onConfirmDismissListener: (){
          if(doneCallback != null) doneCallback();
        }
    );

    showConfirmCancelDialog(
        context,
        textSpan: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
              style: getFont(context, size: 16.0, color: textDarkGray),
              children: [
                TextSpan(text: "Would you like to update your profile?"),
              ]
          ),
        ),
        confirmButton: "Update",
        cancelButton: "Cancel",
        loadingTextSpan: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
              style: getFont(context, size: 16.0, color: textDarkGray),
              children: [
                TextSpan(text: "Hold on, this might take a while ..."),
              ]
          ),
        ),
        handler: controller
    );
  }

  Future<DataResult> updateMemberDetails(UpdateMemberDetailsBean profileDetail) async {
    String gender;
    String originCountry;
    String nestleCountry;
    final userId = NUIAuth.get().getUserId();

    logNUI("ProfileBloc", "Profile detail origin country: ${profileDetail.originCountry}, nestern country: ${profileDetail.nestleCountry}");

    UpdateMemberDetailsRequest updateDetailRequest = UpdateMemberDetailsRequest();

    final update_service = await updateMemberDetailsService();

    switch(profileDetail.gender){
      case "Female":
      case "F":
        gender = "F";
        break;
      case "Male":
      case "M":
        gender = "M";
        break;
      default:
        gender = profileDetail.gender;  // assign null if user have skip step 3
        break;
    }

    final originCountryName = await masterBloc.fetchCountryCode(profileDetail.originCountry);
    originCountryName.entities.forEach((element){
      if(match(element.countryName, profileDetail.originCountry) || match(element.countryCode, profileDetail.originCountry)){
        originCountry = element.countryCode;
      }
    });
    originCountry = originCountry ?? profileDetail.originCountry ?? "";

    final nestleCountryName = await masterBloc.fetchCountryCode(profileDetail.nestleCountry);
    nestleCountryName.entities.forEach((element) {
      if (element.countryName == profileDetail.nestleCountry || match(element.countryCode, profileDetail.nestleCountry)) {
        nestleCountry = element.countryCode;
      }
    });
    nestleCountry = nestleCountry ?? profileDetail.nestleCountry ?? "";

    logNUI("ProfileBloc", "To update origin country: ${profileDetail.originCountry}, nestern country: ${profileDetail.nestleCountry}");

    final UserProfile userProfile = UserProfile(
      id:  profileDetail.id,
      memberEmail:  profileDetail.memberEmail,
      memberName:  profileDetail.memberName,
      userId: userId,
      gender: gender,
      dateOfBirth:  profileDetail.dateOfBirth,
      nestleCountry: nestleCountry,
      originCountry: originCountry,
      avatar: profileDetail.avatar ?? "",
    );

    final result = await update_service.sendUpdateMemberDetailApi(updateDetailRequest, userProfile);
    return result;
  }

  Future<UserProfileService> setMemberInactiveService() async {
    UserProfileService userProfileService = UserProfileService();
    await userProfileService.startInit();
    return userProfileService;
  }

  Future<void> setMemberInactive(String userId) async {
    final service = await setMemberInactiveService();
    service.setMemberInactiveApi(userId);
  }
}
