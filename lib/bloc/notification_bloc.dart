import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:nesternship_flut/data/model/notification.dart';
import 'package:nesternship_flut/datalayer/entity/notification/notification_count_request.dart';
import 'package:nesternship_flut/bloc/user_bloc.dart';
import 'package:nesternship_flut/datalayer/entity/notification/notification_list_information_response.dart';
import 'package:nesternship_flut/datalayer/entity/notification/set_notification_read_request.dart';
import 'package:nesternship_flut/service/notification/notification_service.dart';
import 'package:nesternship_flut/datalayer/entity/notification/notification_create_request.dart';
import 'package:nesternship_flut/service/notification/notification_list_service.dart';
import 'package:nesternship_flut/ui/custom/confirm_cancel_dialog.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_core/nui_core.dart';

class NotificationBloc {
  List<NotificationListInfoResponse> listResponse;
  NUIBlocData<int> notiCountBloc = NUIBlocData();
  UserBloc user = UserBloc();

  void getAndUpdateNotificationCount() async{
    final result = await getNotificationCount(user.getUserId());
    notiCountBloc.notify(result);
  }

  NUIListViewBloc<UserNotification, int> userNotifications(String userId) =>
      NUIListViewBloc<UserNotification, int>(callbackMethod: (List<UserNotification> current, bool isFirst, bool append, int cursor) async {
        final cursorToUse = isFirst == true ? 1 : cursor ?? 1;
        final notis = await getNotificationList(cursorToUse, userId);
        List<UserNotification> notifications = notis.map((e){
          return UserNotification(
              notificationId: e.notificationId,
              userId: e.userId,
              notificationContentId: e.notificationContentId,
              title: e.notificationTitle,
              description: e.notificationDesc,
              dateTime: e.notificationDateTime,
              bannerUrl: e.bannerUrl,
              read: e.isRead
          );
        }).toList();
        return ListResult(
          data: notifications,
          success: true,
          message: "Get notifications success",
          cursor: cursorToUse + 1);
      });

  Future<NotificationService> getNotificationService() async {
    NotificationService notificationService = NotificationService();
    await notificationService.startInit();
    return notificationService;
  }

  Future<int> getNotificationCount(String userId) async {
    final service = await getNotificationService();
    return await service.getNotificationCountApi(userId);
  }

  Future<bool> setNotificationRead(int notifId) async {
    SetNotificationReadRequest setNotificationReadRequest = SetNotificationReadRequest();
    final service = await getNotificationService();
    final isRead = await service.setNotificationReadApi(setNotificationReadRequest, notifId);
    getAndUpdateNotificationCount();
    return isRead;
  }

  Future<void> createNotification() async {
    CreateNotificationRequest request = CreateNotificationRequest();
    final service = await getNotificationService();
    service.createNotificationApi(request);
  }

  Future<NotificationListService> getNotificationListService() async {
    NotificationListService notificationListService = NotificationListService();
    await notificationListService.startInit();
    return notificationListService;
  }

  Future<List<NotificationListInfoResponse>> getNotificationList(int page, String userId) async {
    final service = await getNotificationListService();
    return await service.getNotificationListApi(page, userId);
  }

  Future<void> deleteNotification(String notificationId) async {
    final service = await getNotificationService();
    await service.deleteNotificationApi(notificationId);
    getAndUpdateNotificationCount();
  }

  Future<bool> clearNotificationList(String userId) async {
    final service = await getNotificationService();
    final result = await service.clearNotificationListApi(userId);
    getAndUpdateNotificationCount();
    return result;
  }

  void showClearNotificationConfirmDialog(BuildContext context, String userId, {Function doneCallback}) async{
    ConfirmCancelDialogController controller;
    controller = ConfirmCancelDialogController(
        onLoadingInitiated: () async {
          final result = await clearNotificationList(userId);
          controller.stopLoading();
          if(result == true){
            controller.updateTitle(text: "Your notifications have been cleared!", confirmButton: "All done", confirmDismiss: true);
          }
          else{
            controller.updateTitle(text: "Seems like something went wrong ... Try again?", confirmButton: "Try Again", confirmDismiss: false);
          }
        },
        onConfirmClickListener: (){
          controller.initiateLoading();
        },
        onCancelClickListener: (){
          controller.dismiss();
        },
        onConfirmDismissListener: (){
          if(doneCallback != null) doneCallback();
        }
    );

    showConfirmCancelDialog(
        context,
        textSpan: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
              style: getFont(context, size: 16.0, color: textDarkGray),
              children: [
                TextSpan(text: "Would you like to clear your notifications?"),
              ]
          ),
        ),
        confirmButton: "Clear",
        cancelButton: "Cancel",
        loadingTextSpan: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
              style: getFont(context, size: 16.0, color: textDarkGray),
              children: [
                TextSpan(text: "Hold on, this might take a while ..."),
              ]
          ),
        ),
        handler: controller
    );
  }
}
