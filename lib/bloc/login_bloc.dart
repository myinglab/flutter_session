import 'package:flutter/cupertino.dart';
import 'package:nesternship_flut/bloc/profile_bloc.dart';
import 'package:nui_auth/nui_auth.dart';
import 'package:nui_core/code/nui_code_util.dart';
import 'package:nui_core/model/interfaces.dart';

class LoginBloc{

  Future<DataResult> login({@required String email, @required String password}) async{
    final result = await NUIAuth.get().login(username: email, password: password);

    logNUI("LoginBloc", "Login success : ${result.success}, message : ${result.message}");
    if(result.success == true){
      final profileBloc = ProfileBloc();
      final userProfile = profileBloc.getUserProfile(NUIAuth.get().getUserId());
    }

    return DataResult(success: result.success, message: result.message);
  }

}