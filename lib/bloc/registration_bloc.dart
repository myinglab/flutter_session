import 'dart:async';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nesternship_flut/bloc/master_bloc.dart';
import 'package:nesternship_flut/bloc/profile_bloc.dart';
import 'package:nesternship_flut/data/model/country_model.dart';
import 'package:nesternship_flut/data/model/search_criteria.dart';
import 'package:nesternship_flut/datalayer/db/country_db.dart';
import 'package:nesternship_flut/datalayer/entity/registration/check_duplicate_email_request.dart';
import 'package:nesternship_flut/datalayer/entity/registration/registration_request.dart';
import 'package:nesternship_flut/datalayer/entity/registration/registration_detail.dart';
import 'package:nesternship_flut/datalayer/fetch_entity_result.dart';
import 'package:nesternship_flut/service/registration/registration_service.dart';
import 'package:nesternship_flut/ui/form/country_selection_form.dart';
import 'package:nui_auth/nui_auth.dart';
import 'package:nui_core/nui_core.dart';

class RegistrationBloc {
  MasterBloc masterBloc = MasterBloc();

  List<String> getCountries() {
    return ["North America", "Europe", "South America", "Africa"];
  }

  Future<RegistrationService> getRegistrationService() async {
    RegistrationService registrationService = RegistrationService();
    await registrationService.startInit();
    return registrationService;
  }

  Future<DataResult> registerAccount(String userId, RegistrationDetail registrationDetail, {String avatar}) async {
    String gender;
    String originCountry;
    String nestleCountry;

    switch(registrationDetail.gender){
      case "Female":
        gender = "F";
        break;
      case "Male":
        gender = "M";
        break;
      default:
        gender = registrationDetail.gender;  // assign null if user have skip step 3
        break;
    }

    final originCountryName = await masterBloc.fetchCountryCode(registrationDetail.originCountry);
    originCountryName.entities.forEach((element){
      if(element.countryName == registrationDetail.originCountry){
        originCountry = element.countryCode;
      }
    });

    final nestleCountryName = await masterBloc.fetchCountryCode(registrationDetail.internCountry);
    nestleCountryName.entities.forEach((element) {
      if (element.countryName == registrationDetail.internCountry) {
        nestleCountry = element.countryCode;
      }
    });


    final RegistrationRequest registrationRequest = RegistrationRequest(
      memberEmail:  registrationDetail.email,
      memberName:  registrationDetail.name,
      userId:  userId,
      gender: gender ?? "",
      dateOfBirth:  registrationDetail.dateOfBirth ?? "",
      nestleCountry: nestleCountry,
      originCountry: originCountry ?? "",
      avatar: avatar ?? "",
    );

    final service = await getRegistrationService();
    final result = await service.sendRegisterApi(registrationRequest);
    return result;
  }

  Future<DataResult> checkEmail(String email) async {
    CheckDuplicateEmailRequest checkDuplicateEmailRequest = CheckDuplicateEmailRequest();

    final service = await getRegistrationService();

    return await service.sendCheckEmailApi(checkDuplicateEmailRequest, email);
  }

  var registrationCountryBloc = (SearchCriteria criteria, bool isNesternshipCountry) =>
      NUIListViewBloc<Country, Country>(callbackMethod: (List<Country> current, bool isFirst, bool append, Country cursor) async {
        MasterBloc masterBloc = MasterBloc();
        List<Country> listOfCountries = <Country>[];

        FetchEntityResult<CountryDb> countriesFromDb = await masterBloc.fetchCountries(isNesternshipCountry);

        countriesFromDb.entities.forEach((element) {
          final countryName = utf8.decode(element.countryName.runes.toList());

          listOfCountries.add(Country(
              countryId: element.countryId,
              countryName: countryName,
              countryCode: element.countryCode,
              isNestleCountry: element.isNestleCountry)
          );
        });

        List<Country> filteredData = <Country>[];

        logNUI("RegistrationBloc", "Search criteria keyword : ${criteria.keyword}");

        List<Country> startWithList = [];

        if (criteria.keyword != null && criteria.keyword != "") {
          listOfCountries.forEach((element) {
            if(element.countryName.toLowerCase().startsWith(criteria.keyword.toLowerCase())){
              logNUI("RegistrationBloc", "Starting with keyword : ${criteria.keyword}");
              startWithList.add(element);
            }
            else if (element.countryName.toLowerCase().contains(criteria.keyword.toLowerCase())) {
              filteredData.add(element);
            }
          });
        }
        else{
          filteredData.addAll(listOfCountries);
        }

        if(!isNullOrEmpty(startWithList)){
          filteredData.insertAll(0, startWithList);
        }

        return ListResult(
            data: filteredData,
            success: true,
            message: "Get events success",
            cursor: null,
            reachedEnd: true);
      });

  Future<bool> registerUserDetail(BuildContext context, RegistrationDetail regDetail) async {
    try {
      ProfileBloc profileBloc = ProfileBloc();
      final signUpResult = await NUIAuth.get().signup(
          username: regDetail.email,
          password: regDetail.password,
          email: regDetail.email);

      logNUI("RegistrationBloc", "Finish NUIAuth signup with success : ${signUpResult.success}, message: ${signUpResult.message}");
      if (signUpResult.success != true) return false;

      bool signUpSuccess = false;
      logNUI("RegistrationBloc", "Signing in to IAuth");
      final logIn = await NUIAuth.get().login(username: regDetail.email, password: regDetail.password);
      logNUI("RegistrationBloc", "Finish NUIAuth login with success : ${logIn.success}, message: ${logIn.message}");
      if (logIn.success) {
        final userId = NUIAuth.get().getUserId();
        logNUI("RegistrationBloc", "Updating profile detail with user id : $userId");
        final registerResult = await registerAccount(userId, regDetail);
        signUpSuccess = registerResult.success;
      }

      return signUpSuccess;
    }catch(e, s){
      logNUI("RegistrationBloc", "Failed to register user detail with error : $e, stack: $s");
    }
  }
}
