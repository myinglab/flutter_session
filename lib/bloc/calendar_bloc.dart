import 'package:device_calendar/device_calendar.dart';
import 'package:flutter/cupertino.dart';
import 'package:nui_core/nui_core.dart';
import 'package:permission_handler/permission_handler.dart';

class CalendarBloc{
  DeviceCalendarPlugin _deviceCalendarPlugin = DeviceCalendarPlugin();

  void testCalendarFeature(BuildContext context) async{
    await requestCalendarPermission(context);

    final cal = await getFirstCalendar(context);
    if(cal == null){
      logNUI("CalendarBloc", "No calendar was created in the device, so we cant do anything, byee");
      return;
    }

    logNUI("CalendarBloc", "Cal id : ${cal.id}, name : ${cal.name}");
    final eventId = await saveCalendarEvent(context, calId: cal.id, title: "Testing Calendar Event on Neslay", description: "Nesternship is our company's first ever flutter app", startDate: DateTime(2020, 12, 20, 13, 0), endDate: DateTime(2020, 12, 26, 13, 0));
    if(!isNullOrEmpty(eventId)){
      logNUI("Calendar", "Created Neslay calendar event with eventId : $eventId");
    }
    else{
      logNUI("CalendarBloc", "Failed to create Neslay calendar event");
    }
  }
  
  Future<bool> requestCalendarPermission(BuildContext context) async{
    final hasPermission = await NUIPermissionUtil.hasPermission(Permission.calendar);
    if(!hasPermission) {
      await NUIPermissionUtil.requestPermission(Permission.calendar);
    }
    final hasNextPermission = await NUIPermissionUtil.hasPermission(Permission.calendar);
    return hasNextPermission;
  }

  Future<Calendar> getFirstCalendar(BuildContext context) async{
    await requestCalendarPermission(context);
    final calendars = await getCalendars(context);
    return calendars.getOrNull(0);
  }

  Future<String> saveCalendar(BuildContext context, {@required String calName, @required Color calendarColor, String localAccountName}) async{
    await requestCalendarPermission(context);
    final result = await _deviceCalendarPlugin.createCalendar(calName, calendarColor: calendarColor, localAccountName: localAccountName);
    return result.data; //Returns the calendarID
  }

  Future<List<Calendar>> getCalendars(BuildContext context) async{
    await requestCalendarPermission(context);
    final calendars = await _deviceCalendarPlugin.retrieveCalendars();
    return calendars.data?.toList();
  }

  Future<List<Event>> getCalendarEvents(BuildContext context, {@required String calId, DateTime startDate, DateTime endDate}) async{
    await requestCalendarPermission(context);
    final events = await _deviceCalendarPlugin.retrieveEvents(calId, RetrieveEventsParams(startDate: startDate, endDate: endDate));
    return events.data?.toList();
  }

  Future<String> saveCalendarEvent(BuildContext context, {String calId, @required String title, @required String description, @required DateTime startDate, DateTime endDate}) async{
    await requestCalendarPermission(context);
    final finalCalId = calId ?? (await getFirstCalendar(context)).id;
    logNUI("CalendarBloc", "Creating calendar event with calId : $finalCalId, title : $title, description : $description, startDate : $startDate, endDate : $endDate");
    final event = Event(calId, title: title, start: startDate, end: endDate, description: description, allDay: endDate == null);
    final result = await _deviceCalendarPlugin.createOrUpdateEvent(event);
    logNUI("CalendarBLoc", "Create event result : ${result.isSuccess}, messsage : ${result.errorMessages}");
    return result.data; //Returns the eventID
  }

  Future<bool> removeCalendarEvent(BuildContext context, {@required String calId, @required String eventId}) async{
    await requestCalendarPermission(context);
    final result = await _deviceCalendarPlugin.deleteEvent(calId, eventId);
    return result.isSuccess;
  }

}