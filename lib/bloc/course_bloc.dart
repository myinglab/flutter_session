import 'dart:collection';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nesternship_flut/bloc/category_bloc.dart';
import 'package:nesternship_flut/bloc/master_bloc.dart';
import 'package:nesternship_flut/bloc/profile_bloc.dart';
import 'package:nesternship_flut/bloc/user_bloc.dart';
import 'package:nesternship_flut/data/model/search_course.dart';
import 'package:nesternship_flut/data/model/search_criteria.dart';
import 'package:nesternship_flut/data/model/user_profile.dart';
import 'package:nesternship_flut/datalayer/db/country_db.dart';
import 'package:nesternship_flut/datalayer/entity/category/category_response.dart';
import 'package:nesternship_flut/datalayer/entity/course/course_get_status_response.dart';
import 'package:nesternship_flut/datalayer/entity/course/course_list_response.dart';
import 'package:nesternship_flut/datalayer/entity/course/course_response.dart';
import 'package:nesternship_flut/datalayer/entity/course/course_update_status_request.dart';
import 'package:nesternship_flut/datalayer/fetch_entity_result.dart';
import 'package:nesternship_flut/service/course/course_list_service.dart';
import 'package:nesternship_flut/ui/screen/pdf_viewer_screen.dart';
import 'package:nesternship_flut/ui/screen/video_viewer_screen.dart';
import 'package:nesternship_flut/ui/screen/webview_screen.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_core/nui_core.dart';
import 'package:url_launcher/url_launcher.dart';

class CourseBloc {
  final UserBloc _userBloc = UserBloc();
  final ProfileBloc _profileBloc = ProfileBloc();
  final MasterBloc _masterBloc = MasterBloc();
  final CategoryBloc _categoryBloc = CategoryBloc();

  NUIListViewBloc<CourseResponse, int> coursesByCategoryBloc(int categoryId) =>
      NUIListViewBloc<CourseResponse, int>(callbackMethod:
          (List<CourseResponse> current, bool isFirst, bool append,
              int cursor) async {
        final pageToUse = isFirst == true ? 1 : cursor ?? 1;
        final courses = await getCoursesByCategory(categoryId, page: pageToUse);
        return ListResult(
            data: courses.data,
            success: courses.success,
            message: courses.message,
            cursor: courses.success ? pageToUse + 1 : pageToUse,
            reachedEnd: false);
      });


  NUIListViewBloc<SearchCourse, int> coursesByCountryBloc() => NUIListViewBloc<SearchCourse, int>(callbackMethod: (List<SearchCourse> current, bool isFirst, bool append, int cursor) async {
    final pageToUse = isFirst == true  ? 1 : cursor ?? 1;
    final courses = await getCoursesByCountry(page: pageToUse);
    return ListResult(data: courses.data, success: courses.success, message: courses.message, cursor: courses.success ? pageToUse + 1 : pageToUse, reachedEnd: false);
  });

  NUIListViewBloc<SearchCourse, int> myCourses() => NUIListViewBloc<SearchCourse, int>(callbackMethod: (List<SearchCourse> current, bool isFirst, bool append, int cursor) async {
    final courses = await getMyCourses();
    return ListResult(data: courses.data, success: courses.success, message: courses.message, cursor: 1, reachedEnd: true);
  });


  NUIListViewBloc<SearchCourse, int> coursesByKeyword(int categoryId, SearchCriteria criteria) => NUIListViewBloc<SearchCourse, int>(callbackMethod: (List<SearchCourse> current, bool isFirst, bool append, int cursor) async {
    final pageToUse = isFirst == true ? 1 : cursor ?? 1;
    final courses = await searchCourses(categoryId, page: pageToUse, keyword: criteria.keyword);
    return ListResult(data: courses.data, success: courses.success, message: courses.message, cursor: courses.success ? pageToUse + 1 : pageToUse, reachedEnd: false);
  });

  NUIListViewBloc<CategoryResponse, int> courseCategories() =>
      NUIListViewBloc<CategoryResponse, int>(callbackMethod:
          (List<CategoryResponse> current, bool isFirst, bool append,
              int cursor) async {
        final categories = await getCourseCategories();
        return ListResult(
            data: categories.data,
            success: categories.success,
            message: categories.message,
            cursor: null,
            reachedEnd: false);
      });

  Future<CourseListService> getCourseListService() async {
    CourseListService courseListService = CourseListService();
    await courseListService.startInit();
    return courseListService;
  }

  Future<DataResult<List<CourseResponse>>> getCourseList(
      String userId, int categoryId, int countryId,
      {int page: 1}) async {
    final service = await getCourseListService();
    DataResult result = await service
        .getCourseListApi(userId, categoryId, countryId, page: page);
    if (result.success) {
      final List<CourseResponse> response = result.data;
      return DataResult(success: true, message: result.message, data: response);
    }
    return DataResult(success: false, message: result.message);
  }

  Widget getCourseTypeIcon(BuildContext context, CourseResponse course, {bool isWhite: false}){
    switch (course.type.toLowerCase()) {
      case "youtube":
      case "video":{
        return loadSvg("video_horizontal_outlined.svg", height: 16, width: 16, color: getColor(context, isWhite == true ? textWhite : textGray));
      }
      default: {
        return loadSvg("document_text_outlined.svg", height: 16, width: 16, color: getColor(context, isWhite == true ? textWhite : textGray));
      }
    }
  }

  Widget getCourseInfoIconLabel(BuildContext context, CourseResponse course, {bool isWhite: false}){
    switch (course.type.toLowerCase()) {
      case "youtube":
      case "video":{
        if(isNullOrEmpty(course.videoDuration)){
          return SizedBox.shrink();
        }
        return NUIRow(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          divider: VerticalDivider(width: 5, color: Colors.transparent),
          children: [
            loadSvg("time.svg", width: 16, height: 16, color: getColor(context, isWhite == true ? textWhite :textGray)),
            Text(!isNullOrEmpty(course.videoDuration) ? "${course.videoDuration}" : "", style: getFont(context, size: 12, color: isWhite == true ? textWhite :textLightGray)),
          ],
        );
      }
      default: {
        if(isNullOrEmpty(course.videoDuration)){
          return SizedBox.shrink();
        }
        return NUIRow(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          divider: VerticalDivider(width: 5, color: Colors.transparent),
          children: [
            loadSvg("book_opened.svg", width: 16, height: 16, color: getColor(context, isWhite == true ? textWhite :textGray)),
            Text(!isNullOrEmpty(course.videoDuration) ? "${course.videoDuration} pages" : "", style: getFont(context, size: 12, color: isWhite == true ? textWhite :textLightGray)),
          ],
        );
      }
    }
  }

  void redirectCourseToDetail(BuildContext context, CourseResponse course) async{
    switch (course.type.toLowerCase()) {
      case "youtube":{
        showVideoViewer(context, searchCourse: course, isYoutube: true);
        break;
      }
      case "video":{
        showVideoViewer(context, searchCourse: course);
        break;
      }
      case "pdf":{
        showPDFViewer(context, searchCourse: course);
        break;
      }
      case "pptx":{
        if (await canLaunch(course.sourceUrl)) {
          await launch(course.sourceUrl);
        }
        break;
      }
      case "module":{
        showWebViewer(context, searchCourse: course);
        break;
      }
      default: {
        //Do nothing here
      }
    }
  }

  Future<DataResult<List<SearchCourse>>> searchCourseList(
      String userId, int categoryId, int countryId,
      {int page: 1, String keyword}) async {
    final service = await getCourseListService();
    DataResult result = await service.getCourseSearchListApi(
        userId, categoryId, countryId,
        page: page, keyword: keyword);
    if (result.success) {
      final List<CourseResponse> response = result.data;
      final List<SearchCourse> searched =
          response.map((e) => SearchCourse(e)).toList();
      logNUI("CourseBloc", "Result size: ${response.length}");
      return DataResult(success: true, message: result.message, data: searched);
    }
    return DataResult(success: false, message: result.message);
  }

  String getContentTypeByType(String type) {
    if (match(type, "pdf")) return "PDF";
    if (match(type, "video")) return "Video";
    if (match(type, "pptx")) return "PPT";
    return "Module";
  }

  Future<DataResult<List<CategoryResponse>>> getCourseCategories() async {
    final internCountry = await _profileBloc.getUserNesternshipCountry();
    return await _categoryBloc.getCategoryList(internCountry.countryId);
  }

  Future<DataResult<List<CourseResponse>>> getCoursesByCategory(int categoryId,
      {int page: 1}) async {
    final internCountry = await _profileBloc.getUserNesternshipCountry();
    final result = await getCourseList(
        _userBloc.getUserId(), categoryId, internCountry.countryId,
        page: page);
    return result;
  }

  Future<DataResult<List<SearchCourse>>> getMyCourses() async {
    final categories = await getCourseCategories();
    if(categories.success && !isNullOrEmpty(categories.data)){
      for(var cat in categories.data){
        if(contains(cat.title, "Mandatory")){
          final courses = await getCoursesByCategory(cat.id);
          if(courses.success && !isNullOrEmpty(courses.data)){
            final myCourses = courses.data.map((e) => SearchCourse(e)).toList();
            return DataResult<List<SearchCourse>>(success: true, message: courses.message, data: myCourses);
          }
        }
      }
    }
    return DataResult(success: false, message: "Failed to retrieve my courses");
  }

  Future<DataResult<List<SearchCourse>>> getCoursesByCountryAndCategory({int page: 1}) async{
    final courses = await getCoursesByCountry(page: 1);
    if(courses.success && !isNullOrEmpty(courses.data)){
      final myCourses = <SearchCourse>[];
      for(var c in courses.data){
        if(contains(c.course.title, "Mandatory")){
          myCourses.add(c);
        }
      }
      return DataResult(success: true, message: courses.message, data: myCourses);
    }
    else{
      return courses;
    }
  }

  Future<DataResult<List<SearchCourse>>> getCoursesByCountry({int page: 1}) async {
    final nesternshipCountry = await _profileBloc.getUserNesternshipCountry();
    final service = await getCourseListService();
    DataResult result = await service.getCourseByCountryAPI(nesternshipCountry.countryId, page: page);
    if (result.data != null) {

      final List<CourseResponse> response = result.data;
      final List<SearchCourse> courseList = new List<SearchCourse>();
      for (final item in response) {
        courseList.add(SearchCourse(item));
      }
      return DataResult(success: true, message: result.message, data: courseList);
    }
    return DataResult(success: false, message: result.message);
  }

  Future<DataResult<List<SearchCourse>>> searchCourses(int categoryId, {int page: 1, String keyword}) async {
    final internCountry = await _profileBloc.getUserNesternshipCountry();
    final result = await searchCourseList(
        _userBloc.getUserId(), categoryId, internCountry.countryId,
        page: page, keyword: keyword);
    return result;
  }

  Future<DataResult> updateCourseStatus(int courseId,
      {bool courseCompleted}) async {
    final CourseUpdateStatusRequest requestModel = CourseUpdateStatusRequest(
        pCourseId: courseId.toString(),
        pUserId: _userBloc.getUserId(),
        pCourseCompleted: courseCompleted);
    final service = await getCourseListService();
    final result = await service.updateCourseStatus(requestModel);
    logNUI("CourseBloc", "Update Course Status: ${result.success}");
    return result;
  }

  Future<bool> getCourseStatus(int courseId) async {
    final service = await getCourseListService();
    DataResult result = await service.getCourseReadStatus(
        _userBloc.getUserId(), courseId.toString());
    CourseGetStatusResponse readStatus = result.data;
    logNUI("CourseBloc",
        "CourseId ${courseId.toString()} Read Status Api: ${result.success}");
    logNUI("CourseBloc",
        "CourseId ${courseId.toString()} Read Status: ${readStatus.courseCompleted}");
    return readStatus.courseCompleted;
  }

  bool isRawMP4(String url){
    return contains(url, ".mp4");
  }
}
