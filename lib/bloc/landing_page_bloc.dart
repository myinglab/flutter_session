import 'package:nui_core/nui_core.dart';

import '../data/model/banner.dart';

class LandingPageBloc {
  List<BannerImage> getLandingPageSliderImages(){
    return [
      BannerImage.landing(getString("landingTitle1"), getString("landingDesc1"), 'assets/images/landing_bg.png'),
      BannerImage.landing(getString("landingTitle2"), getString("landingDesc2"), 'assets/images/nesternship_landingbg_2.png'),
      BannerImage.landing(getString("landingTitle3"), getString("landingDesc3"), 'assets/images/nesternship_landingbg_3.png')
    ];
  }
}