import 'package:nesternship_flut/data/model/banner.dart';
import 'package:nesternship_flut/data/model/training_course.dart';
import 'package:nui_core/code/nui_code_util.dart';
import 'package:nui_core/model/list_result.dart';
import 'package:nui_core/ui/component/listview/nui_listview_bloc.dart';

class TrainingCourseBloc{
  var coursesBloc = NUIListViewBloc<TrainingCourse, TrainingCourse>(callbackMethod: (List<TrainingCourse> current, bool isFirst, bool append, TrainingCourse cursor) async {
    final courses = [TrainingCourse(), TrainingCourse(), TrainingCourse(), TrainingCourse(), TrainingCourse()];
    await NUIAsync.delayForResult(Duration(seconds: 3), () => null);
    return ListResult(data: courses, success: true, message: "Get courses success", cursor: null, reachedEnd: false);
  });
  var coursesBloc2 = NUIListViewBloc<TrainingCourse, TrainingCourse>(callbackMethod: (List<TrainingCourse> current, bool isFirst, bool append, TrainingCourse cursor) async {
    final courses = [TrainingCourse(), TrainingCourse(), TrainingCourse(), TrainingCourse(), TrainingCourse()];
    await NUIAsync.delayForResult(Duration(seconds: 3), () => null);
    return ListResult(data: courses, success: true, message: "Get courses success", cursor: null, reachedEnd: false);
  });
  var coursesBloc3 = NUIListViewBloc<TrainingCourse, TrainingCourse>(callbackMethod: (List<TrainingCourse> current, bool isFirst, bool append, TrainingCourse cursor) async {
    final courses = [TrainingCourse(), TrainingCourse(), TrainingCourse(), TrainingCourse(), TrainingCourse()];
    await NUIAsync.delayForResult(Duration(seconds: 3), () => null);
    return ListResult(data: courses, success: true, message: "Get courses success", cursor: null, reachedEnd: false);
  });

   /*List<BannerImage> getBannerSliderImages(){
    return [
    BannerImage('image 1', "https://firebasestorage.googleapis.com/v0/b/xplore-cbd14.appspot.com/o/uat%2Fimages%2Fassets%2Fbackground_new_page.jpg?alt=media&token=522bee34-181d-4bbf-a119-d6bb54dbbfb3"),
    BannerImage('image 2', "https://firebasestorage.googleapis.com/v0/b/xplore-cbd14.appspot.com/o/uat%2Fimages%2Fassets%2Fbackground_new_page.jpg?alt=media&token=522bee34-181d-4bbf-a119-d6bb54dbbfb3"),
    BannerImage('image 3', "https://firebasestorage.googleapis.com/v0/b/xplore-cbd14.appspot.com/o/uat%2Fimages%2Fassets%2Fbackground_new_page.jpg?alt=media&token=522bee34-181d-4bbf-a119-d6bb54dbbfb3")
    ];
  }*/
}