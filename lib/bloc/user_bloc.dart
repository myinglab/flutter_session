import 'package:flutter/widgets.dart';
import 'package:nui_auth/nui_auth.dart';
import 'package:nui_core/code/nui_code_util.dart';
import 'package:nui_core/model/interfaces.dart';

class UserBloc{
  String getUserId(){
    return NUIAuth.get().getUserId();
  }

  Future<bool> logout() async{
    final result = await NUIAuth.get().logout();
    return result.success;
  }

  Future<DataResult> forgotPassword(BuildContext context, String email) async{
    final result = await NUIAuth.get().forgotPassword(email: email);
    return DataResult(success: result.success, message: result.message ?? "Failed to reset password");
  }

  Future<DataResult> changePassword(BuildContext context, String oldPassword, String newPassword) async{
    final result = await NUIAuth.get().changePassword(oldPassword: oldPassword, newPassword: newPassword, confirmPassword: newPassword);
    return DataResult(success: result.success, message: result.message ?? "Invalid password entered");
  }

  bool isLoggedIn(){
    final userId = getUserId();
    final loggedIn = NUIAuth.get().isLoggedIn();
    logNUI("UserBloc", "User is logged in : $loggedIn, userId : $userId");
    return loggedIn;
  }
}
