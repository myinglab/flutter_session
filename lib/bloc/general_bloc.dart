class GeneralBloc{
  String formatNotificationCount(int count){
    if(count == null) return "";
    if(count > 9) return "9+";
    return count.toString();
  }

  int getNotificationCount(){
    return 9;
  }
}