import 'package:nesternship_flut/datalayer/entity/category/category_response.dart';
import 'package:nesternship_flut/service/category/category_list_service.dart';
import 'package:nui_core/model/interfaces.dart';

class CategoryBloc {
  Future<CategoryListService> getCategoryListService() async {
    CategoryListService categoryListService = CategoryListService();
    await categoryListService.startInit();
    return categoryListService;
  }

  Future<DataResult<List<CategoryResponse>>> getCategoryList(int countryId) async {
    final service = await getCategoryListService();
    DataResult result = await service.getCategoryListApi(countryId);
    if (result.success) {
      final List<CategoryResponse> response = result.data;
      return DataResult(success: true, message: result.message, data: response);
    }
    return DataResult(success: false, message: result.message);
  }
}
