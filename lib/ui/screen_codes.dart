class ScreenCodes {
  ScreenCodes._builder();
  static const String main = "Main";
  static const String splash = "Splash";
  static const String login = "Login";
  static const String landing = "Landing";
  static const String registration = "Registration";
  static const String notificationList = "NotificationList";
  static const String notificationDetail = "NotificationDetail";
  static const String registrationSteps = "RegistrationSteps";
  static const String countrySelection = "CountrySelection";
  static const String pdfViewer = "PDFViewer";
  static const String webViewer = "WebViewer";
  static const String videoViewer = "VideoViewer";
  static const String search = "Search";
}
