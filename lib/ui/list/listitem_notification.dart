import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:nesternship_flut/bloc/notification_bloc.dart';
import 'package:nesternship_flut/data/model/notification.dart';
import 'package:nui_core/nui_core.dart';
import '../style/app_theme.dart';

class LINotification extends NUIListStatefulWidget {
  LINotification({@required this.notification, NUIListActionController controller}) : super(controller: controller);
  final UserNotification notification;
  @override
  _LINotificationState createState() => _LINotificationState();
}

class _LINotificationState extends NUIListState<LINotification> {
  NotificationBloc notifBloc = NotificationBloc();
  Widget _getMailIcon(){
    final bool isRead = widget.notification.read;
    return Icon(
      isRead == true ?  Icons.mail_outlined : Icons.mail,
      size: 24,
      color: isRead ? getColor(context, textGray) : getColor(context, accentBright));
  }

  Widget _renderNotificationItem(BuildContext context) {
    return Slidable(
      key: Key(widget.notification.notificationId.toString()),
      direction: Axis.horizontal,
      dismissal: SlidableDismissal(
        child: SlidableDrawerDismissal(),
        dragDismissible: false,
        onDismissed: (actionType) {
//          setState(() {
//            items.removeAt(index);
//          });
        },
      ),
      actionPane: SlidableScrollActionPane(),
      actionExtentRatio: 0.25,
      child: horizontalListItem(),
      secondaryActions: <Widget>[
        NUIRippleContainer(
          onTap: (){
            getController().onItemClick(widget.notification, NUIListActions.REMOVE);
           // getController().onItemClick(widget.notification, NUIListActions.NORMAL);
          },
          normalColor: getColor(context, accentBright),
          rippleColor: getColor(context, black).withOpacity(0.2),
          child: Container(
            height: double.infinity,
            alignment: Alignment.center,
            child: NUIColumn(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              divider: Divider(height: 8, color: NUIColors.NUITransparent),
              children: [
                Icon(Icons.delete, color: getColor(context, white), size: 26),
                Text("DELETE", style: getFont(context, size: 12, isBold: true, color: textWhite))
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget horizontalListItem(){
    final isRead = widget.notification.read;
    return Container(
        width: double.infinity,
        child: NUIRippleContainer(
          normalColor: getColor(context, white),
            onTap: () {
              getController().onItemClick(widget.notification, NUIListActions.NORMAL);
            },
            rippleColor: getColor(context, accentBright).withOpacity(0.3),
            child: IntrinsicHeight(
              child: NUIRow(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.only(left: 20),
                      decoration: BoxDecoration(
                          border: Border(
                              top: BorderSide(color: getColor(context, grayLine), width: 1),
                              bottom: BorderSide(
                                  color: getColor(context, grayLine), width: 1)
                          )
                      ),
                      child: NUIRow(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        divider: VerticalDivider(width: 23, color: NUIColors.NUITransparent),
                        children: [
                          _getMailIcon(),
                          Expanded(
                            child: Padding(
                              padding: EdgeInsets.only(top: 15, bottom: 15, right: 30),
                              child: NUIColumn(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Expanded(
                                      child: NUIRow(
                                        mainAxisSize: MainAxisSize.max,
                                        divider: VerticalDivider(width: 8, color: NUIColors.NUITransparent,),
                                        children: [
                                          Expanded(
                                              child: Text(widget.notification.title, style: getFont(context, size: 16, color: !isRead ? textBrown : textLightGray), overflow: TextOverflow.fade, softWrap: false, maxLines: 1,)
                                          ),
                                          Text(NUIDTUtil.getDateTimeDiffAsString(widget.notification.dateTime), style: getFont(context, size: 12, color: textLightGray))
                                        ],
                                      ),
                                    ),
                                    Text(widget.notification.description, style: getFont(context, size: 12, color: textDarkGray), maxLines: 1, overflow: TextOverflow.ellipsis,)
                                  ],
                                  divider: Divider(height: 8, color: NUIColors.NUITransparent)
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  VerticalDivider(
                    color: getColor(context, accentBright),
                    thickness: 3,
                    width: 3,
                  )
                ],
              ),
            )
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    return _renderNotificationItem(context);
  }
}
