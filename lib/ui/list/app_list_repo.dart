import 'package:nesternship_flut/data/model/notification.dart';
import 'package:nesternship_flut/data/model/search_course.dart';
import 'package:nesternship_flut/data/model/skeletal_model.dart';
import 'package:nesternship_flut/data/model/training_course.dart';
import 'package:nesternship_flut/data/model/country_model.dart';
import 'package:nesternship_flut/datalayer/entity/course/course_response.dart';
import 'package:nesternship_flut/ui/list/listItem_country.dart';
import 'package:nesternship_flut/ui/list/listitem_course.dart';
import 'package:nesternship_flut/ui/list/listitem_notification.dart';
import 'package:nesternship_flut/ui/list/listitem_search_course.dart';
import 'package:nesternship_flut/ui/list/listitem_training.dart';
import 'package:nesternship_flut/ui/list/skeletal/listitem_lottie_skeletal.dart';
import 'package:nesternship_flut/ui/list/skeletal/listitem_notification_skeletal.dart';
import 'package:nesternship_flut/ui/list/skeletal/listitem_reg_country_skeletal.dart';
import 'package:nesternship_flut/ui/list/skeletal/listitem_search_course_skeletal.dart';
import 'package:nesternship_flut/ui/list/skeletal/listitem_training_skeletal.dart';
import 'package:nui_core/nui_core.dart';

import '../../data/model/skeletal_model.dart';

class AppListRepo extends NUIListItemRepository {
  @override
  void registerListItems() {
    //Todo: Register your list items here
    registerItem(NUIListItem(
        entityClass: TrainingCourse,
        shadowRadius: 5,
        inflater:
            (dynamic data, int position, NUIListActionController controller) =>
                LITrainingCourse(course: data, controller: controller)));
    registerItem(NUIListItem(
        entityClass: CourseResponse,
        shadowRadius: 5,
        inflater:
            (dynamic data, int position, NUIListActionController controller) =>
            LICourse(course: data, controller: controller)));
    registerItem(NUIListItem(
        entityClass: Country,
        inflater:
            (dynamic data, int position, NUIListActionController controller) =>
                LICountry(country: data, controller: controller)));
    registerItem(NUIListItem(
        entityClass: SearchCourse,
        shadowRadius: 5,
        inflater:
            (dynamic data, int position, NUIListActionController controller) =>
                LISearchCourse(course: data, controller: controller)));
    //Todo: Register your skeletal items here
    registerItem(NUIListItem(
        entityClass: TrainingCourseSkeletal,
        shadowRadius: 5,
        inflater:
            (dynamic data, int position, NUIListActionController controller) =>
                LITrainingCourseSkeletal(controller: controller)));
    registerItem(NUIListItem(
        entityClass: UserNotification,
        inflater:
            (dynamic data, int position, NUIListActionController controller) =>
                LINotification(notification: data, controller: controller)));
    registerItem(NUIListItem(
        entityClass: SearchCourseSkeletal,
        shadowRadius: 5,
        inflater:
            (dynamic data, int position, NUIListActionController controller) =>
                LISearchCourseSkeletal(controller: controller)));
    registerItem(NUIListItem(
        entityClass: RegistrationCountrySkeletal,
        inflater:
            (dynamic data, int position, NUIListActionController controller) =>
                LIRegistrationCountrySkeletal(controller: controller)));
    registerItem(NUIListItem(
        entityClass: NotificationSkeletal,
        inflater:
            (dynamic data, int position, NUIListActionController controller) =>
                LINotificationSkeletal(controller: controller)));
    registerItem(NUIListItem(
        entityClass: LottieSkeletal,
        inflater:
            (dynamic data, int position, NUIListActionController controller) =>
                LILottieSkeletal(controller: controller)));
  }
}
