import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:nesternship_flut/data/model/training_course.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_animation/nui_animation.dart';
import 'package:nui_core/nui_core.dart';

class LITrainingCourse extends NUIListStatefulWidget {
  LITrainingCourse({@required this.course, NUIListActionController controller}) : super(controller: controller);
  final TrainingCourse course;
  @override
  _LITrainingCourseState createState() => _LITrainingCourseState();
}

class _LITrainingCourseState extends NUIListState<LITrainingCourse> {
  @override
  Widget build(BuildContext context) {
    return NUIBouncingAnimation(
      onTap: (){
        getController().onItemClick(widget.course, NUIListActions.NORMAL);
      },
      child: Container(
        width: 146,
        child: NUIColumn(
          children: [
            Container(
              height: 195,
              width: 146,
              decoration: NUIBoxDecoration(
                  shadowRadius: 10,
                  shadowColor: getColor(context, toolbarShadow),
                  shadowOffset: Offset(1,2),
                  color: getColor(context, white),
                  borderRadius: BorderRadius.circular(5)
              ),
              child: NUIClipView(
                borderRadius: BorderRadius.circular(5),
                child: Container(
                  height: 195,
                  width: 146,
                  decoration: BoxDecoration(
                      image:  DecorationImage(
                          image: loadImageProvider(widget.course.imageUrl),
                          fit: BoxFit.cover
                      )
                  ),
                ),
              ),
            ),
            Text(widget.course.title, style: getFont(context, size: 14, color: textDarkGray), maxLines: 2, overflow: TextOverflow.ellipsis,),
            SizedBox(
              width: 146,
              child: NUIRow(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                divider: VerticalDivider(width: 5, color: NUIColors.NUITransparent),
                children: [
                  widget.course.type.toLowerCase() == "pdf"
                      ? loadSvg("document_text_outlined.svg", height: 16, width: 16, color: getColor(context, textGray))
                      : loadSvg("video_horizontal_outlined.svg", height: 16, width: 16, color: getColor(context, textGray)),
//                            : Icon(LineAwesomeIcons.play, color: getColor(context, textGray), size: 16,),
                  Text(widget.course.type, style: getFont(context, size: 12, color: textLightGray)),
                  Expanded(
                      child: SizedBox(height: 1)
                  ),
                  widget.course.type.toLowerCase() == "pdf"
                      ? loadSvg("book_opened.svg", width: 16, height: 16, color: getColor(context, textGray))
                      : loadSvg("time.svg", width: 16, height: 16, color: getColor(context, textGray)),
//                            : Icon(LineAwesomeIcons.clock, color: getColor(context, textGray), size: 16,),
                  Text(widget.course.duration, style: getFont(context, size: 12, color: textLightGray)),
                ],
              )
//            child: NUIRow(
//              mainAxisSize: MainAxisSize.max,
//              crossAxisAlignment: CrossAxisAlignment.center,
//              divider: VerticalDivider(width: 3, color: NUIColors.NUITransparent),
//              children: [
//                Icon(LineAwesomeIcons.play, color: getColor(context, textLightGray), size: 16,),
//                Text(widget.course.type, style: getFont(context, size: 12, color: textLightGray)),
//                Expanded(
//                    child: SizedBox(height: 1)
//                ),
//                Icon(LineAwesomeIcons.clock, color: getColor(context, textLightGray), size: 16,),
//                Text(widget.course.duration, style: getFont(context, size: 12, color: textLightGray)),
//              ],
//            )
            )
          ],
          divider: Divider(height: 10, color: NUIColors.NUITransparent),
        ),
      ),
    );
  }
}
