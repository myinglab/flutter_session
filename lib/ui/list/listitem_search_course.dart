import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nesternship_flut/bloc/course_bloc.dart';
import 'package:nesternship_flut/data/model/search_course.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_animation/nui_animation.dart';
import 'package:nui_core/nui_core.dart';

class LISearchCourse extends NUIListStatefulWidget {
  LISearchCourse({@required this.course, NUIListActionController controller})
      : super(controller: controller);
  final SearchCourse course;
  @override
  _LISearchCourseState createState() => _LISearchCourseState();
}

class _LISearchCourseState extends NUIListState<LISearchCourse> {
  CourseBloc bloc = CourseBloc();
  bool isCompleted = false;

  @override
  Widget build(BuildContext context) {
    return NUIBouncingAnimation(
      scaleFactor: 0.3,
      onTap: () {
        getController().onItemClick(widget.course, NUIListActions.NORMAL);
      },
//      rippleColor: getColor(context, accentBright).withOpacity(0.3),
      child: Container(
        width: double.infinity,
        padding: EdgeInsets.only(left: 15),
        child: NUIColumn(
          children: [
            NUIRow(children: [
              Container(
                height: 118,
                width: 88,
                decoration: NUIBoxDecoration(
                  shadowRadius: 10,
                  shadowColor: getColor(context, toolbarShadow),
                  shadowOffset: Offset(1, 2),
                  borderRadius: BorderRadius.circular(5),
                  gradient: NUILinearGradient(
                      colors: [
                        getColor(context, backgroundWhite),
                        getColor(context, backgroundGray)
                      ],
                      orientation: GradientOrientation.TL_BR
                  )
                ),
                child: NUIClipView(
                  borderRadius: BorderRadius.circular(5),
                  child: Container(
                    height: double.infinity,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      image: DecorationImage(image: loadImageProvider(widget.course.course.thumbnailUrl), fit: BoxFit.cover)
                    ),
                  ),
                ),
              ),
              SizedBox(width: 24),
              Expanded(
                child: NUIColumn(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(right: 22),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Flexible(
                            child: Text(
                              widget.course.course.title,
                              style: getFont(context, size: 12, color: accentBright),
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                          NUIFutureWidget<bool>(
                            futureValue: isCompleted != true ? bloc.getCourseStatus(widget.course.course.id) : NUIAsync.delayForResult(Duration(milliseconds: 100), () async => isCompleted),
                            callback: (state, data, message) {
                              if(data != null && data == true) isCompleted = true;
                              return Opacity(
                                opacity: data != null && data == true ? 1.0 : 0,
                                child: Container(
                                    width: 22,
                                    height: 22,
                                    alignment: Alignment.center,
                                    decoration: NUIBoxDecoration(
                                        color: getColor(
                                            context, greenHighlight2),
                                        shadowOffset: Offset(1, 1),
                                        shadowRadius: 3,
                                        borderRadius: BorderRadius.circular(12),
                                        shadowColor: getColor(
                                            context, greenHighlight2)
                                            .withOpacity(0.2)
                                    ),
                                    child: Icon(
                                        Icons.check, color: Colors.white,
                                        size: 16)
                                ),
                              );
                            }
                          )
                        ],
                      ),
                    ),
                    Divider(height: 7, color: NUIColors.NUITransparent),
                    Text(
                      widget.course.course.description,
                      style: getFont(context, size: 14, color: textLightGray2),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                    Divider(height: 8, color: NUIColors.NUITransparent),
                    NUIRow(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      divider: VerticalDivider(width: 5, color: NUIColors.NUITransparent),
                      children: [
                        bloc.getCourseTypeIcon(context, widget.course.course),
                        Text(bloc.getContentTypeByType(widget.course.course.type), style: getFont(context, size: 12, color: textLightGray)),
                        VerticalDivider(width: 10),
                        bloc.getCourseInfoIconLabel(context, widget.course.course)
                      ],
                    )
                  ],
                ),
              ),
            ]),
          ],
          divider: Divider(height: 8, color: NUIColors.NUITransparent),
        ),
      ),
    );
  }
}
