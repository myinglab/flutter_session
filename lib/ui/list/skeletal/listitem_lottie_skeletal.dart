import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_core/nui_core.dart';

class LILottieSkeletal extends NUIListStatefulWidget {
  LILottieSkeletal({NUIListActionController controller}) : super(controller: controller);
  @override
  _LILottieSkeletalState createState() => _LILottieSkeletalState();
}

class _LILottieSkeletalState extends NUIListState<LILottieSkeletal> {

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(50),
      child: Lottie.asset(
          'assets/lottie/gathering_info_lottie.json',
          width: screenWidth(context) * 0.8,
          height: screenWidth(context) * 0.8,
      )
    );
  }
}
