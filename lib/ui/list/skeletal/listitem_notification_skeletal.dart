import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_core/nui_core.dart';

class LINotificationSkeletal extends NUIListStatefulWidget {
  LINotificationSkeletal({NUIListActionController controller}) : super(controller: controller);
  @override
  _LINotificationSkeletalState createState() => _LINotificationSkeletalState();
}

class _LINotificationSkeletalState extends NUIListState<LINotificationSkeletal> {

  Widget _getMailIcon(){
    return NUISkeletonContainer(
      child: NUISkeletonView(
        width: 40,
        height: 40,
        radius: BorderRadius.circular(10),
      )
    );
  }

  Widget horizontalListItem(){

    return Container(
        color: getColor(context, white),
        width: double.infinity,
        child: IntrinsicHeight(
          child: NUIRow(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(left: 20),
                  decoration: BoxDecoration(
                      border: Border(
                          top: BorderSide(color: getColor(context, grayLine), width: 1),
                          bottom: BorderSide(
                              color: getColor(context, grayLine), width: 1)
                      )
                  ),
                  child: NUIRow(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    divider: VerticalDivider(width: 23, color: NUIColors.NUITransparent),
                    children: [
                      _getMailIcon(),
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.only(top: 15, bottom: 15, right: 30),
                          child: NUIColumn(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  child: NUISkeletonContainer(
                                    child: NUIRow(
                                      mainAxisSize: MainAxisSize.max,
                                      divider: VerticalDivider(width: 8, color: NUIColors.NUITransparent,),
                                      children: [
                                        Expanded(
                                            child: NUISkeletonView(
                                              radius: BorderRadius.circular(10),
                                              child: Text("Upcoming Webinar", style: getFont(context, size: 16, singleColor: NUIColors.NUITransparent), overflow: TextOverflow.fade, softWrap: false, maxLines: 1,)
                                            )
                                        ),
                                        NUISkeletonView(
                                          radius: BorderRadius.circular(10),
                                          child: Text(NUIDTUtil.getDateTimeDiffAsString(DateTime.now()), style: getFont(context, size: 12, singleColor: NUIColors.NUITransparent))
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                NUISkeletonView(
                                    radius: BorderRadius.circular(10),
                                    child: Text("Nur Diana, you recently signed up for ...", style: getFont(context, size: 12, singleColor: NUIColors.NUITransparent), maxLines: 1, overflow: TextOverflow.ellipsis,)
                                )
                              ],
                              divider: Divider(height: 8, color: NUIColors.NUITransparent)
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              VerticalDivider(
                color: getColor(context, accentBright),
                thickness: 3,
                width: 3,
              )
            ],
          ),
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    return horizontalListItem();
  }
}
