import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_core/nui_core.dart';

class LITrainingCourseSkeletal extends NUIListStatefulWidget {
  LITrainingCourseSkeletal({NUIListActionController controller})
      : super(controller: controller);
  @override
  _LITrainingCourseSkeletalState createState() =>
      _LITrainingCourseSkeletalState();
}

class _LITrainingCourseSkeletalState
    extends NUIListState<LITrainingCourseSkeletal> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: NUIColumn(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            height: 195,
            width: 146,
            decoration: NUIBoxDecoration(
//                shadowRadius: 10,
//                shadowColor: getColor(context, toolbarShadow),
//                shadowOffset: Offset(1,2),
                color: getColor(context, white),
                borderRadius: BorderRadius.circular(4)
            ),
            child:  NUISkeletonContainer(
              child: NUISkeletonView(
                width: 146,
                height: 195,
                radius: BorderRadius.circular(4),
              ),
            ),
          ),
          Divider(
            height: 5,
            color: NUIColors.NUITransparent,
          ),
          NUISkeletonContainer(
            child: NUIColumn(
              crossAxisAlignment: CrossAxisAlignment.start,
              divider: Divider(
                height: 3,
                color: NUIColors.NUITransparent,
              ),
              children: [
                NUISkeletonView(
                  child: Text("Title here",
                      style: getFont(context, size: 14, color: textBlack)),
                ),
                NUISkeletonView(
                  child: Text("Description here",
                      style: getFont(context, size: 14, color: textBlack)),
                ),
              ],
            ),
          ),
        ],
        divider: Divider(
          height: 5,
          color: NUIColors.NUITransparent,
          thickness: 0,
        ),
      ),
    );
  }
}
