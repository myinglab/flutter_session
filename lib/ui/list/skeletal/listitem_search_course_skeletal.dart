import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_core/nui_core.dart';
import 'package:nui_core/ui/component/listview/nui_listview_bloc.dart';
import 'package:nui_core/ui/component/listview/nui_listview_widgets.dart';

class LISearchCourseSkeletal extends NUIListStatefulWidget {
  LISearchCourseSkeletal({NUIListActionController controller})
      : super(controller: controller);
  @override
  _LISearchCourseSkeletalState createState() => _LISearchCourseSkeletalState();
}

class _LISearchCourseSkeletalState
    extends NUIListState<LISearchCourseSkeletal> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: NUIColumn(
        children: [
          NUIRow(children: [
            Container(
                height: 118,
                width: 88,
                decoration: NUIBoxDecoration(
                    shadowRadius: 10,
                    shadowColor: getColor(context, toolbarShadow),
                    shadowOffset: Offset(1, 2),
                    borderRadius: BorderRadius.circular(4)),
                child: NUISkeletonContainer(
                    child: NUISkeletonView(
                  height: 118,
                  width: 88,
                  radius: BorderRadius.circular(4),
                ))),
            SizedBox(width: 24),
            NUISkeletonContainer(
              child: NUIColumn(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  NUISkeletonView(
                    child: Text(
                      "Mandatory Training",
                      style: getFont(context, size: 12, color: accentBright),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  Divider(height: 7, color: NUIColors.NUITransparent),
                  NUISkeletonView(
                    child: Text(
                      "Nestle Purpose and Values",
                      style: getFont(context, size: 14, color: textDarkGray),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  Divider(height: 7, color: NUIColors.NUITransparent),
                  SizedBox(
                      width: 146,
                      child: NUISkeletonContainer(
                        child: NUIRow(
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          divider: VerticalDivider(
                              width: 3, color: NUIColors.NUITransparent),
                          children: [
                            NUISkeletonView(
                              child: Icon(
                                LineAwesomeIcons.play,
                                color: getColor(context, textLightGray),
                                size: 16,
                              ),
                            ),
                            NUISkeletonView(
                              child: Text("Video",
                                  style: getFont(context,
                                      size: 12, color: textLightGray)),
                            ),
                          ],
                        ),
                      ))
                ],
              ),
            ),
          ]),
        ],
        divider: Divider(height: 8, color: NUIColors.NUITransparent),
      ),
    );
  }
}
