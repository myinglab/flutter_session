import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_core/nui_core.dart';

class LIRegistrationCountrySkeletal extends NUIListStatefulWidget {
  LIRegistrationCountrySkeletal({NUIListActionController controller})
      : super(controller: controller);
  @override
  _LIRegistrationCountrySkeletalState createState() =>
      _LIRegistrationCountrySkeletalState();
}

class _LIRegistrationCountrySkeletalState
    extends NUIListState<LIRegistrationCountrySkeletal> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: NUIColumn(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            NUISkeletonContainer(
              child: Container(
                padding: EdgeInsets.only(left: 15, right: 15),
                child: NUISkeletonView(
                    child: Text("South Africa America",
                        style: getFont(context, size: 16))),
              ),
            ),
            Divider(
              height: 5,
              color: NUIColors.NUITransparent,
            ),
          ]),
    );
  }
}
