import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nui_core/nui_core.dart';
import 'package:nesternship_flut/data/model/country_model.dart';
import '../style/app_theme.dart';

class LICountry extends NUIListStatefulWidget {
  LICountry({@required this.country, NUIListActionController controller})
      : super(controller: controller);
  final Country country;
  @override
  _LICountryState createState() => _LICountryState();
}

class _LICountryState extends NUIListState<LICountry> {
  @override
  Widget build(BuildContext context) {
    return NUIRippleContainer(
        onTap: () {
          getController().onItemClick(widget.country, NUIListActions.NORMAL);
        },
        rippleColor: getColor(context, accentBright).withOpacity(0.3),
        child: Container(
            width: double.infinity,
            padding: EdgeInsets.only(left: 24, right: 24, top: 18, bottom: 18),
            child: Text(
              widget.country.countryName,
              style: getFont(context, size: 16, color: textDarkGray),
            )));
  }
}
