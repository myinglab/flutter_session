import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nesternship_flut/bloc/course_bloc.dart';
import 'package:nesternship_flut/datalayer/entity/course/course_response.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_animation/nui_animation.dart';
import 'package:nui_core/nui_core.dart';
import 'package:nui_core/ui/component/listview/nui_listview_widgets.dart';

class LICourse extends NUIListStatefulWidget {
  LICourse({@required this.course, NUIListActionController controller}) : super(controller: controller);
  final CourseResponse course;
  @override
  _LICourseState createState() => _LICourseState();
}

class _LICourseState extends NUIListState<LICourse> {
  CourseBloc bloc = CourseBloc();

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: NUIBouncingAnimation(
        scaleFactor: 0.3,
        onTap: (){
          getController().onItemClick(widget.course, NUIListActions.NORMAL);
        },
        child: Container(
          width: 168,
          height: 224,
          decoration: NUIBoxDecoration(
              shadowRadius: 10,
              shadowColor: getColor(context, toolbarShadow),
              shadowOffset: Offset(1,2),
              borderRadius: BorderRadius.circular(5),
              gradient: NUILinearGradient(
                colors: [
                  getColor(context, backgroundWhite),
                  getColor(context, backgroundWhite)
                ],
                orientation: GradientOrientation.TL_BR
              )
          ),
          child: Container(
            height: 224,
            width: 168,
            child: Stack(
              children: [
                NUIClipView(
                  borderRadius: BorderRadius.circular(5),
                  child: Container(
                    height: double.infinity,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      image:  DecorationImage(
                        image: loadImageProvider(widget.course.thumbnailUrl),
                        fit: BoxFit.cover
                      )
                    ),
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: double.infinity,
                  alignment: Alignment.bottomLeft,
                  child: Container(
                    width: 168,
                    decoration: NUIBoxDecoration(
                      borderRadius: BorderRadius.only(bottomLeft: Radius.circular(5), bottomRight: Radius.circular(5)),
                        gradient: NUILinearGradient(
                            colors: [
                              Colors.transparent,
                              Colors.black54
                            ],
                            orientation: GradientOrientation.T_B
                        )
                    ),
                    padding: EdgeInsets.only(top: 20, bottom: 10, left: 10, right: 10),
                    child: NUIColumn(
                      mainAxisSize: MainAxisSize.min,
                      divider: Divider(height: 10, color: Colors.transparent),
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(widget.course.description, style: getFont(context, size: 14, color: textWhite), maxLines: 2, overflow: TextOverflow.ellipsis),
                        NUIRow(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          divider: VerticalDivider(width: 5, color: NUIColors.NUITransparent),
                          children: [
                            bloc.getCourseTypeIcon(context, widget.course, isWhite: true),
                            Text(bloc.getContentTypeByType(widget.course.type), style: getFont(context, size: 12, color: textWhite)),
                            Expanded(
                              child: SizedBox(height: 1)
                            ),
                            bloc.getCourseInfoIconLabel(context, widget.course, isWhite: true)
                          ],
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
