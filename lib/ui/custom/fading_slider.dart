import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nui_core/bloc/nui_bloc.dart';
import 'package:nui_core/code/nui_code_util.dart';
import 'package:nui_core/data/nui_data_util.dart';
import 'package:visibility_detector/visibility_detector.dart';
import 'package:nui_core/nui_core.dart';

class FadingSliderItem{
  FadingSliderItem();
//  Widget background;
//  Widget foreground;
  int _position;
  double _visibleFraction;
  String _key;
}

typedef FadingSliderInflater = Widget Function(BuildContext context, int position, double fraction);

class FadingSlider extends StatefulWidget {
  FadingSlider({@required this.count, this.width, this.height, this.notifier, this.carouselOptions, @required this.backgroundInflater, this.foregroundInflater});
//  final List<FadingSliderItem> sliders;
  final double width;
  final double height;
  final int count;
  final ValueNotifier<int> notifier;
  final CarouselOptions carouselOptions;
  final FadingSliderInflater backgroundInflater;
  final FadingSliderInflater foregroundInflater;

  @override
  _FadingSliderState createState() => _FadingSliderState();
}

class _FadingSliderState extends State<FadingSlider> {
  PageController _pageController;
  CarouselOptions _carouselOptions;
  List<NUIBlocData<double>> _blocs;
  List<FadingSliderItem> _items;
  bool _pageChanged = false;
  Map<int, double> visibilityMap = Map();

  @override void initState(){
    super.initState();
    VisibilityDetectorController.instance.updateInterval = Duration.zero;
    _pageController = PageController(viewportFraction: 1.0, initialPage: 0);
    _carouselOptions = widget.carouselOptions ?? CarouselOptions(
        height: widget.height,
        viewportFraction: 1.0,
        enlargeCenterPage: false,
        autoPlay: true,
        autoPlayInterval: Duration(seconds: 8),
        autoPlayAnimationDuration: Duration(milliseconds: 500),
        autoPlayCurve: Curves.fastOutSlowIn,
        onPageChanged: (index, reason) {
          _pageChanged = true;
          if(widget.notifier != null) widget.notifier.value = index;
        });

    int position = 0;
    _blocs = [];
    _items = [];
    for(int i = 0; i< widget.count; i++){
      final innerItem = FadingSliderItem();
      innerItem._key = position.toString();
      innerItem._position = position;
      _items.add(innerItem);

      _blocs.add(NUIBlocData<double>());
      position ++;
    }
//    widget.sliders.forEach((element) {
//      final innerItem = FadingSliderItem(background: element.background, foreground: element.foreground);
//      innerItem._key = position.toString();
//      innerItem._position = position;
//      _items.add(innerItem);
//
//      _blocs.add(NUIBlocData<double>());
//      position ++;
//    });
  }

  double _getItemStoredFraction(int position){
    final fraction = visibilityMap[position];
    if(fraction == null && _pageChanged != true && position == 0) return 1;
    return fraction ?? 0;
  }

  Widget _renderBackground(BuildContext context, int position){
    return NUIBlocStream<double>(
        bloc: _blocs[position],
        stream: (context, status, value) {
          final fraction = _getItemStoredFraction(position);
          return Opacity(opacity: fraction, child: widget.backgroundInflater(context, position, fraction));
        }
    );
  }

  Widget _renderForeground(BuildContext context, int position){
    final item = _items[position];
    return VisibilityDetector(
      key: Key(item._key),
      child: widget.foregroundInflater != null ? widget.foregroundInflater(context, position, _getItemStoredFraction(position)) : Container(width: double.infinity, height: double.infinity),
      onVisibilityChanged: (visibility){
        visibilityMap[position] = visibility.visibleFraction;
        final visibleFraction = visibility.visibleFraction;
        _visibilityChanged(position, visibleFraction);
      },
    );
  }

  List<Widget> _getSliderBackgrounds(BuildContext context){
    return _items.map((e){
      return _renderBackground(context, e._position);
    }).toList();
  }


  void _notifyBannerOnFractionChanged(int position, double fraction){
    _blocs[position].notify(fraction);
  }

  void _visibilityChanged(int position, double fraction){
    final item = _items[position];
    if(item._visibleFraction != fraction){
      //Fraction changed
      item._visibleFraction = fraction;
      _notifyBannerOnFractionChanged(position, fraction);
    }
    setState(() {

    });
  }

  Widget _getPageView(BuildContext context){
    final carousel = CarouselSlider(
      options: _carouselOptions,
      items: _items.map((item) {
        return _renderForeground(context, item._position);
      }).toList(),
    );
    return carousel;
//    return PageView.builder(
//        controller: _pageController,
//        onPageChanged: (index) {
//          _pageChanged(index);
//          if(widget.notifier != null){
//            widget.notifier.value = index;
//          }
//        },
//        itemCount: widget.sliders.length,
//        itemBuilder: (context, position) {
//          return widget.sliders[position].foreground ?? Container(width: double.infinity, height: double.infinity);
//        }
//    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.width,
      height: widget.height,
      child: Stack(
        children: [
          Stack(
            children: _getSliderBackgrounds(context),
          ),
          _getPageView(context)
        ],
      ),
    );
  }
}
