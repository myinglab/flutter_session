import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:nesternship_flut/ui/screen_codes.dart';
import 'package:nui_animation/nui_button_animation.dart';
import 'package:nui_core/nui_core.dart';
import '../style/app_theme.dart';

class BackToolbar extends StatefulWidget {
  BackToolbar(this.title);

  final String title;

  @override
  BackToolbarState createState() => BackToolbarState();
}

class BackToolbarState extends State<BackToolbar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: toolbarWhiteDecoration(context),
      child: Column(
        children: [
          NUIStatusBar(),
          Container(
            height: 48,
            width: double.infinity,
            child: Padding(
              padding: EdgeInsets.only(left: 8, right: 8),
              child: NUIRow(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                divider:
                    VerticalDivider(width: 10, color: NUIColors.NUITransparent),
                children: [
                  NUIBouncingAnimation(
                      child: Padding(
                        padding: EdgeInsets.all(10),
                        child: loadSvg("back_ui.svg", width: 28, height: 28, color: getColor(context, accentBright))
//                        child: Icon(
//                          LineAwesomeIcons.angle_left,
//                          color: getColor(context, accentBright),
//                          size: 28,
//                        ),
                      ),
                      onTap: () {
                        pop(context);
                      }),
                  Expanded(
                    child: Text(widget.title,
                        style: getFont(context, size: 16, color: textDarkGray)),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
