import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nesternship_flut/ui/custom/yellow_background_button.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_core/ui/component/nui_app.dart';
import 'package:nui_core/ui/component/nui_column.dart';
import 'package:nui_core/nui_core.dart';
import 'package:nesternship_flut/service/base_service.dart';

class NoInternetConnectionHolder extends StatelessWidget {
  NoInternetConnectionHolder({@required this.onRetry});
  final Function onRetry;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Expanded(
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image(
                      image: AssetImage("assets/images/no_wifi.png")
                  ),
                  Divider(height: 16, color: Colors.transparent),
                  Padding(
                    padding: EdgeInsets.only(left: 26, right: 26),
                    child: Text(
                      getString("noInternetConnection"),
                      style: getFont(context, size: 18, color: textBlack)
                    ),
                  ),
                  Divider(height: 16, color: Colors.transparent),
                  Padding(
                    padding: EdgeInsets.only(left: 26, right: 26),
                    child: Text(
                      getString("cantFindConnection"),
                      style: getFont(context, size: 16, color: textDarkGray),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ]
              ),
            ),
          ),
          YellowBackgroundButton(
            text: "Retry",
            onTap: (){
              if(onRetry != null) onRetry();
            },
          ),
          Divider(height: 64, color: Colors.transparent)
        ],
      )
    );
  }
}

class NoInternetConnectionChecker extends StatefulWidget {
  NoInternetConnectionChecker({@required this.child, this.onRetry});
  final Widget child;
  final Function onRetry;
  @override
  _NoInternetConnectionCheckerState createState() => _NoInternetConnectionCheckerState();
}

class _NoInternetConnectionCheckerState extends State<NoInternetConnectionChecker> {
  @override
  Widget build(BuildContext context) {
    return NUIFutureWidget<bool>(
        futureValue: BaseService.hasInternetConnection,
        callback: (state, hasInternet, message) {
          if(hasInternet == true){
            return widget.child;
          }
          else{
            return NoInternetConnectionHolder(onRetry: (){
              if(widget.onRetry != null) {
                widget.onRetry();
              }
              setState(() {

              });
            });
          }
        });
  }
}

