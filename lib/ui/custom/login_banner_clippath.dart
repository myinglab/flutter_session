import 'dart:ui';
import 'package:flutter/cupertino.dart';

class LoginBannerClipper extends CustomClipper<Path> {
  LoginBannerClipper({this.bottomRadius, this.heightBeforeRadius: 40});
  final double bottomRadius;
  final double heightBeforeRadius;

  Path getLoginImageClipPath(Size size, {double bottomRadius, double heightBeforeRadius: 40}){
    final circleRadius = bottomRadius ?? size.height /2;

    final mPath = Path();
    mPath.lineTo(0, 0);
    mPath.lineTo(size.width, 0);
    mPath.lineTo(size.width, size.height - heightBeforeRadius);
    mPath.arcToPoint(Offset(0, size.height - heightBeforeRadius), radius: Radius.circular(circleRadius), rotation: 150);
    mPath.lineTo(0, 0);
    mPath.close();
    return mPath;
  }

  @override
  Path getClip(Size size) {
    return getLoginImageClipPath(size, bottomRadius: bottomRadius, heightBeforeRadius: heightBeforeRadius);
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}