import 'package:date_util/date_util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_core/nui_core.dart';

class DateOfBirth{
  DateOfBirth({this.year, this.month, this.day});
  int year;
  int month;
  int day;

  @override
  String toString(){
    final dateTime = DateTime(year, month, day);
    return NUIDTUtil.dateToString(dateTime, format: NUIDTUtil.FORMAT_yyyy_MM_dd);
  }
}

typedef DOBSelectListener = DateOfBirth Function();
class DOBPickerController{
  DOBPickerController({this.valueChangedListener});
  DOBSelectListener _listener;
  Function valueChangedListener;

  DateOfBirth getDateOfBirth(){
    return _listener();
  }

  void _valueChanged(){
    if(valueChangedListener != null) valueChangedListener();
  }
}

class DOBPicker extends StatefulWidget {
  DOBPicker({this.controller});
  static const MONTHS = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  static const MONTHS_SHORT = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  final DOBPickerController controller;

  @override
  _DOBPickerState createState() => _DOBPickerState();
}

class _DOBPickerState extends State<DOBPicker> {
  int _selectedYear;
  int _selectedMonth;
  int _selectedDay;

  int _prevSelectedMonth;
  int _prevSelectedYear;
  DOBPickerController _controller;
  FixedExtentScrollController dayController;

  List<int> _getYearsSelection(){
    final currentYear = DateTime.now().year;
    List<int> years = List();
    for(int i = 0; i< 100; i++){
      years.add(currentYear - (100 - i));
    }
    years.add(currentYear);
//    for(int i = 1; i< 100; i++){
//      years.add(currentYear + i);
//    }
    return years;
  }

  List<int> _getDaysInMonthYear(int month, int year){
    final days = DateUtil().daysInMonth(month, year);
    final daysSelection = List<int>();
    for(int i = 1; i<= days; i++){
      daysSelection.add(i);
    }
    return daysSelection;
  }

  @override
  void initState(){
    super.initState();
    final curDate = DateTime.now();
    _selectedDay = curDate.day;
    _selectedMonth = curDate.month;
    _prevSelectedMonth = curDate.month;
    _selectedYear = curDate.year;
    _prevSelectedYear = curDate.year;
    _controller = widget.controller ?? DOBPickerController();
    logNUI("DOBPicker", "Current selected year :$_selectedYear");
    _controller._listener = (){
      return DateOfBirth(year: _selectedYear, month: _selectedMonth, day: _selectedDay);
    };

    dayController = FixedExtentScrollController(initialItem: _selectedDay - 1);
  }

  void _updateSelectedDayOnMonthChange() async{
    final daysCur = DateUtil().daysInMonth(_selectedMonth, _selectedYear);
    final daysPrev = DateUtil().daysInMonth(_prevSelectedMonth, _selectedYear);

    logNUI("DOBPicker", "Days cur : $daysCur, days prev : $daysPrev, selected day : $_selectedDay");
    if(_selectedDay == daysPrev){
      _selectedDay = daysCur;
      dayController.animateToItem(_selectedDay - 1, duration: Duration(milliseconds: 250), curve: Curves.ease);
    }
  }

  @override
  Widget build(BuildContext context) {

    final yearsSelection = _getYearsSelection();
    final curYearIndex = yearsSelection.indexOf(_selectedYear);
    final curMonthIndex = _selectedMonth - 1;
    final curDayIndex = _selectedDay - 1;

    return Container(
      width: double.infinity,
      height: 160,
      child: Stack(
        children: [
          Container(
            height: 160,
            width: double.infinity,
            alignment: Alignment.center,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: CupertinoPicker(
                      itemExtent: 64,
                      scrollController: dayController,
                      magnification: 1,
                      onSelectedItemChanged: (position){
                        _selectedDay = position + 1;
                        widget.controller._valueChanged();
                      },
                      children: _getDaysInMonthYear(_selectedMonth, _selectedDay).map((e) {
                        return Center(child: Text(e.toString(), style: getFont(context, size: 24, color: textWhite)));
                      }).toList()
                  ),
                ),
                Expanded(
                  child: CupertinoPicker(
                      scrollController: FixedExtentScrollController(initialItem: curMonthIndex),
                      itemExtent: 64,
                      magnification: 1,
                      onSelectedItemChanged: (position){
                        setState(() {
                          _prevSelectedMonth = _selectedMonth;
                          _selectedMonth = position + 1;
                          _updateSelectedDayOnMonthChange();
                          widget.controller._valueChanged();
                        });
                      },
                      children: DOBPicker.MONTHS_SHORT.map((e) {
                        return Center(child: Text(e, style: getFont(context, size: 22, color: textWhite)));
                      }).toList()
                  ),
                ),
                Expanded(
                  child: CupertinoPicker(
                      scrollController: FixedExtentScrollController(initialItem: curYearIndex),
                      itemExtent: 64,
                      magnification: 1,
                      onSelectedItemChanged: (position){
                        setState(() {
                          _prevSelectedYear = _selectedYear;
                          _selectedYear = yearsSelection[position];
                          _updateSelectedDayOnMonthChange();
                          widget.controller._valueChanged();
                        });
                      },
                      children: yearsSelection.map((e) {
                        return Center(child: Text(e.toString(), style: getFont(context, size: 24, color: textWhite)));
                      }).toList()
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 48),
              width: double.infinity,
              height: 1,
              color: getColor(context, white).withOpacity(0.5),
            ),
          Container(
            margin: EdgeInsets.only(top: 112),
            width: double.infinity,
            height: 1,
            color: getColor(context, white).withOpacity(0.5),
          )
        ],
      ),
    );
  }
}
