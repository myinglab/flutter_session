import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nesternship_flut/ui/style/app_colors.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_animation/nui_animation.dart';
import 'package:nui_core/ui/component/nui_app.dart';

typedef OnCheckChange = void Function(bool checked);

class CheckBoxController{
  OnCheckChange _setChecked;
  void setChecked(bool checked){
    if(_setChecked != null) _setChecked(checked);
  }
}

class CustomCheckbox extends StatefulWidget {

  final double height;
  final double width;
  final double borderRadius;
  final double borderWidth;
  final bool checked;
  final OnCheckChange onCheckChange;
  final CheckBoxController controller;

  CustomCheckbox({this.height, this.width, this.borderRadius, this.checked: false, this.borderWidth, this.onCheckChange, this.controller});

  @override
  _CustomCheckboxState createState() => _CustomCheckboxState();
}

class _CustomCheckboxState extends State<CustomCheckbox> {

  bool isChecked;
  CheckBoxController controller;

  @override
  void initState(){
    super.initState();
    isChecked = widget.checked ?? false;
    controller = widget.controller ?? CheckBoxController();
    controller._setChecked = (checked){
      isChecked = checked;
      setState(() {

      });
    };
  }

  @override
  Widget build(BuildContext context) {
    return NUIBouncingAnimation(
        onTap: () {
          setState(() {
            isChecked = !isChecked;
            if(widget.onCheckChange != null) widget.onCheckChange(isChecked);
          });
        },
        child: AnimatedContainer(
            duration: Duration(milliseconds: 250),
            curve: Curves.fastLinearToSlowEaseIn,
            height: widget.height ?? 20,
            width: widget.width ?? 20,
            decoration: BoxDecoration(
                color: isChecked ? getColor(context, accentBright) : getColor(context, transparent),
                borderRadius: BorderRadius.circular(widget.borderRadius ?? 4),
                border: isChecked != true ? Border.all(
                    color: getColor(context, textGray),
                    width: widget.borderWidth ?? 1
                ): null
            ),
            child:
            isChecked
                ?
            Icon(
                Icons.check,
                color: Colors.white,
                size: 16
            )
                :
            null
        )
    );
  }
}