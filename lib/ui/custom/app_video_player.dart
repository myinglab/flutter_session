import 'package:better_player/better_player.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_core/nui_core.dart';
import 'package:nui_media/nui_media.dart';

class AppVideoPlayer extends StatelessWidget {
  AppVideoPlayer(this.videoUrl);
  final String videoUrl;

  NUIMediaFile _fileFromUrl(){
    final file = NUIMediaFile(path: videoUrl, videoPath: videoUrl, type: NUIMediaFile.MEDIA_TYPE_VIDEO, isLocal: false);
    return file;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
//      child: NUIVideoListViewPlayer(mediaFile: _fileFromUrl(), boxFit: BoxFit.cover, autoPlay: false, mute: false, playFraction: 0.8, aspectRatio: 1.4, showButtons: false, width: screenWidth(context), height: screenHeight(context)*0.66)
      child: NUIVideoPlayer(
        mediaFile: _fileFromUrl(),
        adjustOrientation: true,
        configuration: BetterPlayerConfiguration(
          aspectRatio: 1.4,
          autoPlay: false,
          deviceOrientationsAfterFullScreen: [
            DeviceOrientation.portraitUp,
            DeviceOrientation.portraitDown
          ],
          fit: BoxFit.cover,
          controlsConfiguration: BetterPlayerControlsConfiguration(
            iconsColor: getColor(context, white),
            progressBarBufferedColor: getColor(context, grayLine),
            progressBarPlayedColor: getColor(context, accentBright),
            controlBarColor: getColor(context, toolbarShadow),
            enableSkips: false,
            enableSubtitles: false,
            enableQualities: false,
            showControls: true,
            showControlsOnInitialize: true,
            controlsHideTime: Duration(milliseconds: 300),
            enablePlaybackSpeed: false,
            enablePlayPause: true,
            enableOverflowMenu: true,
            pauseIcon: Icons.pause_circle_filled,
            playIcon: Icons.play_circle_filled,
            enableFullscreen: true,
          )
        ),
      ),
    );
  }
}
