import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nui_core/nui_core.dart';

class PageViewIndicator extends StatefulWidget {
  static const double _defaultSize = 8.0;
  static const double _defaultSelectedSize = 8.0;
  static const double _defaultSpacing = 8.0;
  static const Color _defaultDotColor = const Color(0x509E9E9E);
  static const Color _defaultSelectedDotColor = Colors.grey;

  final ValueNotifier<int> currentPageNotifier;
  final int itemCount;
  final ValueChanged<int> onPageSelected;
  final Color dotColor;
  final Color selectedDotColor;
  final double size;
  final double selectedSize;
  final double dotSpacing;
  final double borderWidth;
  final Color borderColor;
  final Color selectedBorderColor;

  PageViewIndicator({
    Key key,
    @required this.currentPageNotifier,
    @required this.itemCount,
    this.onPageSelected,
    this.size = _defaultSize,
    this.dotSpacing = _defaultSpacing,
    Color dotColor,
    Color selectedDotColor,
    this.selectedSize = _defaultSelectedSize,
    this.borderWidth = 0,
    this.borderColor,
    this.selectedBorderColor,
  })  : this.dotColor = dotColor ??
      ((selectedDotColor?.withAlpha(150)) ?? _defaultDotColor),
        this.selectedDotColor = selectedDotColor ?? _defaultSelectedDotColor,
        assert(borderWidth < size, 'Border width cannot be bigger than dot size'),
        super(key: key);

  @override
  PageViewIndicatorState createState() {
    return new PageViewIndicatorState();
  }
}

class PageViewIndicatorState extends State<PageViewIndicator>{
  int _currentPageIndex = 0;
  Color _borderColor;
  Color _selectedBorderColor;

  @override
  void initState() {
    _readCurrentPageIndex();
    widget.currentPageNotifier.addListener(_handlePageIndex);
    _borderColor = widget.borderColor ?? widget.dotColor;
    _selectedBorderColor =
        widget.selectedBorderColor ?? widget.selectedDotColor;
    super.initState();
  }

  @override
  void dispose() {
    widget.currentPageNotifier.removeListener(_handlePageIndex);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
        alignment: WrapAlignment.center,
        crossAxisAlignment: WrapCrossAlignment.center,
        children: List<Widget>.generate(widget.itemCount, (int index) {
          double size = widget.size;
          Color color = widget.dotColor;
          Color borderColor = _borderColor;
          if (isSelected(index)) {
            size = widget.selectedSize;
            color = widget.selectedDotColor;
            borderColor = _selectedBorderColor;
          }
          return GestureDetector(
            onTap: () => widget.onPageSelected == null ? null : widget.onPageSelected(index),
            child: Container(
              width: size,
              height: size,
              margin: EdgeInsets.only(left: widget.dotSpacing/2, right: widget.dotSpacing/2),
              decoration: widget.borderWidth > 0 ?
                NUIOutlineDecoration(
                  thickness: widget.borderWidth,
                  outlineColor: borderColor,
                  borderRadius: BorderRadius.circular(30)
                ) : NUIBoxDecoration(
                  color: color,
                  borderRadius: BorderRadius.circular(30)
                ),
              child: Container(
                width: size,
                height: size,
                child: Center(
                  child: Container(
                    width: size - widget.borderWidth,
                    height: size - widget.borderWidth,
                    decoration: NUIBoxDecoration(
                      color: color,
                      borderRadius: BorderRadius.circular(30)
                    ),
                  ),
                ),
              ),
            ),
          );
        }));
  }

  bool isSelected(int dotIndex) => _currentPageIndex == dotIndex;

  _handlePageIndex() {
    setState(_readCurrentPageIndex);
  }

  _readCurrentPageIndex() {
    _currentPageIndex = widget.currentPageNotifier.value;
  }
}
