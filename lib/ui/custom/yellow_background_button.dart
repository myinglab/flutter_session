import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_animation/nui_animation.dart';
import 'package:nui_core/nui_core.dart';
import 'package:nui_core/ui/component/nui_search_text.dart';
import 'package:nui_form/nuif_form.dart';

typedef YellowButtonCallback = void Function(bool isLoading, String text);
class YellowButtonController{
  YellowButtonCallback _callback;
  void updateState(bool isLoading, String text){
    if(_callback != null) _callback(isLoading, text);
  }
}

class YellowBackgroundButton extends StatefulWidget {
  final String text;
  final Function onTap;
  final EdgeInsetsGeometry padding;
  final YellowButtonController controller;

  YellowBackgroundButton({ @required this.text, this.onTap, this.padding, this.controller});

  @override
  _YellowBackgroundButtonState createState() => _YellowBackgroundButtonState();
}

class _YellowBackgroundButtonState extends State<YellowBackgroundButton> with TickerProviderStateMixin{
  bool _isLoading = false;
  String _text;
  YellowButtonController _controller;

  @override
  void initState(){
    super.initState();
    _text = widget.text;
    _controller = widget.controller ?? YellowButtonController();
    _controller._callback = (isLoading, text){
      setState(() {
        _isLoading = isLoading;
        _text = text ?? _text;
      });
    };
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: widget.padding ?? EdgeInsets.only(left: 16, right: 16),
      child: NUIBouncingAnimation(
        scaleFactor: 0.3,
        onTap: (){
          if(_isLoading) return;
          widget.onTap();
        },
        child: Container(
          alignment: Alignment.center,
          height: 48,
          decoration: NUIBoxDecoration(
            color: getColor(context, accentBright),
            borderRadius: BorderRadius.circular(30),
            shadowRadius: 8,
            shadowOffset: Offset(0, 2),
            shadowColor: getColor(context, accentBright).withOpacity(0.6)
          ),
          child: AnimatedSize(
            duration: Duration(milliseconds: 250),
            vsync: this,
            child: AnimatedSwitcher(
              duration: Duration(milliseconds: 250),
              child: _isLoading != true ? Container(
                width: double.infinity,
                alignment: Alignment.center,
                child: Text(_text, style: getFont(context, size: 16, color: textWhite, isBold: true))
              ) :
              Container(
                width: 48,
                height: 48,
                padding: EdgeInsets.all(10),
                child: CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(getColor(context, backgroundWhite)), strokeWidth: 3.0, backgroundColor: getColor(context, backgroundWhite).withOpacity(0.4))
              ),
            ),
          ),
        ),
      ),
    );
  }
}

