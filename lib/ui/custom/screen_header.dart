import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_animation/nui_button_animation.dart';

import 'package:nui_core/nui_core.dart';


class ScreenHeader extends StatelessWidget {
  final bool showBackButton;
  final BuildContext screenContext;
  final Color backIconColor;
  final Function onBackPressed;

  ScreenHeader(this.showBackButton, this.screenContext,
      {this.backIconColor, this.onBackPressed});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        NUIStatusBar(),
        showBackButton
            ? Container(
                width: double.infinity,
                height: 40,
                child: NUIRow(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.max,
                  showDividerEnd: true,
                  showDividerStart: true,
                  children: [
                    NUIBouncingAnimation(
                        scaleFactor: 0.5,
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(16, 0, 16, 0),
                            child: loadSvg("back_ui.svg", width: 28, height: 28, color: backIconColor)
                        ),
                        onTap: () {
                          onBackPressed(context);
                        }),
                    // NUIBouncingAnimation(
                    //     scaleFactor: 0.5,
                    //     child: Padding(
                    //       padding: EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
                    //       child: Text("LOGIN", style: getFont(context, size: 14, isBold: true, color: accentBright)),
                    //     ),
                    //     onTap: (){
                    //
                    //     }
                    // )
                  ],
                ),
              )
            : Container()
      ],
    );
  }
}
