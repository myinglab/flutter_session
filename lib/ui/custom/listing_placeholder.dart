import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_core/nui_core.dart';

class ListingPlaceHolder extends StatelessWidget {
  ListingPlaceHolder({this.title, this.desc});
  final String title;
  final String desc;
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.only(top: screenHeight(context) * 0.2, left: 15, right: 15),
      child: NUIColumn(
        divider: Divider(height: 16, color: Colors.transparent),
        children: [
          Image(
            image: AssetImage("assets/images/error_msg.png")
          ),
          Padding(
            padding: EdgeInsets.only(left: 35, right: 35),
            child: Text(title ?? "Oops!", style: getFont(context, size: 18, color: textBlack), textAlign: TextAlign.center,),
          ),
          Padding(
            padding: EdgeInsets.only(left: 35, right: 35),
            child: Text(desc ?? "Something went wrong!", style: getFont(context, size: 16, color: textDarkGray), textAlign: TextAlign.center,),
          ),
        ],
      )
    );
  }
}


class ListingPagination extends StatelessWidget {
  @override
  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        child: Lottie.asset(
          'assets/lottie/bouncing_ball_lottie.json',
          width: screenWidth(context) * 0.8,
          height: 80,
        )
    );
  }
}
