import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nesternship_flut/data/model/country_model.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_animation/nui_animation.dart';
import 'package:nui_core/nui_core.dart';
import 'package:nui_core/ui/component/nui_search_text.dart';

class SearchToolbar extends StatefulWidget {
  SearchToolbar(this.searchController, this.searchHint, this.hasSearchIcon, this.hasCrossIcon, this.searchQuery);
  final NUISearchTextController searchController;
  final String searchHint;
  final bool hasSearchIcon, hasCrossIcon;
  final String searchQuery;
  @override
  _SearchToolbarState createState() => _SearchToolbarState();
}

class _SearchToolbarState extends State<SearchToolbar> {
  NUISearchTextController controller;

  @override
  void initState() {
    super.initState();
    controller = widget.searchController ?? NUISearchTextController(searchTrigger: (keyword) {});
  }

  @override
  Widget build(BuildContext context) {
    if (widget.searchQuery.isNotEmpty) {
      controller.setKeyword(widget.searchQuery);
      controller.searchTrigger(widget.searchQuery);
    }
    return Container(
      width: double.infinity,
      decoration: toolbarWhiteDecoration(context),
      child: Column(
        children: [
          NUIStatusBar(),
          Container(
            height: 48,
            width: double.infinity,
            child: Padding(
              padding: EdgeInsets.only(left: 24, right: 8),
              child: NUIRow(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                divider: VerticalDivider(width: 10, color: NUIColors.NUITransparent),
                children: [
                  widget.hasSearchIcon
                      ? Icon(
                          Icons.search,
                          size: 18,
                          color: getColor(context, textGray),
                        )
                      : null,
                  Expanded(
                    child: NUISearchText(
                      controller: controller,
                      hint: widget.searchHint,
                      initialValue: widget.searchQuery,
                      searchOnChanged: true,
                      showDone: true,
                      searchDelay: Duration(seconds: 1),
                      hintTextStyle: getFont(context, color: textLightGray, size: 16),
                      textStyle: getFont(context, color: textBlack, size: 16),
                    ),
                  ),
                  widget.hasCrossIcon
                      ? NUIBouncingAnimation(
                          child: Padding(
                            padding: EdgeInsets.all(10),
                            child: Icon(
                              Icons.close,
                              color: getColor(context, accentBright),
                              size: 28,
                            ),
                          ),
                          onTap: () {
                            popWithResult<Country>(context, Country());
                          })
                      : null
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
