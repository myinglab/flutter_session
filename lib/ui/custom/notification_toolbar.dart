import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nesternship_flut/data/model/country_model.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_animation/nui_animation.dart';
import 'package:nui_core/nui_core.dart';
import 'package:nui_core/ui/component/nui_search_text.dart';

class NotificationToolbar extends StatefulWidget {
  NotificationToolbar(this.notificationTitle);

  final String notificationTitle;

  @override
  _NotificationToolbarState createState() => _NotificationToolbarState();
}

class _NotificationToolbarState extends State<NotificationToolbar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: toolbarWhiteDecoration(context),
      child: Column(
        children: [
          NUIStatusBar(),
          Container(
            height: 48,
            width: double.infinity,
            child: Padding(
              padding: EdgeInsets.only(left: 24, right: 8),
              child: NUIRow(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                divider:
                    VerticalDivider(width: 10, color: NUIColors.NUITransparent),
                children: [
                  Expanded(
                    child: Text(widget.notificationTitle,
                        style: getFont(context, size: 16, color: textGray)),
                  ),
                  NUIBouncingAnimation(
                      child: Padding(
                        padding: EdgeInsets.all(10),
                        child: Icon(
                          Icons.close,
                          color: getColor(context, accentBright),
                          size: 28,
                        ),
                      ),
                      onTap: () {
                        popWithResult<Country>(context, Country());
                      })
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
