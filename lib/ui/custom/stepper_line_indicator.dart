import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nui_core/nui_core.dart';

class StepperLineIndicator extends StatefulWidget {
  static const double _defaultSize = 4.0;
  static const double _defaultSelectedSize = 8.0;
  static const double _defaultSpacing = 8.0;
  static Color _defaultIndicatorColor = Colors.white.withOpacity(0.6);
  static Color _defaultSelectedIndicatorColor = Colors.white;

  final ValueNotifier<int> currentPageNotifier;
  final int itemCount;
  final ValueChanged<int> onPageSelected;
  final Color indicatorColor;
  final Color selectedIndicatorColor;
  final double size;
  final double selectedSize;
  final double spacing;

  StepperLineIndicator({
    Key key,
    @required this.currentPageNotifier,
    @required this.itemCount,
    this.onPageSelected,
    this.size = _defaultSize,
    this.spacing = _defaultSpacing,
    Color color,
    Color selectedColor,
    this.selectedSize = _defaultSelectedSize,
  })  : this.indicatorColor = color ??
      ((selectedColor?.withAlpha(150)) ?? _defaultIndicatorColor),
        this.selectedIndicatorColor = selectedColor ?? _defaultSelectedIndicatorColor,
        super(key: key);

  @override
  StepperLineIndicatorState createState() {
    return new StepperLineIndicatorState();
  }
}

class StepperLineIndicatorState extends State<StepperLineIndicator> with TickerProviderStateMixin{
  int _currentPageIndex = 0;

  @override
  void initState() {
    _readCurrentPageIndex();
    widget.currentPageNotifier.addListener(_handlePageIndex);
    super.initState();
  }

  @override
  void dispose() {
    widget.currentPageNotifier.removeListener(_handlePageIndex);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: widget.selectedSize ?? StepperLineIndicator._defaultSelectedSize,
      child: NUIRow(
        divider: VerticalDivider(width: widget.spacing ?? StepperLineIndicator._defaultSpacing, color: NUIColors.NUITransparent),
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: List<Widget>.generate(widget.itemCount, (int index) {
          double size = widget.size ?? StepperLineIndicator._defaultSize;
          double selectedSize = widget.selectedSize ?? StepperLineIndicator._defaultSelectedSize;
          Color color = widget.indicatorColor ?? StepperLineIndicator._defaultIndicatorColor;
          Color selectedColor = widget.selectedIndicatorColor ?? StepperLineIndicator._defaultSelectedIndicatorColor;

          bool selected = isSelected(index);

          return Expanded(
            child: GestureDetector(
              onTap: () => widget.onPageSelected == null ? null : widget.onPageSelected(index),
              child: AnimatedSize(
                vsync: this,
                duration: Duration(milliseconds: 250),
                child: Container(
                  width: double.infinity,
                  height: selected ? selectedSize : size,
                  color: selected ? selectedColor : color
                ),
              ),
            ),
          );
        })
      ),
    );
  }

  bool isSelected(int dotIndex) => dotIndex <= _currentPageIndex;

  _handlePageIndex() {
    setState(_readCurrentPageIndex);
  }

  _readCurrentPageIndex() {
    _currentPageIndex = widget.currentPageNotifier.value;
  }
}
