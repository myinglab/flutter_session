import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:nesternship_flut/ui/style/app_colors.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_animation/nui_animation.dart';
import 'package:nui_core/nui_core.dart';
import 'package:nui_core/ui/component/nui_search_text.dart';
import 'package:nui_form/nuif_form.dart';

class AppleLoginButton extends StatelessWidget {
  AppleLoginButton({@required this.text, this.onTap});
  final String text;
  final Function onTap;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 16, right: 16),
      child: NUIBouncingAnimation(
        scaleFactor: 0.3,
        onTap: (){
          if(onTap != null) onTap();
        },
        child: Container(
          alignment: Alignment.center,
          height: 48,
          width: double.infinity,
          decoration: NUIBoxDecoration(
              color: getColor(context, black),
              borderRadius: BorderRadius.circular(30),
              shadowRadius: 16,
              shadowOffset: Offset(1, 2),
              shadowColor: getColor(context, toolbarShadow)
          ),
          child: NUIRow(
            divider: VerticalDivider(width: 20, color: NUIColors.NUITransparent),
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              loadSvg("apple_login_icon.svg", width: 24, height: 24, color: AppColors.AppleIcon),
              Text(text, style: getFont(context, size: 16, color: white, isBold: true),),
            ],
          ),
        ),
      ),
    );
  }
}

