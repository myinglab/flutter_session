import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_animation/nui_animation.dart';
import 'package:nui_core/nui_core.dart';

void showConfirmCancelDialog(BuildContext context, {@required String title, RichText textSpan, RichText loadingTextSpan, String loadingText, bool showCancel: true, @required String confirmButton, @required String cancelButton, @required ConfirmCancelDialogController handler}){
  showNUIDialog(
      transition: PageTransition.BLUR_BACKGROUND,
      transitionDuration: Duration(milliseconds: 250),
      context: context,
      dismissible: false,
      content: (context) => ConfirmCancelDialog(title: title, confirmButton: confirmButton, loadingTextSpan: loadingTextSpan, showCancel: showCancel, controller: handler, loadingText: loadingText, textSpan:textSpan, cancelButton: cancelButton)
  );
}

void showSessionExpiredDialog(BuildContext context, {@required String title, RichText textSpan, RichText loadingTextSpan, String loadingText, bool showCancel: true, @required String confirmButton, @required String cancelButton, @required ConfirmCancelDialogController handler}){
  if(SessionExpiredDialog.isShowing == true) return;
  showNUIDialog(
      transition: PageTransition.BLUR_BACKGROUND,
      transitionDuration: Duration(milliseconds: 250),
      context: context,
      dismissible: false,
      content: (context) => SessionExpiredDialog(title: title, confirmButton: confirmButton, loadingTextSpan: loadingTextSpan, showCancel: showCancel, controller: handler, loadingText: loadingText, textSpan:textSpan, cancelButton: cancelButton)
  );
}

typedef ConfirmCancelDialogUpdate = void Function({String text, RichText textSpan, String confirmButton, bool confirmDismiss});

class ConfirmCancelDialogController{
  ConfirmCancelDialogController({this.onLoadingInitiated, this.onConfirmClickListener, this.onCancelClickListener, this.onConfirmDismissListener});
  Function onLoadingInitiated;
  Function onConfirmDismissListener;
  Function _stopLoadingListener;
  Function onConfirmClickListener;
  Function onCancelClickListener;
  Function _dismissListener;
  Function _initiateLoadingListener;
  ConfirmCancelDialogUpdate _updateListener;

  void updateTitle({String text, RichText textSpan, String confirmButton, bool confirmDismiss}){
    if(_updateListener != null) _updateListener(text: text, textSpan: textSpan, confirmButton: confirmButton, confirmDismiss: confirmDismiss);
  }

  void _confirmClicked(){
    if(onConfirmClickListener != null) onConfirmClickListener();
  }

  void _cancelClicked(){
    if(onCancelClickListener != null) onCancelClickListener();
  }

  void _confirmDismiss(){
    if(onConfirmDismissListener != null) onConfirmDismissListener();
  }

  void _loadingInitiated(){
    if(onLoadingInitiated != null) onLoadingInitiated();
  }

  void initiateLoading(){
    if(_initiateLoadingListener != null) _initiateLoadingListener();
  }

  void stopLoading(){
    if(_stopLoadingListener != null) _stopLoadingListener();
  }

  void dismiss(){
    if(_dismissListener != null) _dismissListener();
  }
}

class ConfirmCancelDialog extends StatefulWidget {
  ConfirmCancelDialog({@required this.title, @required this.confirmButton, this.textSpan, this.loadingText, this.loadingTextSpan, @required this.controller, @required this.cancelButton, this.showCancel: true});
  final ConfirmCancelDialogController controller;
  final RichText textSpan;
  final String title;
  final String confirmButton;
  final String cancelButton;
  final String loadingText;
  final RichText loadingTextSpan;
  final bool showCancel;

  @override
  _ConfirmCancelDialogState createState() => _ConfirmCancelDialogState();
}

class _ConfirmCancelDialogState extends State<ConfirmCancelDialog> with TickerProviderStateMixin{
  bool _isLoading = false;
  RichText _textSpan;
  String _title;
  String _confirmButton;
  bool _confirmDismiss = false;

  @override
  void initState(){
    super.initState();
    _textSpan = widget.textSpan;
    _title = widget.title;
    _confirmButton = widget.confirmButton;

    widget.controller._updateListener = ({String text, RichText textSpan, String confirmButton, bool confirmDismiss}){
      setState(() {
        _textSpan = textSpan ?? (text.isNullOrEmpty() ? _textSpan : null);
        _title = text ?? _title;
        _confirmButton = confirmButton ?? _confirmButton;
        _confirmDismiss = confirmDismiss ?? _confirmDismiss;
      });
    };
    widget.controller._initiateLoadingListener = (){
      _startLoading();
    };
    widget.controller._stopLoadingListener = (){
      setState(() {
        _isLoading = false;
      });
    };
  }

  void _startLoading(){
    setState(() {
      _isLoading = true;
    });
  }

  bool _hasLoadingState(){
    return !widget.loadingText.isNullOrEmpty() || widget.loadingTextSpan != null;
  }

  @override
  Widget build(BuildContext context) {
    widget.controller._dismissListener = (){
      pop(context);
    };

    return WillPopScope(
      onWillPop: () async{
        return _isLoading != true;
      },
      child: ListView(
        shrinkWrap: true,
        children: [
          Container(
            alignment: Alignment.center,
            margin: EdgeInsets.all(50),
            padding: EdgeInsets.only(left: 15, right: 15, top: 20, bottom: 15),
            decoration: BoxDecoration(
                color: getColor(context, background),
                borderRadius: BorderRadius.circular(15)
            ),
            child: NUIColumn(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              divider: Divider(height: 10, color: Colors.transparent),
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 20),
                  child: _isLoading != true ? _textSpan ?? Text(_title, style: getFont(context, size: 16, color: textDarkGray), textAlign: TextAlign.center,)
                      : widget.loadingTextSpan ?? Text(widget.loadingText, style: getFont(context, size: 16, color: textDarkGray), textAlign: TextAlign.center,),
                ),
                NUIBouncingAnimation(
                    scaleFactor: 0.3,
                    child: Container(
                        width: double.infinity,
                        padding: EdgeInsets.all(10),
                        alignment: Alignment.center,
                        decoration: NUIBoxDecoration(
                            gradient: NUILinearGradient(
                                colors: [getColor(context, accentBright), getColor(context, accentBright)],
                                orientation: GradientOrientation.TL_BR
                            ),
                            borderRadiusCircular: 10,
                            shadowOffset: Offset(1,1),
                            shadowColor: getColor(context, accentBright).withOpacity(0.5),
                            shadowRadius: 6
                        ),
                        child: AnimatedSize(
                          duration: Duration(milliseconds: 150),
                          curve: Curves.elasticInOut,
                          vsync: this,
                          child: _isLoading != true ? Text(_confirmButton, style: getFont(context, size: 14, isBold: true, color: textWhite)) :
                          SizedBox(width: 30, height: 30,child: CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(getColor(context, backgroundWhite)), strokeWidth: 3.0, backgroundColor: getColor(context, backgroundWhite).withOpacity(0.4))),
                        )
                    ),
                    onTap: (){
                      if(_confirmDismiss == true){
                        pop(context);
                        widget.controller._confirmDismiss();
                      }
                      else if(_hasLoadingState()){
                        if(_isLoading != true) {
                          _startLoading();
                          widget.controller._loadingInitiated();
                        }
                      }
                      else {
                        widget.controller._confirmClicked();
                      }
                    }
                ),
                widget.showCancel == true && _confirmDismiss != true ? NUIBouncingAnimation(
                    scaleFactor: 0.3,
                    child: Container(
                      width: double.infinity,
                      padding: EdgeInsets.all(10),
                      alignment: Alignment.center,
                      decoration: NUIBoxDecoration(
                          color: Colors.transparent,
                          borderRadius: BorderRadius.circular(10)
                      ),
                      child: Text(widget.cancelButton, style: getFont(context, size: 14, isBold: true, color: accentBright)),
                    ),
                    onTap: (){
                      if(_isLoading != true) {
                        if(_confirmDismiss == true){
                          pop(context);
                        }
                        else {
                          widget.controller._cancelClicked();
                          if (widget.controller.onCancelClickListener == null) {
                            //Only dismiss when no custom listener is implemented
                            pop(context);
                          }
                        }
                      }
                    }
                ) : null,
              ],
            ),
          ),
        ],
      ),
    );
  }
}



class SessionExpiredDialog extends StatefulWidget {
  SessionExpiredDialog({@required this.title, @required this.confirmButton, this.textSpan, this.loadingText, this.loadingTextSpan, @required this.controller, @required this.cancelButton, this.showCancel: true});

  static bool isShowing = false;

  final ConfirmCancelDialogController controller;
  final RichText textSpan;
  final String title;
  final String confirmButton;
  final String cancelButton;
  final String loadingText;
  final RichText loadingTextSpan;
  final bool showCancel;

  @override
  _SessionExpiredDialogState createState() => _SessionExpiredDialogState();
}

class _SessionExpiredDialogState extends State<SessionExpiredDialog> with TickerProviderStateMixin{
  bool _isLoading = false;
  RichText _textSpan;
  String _title;
  String _confirmButton;
  bool _confirmDismiss = false;

  @override
  void initState(){
    super.initState();
    SessionExpiredDialog.isShowing = true;
    _textSpan = widget.textSpan;
    _title = widget.title;
    _confirmButton = widget.confirmButton;

    widget.controller._updateListener = ({String text, RichText textSpan, String confirmButton, bool confirmDismiss}){
      setState(() {
        _textSpan = textSpan ?? (text.isNullOrEmpty() ? _textSpan : null);
        _title = text ?? _title;
        _confirmButton = confirmButton ?? _confirmButton;
        _confirmDismiss = confirmDismiss ?? _confirmDismiss;
      });
    };
    widget.controller._initiateLoadingListener = (){
      _startLoading();
    };
    widget.controller._stopLoadingListener = (){
      setState(() {
        _isLoading = false;
      });
    };
  }

  void _startLoading(){
    setState(() {
      _isLoading = true;
    });
  }

  bool _hasLoadingState(){
    return !widget.loadingText.isNullOrEmpty() || widget.loadingTextSpan != null;
  }

  @override
  void dispose(){
    super.dispose();
    SessionExpiredDialog.isShowing = false;
  }

  @override
  Widget build(BuildContext context) {
    widget.controller._dismissListener = (){
      pop(context);
    };

    return WillPopScope(
      onWillPop: () async{
        return _isLoading != true;
      },
      child: ListView(
        shrinkWrap: true,
        children: [
          Container(
            alignment: Alignment.center,
            margin: EdgeInsets.all(50),
            padding: EdgeInsets.only(left: 15, right: 15, top: 20, bottom: 15),
            decoration: BoxDecoration(
                color: getColor(context, background),
                borderRadius: BorderRadius.circular(15)
            ),
            child: NUIColumn(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              divider: Divider(height: 10, color: Colors.transparent),
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 20),
                  child: _isLoading != true ? _textSpan ?? Text(_title, style: getFont(context, size: 16, color: textDarkGray), textAlign: TextAlign.center,)
                      : widget.loadingTextSpan ?? Text(widget.loadingText, style: getFont(context, size: 16, color: textDarkGray), textAlign: TextAlign.center,),
                ),
                NUIBouncingAnimation(
                    scaleFactor: 0.3,
                    child: Container(
                        width: double.infinity,
                        padding: EdgeInsets.all(10),
                        alignment: Alignment.center,
                        decoration: NUIBoxDecoration(
                            gradient: NUILinearGradient(
                                colors: [getColor(context, accentBright), getColor(context, accentBright)],
                                orientation: GradientOrientation.TL_BR
                            ),
                            borderRadiusCircular: 10,
                            shadowOffset: Offset(1,1),
                            shadowColor: getColor(context, accentBright).withOpacity(0.5),
                            shadowRadius: 6
                        ),
                        child: AnimatedSize(
                          duration: Duration(milliseconds: 150),
                          curve: Curves.elasticInOut,
                          vsync: this,
                          child: _isLoading != true ? Text(_confirmButton, style: getFont(context, size: 14, isBold: true, color: textWhite)) :
                          SizedBox(width: 30, height: 30,child: CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(getColor(context, backgroundWhite)), strokeWidth: 3.0, backgroundColor: getColor(context, backgroundWhite).withOpacity(0.4))),
                        )
                    ),
                    onTap: (){
                      if(_confirmDismiss == true){
                        pop(context);
                        widget.controller._confirmDismiss();
                      }
                      else if(_hasLoadingState()){
                        if(_isLoading != true) {
                          _startLoading();
                          widget.controller._loadingInitiated();
                        }
                      }
                      else {
                        widget.controller._confirmClicked();
                      }
                    }
                ),
                widget.showCancel == true && _confirmDismiss != true ? NUIBouncingAnimation(
                    scaleFactor: 0.3,
                    child: Container(
                      width: double.infinity,
                      padding: EdgeInsets.all(10),
                      alignment: Alignment.center,
                      decoration: NUIBoxDecoration(
                          color: Colors.transparent,
                          borderRadius: BorderRadius.circular(10)
                      ),
                      child: Text(widget.cancelButton, style: getFont(context, size: 14, isBold: true, color: accentBright)),
                    ),
                    onTap: (){
                      if(_isLoading != true) {
                        if(_confirmDismiss == true){
                          pop(context);
                        }
                        else {
                          widget.controller._cancelClicked();
                          if (widget.controller.onCancelClickListener == null) {
                            //Only dismiss when no custom listener is implemented
                            pop(context);
                          }
                        }
                      }
                    }
                ) : null,
              ],
            ),
          ),
        ],
      ),
    );
  }
}

