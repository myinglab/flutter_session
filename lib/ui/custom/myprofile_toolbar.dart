import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:nesternship_flut/bloc/user_bloc.dart';
import 'package:nesternship_flut/data/model/country_model.dart';
import 'package:nesternship_flut/ui/screen/landing_screen.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_animation/nui_animation.dart';
import 'package:nui_core/nui_core.dart';

import 'confirm_cancel_dialog.dart';

class MyProfileToolbar extends StatefulWidget {
  @override
  _MyProfileToolbarState createState() => _MyProfileToolbarState();
}

class _MyProfileToolbarState extends State<MyProfileToolbar> {
  UserBloc userBloc;

  @override
  void initState(){
    super.initState();
    userBloc = UserBloc();
  }


  void _logout(BuildContext context) async{
    ConfirmCancelDialogController controller;
    controller = ConfirmCancelDialogController(onConfirmClickListener: () async{
      final logOut = await userBloc.logout();
      if(logOut == true) {
        popAndPush(context, LandingScreen(), transition: PageTransition.FADE, duration: Duration(milliseconds: 250));
      }
    }, onCancelClickListener: (){
      controller.dismiss();
    });

    showConfirmCancelDialog(context, title: "Would you like to sign out from your account?", confirmButton: "Logout", cancelButton: "Cancel", handler: controller);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: toolbarWhiteDecoration(context),
      child: Column(
        children: [
          NUIStatusBar(),
          Container(
            height: 48,
            width: double.infinity,
            child: Padding(
              padding: EdgeInsets.only(left: 8, right: 8),
              child: NUIRow(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                divider:
                    VerticalDivider(width: 10, color: NUIColors.NUITransparent),
                children: [
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.only(left: 16),
                      child: Text(getString("toolbarMyProfile"),
                          style:
                              getFont(context, size: 16, color: textDarkGray)),
                    ),
                  ),
                  NUIBouncingAnimation(
                    child: Padding(
                        padding: EdgeInsets.all(10),
                        child: Text(getString("toolbarLogOut"),
                            style: getFont(context,
                                size: 12,
                                color: accentBright,
                                isBold: true))),
                    onTap: () {
                      _logout(context);
                    }
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
