import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:nesternship_flut/bloc/notification_bloc.dart';
import 'package:nesternship_flut/data/model/country_model.dart';
import 'package:nesternship_flut/ui/screen/notification/notification_listing_screen.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_animation/nui_animation.dart';
import 'package:nui_core/nui_core.dart';

class NotificationListingToolbar extends StatefulWidget {
  NotificationListingToolbar(this.userId, this.bloc, {this.clearCallback});
  final NotificationBloc bloc;
  final String userId;
  final Function clearCallback;
  @override
  _NotificationListingToolbarState createState() => _NotificationListingToolbarState();
}

class _NotificationListingToolbarState extends State<NotificationListingToolbar> {
  NotificationBloc notificationBloc;

  @override
  void initState(){
    super.initState();
    notificationBloc = widget.bloc;
  }

  void _clearNotifications() async{
//    await notificationBloc.clearNotificationList(widget.userId);
//    if(widget.clearCallback != null) widget.clearCallback();
    notificationBloc.showClearNotificationConfirmDialog(context, widget.userId, doneCallback: (){
      if(widget.clearCallback != null) widget.clearCallback();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: toolbarWhiteDecoration(context),
      child: Column(
        children: [
          NUIStatusBar(),
          Container(
            height: 48,
            width: double.infinity,
            child: Padding(
              padding: EdgeInsets.only(left: 8, right: 8),
              child: NUIRow(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                divider:
                VerticalDivider(width: 10, color: NUIColors.NUITransparent),
                children: [
                  NUIBouncingAnimation(
                      child: Padding(
                        padding: EdgeInsets.all(10),
                        child: loadSvg("back_ui.svg", width: 28, height: 28, color: getColor(context, accentBright))
//                        child: Icon(
//                          LineAwesomeIcons.angle_left,
//                          color: getColor(context, accentBright),
//                          size: 28,
//                        ),
                      ),
                      onTap: () {
                        pop(context);
                      }),
                  Expanded(
                    child: Text(
                        getString("toolbarNotificationList"),
                        style:getFont(context, size:16, color:textDarkGray)
                    ),
                  ),
                  NUIBouncingAnimation(
                      child: Padding(
                        padding: EdgeInsets.all(10),
                        child: Text(getString("toolbarNotificationClearAll"), style: getFont(context, size: 12, color: accentBright, isBold: true))
                      ),
                      onTap: () async {
                        _clearNotifications();
                      }),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}