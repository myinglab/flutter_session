import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nesternship_flut/bloc/user_bloc.dart';
import 'package:nesternship_flut/ui/custom/back_toolbar.dart';
import 'package:nesternship_flut/ui/custom/yellow_background_button.dart';
import 'package:nesternship_flut/ui/form/change_password_form.dart';
import 'package:nesternship_flut/ui/screen/landing_screen.dart';
import 'package:nui_core/nui_core.dart';
import 'package:nui_form/nuif_form.dart';
import '../style/app_theme.dart';
import 'main_screen.dart';

class ChangePasswordScreen extends StatefulWidget{
  _ChangePasswordScreenState  createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen> with TickerProviderStateMixin {
  YellowButtonController buttonController;
  NUIForm form;
  String _errorMsg;
  UserBloc userBloc;
  bool _passwordChanged = false;

  @override
  void initState(){
    super.initState();
    userBloc = UserBloc();
    buttonController = YellowButtonController();

    form = ChangePasswordForm().changePasswordForm(context, (id, value, form) {
      logNUI("IFormPreview", "Text changed on $id, cur value : $value");
    }, (id, clickType, form) {
      logNUI("IFormPreview", "Button clicked on $id, click type : $clickType");
      final values = form.extractValues();
    }, (id, selection, selected, form) {
      logNUI("IFormPreview",
          "Item selected on $id, cur value : ${selection.value}, selected : $selected");
    });
  }

  void _changePassword(BuildContext context) async{
    if(_passwordChanged == true) return; //Password changed and showing the success banner, disable click
    setState(() {
      _errorMsg = null;
    });

    final validated = form.validate(false);

    if(!validated) return;

    final values = form.extractValues();
    final currentPassword = values["currentPassword"];
    final newPassword = values["newPassword"];

    buttonController.updateState(true, null);

    final result = await userBloc.changePassword(context, currentPassword.value.value, newPassword.value.value);
    buttonController.updateState(false, null);

    if(!result.success){
      setState(() {
        _errorMsg = result.message;
      });
    }
    else{
      setState(() {
        _passwordChanged = true;
      });
      NUIAsync.delay(Duration(seconds: 2), () {
        pop(context);
      });
    }
  }

  Widget _getUpdateSuccessGreenBanner(BuildContext context){
    return Container(
        width: double.infinity,
        padding: EdgeInsets.fromLTRB(36, 14, 36, 14),
        margin: EdgeInsets.fromLTRB(32, 12, 32, 0),
        alignment: Alignment.center,
        decoration: NUIBoxDecoration(
            borderRadius:
            BorderRadius.all(Radius.circular(4)),
            color: getColor(context, greenHighlight2),
            shadowRadius: 8,
            shadowColor:
            getColor(context, greenHighlight2).withOpacity(0.3),
            shadowOffset: Offset(1, 1)),
        child: Text(
          getString("changePasswordSuccess"),
          textAlign: TextAlign.center,
          style: getFont(context, size: 14, color: textWhite),
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
        height: double.infinity,
        width: double.infinity,
        color: getColor(context, white),
        alignment: Alignment.center,
        child: NUIColumn(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              BackToolbar(getString("toolbarChangePassword")),
              Expanded(
                  child: Container(
                    width: double.infinity,
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(top: 46, left: 32, right: 32),
                    child: NUIColumn(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                              getString("enterPasswordText"),
                              style: getFont(context, size: 18, color: textBlack),
                              textAlign: TextAlign.left,
                          ),
                          Divider(height: 15, color: Colors.transparent,),
                          Text(
                            getString("passwordLengthText"),
                            style: getFont(context, size: 16, color: textLightGray2),
                            textAlign: TextAlign.left,
                          ),
                          Divider(height: 32, color: Colors.transparent,),
                          Container(
                            width: double.infinity,
                            child: NUIFormContent(form: form),
                          ),
                    ]),
                  )
              ),
              Center(
                child: Container(
                  child: AnimatedSize(
                    duration: Duration(milliseconds: 200),
                    vsync: this,
                    child: Visibility(
                        visible: _passwordChanged == true,
                        child: _getUpdateSuccessGreenBanner(context)
                    ),
                  ),
                ),
              ),
              Divider(height: 16, color: Colors.transparent),
              YellowBackgroundButton(
                text: getString("myProfileUpdateButton"),
                controller: buttonController,
                onTap: (){
                  _changePassword(context);
                },
              ),
              Container(
                width: double.infinity,
                alignment: Alignment.center,
                child: AnimatedSize(
                  duration: Duration(milliseconds: 250),
                  vsync: this,
                  child: Visibility(
                      visible: _errorMsg != null,
                      child: Padding(
                        padding: EdgeInsets.only(top: 10),
                        child: Text(_errorMsg ?? "", style: getFont(context, size: 12, color: formErrorRed, isBold: true)),
                      )
                  ),
                ),
              ),
              Divider(height: 60, color: Colors.transparent,)
            ]
        ),
      ),
    );
  }
}

class ChangePasswordFormBody extends StatefulWidget{
  @override
  _ChangePasswordFormBodyState createState() => _ChangePasswordFormBodyState();
}

class _ChangePasswordFormBodyState extends State<ChangePasswordFormBody> {
  NUIForm form;

  @override
  void initState(){
    super.initState();
    form = ChangePasswordForm().changePasswordForm(context, (id, value, form) {
      logNUI("IFormPreview", "Text changed on $id, cur value : $value");
    }, (id, clickType, form) {
      logNUI("IFormPreview", "Button clicked on $id, click type : $clickType");
      final values = form.extractValues();
    }, (id, selection, selected, form) {
      logNUI("IFormPreview",
          "Item selected on $id, cur value : ${selection.value}, selected : $selected");
    });
  }

  Widget _renderForm(BuildContext context) {
    return NUIFormContent(form: form);
  }

  @override
  Widget build(BuildContext context) {
    return NUIColumn(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Container(
          width: double.infinity,
          child: _renderForm(context),
        ),
      ],
    );
  }
}

