import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:nesternship_flut/ui/custom/back_toolbar.dart';
import 'package:nesternship_flut/ui/custom/yellow_background_button.dart';
import 'package:nesternship_flut/ui/screen/landing_screen.dart';
import 'package:nui_animation/nui_button_animation.dart';
import 'package:nui_core/nui_core.dart';
import '../style/app_theme.dart';

enum SettingFrame{
  contactUs,
  aboutInternship,
  changePassword,
  logout
}
class SettingScreen extends StatefulWidget {
  SettingScreen({
    this.nationalityIcon: Icons.flag,
    this.nationality: "Malaysia",
    this.versionNumber: "1.1.0"
  });
  final String versionNumber;
  final IconData nationalityIcon;
  final String nationality;
  @override
  _SettingScreenState  createState() => _SettingScreenState();
}

class  _SettingScreenState  extends State<SettingScreen> {

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      color: getColor(context, white),
      alignment: Alignment.center,
      child:NUIColumn(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          BackToolbar(getString("settingToolbar")),
          Expanded(
            child:Padding(
              padding: EdgeInsets.only(top:11),
              child: Container(
                width: double.infinity,
                alignment: Alignment.center,
                child:NUIColumn(
                  children:[
                    nationalitySetting(context, widget.nationalityIcon, widget.nationality),
                    standardFrame(context, getString("settingContactUs"), SettingFrame.contactUs),
                    standardFrame(context, getString("settingAboutInternship"), SettingFrame.aboutInternship),
                    versionSetting(context, widget.versionNumber),
                    standardFrame(context, "Change Password", SettingFrame.changePassword),
                    standardFrame(context, getString("settingLogout"), SettingFrame.logout),
                  ]
                )
              ),
            )
          )
        ],
      ),
    );
  }
}

Widget nationalitySetting(BuildContext context, IconData icon, String nationality){
  return NUIRippleContainer(
    child: Padding(
      padding: const EdgeInsets.only(left:32, right:32, top:15, bottom:15),
      child: NUIRow(
        mainAxisSize: MainAxisSize.max,
        children: [
          Expanded(
            child: Container(
              child: Text(
                getString("settingNationality"),
                style: getFont(context,
                    size: 16, color: textLightGray),
                textAlign: TextAlign.left,
              ),
            ),
          ),
          Container(
            child: NUIRow(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Icon(
                    icon,
                    color: getColor(context, accentBright)
                ),
                Padding(
                  padding: EdgeInsets.only(left: 16),
                  child: Text(
                    nationality,
                    style: getFont(context,
                        size: 16, color: textLightGray),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    ),
  );
}

Widget versionSetting(BuildContext context, String versionNumber){
  return NUIRippleContainer(
    child: Padding(
      padding: const EdgeInsets.only(left:32, right:32, top:15, bottom:15),
      child: NUIRow(
        mainAxisSize: MainAxisSize.max,
        children: [
          Expanded(
            child: Container(
              child: Text(
                getString("settingVersion")+ versionNumber,
                style: getFont(context,
                    size: 16, color: textLightGray),
                textAlign: TextAlign.left,
              ),
            ),
          ),
          Container(
            child: NUIRow(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                RichText(
                  text:TextSpan(
                    text: getString("settingUpdateAvailability"),
                    style:getFont(context,size:16, color:accentBright),
                  )
                )
              ],
            ),
          ),
        ],
      ),
    ),
  );
}

Widget standardFrame(BuildContext context,String settingSection, SettingFrame frame){
  return NUIRippleContainer(
    highlightColor: getColor(context, accentBright).withOpacity(0.3),
    onTap:(){
      switch(frame){
          case SettingFrame.contactUs:{}
          break;
          case  SettingFrame.aboutInternship:{}
          break;
          case SettingFrame.changePassword: {}
          break;
          case SettingFrame.logout: { push(context, LandingScreen());}
          break;
        }
    },
    child: Padding(
      padding: const EdgeInsets.only(left:32, right:32, top:15, bottom:15),
      child: NUIRow(
        mainAxisSize: MainAxisSize.max,
        children: [
          Expanded(
            child: Container(
              child: Text(
                settingSection,
                style: getFont(context,
                    size: 16, color: textLightGray),
                textAlign: TextAlign.left,
              ),
            ),
          ),
        ],
      ),
    ),
  );
}













