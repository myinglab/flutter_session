import 'dart:async';
import 'dart:io';
import 'package:nesternship_flut/ui/custom/no_internet_connection_holder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_pdfview/flutter_pdfview.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:nesternship_flut/bloc/course_bloc.dart';
import 'package:nesternship_flut/data/model/search_course.dart';
import 'package:nesternship_flut/datalayer/entity/course/course_response.dart';
import 'package:nesternship_flut/ui/screen/video_viewer_screen.dart';
import 'package:nesternship_flut/ui/screen_codes.dart';
import 'package:nui_animation/nui_button_animation.dart';
import 'package:nui_core/nui_core.dart';
import 'package:nui_toolbox/nui_pdf_viewer.dart';
import 'package:path_provider/path_provider.dart';
import '../style/app_theme.dart';

void showPDFViewer(BuildContext context, {@required CourseResponse searchCourse}) {
  push(context, PDFViewerScreen(searchCourse), transition: PageTransition.BOTTOM_TOP);
}

class PDFViewerScreen extends StatefulWidget {
  PDFViewerScreen(this.searchCourse);
  final CourseResponse searchCourse;
  @override
  _PDFViewerScreenState createState() => _PDFViewerScreenState();
}

class _PDFViewerScreenState extends State<PDFViewerScreen> {
  final CourseBloc _courseBloc = CourseBloc();
  Future<bool> isCompleted;

  void getCourseStatus() async {
    _courseBloc.updateCourseStatus(widget.searchCourse.id);
    this.isCompleted = _courseBloc.getCourseStatus(widget.searchCourse.id);
  }

  @override
  void initState() {
    super.initState();
    getCourseStatus();
  }

  @override
  Widget build(BuildContext context) {
    return NUIScreen(
      overlayStyle: NUIOverlayStyle(darkStatusBarIcon: true),
      screenCode: ScreenCodes.pdfViewer,
      content: (context) => Scaffold(
          body: Container(
              color: getColor(context, white),
              alignment: Alignment.center,
              child: NUIColumn(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    NUIFutureWidget<bool>(
                        futureValue: isCompleted,
                        callback: (state, data, msg) {
                          if (state == FutureState.SUCCESS && data != null) {
                            return CourseToolBar(
                                key: Key("Loaded"),
                                searchCourse: widget.searchCourse,
                                isCompleted: data);
                          } else {
                            return CourseToolBar(
                                key: Key("Loading"),
                                searchCourse: widget.searchCourse,
                                isCompleted: false);
                          }
                        }),
                    Expanded(
                      child: NoInternetConnectionChecker(
                        child: PDFViewerContent(widget.searchCourse),
                      )
                    )
                  ]))),
    );
  }
}

class PDFViewerContent extends StatefulWidget {
  PDFViewerContent(this.searchCourse);
  final CourseResponse searchCourse;
  @override
  _PDFViewerContentState createState() => _PDFViewerContentState();
}

class _PDFViewerContentState extends State<PDFViewerContent> {
  final CourseBloc _courseBloc = CourseBloc();
  Future<bool> isCompleted;
  String pathPDF;
  final Completer<PDFViewController> _controller =
  Completer<PDFViewController>();
  int pages = 0;
  int currentPage = 0;
  bool isReady = false;
  String errorMessage = '';

  void getCourseStatus() async {
    _courseBloc.updateCourseStatus(widget.searchCourse.id);
    this.isCompleted = _courseBloc.getCourseStatus(widget.searchCourse.id);
  }

  Future<File> createFileOfPdfUrl(String urlString) async {
    Completer<File> completer = Completer();
    print("Start download file from the source url : $urlString");
    try {
      final url = urlString;
      final filename = url.substring(url.lastIndexOf("/") + 1);
      var request = await HttpClient().getUrl(Uri.parse(url));
      var response = await request.close();
      var bytes = await consolidateHttpClientResponseBytes(response);
      var dir = await getApplicationDocumentsDirectory();
      print("Download files");
      print("${dir.path}/$filename");
      File file = File("${dir.path}/$filename");

      await file.writeAsBytes(bytes, flush: true);
      completer.complete(file);
    } catch (e) {
      throw Exception('Error parsing asset file!');
    }

    return completer.future;
  }

  @override
  void initState() {
    super.initState();
    getCourseStatus();
    createFileOfPdfUrl(widget.searchCourse.sourceUrl).then((f) {
      setState(() {
        pathPDF = f.path;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
          height: double.infinity,
          width: double.infinity,
          child: Stack(
            children: [
              pathPDF == null ? Center(
                child: Container(
                    alignment: Alignment.center,
                    width: double.infinity,
                    child: CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(getColor(context, backgroundWhite)), strokeWidth: 3.0, backgroundColor: getColor(context, accentBright))),
              ) :
              PDFView(
                filePath: pathPDF,
                enableSwipe: true,
                swipeHorizontal: true,
                autoSpacing: false,
                pageFling: true,
                pageSnap: true,
                defaultPage: currentPage,
                fitPolicy: FitPolicy.BOTH,
                preventLinkNavigation:
                false,
                onRender: (_pages) {
                  setState(() {
                    pages = _pages;
                    isReady = true;
                  });
                },
                onError: (error) {
                  setState(() {
                    errorMessage = error.toString();
                  });
                  print(error.toString());
                },
                onPageError: (page, error) {
                  setState(() {
                    errorMessage = '$page: ${error.toString()}';
                  });
                  print('$page: ${error.toString()}');
                },
                onViewCreated: (PDFViewController pdfViewController) {
                  _controller.complete(pdfViewController);
                },
                onLinkHandler: (String uri) {
                  print('goto uri: $uri');
                },
                onPageChanged: (int page, int total) {
                  print('page change: $page/$total');
                  setState(() {
                    currentPage = page;
                  });
                },
              ),

              // NUIPdfViewer(
              //   documentRetriever: () async {
              //     return PDFDocument.fromFile(
              //         File(pathPDF));
              //   },
              // ),
              Container(
                height: 10,
                width: double.infinity,
                decoration: NUIBoxDecoration(
                    gradient: NUILinearGradient(colors: [
                      getColor(context, toolbarShadow),
                      NUIColors.NUITransparent
                    ], orientation: GradientOrientation.T_B)),
              )
            ],
          ),
      );
  }
}

