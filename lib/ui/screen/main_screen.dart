import 'package:flutter/cupertino.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:nesternship_flut/ui/screen/change_password_screen.dart';
import 'package:nesternship_flut/ui/screen/home_screen.dart';
import 'package:nesternship_flut/ui/screen/search_screen.dart';
import 'package:nesternship_flut/ui/screen/my_courses_screen.dart';
import 'package:nesternship_flut/ui/screen_codes.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_core/nui_core.dart';
import 'package:nui_core/ui/component/nui_screen_widget.dart';
import 'package:nui_toolbox/nui_bottom_nav_pager.dart';
import 'myprofile_screen.dart';
import 'package:nui_core/ui/component/nui_search_text.dart';
import 'package:nesternship_flut/data/model/search_criteria.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  NUIBottomNavPageController controller = new NUIBottomNavPageController();
  SearchCriteria searchCriteria = SearchCriteria();
  NUIListController listController;
  NUISearchTextController searchController;

  @override
  void initState() {
    super.initState();
    listController = NUIListController(onLoadComplete: () {
      searchController.endSearching();
    });

    searchController = NUISearchTextController(searchTrigger: (keyword) {
      logNUI("MainScreen", "Search triggered on keyword : $keyword");
      FocusScope.of(context).unfocus();
      searchCriteria.keyword = keyword;
      listController.reload();
    });
  }

  void homeScreenMoreClickListener(String category) {
    logNUI("MainScreen", "Home screen clicked on category : $category");
    if(searchCriteria.keyword == null){
      searchCriteria.keyword = category;
    }
    searchController.setKeyword(category);
//    listController.reload();
    controller.setCurrentPage(1);
  }

  @override
  Widget build(BuildContext context) {

    NUIDisplayUtil.setStatusBarStyleAttr(NUIOverlayStyle(darkStatusBarIcon: true));
    NUIDisplayUtil.setStatusBarStyleAttr(NUIOverlayStyle(darkStatusBarIcon: true));
    return NUIScreen(
      screenCode: ScreenCodes.main,
      overlayStyle: NUIOverlayStyle(darkStatusBarIcon: true),
      content: (context) => NUIBottomNavPage(
        shadowOffset: Offset(0, -3),
        shadowColor: getColor(context, toolbarShadow),
        controller: controller,
        pages: [
          HomeScreen(homeScreenMoreClickListener),
//          SearchScreen(),
          NUITabChildStatefulWidget(onScreenContent: SearchScreen(searchController: searchController, listController: listController, searchCriteria: searchCriteria), backgroundContent: SearchScreenBlank(),),
          NUITabChildStatefulWidget(onScreenContent: MyCoursesScreen(), backgroundContent: MyCoursesScreenBlank(),),
          NUITabChildStatefulWidget(onScreenContent: MyProfileScreen(), backgroundContent: MyProfileScreenBlank(),),
        ],
        navItems: [
          NUIBottomNavItem(
              id: "home",
              icon: loadSvg("nav_home.svg", color: getColor(context, textLightGray), width: 24, height: 24),
//              icon: Icon(LineAwesomeIcons.home, size: 24, color: getColor(context, textLightGray)),
              activeIcon: loadSvg("nav_home.svg", color: getColor(context, accentBright), width: 24, height: 24),
//              activeIcon: Icon(LineAwesomeIcons.home, size: 24, color: getColor(context, accentBright)),
              label: "Home"),
          NUIBottomNavItem(
              id: "search",
              icon: loadSvg("nav_search.svg", color: getColor(context, textLightGray), width: 24, height: 24),
//              icon: Icon(LineAwesomeIcons.search, size: 24, color: getColor(context, textLightGray)),
              activeIcon: loadSvg("nav_search.svg", color: getColor(context, accentBright), width: 24, height: 24),
//              activeIcon: Icon(LineAwesomeIcons.search, size: 24, color: getColor(context, accentBright)),
              label: "Search"),
          NUIBottomNavItem(
              id: "saved",
              icon: loadSvg("nav_book.svg", color: getColor(context, textLightGray), width: 24, height: 24),
//              icon: Icon(LineAwesomeIcons.book, size: 24, color: getColor(context, textLightGray)),
              activeIcon: loadSvg("nav_book.svg", color: getColor(context, accentBright), width: 24, height: 24),
//              activeIcon: Icon(LineAwesomeIcons.book, size: 24, color: getColor(context, accentBright)),
              label: "Saved"),
          NUIBottomNavItem(
              id: "profile",
              icon: loadSvg("nav_user_outline.svg", color: getColor(context, textLightGray), width: 24, height: 24),
//              icon: Icon(LineAwesomeIcons.user, size: 24, color: getColor(context, textLightGray)),
              activeIcon: loadSvg("nav_user.svg", color: getColor(context, accentBright), width: 24, height: 24),
//              activeIcon: Icon(LineAwesomeIcons.user, size: 24, color: getColor(context, accentBright)),
              label: "Profile")
        ],
        showUnselectedLabels: false,
        showSelectedLabels: false,
        animate: false,
        elevation: 10,
        backgroundColor: getColor(context, white),
        bottomNavColor: getColor(context, white),
      ),
    );
  }
}

