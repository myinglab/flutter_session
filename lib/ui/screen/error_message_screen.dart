import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:nesternship_flut/ui/custom/back_toolbar.dart';
import 'package:nesternship_flut/ui/custom/yellow_background_button.dart';
import 'package:nui_animation/nui_button_animation.dart';
import 'package:nui_core/nui_core.dart';
import '../style/app_theme.dart';


class ErrorMessageScreen extends StatefulWidget {
  ErrorMessageScreen({
    this.errorTitle: "Lorem Ipsum dolor sit...",
    this.errorMessage: "Oops!",
    this.errorDescription: "Its not you its us, we are working quickly to fix this. Try again or contact our technical operator" ,
    this.btnText: "Go Back"
  });
  final String errorTitle;
  final String errorMessage;
  final String errorDescription;
  final String btnText;
  @override
  _ErrorMessageScreenState  createState() => _ErrorMessageScreenState();
}

class  _ErrorMessageScreenState  extends State<ErrorMessageScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      color: getColor(context, white),
      alignment: Alignment.center,
      child:NUIColumn(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          BackToolbar(widget.errorTitle),
          Expanded(
              child: Container(
                width: double.infinity,
                alignment: Alignment.center,
                child: NUIColumn(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children:[
                      Expanded(
                        child:NUIColumn(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            AspectRatio(
                                aspectRatio:3/2,
                                child: Expanded(
                                  child: Image.asset("assets/images/error_msg.png") ,
                                )
                            ),
                            Padding(
                              padding:EdgeInsets.only(left:57.5, right:57.5),
                              child: NUIColumn(
                                children: [
                                  Text(
                                      widget.errorMessage,
                                      style:getFont(context, size:18, color: textBlack)
                                  ),
                                  Padding(
                                      padding:EdgeInsets.only(top: 16),
                                      child:Flexible(
                                          child:Text(
                                              widget.errorDescription,
                                              textAlign: TextAlign.center,
                                              style:getFont(context, size:16, color: textDarkGray)
                                          )
                                      )
                                  )
                                ],
                              ),
                            )
                          ],
                        )
                      ),
                      Padding(
                        padding:EdgeInsets.only(bottom: 64),
                        child: YellowBackgroundButton(
                            text:widget.btnText
                        )
                      )
                    ]
                ),
              )
          ),
        ],
      ),
    );
  }
}
