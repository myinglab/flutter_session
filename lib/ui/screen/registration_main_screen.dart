import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:nesternship_flut/bloc/registration_bloc.dart';
import 'package:nesternship_flut/datalayer/entity/registration/registration_detail.dart';
import 'package:nesternship_flut/ui/custom/screen_header.dart';
import 'package:nesternship_flut/ui/custom/yellow_background_button.dart';
import 'package:nesternship_flut/ui/form/registration_form.dart';
import 'package:nesternship_flut/ui/screen/registration_steps_screen.dart';
import 'package:nesternship_flut/ui/screen_codes.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_core/nui_core.dart';
import 'package:nui_form/nuif_form.dart';

class RegistrationMainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return NUIScreen(
        overlayStyle: NUIOverlayStyle(darkStatusBarIcon: false),
        content: (screenContext) => Scaffold(
              body: Container(
                  width: double.infinity,
                  height: double.infinity,
                  decoration: defaultGradientBackground(context),
                  child: SingleChildScrollView(
                      child: SizedBox(
                          height: MediaQuery.of(context).size.height,
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                ScreenHeader(true, context,
                                  backIconColor: getColor(context, accentBright),
                                  onBackPressed: (ctx) => pop(context),
                                ),
                                Divider(
                                    height: 40,
                                    color: NUIColors.NUITransparent),
                                Align(
                                  alignment: Alignment.center,
                                  child: Image.asset(
                                    'assets/images/logo.png',
                                    color: Colors.white,
                                    height: 54,
                                  ),
                                ),
                                Divider(
                                    height: 67,
                                    color: NUIColors.NUITransparent),
                                Padding(
                                  padding: EdgeInsets.only(left: 32, right: 32),
                                  child: Text(
                                      getString("registrationScreenTitle"),
                                      style: getFont(context,
                                          size: 21, color: textWhite)),
                                ),
                                Divider(
                                    height: 40,
                                    color: NUIColors.NUITransparent),
                                RegistrationMainFormBody(),
                              ])))),
            ),
        screenCode: ScreenCodes.splash);
  }
}

class RegistrationMainFormBody extends StatefulWidget {
  @override
  _RegistrationMainFormBodyState createState() =>
      _RegistrationMainFormBodyState();
}

class _RegistrationMainFormBodyState extends State<RegistrationMainFormBody> with TickerProviderStateMixin{
  NUIForm form;
  int curValue = 3;
  int _currentValue = 15;
  bool validated;
  final RegistrationBloc registrationBloc = new RegistrationBloc();
  YellowButtonController _controller;
  String _registerErrorMsg;

  @override
  void initState() {
    super.initState();
    _controller = YellowButtonController();
    form = RegistrationForm().emailPasswordForm(context, (id, value, form) {
      logNUI("IFormPreview", "Text changed on $id, cur value : $value");
    }, (id, clickType, form) {
      logNUI("IFormPreview", "Button clicked on $id, click type : $clickType");
      validated = form.validate(false);
      final values = form.extractValues();
    }, (id, selection, selected, form) {
      logNUI("IFormPreview",
          "Item selected on $id, cur value : ${selection.value}, selected : $selected");
    });
  }

  Widget _renderForm(BuildContext context) {
    return NUIFormContent(form: form);
  }

  Future<DataResult> _isEmailAvailable(String email) async{
    final delay = NUIAsync.delayForResult(Duration(seconds: 1), () async => false);
    final result = await registrationBloc.checkEmail(email);
    await delay;
    return result;
  }

  void _validateForm(BuildContext context) async{
    bool valid = form.validate(false);
    if (valid) {
      final values = form.extractValues();
      final email = values["edtEmail"].value.value;
      final password = values["edtPassword"].value.value;

      _controller.updateState(true, null);
      final checkEmailResult = await _isEmailAvailable(email);
      if(checkEmailResult.success == true) {
        _controller.updateState(false, null);
        final regBean = RegistrationDetail(email: email, password: password);
        push(context, RegistrationStepsScreen(regBean));
        _registerErrorMsg = null;
      }
      else{
        _controller.updateState(false, null);
        logNUI("RegMainScreen", checkEmailResult.message);
        setState(() {
          _registerErrorMsg = checkEmailResult.message.isNullOrEmpty() ? "Failed to register, please try again" : checkEmailResult.message;
        });
        // toast("This email has already been taken", context: context, textStyle: getFont(context, size: 14, color: textWhite));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Material(
        color: Colors.transparent,
        child: NUIColumn(
          mainAxisSize: MainAxisSize.max,
          children: [
            Expanded(
              child: Container(
                width: double.infinity,
                padding: EdgeInsets.only(left: 32, right: 32),
                child: _renderForm(context),
              ),
            ),
            YellowBackgroundButton(
                text: getString('registrationScreenContinueButton'),
                controller: _controller,
                onTap: () {
                  _validateForm(context);
                }),
            AnimatedSize(
              duration: Duration(milliseconds: 250),
              vsync: this,
              child: Visibility(
                  visible: _registerErrorMsg != null,
                  child: Padding(
                    padding: EdgeInsets.only(top: 10),
                    child: Text(_registerErrorMsg ?? "", style: getFont(context, size: 12, color: formErrorRed, isBold: true)),
                  )
              ),
            ),
            Divider(height: 60, color: Colors.transparent,)
          ],
        ),
      ),
    );
  }
}
