import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:iversioning/iversioning.dart';
import 'package:nesternship_flut/bloc/calendar_bloc.dart';
import 'package:nesternship_flut/bloc/course_bloc.dart';
import 'package:nesternship_flut/bloc/general_bloc.dart';
import 'package:nesternship_flut/bloc/master_bloc.dart';
import 'package:nesternship_flut/bloc/notification_bloc.dart';
import 'package:nesternship_flut/bloc/training_course_bloc.dart';
import 'package:nesternship_flut/bloc/banner_bloc.dart';
import 'package:nesternship_flut/bloc/user_bloc.dart';
import 'package:nesternship_flut/data/model/banner.dart';
import 'package:nesternship_flut/datalayer/entity/banner/banner_image_response.dart';
import 'package:nesternship_flut/datalayer/entity/category/category_response.dart';
import 'package:nesternship_flut/datalayer/entity/course/course_response.dart';
import 'package:nesternship_flut/ui/custom/confirm_cancel_dialog.dart';
import 'package:nesternship_flut/ui/custom/no_internet_connection_holder.dart';
import 'package:nesternship_flut/ui/list/app_list_repo.dart';
import 'package:nesternship_flut/ui/list/skeletal/listitem_training_skeletal.dart';
import 'package:nesternship_flut/ui/screen/landing_screen.dart';
import 'package:nesternship_flut/ui/screen_codes.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nesternship_flut/ui/custom/pageview_indicator.dart';
import 'package:nui_animation/nui_animation.dart';
import 'package:nui_auth/ui/nui_auth_activity_screen.dart';
import 'package:nui_core/nui_core.dart';
import 'package:nui_media/nui_media.dart';
import 'package:nui_toolbox/nui_header_tab_widget.dart';
import 'package:nui_toolbox/nui_persistent_bottom_sheet.dart';
import 'package:photo_manager/photo_manager.dart';
import 'notification/notification_listing_screen.dart';
import 'package:nesternship_flut/ui/custom/fading_slider.dart';

class HomeScreen extends NUIAuthScreen {

  final Function moreClickListener;

  HomeScreen(this.moreClickListener) : super(
    child: Container(
        width: double.infinity,
        height: double.infinity,
        child: MainBodyContent(moreClickListener)
    ),
    module: ScreenCodes.landing,
    overlay: NUIOverlayStyle(darkStatusBarIcon: true),
  );
}

class MainBodyContent extends StatefulWidget {
  MainBodyContent(this.moreClickListener);
  final Function moreClickListener;
  @override
  _MainBodyContentState createState() => _MainBodyContentState();
}

class _MainBodyContentState extends State<MainBodyContent> {
  @override
  Widget build(BuildContext context) {
    return NoInternetConnectionChecker(
      child: MainBody(widget.moreClickListener),
    );
  }
}


class MainBody extends StatelessWidget {

  MainBody(this.moreClickListener);

  static bool checkedAppVersion = false;

  final Function moreClickListener;
  final PageController _pageController = PageController(viewportFraction: 1.0, initialPage: 0);
  final TrainingCourseBloc _bloc = TrainingCourseBloc();
  final GeneralBloc _generalBloc = GeneralBloc();
  final NotificationBloc _notificationBloc = NotificationBloc();
  final UserBloc _userBloc = UserBloc();
  final BannerBloc _bannerBloc = BannerBloc();
  final _currentPageNotifier = ValueNotifier<int>(0);
  final CourseBloc _courseBloc = CourseBloc();
  final NUIBlocData<bool> _statusBarNotifier = NUIBlocData();
  final NUIBlocData<bool> _titleCoverNotifier = NUIBlocData();
  final NUIBlocData<double> _bannerOffsetNotifier = NUIBlocData();
  final NUIBlocData<double> _pageIndicatorOffsetNotifier = NUIBlocData();
  final NUIBlocData<double> _innerScrollOffsetNotifier = NUIBlocData();
  List<BannerImage> _banner;

  void _listenToEvent(BuildContext context) async{
    NUIBusEvent.get().listenInvoke(randomUUID(), listener: (payload) async{
      if(match(payload.code, "401")) {
        logNUI("HomeScreen", "Session expired on home screen");
          logNUI("HomeScreen", "Showing session expired on home screen");
          ConfirmCancelDialogController controller;
          controller = ConfirmCancelDialogController(onConfirmClickListener: () async {
                await UserBloc().logout();
                SessionExpiredDialog.isShowing = false;
                popAllAndPush(context, LandingScreen());
              }, onCancelClickListener: () {
                controller.dismiss();
              });

          showSessionExpiredDialog(context, title: getString("sessionExpired"),
              confirmButton: "Continue",
              cancelButton: "Cancel",
              handler: controller,
              showCancel: false);
        }
    });
  }

  void getImages() async{
    final images = await NUIMedia.get().getFromGallery();
    logNUI("HomeScreen", "Total images size: ${images?.length}");
  }

  // void _animateSlider() {
  //   Future.delayed(Duration(seconds: 5)).then((_) {
  //     int nextPage = _pageController.page.round() + 1;
  //
  //     if (nextPage == _banner.length) {
  //       nextPage = 0;
  //     }
  //
  //     _pageController
  //         .animateToPage(nextPage, duration: Duration(milliseconds: 300), curve: Curves.linear)
  //         .then((_) => _animateSlider());
  //   });
  // }

  Widget _getBanner(BuildContext context){
    return Container(
        width: double.infinity,
        height: screenWidth(context),
        color: getColor(context, grayOutline),
        child: NUIFutureWidget<List<BannerImageResponse>>(
          futureValue: _bannerBloc.getBannerImagesList(_userBloc.getUserId()),
          callback: (state, list, message) {
            if (state == FutureState.SUCCESS && list != null) {
              List<BannerImage> banner = list.map((e) {
                return BannerImage(
                  bannerId: e.bannerId,
                  bannerUrl: e.bannerUrl,
                  actionUrl: e.actionUrl,
                  bannerTitle: e.bannerTitle,
                  bannerDescription: e.bannerDescription,
                  sequence: e.sequence,
                  status: e.status
                );
              }).toList();
              this._banner = banner;

              return Stack(
                children: [
                  NUIBlocStream<double>(
                    bloc: _bannerOffsetNotifier,
                    stream: (context, status, ratio) => Container(
                      width: screenWidth(context),
                      height: status == BlocStatus.COMPLETED ? (screenWidth(context)) * ratio : (screenWidth(context)),
                      child: FadingSlider(
                        count: banner.length,
                        width: screenWidth(context),
                        height: status == BlocStatus.COMPLETED ? (screenWidth(context)) * ratio : (screenWidth(context)),
                        notifier: _currentPageNotifier,
                        carouselOptions: CarouselOptions(
                            height: screenWidth(context),
                            viewportFraction: 1.0,
                            enlargeCenterPage: false,
                            autoPlay: true,
                            autoPlayInterval: Duration(seconds: 5),
                            autoPlayAnimationDuration: Duration(milliseconds: 500),
                            autoPlayCurve: Curves.fastOutSlowIn,
                            onPageChanged: (index, reason) {
                              _currentPageNotifier.value = index;
                            }
                        ),
                        backgroundInflater: (context, position, fraction){
                          return BannerSliderImage(image: banner[position]);
                        },
                        foregroundInflater: (context, position, fraction){
                          return BannerSliderText(image: banner[position], visibility: fraction);
                        },
                      ),
                    ),
                  ),
                  NUIBlocStream<double>(
                    bloc: _pageIndicatorOffsetNotifier,
                    stream: (context, status, ratio) => Positioned(
                        right: 16,
                        bottom: status == BlocStatus.COMPLETED
                            ? (31 + 20.0) + ((screenWidth(context)) * (1 - ratio))
                            : (31 + 20.0),
                        child: PageViewIndicator(
                          currentPageNotifier: _currentPageNotifier,
                          dotSpacing: 16,
                          itemCount: list.length,
                          size: 8,
                          selectedSize: 12,
                          borderColor: getColor(context, white),
                          borderWidth: 1,
                          dotColor: NUIColors.NUITransparent,
                          selectedDotColor: getColor(context, white),
                          selectedBorderColor: getColor(context, white),
                        )),
                  )
                ],
              );
            } else {
              return Stack(
                children: [
                  NUISkeletonContainer(
                    child: NUISkeletonView(
                      width: double.infinity,
                      height: double.infinity,
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    height: double.infinity,
                    decoration: BoxDecoration(
                        gradient: NUILinearGradient(
                            colors: [
                              Colors.transparent,
                              Colors.transparent,
                              Colors.black12,
                              Colors.black26,
                              Colors.black38
                            ],
                            orientation: GradientOrientation.TL_BR
                        )
                    ),
                  ),
                ],
              );
            }
          },
        )
    );
  }

  double _getCalculatedCollapseRatio(BuildContext context){
    final height = screenHeight(context);
    final width = screenWidth(context);

    final diffHeight = height - width;
    final diffHeightMargin = diffHeight + 20.0;
    return diffHeightMargin/height;
  }

  @override
  Widget build(BuildContext context) {
    _listenToEvent(context);
    if(checkedAppVersion != true) {
      checkedAppVersion = true;
      NUIAsync.delay(Duration(seconds: 1), () {
        IVersioning.get().checkAppVersion(context);
      });
    }

    _notificationBloc.getAndUpdateNotificationCount();

    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            color: getColor(context, white),
          ),
          _getBanner(context),
          Container(
            margin: EdgeInsets.only(top: NUIDisplayUtil.statusBarHeight(context)),
            child: NUIDraggableSheet(
              collapseRatio: _getCalculatedCollapseRatio(context),
              expandAcceptMargin: 0.01,
              expandRatio: 1,
              innerScrollListener: (offset) {
                _updateInnerScrollOffset(offset);
              },
              scrollListener: (noti, expanded, collapsed) {
                _updateTopCover(expanded);
                _updateBannerOffset(noti);
              },
              decoration: NUIBoxDecoration(
                  color: getColor(context, white),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20))),
              child: ExploreBottomSheetContent(moreClickListener),
              header: NUIPersistentHeader(
                height: 69,
                child: NUIBlocStream<double>(
                  bloc: _innerScrollOffsetNotifier,
                  stream: (context, status, offset) => Container(
                    width: double.infinity,
                    height: 69,
                    decoration: NUIBoxDecoration(
                        color: getColor(context, white),
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20)),
                        shadowRadius: (offset != null && offset > 5) ? 10 : 0,
                        shadowColor: getColor(context, toolbarShadow)),
                    child: NUIColumn(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          NUIBlocStream<bool>(
                            bloc: _titleCoverNotifier,
                            stream: (context, status, expanded) =>
                                AnimatedOpacity(
                                    opacity: expanded == true ? 1.0 : 0.0,
                                    duration: Duration(milliseconds: 250),
                                    child: Container(
                                      height: 15,
                                      width: double.infinity,
                                      color: getColor(context, white),
                                    )),
                          ),
                          Container(
                            height: 44,
                            alignment: Alignment.centerLeft,
                            padding: EdgeInsets.only(left: 20, right: 20),
                            child: AnimatedSwitcher(
                                duration: Duration(milliseconds: 100),
                                child: (offset != null && offset > 5) != true
                                    ? SizedBox(
                                        key: Key("bigTitle"),
                                        width: 200,
                                        child: Text(
                                            getString("homeScreenTitle"),
                                            style: getFont(context,
                                                size: 32, color: textBlue)))
                                    : SizedBox(
                                        key: Key("smallTitle"),
                                        width: 200,
                                        child: Text(
                                            getString("homeScreenTitle"),
                                            style: getFont(context,
                                                size: 24, color: textBlue)))),
                          ),
                          Divider(height: 10, color: NUIColors.NUITransparent)
                        ]),
                  ),
                ),
              ),
            ),
          ),
          NUIBlocStream<bool>(
              bloc: _statusBarNotifier,
              stream: (context, status, data) => Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  AnimatedOpacity(
                        duration: Duration(milliseconds: 250),
                        opacity: data == true ? 1.0 : 0.0,
                        child: Container(
                          width: double.infinity,
                          height: NUIDisplayUtil.statusBarHeight(context) + 5,
                          color: getColor(context, white),
                        ),
                      ),
                  Padding(
                      padding: EdgeInsets.only(top: 10),
                      child: NUIBlocStream<int>(
                          bloc: _notificationBloc.notiCountBloc,
                          initial: 0,
                          stream: (context, state, count) {
                            return NotificationBell(_notificationBloc, count: count ?? 0, expanded: data == true);
                          }
                      )
                  )
                ],
              ))
        ],
      ),
    );
  }

  void _updateTopCover(bool expanded) {
    _statusBarNotifier.notify(expanded);
    _titleCoverNotifier.notify(expanded);
  }

  void _updateInnerScrollOffset(double offset) {
    _innerScrollOffsetNotifier.notify(offset);
  }

  void _updateBannerOffset(DraggableScrollableNotification noti) {
    final maxRatio = 0.7;
    final ratio = ((noti.maxExtent - noti.extent).toDouble() /
        (noti.maxExtent - noti.minExtent));
    final finalRatio = ratio >= maxRatio ? ratio : maxRatio;

    final diff = 1 - finalRatio;
    final slowedDiff = diff * 0.6;
    final slowedRatio = 1 - slowedDiff;
    _bannerOffsetNotifier.notify(slowedRatio);
    _pageIndicatorOffsetNotifier.notify(slowedRatio);
  }
}

class BannerSliderText extends StatelessWidget {
  BannerSliderText({this.image, this.visibility});
  final BannerImage image;
  final double visibility;

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        height: double.infinity,
        child: Stack(
          children: [
            Positioned(
              bottom: 31 + 20.0 + 12 + 16,
              right: 0,
              child: Padding(
                padding: EdgeInsets.only(right: 16, left: 40),
                child: SizedBox(
                  width: screenWidth(context) * 0.7,
                  child: Opacity(
                    opacity: visibility ?? 1,
                    child: NUIColumn(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      divider: Divider(height: 16, color: NUIColors.NUITransparent),
                      children: [
                        Text(image.bannerTitle,
                            style: getFont(context,
                                size: 32, color: textWhite, isBold: true),
                            textAlign: TextAlign.end,
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis),
                        Text(image.bannerDescription,
                            style:
                            getFont(context, size: 16, color: textWhite),
                            textAlign: TextAlign.end,
                            maxLines: 4,
                            overflow: TextOverflow.ellipsis),
                      ],
                    ),
                  ),
                ),
              ),
            )
          ],
        ));
  }
}


class BannerSliderImage extends StatefulWidget {
  BannerSliderImage({this.image});
  final BannerImage image;
  @override
  _BannerSliderImageState createState() => _BannerSliderImageState();
}

class _BannerSliderImageState extends State<BannerSliderImage> {
  String id;
  bool showingText = false;
  @override
  void initState() {
    super.initState();
    id = randomUUID();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        height: double.infinity,
        child: Stack(
          children: [
            Container(
              width: double.infinity,
              height: double.infinity,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: loadImageProvider(widget.image.bannerUrl),
                  fit: BoxFit.cover
                )
              ),
            ),
            Container(
              width: double.infinity,
              height: double.infinity,
              decoration: BoxDecoration(
                gradient: NUILinearGradient(
                  colors: [
                    Colors.transparent,
                    Colors.transparent,
                    Colors.black12,
                    Colors.black26,
                    Colors.black38
                  ],
                  orientation: GradientOrientation.TL_BR
                )
              ),
            ),
          ],
        ));
  }
}

class ExploreBottomSheetContent extends StatefulWidget {
  final Function moreClickListener;

  const ExploreBottomSheetContent(this.moreClickListener);


  @override
  _ExploreBottomSheetContentState createState() => _ExploreBottomSheetContentState();
}

class _ExploreBottomSheetContentState extends State<ExploreBottomSheetContent> {
  CourseBloc courseBloc;
  MasterBloc masterBloc;
  Future<DataResult<List<CategoryResponse>>> _courseCategories;

  @override
  void initState(){
    super.initState();
    courseBloc = CourseBloc();
    masterBloc = MasterBloc();
    _courseCategories = courseBloc.getCourseCategories();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: NUIBoxDecoration(
        color: getColor(context, white),
      ),
      child: NUIColumn(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(left: 20, right: 20),
                child: Text(getString("homeScreenDesc"), style: getFont(context, size: 14, color: textDarkGray)),
              ),
              Divider(
                height: 30,
                color: NUIColors.NUITransparent,
              ),
              NUIFutureWidget<DataResult<List<CategoryResponse>>>(
                futureValue: _courseCategories,
                callback: (state, data, message) {
                  if(state == FutureState.LOADING){
                    return Container(
                      child: NUIColumn(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          NUISkeletonContainer(
                            child: Padding(
                              padding: EdgeInsets.only(left: 20, right: 20),
                              child: NUISkeletonView(child: Text("Course Category", style: getFont(context, size: 15, color: textDarkGray))),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 10, top: 10, left: 15, right: 15),
                            child: NUIRow(
                              divider: VerticalDivider(width: 10),
                              children: [
                                LITrainingCourseSkeletal(),
                                LITrainingCourseSkeletal()
                              ],
                            ),
                          )
                        ],
                      ),
                    );
                  }
                  else{
                    if(data != null && data.success){
                      final courseDict = data.data;
                      logNUI("HomeScreen", "Total categories size : ${courseDict.length}");
                      return Column(
                        children: courseDict.map((e){
                          final category = e;
                          return NUIColumn(children: [
                            NUIRow(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Expanded(
                                  child: Padding(
                                    padding: EdgeInsets.only(left: 20, right: 20),
                                    child: Text(category.title, style: getFont(context, size: 18, color: textDarkGray), overflow: TextOverflow.fade, softWrap: false, maxLines: 1),
                                  ),
                                ),
                                NUIBouncingAnimation(
                                  scaleFactor: 0.5,
                                  onTap: () {
                                    widget.moreClickListener(category.title);
                                  },
                                  child: Padding(
                                    padding: EdgeInsets.only(left: 20, right: 20),
                                    child: NUIRow(
                                      divider: VerticalDivider(width: 5, color: NUIColors.NUITransparent,),
                                      children: [
                                        Text("More", style: getFont(context, size: 12, color: accentBright, isBold: true)),
                                        loadSvg("arrow_right.svg", width: 17, height: 17, color: getColor(context, accentBright))
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                            SizedBox(
                              height: 300,
                              width: screenWidth(context),
                              child: NUIAnimatedList(
                                repo: AppListRepo(),
                                enablePagination: false,
                                enableSwipeRefresh: false,
                                orientation: Axis.horizontal,
                                skeletalCount: 1,
                                itemAnimation: NUIListItemAnimation.FADE,
                                paginationInflater: NUILoadingListItem(shadowRadius: 5,inflater: (context) =>LITrainingCourseSkeletal()),
                                skeletalInflater: NUILoadingListItem(shadowRadius: 5,inflater: (context) =>LITrainingCourseSkeletal()),
                                middleDivider: 5,
                                padding: const EdgeInsets.only(bottom: 30, top: 10, left: 15, right: 15),
                                initialList: [],
                                bloc: courseBloc.coursesByCategoryBloc(category.id),
                                clickListener: (data, position, clickType) {
                                  if(data is CourseResponse){
                                    courseBloc.redirectCourseToDetail(context, data);
                                  }
                                },
                              ),
                            )
                          ], divider: null);
                        }).toList(),
                      );
                    }
                    else{
                      return Center(
                        child: Padding(
                          padding: const EdgeInsets.all(25.0),
                          child: Text("Failed to fetch categories", style: getFont(context, size: 12, color: textGray)),
                        ),
                      );
                    }
                  }
                }
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class NotificationBell extends StatelessWidget {
  NotificationBell(this.notiBloc, {this.count: 0, this.expanded: false});
  final GeneralBloc bloc = GeneralBloc();
  final CalendarBloc calBloc = CalendarBloc();
  final NotificationBloc notiBloc;
  final bool expanded;

  final int count;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 15, left: 15),
      child: Stack(
        children: [
          Visibility(
            visible: count != null && count > 0,
            child: Positioned(
                right: 0,
                child: Container(
                  padding: EdgeInsets.only(left: 3, right: 3, top: 2, bottom: 2),
                  decoration: NUIBoxDecoration(color: getColor(context, accentBright), borderRadius: BorderRadius.circular(4)),
                  child: Text(bloc.formatNotificationCount(count), style: getFont(context, size: 8, color: textWhite, isBold: true)),
                )),
          ),
          GestureDetector(
            onTap: () {
              showNotifications(context, notiBloc);
            },
            child: Padding(
              padding: EdgeInsets.only(left: 5, right: 5, top: 2, bottom: 2),
              child: loadSvg("nav_notification.svg", width: 35, height: 35, color: expanded == true ? getColor(context, black) : getColor(context, textWhite))
            ),
          )
        ],
      ),
    );
  }
}
