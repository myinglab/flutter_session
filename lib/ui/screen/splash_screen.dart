import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nesternship_flut/ui/screen_codes.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_core/nui_core.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return NUIScreen(
        content: (context) => Container(
          width: double.infinity,
          height: double.infinity,
          decoration: NUIBoxDecoration(
              gradient: NUILinearGradient(
                  colors: [
                    getColor(context, accentBlueDark),
                    getColor(context, accentBlueBright),
                  ],
                  orientation: GradientOrientation.T_B
              )
          ),
          child: Center(
              child: SizedBox(
                width: screenWidth(context) * 0.5,
                child: Image(image: AssetImage("assets/images/logo_white.png"))
              ),
            )
        ),
        screenCode: ScreenCodes.splash
    );
  }
}
