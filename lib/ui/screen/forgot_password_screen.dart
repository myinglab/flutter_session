import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nesternship_flut/ui/custom/screen_header.dart';
import 'package:nesternship_flut/ui/screen_codes.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';

import 'package:nui_core/nui_core.dart';

class ForgotPasswordScreen extends StatelessWidget {

  double screenHeight;

  @override
  Widget build(BuildContext context) {
    screenHeight =  MediaQuery.of(context).size.height;
    return NUIScreen(
        overlayStyle: NUIOverlayStyle(darkStatusBarIcon: false),
        content: (context) => Material(
          child: Container(
              width: double.infinity,
              height: double.infinity,
              child: NUIColumn(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                      child: Container(
                        child: Stack(
                          children: [
                            BackgroundImage(screenHeight),
                            Container(
                                child: NUIColumn(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      ScreenHeader(true, context),
                                      Expanded(
                                          child: LoginPageTitle()
                                      ),
                                      //BottomWhiteSheet()
                                    ]
                                )
                            )
                          ],
                        ),
                      )
                  ),
                ],
              )
          ),
        ),
        screenCode: ScreenCodes.login
    );
  }
}

class BackgroundImage extends StatefulWidget {
  final double originalHeight;

  BackgroundImage(this.originalHeight);

  @override
  _BackgroundImageState createState() => _BackgroundImageState();
}

class _BackgroundImageState extends State<BackgroundImage> with TickerProviderStateMixin {

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: widget.originalHeight * 0.8,
      child: Container(
          color: getColor(context, backgroundGray),
          child: Image.asset(
            'assets/images/login_bg.png',
            fit: BoxFit.cover,
          )
      ),
    );
  }
}


class LoginPageTitle extends StatefulWidget {
  @override
  _LoginPageTitleState createState() => _LoginPageTitleState();
}

class _LoginPageTitleState extends State<LoginPageTitle> with TickerProviderStateMixin{

  @override
  Widget build(BuildContext context) {
    return Container(
        alignment: Alignment.topCenter,
        child: Image.asset(
        'assets/images/logo.png'
        )
    );
  }
}





