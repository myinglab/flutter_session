import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:nesternship_flut/bloc/course_bloc.dart';
import 'package:nesternship_flut/data/model/search_course.dart';
import 'package:nesternship_flut/datalayer/entity/course/course_response.dart';
import 'package:nesternship_flut/ui/custom/app_web_view.dart';
import 'package:nesternship_flut/ui/screen/video_viewer_screen.dart';
import 'package:nesternship_flut/ui/screen_codes.dart';
import 'package:nui_animation/nui_button_animation.dart';
import 'package:nui_core/nui_core.dart';
import '../style/app_theme.dart';
import 'package:nesternship_flut/ui/custom/no_internet_connection_holder.dart';

void showWebViewer(BuildContext context, {@required CourseResponse searchCourse}) {
  push(context, WebViewerScreen(searchCourse), transition: PageTransition.BOTTOM_TOP);
}

class WebViewerScreen extends StatefulWidget {
  WebViewerScreen(this.searchCourse);
  final CourseResponse searchCourse;
  @override
  _WebViewerScreenState createState() => _WebViewerScreenState();
}

class _WebViewerScreenState extends State<WebViewerScreen> {
  final CourseBloc _courseBloc = CourseBloc();
  Future<bool> isCompleted;

  void getCourseStatus() async {
    _courseBloc.updateCourseStatus(widget.searchCourse.id);
    this.isCompleted = _courseBloc.getCourseStatus(widget.searchCourse.id);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getCourseStatus();
  }

  @override
  Widget build(BuildContext context) {
    return NUIScreen(
      overlayStyle: NUIOverlayStyle(darkStatusBarIcon: true),
      screenCode: ScreenCodes.webViewer,
      content: (context) => Scaffold(
          body: Container(
              color: getColor(context, white),
              alignment: Alignment.center,
              child: NUIColumn(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    NUIFutureWidget<bool>(
                        futureValue: isCompleted,
                        callback: (state, data, msg) {
                          if (state == FutureState.SUCCESS && data != null) {
                            return CourseToolBar(
                                key: Key("Loaded"),
                                searchCourse: widget.searchCourse,
                                isCompleted: data);
                          } else {
                            return CourseToolBar(
                                key: Key("Loading"),
                                searchCourse: widget.searchCourse,
                                isCompleted: false);
                          }
                        }),
                    Expanded(
                        child: Container(
                      height: double.infinity,
                      width: double.infinity,
                      child: Stack(
                        children: [
                          NoInternetConnectionChecker(
                            child: AppWebView(widget.searchCourse.sourceUrl)
                          ),
                          Container(
                            height: 10,
                            width: double.infinity,
                            decoration: NUIBoxDecoration(
                                gradient: NUILinearGradient(colors: [
                              getColor(context, toolbarShadow),
                              NUIColors.NUITransparent
                            ], orientation: GradientOrientation.T_B)),
                          )
                        ],
                      ),
                    ))
                  ]))),
    );
  }
}
