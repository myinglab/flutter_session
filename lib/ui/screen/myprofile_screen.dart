import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:nesternship_flut/bloc/registration_bloc.dart';
import 'package:nesternship_flut/bloc/user_bloc.dart';
import 'package:nesternship_flut/data/model/country_model.dart';
import 'package:nesternship_flut/bloc/profile_bloc.dart';
import 'package:nesternship_flut/bloc/master_bloc.dart';
import 'package:nesternship_flut/datalayer/entity/user_profile/update_member_details_bean.dart';
import 'package:nesternship_flut/data/model/user_profile.dart';
import 'package:nesternship_flut/ui/custom/myprofile_toolbar.dart';
import 'package:nesternship_flut/ui/custom/no_internet_connection_holder.dart';
import 'package:nesternship_flut/ui/custom/yellow_background_button.dart';
import 'package:nesternship_flut/ui/form/country_selection_form.dart';
import 'package:nesternship_flut/ui/form/myprofile_form.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_auth/ui/nui_auth_activity_screen.dart';
import 'package:nui_core/nui_core.dart';
import 'package:nui_form/nuif_form.dart';
import 'package:nui_toolbox/nui_outline_circle_avatar.dart';
import 'change_password_screen.dart';
import 'country_selection_screen.dart';

class MyProfileScreen extends StatefulWidget {
  MyProfileScreen({this.callBack});
  final Function callBack;
  @override
  _MyProfileScreenState createState() => _MyProfileScreenState();
}

class _MyProfileScreenState extends State<MyProfileScreen> with TickerProviderStateMixin {
  ProfileBloc profileBloc = ProfileBloc();
  UserBloc userBloc = UserBloc();
  MasterBloc masterBloc = MasterBloc();
  UpdateMemberDetailsBean profileBean;
  bool updatestate = false;
  bool passwordstate = false;
  bool blank = true;
  String userNestleCountry;
  String selectedNestleCountry;
  Future<UserProfile> userProfile;
  bool _profileUpdated = false;
  bool _updatingProfile = false;
  YellowButtonController buttonController;
  String failureMessage;

  @override
  void initState() {
    super.initState();
    buttonController = YellowButtonController();
    userProfile = profileBloc.getUserProfile(userBloc.getUserId());
  }

  void setNestleCountry(String nestleCountry){
    userNestleCountry  = nestleCountry;
  }

  void setUpdateBean(UserProfile profile){
    if(profileBean == null) profileBean = UpdateMemberDetailsBean.from(field: profile);
  }

  Widget _getUpdateBlueBanner(BuildContext context, UserProfile user){
    return Container(
        width: double.infinity,
        padding: EdgeInsets.fromLTRB(36, 14, 36, 14),
        margin: EdgeInsets.fromLTRB(32, 20, 32, 0),
        alignment: Alignment.center,
        decoration: NUIBoxDecoration(
            borderRadius:
            BorderRadius.all(Radius.circular(4)),
            color: getColor(context, textBlue),
            shadowRadius: 8,
            shadowColor:
            getColor(context, accentBlueBright).withOpacity(0.3),
            shadowOffset: Offset(1, 1)),
        child: Text(
          user.getUpdateProfileSteps() ?? "",
          textAlign: TextAlign.center,
          style: getFont(context, size: 14, color: textWhite),
        )
    );
  }

  Widget _getUpdateSuccessGreenBanner(BuildContext context, UserProfile user){
    final isFailed = !isNullOrEmpty(failureMessage);
    return Container(
        width: double.infinity,
        padding: EdgeInsets.fromLTRB(36, 14, 36, 14),
        margin: EdgeInsets.fromLTRB(32, 12, 32, 0),
        alignment: Alignment.center,
        decoration: NUIBoxDecoration(
            borderRadius:
            BorderRadius.all(Radius.circular(4)),
            color: getColor(context, isFailed ? formErrorRed : greenHighlight2),
            shadowRadius: 8,
            shadowColor:
            getColor(context, isFailed ? formErrorRed : greenHighlight2).withOpacity(0.3),
            shadowOffset: Offset(1, 1)),
        child: Text(
          isFailed ? failureMessage : getString("updateProfileSuccess"),
          textAlign: TextAlign.center,
          style: getFont(context, size: 14, color: textWhite),
        )
    );
  }

  void _updateUserProfile(BuildContext context, UserProfile user) async{
    if(_updatingProfile) return;

    if(!isNullOrEmpty(selectedNestleCountry)){
      profileBean.setNestleCountry(selectedNestleCountry);
    }

    logNUI("MyProfileScreen", "To update user gender : ${profileBean.gender}");
    buttonController.updateState(true, null);
    final result = await profileBloc.updateMemberDetails(profileBean);
    buttonController.updateState(false, null);
    _updatingProfile = false;
    if(result.success == true){
      failureMessage = null;
      userProfile = profileBloc.getUserProfile(userBloc.getUserId());
      setState(() {
        _profileUpdated = true;
      });
      NUIAsync.delay(Duration(seconds: 3), () {
        setState(() {
          _profileUpdated = false;
        });
      });
    }
    else{
      failureMessage = result.message ?? getString("updateProfileFail");
    }

//    profileBloc.showUpdateProfileConfirmDialog(context, profileBean, doneCallback: (){
//      userProfile = profileBloc.getUserProfile(userBloc.getUserId());
//      setState(() {
//        _profileUpdated = true;
//      });
//      NUIAsync.delay(Duration(seconds: 3), () {
//        setState(() {
//          _profileUpdated = false;
////          _updatingProfile = false;
//        });
//      });
//    });

//    _updatingProfile = true;
//    final updateResult = await profileBloc.updateMemberDetails(profileBean);
//    if(updateResult.success) {
//      userProfile = profileBloc.getUserProfile(userBloc.getUserId());
//      setState(() {
//        _profileUpdated = true;
//      });
//      NUIAsync.delay(Duration(seconds: 3), () {
//        setState(() {
//          _profileUpdated = false;
//          _updatingProfile = false;
//        });
//      });
//    }
//    else{
//      _updatingProfile = false;
//    }
  }

  @override
  Widget build(BuildContext context) {
    return NUIAuthScreen(
      overlay: NUIOverlayStyle(darkStatusBarIcon: true),
      module: "My Profile",
      child: Container(
        width: double.infinity,
        height: double.infinity,
        color: getColor(context, backgroundWhite),
        child: NUIColumn(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            MyProfileToolbar(),
            Expanded(
              child: NoInternetConnectionChecker(
                child: NUIFutureWidget<UserProfile>(
                    futureValue: userProfile,
                    callback: (state, user, message) {
                      if (state == FutureState.SUCCESS && user != null) {
                        setUpdateBean(user);
                        return SingleChildScrollView(
                          child: NUIColumn(
                            children: [
                              AnimatedSize(
                                duration: Duration(milliseconds: 250),
                                vsync: this,
                                child: Visibility(
                                  visible: user.getUpdateProfileSteps() != null,
                                  child: _getUpdateBlueBanner(context, user)
                                ),
                              ),
                              Container(
                                child: AnimatedSize(
                                  duration: Duration(milliseconds: 200),
                                  vsync: this,
                                  child: Visibility(
                                      visible: _profileUpdated,
                                      child: _getUpdateSuccessGreenBanner(context, user)
                                  ),
                                ),
                              ),
                              Divider(height: 30, color: Colors.transparent),
                              Container(
                                alignment: Alignment.center,
                                child: Container(
                                  width: 100,
                                  height: 100,
                                  alignment: Alignment.center,
                                  decoration: NUIBoxDecoration(
                                    color: getColor(context, backgroundGray),
                                    borderRadius: BorderRadius.circular(50)
                                  ),
                                  child: NUIClipView(
                                    child: profileBloc.getUserAvatar(user),
                                    borderRadius: BorderRadius.circular(50),
                                  )
                                )
                              ),
                              Divider(height: 8, color: Colors.transparent),
                              MyProfileFormBody(user,  profileBean),
                              Divider(height: 8, color: getColor(context, grayLine)),
                              Padding(
                                padding: EdgeInsets.only(top: 16),
                                child: NUIRippleContainer(
                                  rippleColor: getColor(context, accentBright).withOpacity(0.3),
                                  child: NUIRow(
                                    children: [
                                      Expanded(
                                        child: Container(
                                          padding: EdgeInsets.fromLTRB(32, 20, 32, 15),
                                          child: Text(
                                            getString("myProfileNesternship"),
                                            style: getFont(context, size: 16, color: textLightGray, isBold: true),
                                            textAlign: TextAlign.left,
                                          ),
                                        ),
                                      ),
                                      Container(
                                        padding: EdgeInsets.fromLTRB(32, 10, 32, 10),
                                        child: NUIRow(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
//                                          Icon(Icons.flag, color: getColor(context, accentBright)),
                                            Padding(
                                              padding: EdgeInsets.only(left: 16),
                                              child: Text(
                                                selectedNestleCountry ?? user.nestleCountry ?? "",
                                                style: getFont(context, size: 16, color: textDarkGray),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                  onTap: () async {
                                    final selectedCountry =
                                    await pushForResult<Country>(context, CountrySelectionScreen(true), transition: PageTransition.BOTTOM_TOP);
                                    if (selectedCountry.countryName != null) {
                                      setState(() {
                                        selectedNestleCountry = selectedCountry.countryName;
                                      });
                                    }
                                  },
                                ),
                              ),
                              NUIRippleContainer(
                                rippleColor:
                                getColor(context, accentBright).withOpacity(0.3),
                                child: NUIRow(
                                  children: [
                                    Container(
                                      padding: EdgeInsets.fromLTRB(32, 15, 32, 15),
                                      child: Text(
                                        getString("myProfileChangePassword"),
                                        style: getFont(context, size: 16, color: textLightGray, isBold: true),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                    Expanded(
                                      child: Container(),
                                    )
                                  ],
                                ),
                                onTap: () {
                                  setState(() {
                                    passwordstate = true;
                                    updatestate = false;
                                    blank = false;
                                    push(context, ChangePasswordScreen());
                                  });
                                },
                              ),
                              YellowBackgroundButton(
                                controller: buttonController,
                                padding: EdgeInsets.fromLTRB(16, 24, 16, 40),
                                text: getString("myProfileUpdateButton"),
                                onTap: (){
                                  _updateUserProfile(context, user);
                                },
                              ),
                            ],
                          ),
                        );
                      } else {
                        return Container(
                          width: double.infinity,
                          height: double.infinity,
                          alignment: Alignment.center,
                          child: NUISkeletonContainer(
                            child: Column(
                              children: [
                                Divider(height: 30, color: Colors.transparent),
                                Container(
                                  alignment: Alignment.center,
                                  child: NUISkeletonView(
                                    radius: BorderRadius.circular(50),
                                    width: 100,
                                    height: 100,
                                  )
                                )
                              ],
                            ),
                          )
                        );
                      }
                    }
                  ),
              )
            )
          ],
        ),
      ),
    );
  }
}

class MyProfileFormBody extends StatefulWidget {
  MyProfileFormBody(this.userProfile , this.updateBean);
  final UserProfile userProfile;
  final UpdateMemberDetailsBean updateBean;
  @override
  _MyProfileFormBodyState createState() => _MyProfileFormBodyState();
}

class _MyProfileFormBodyState extends State<MyProfileFormBody> {
  MasterBloc masterBloc = MasterBloc();
  ProfileBloc profileBloc = ProfileBloc();
  UserBloc userBloc = UserBloc();
  Country selectedCountry;
  NUIForm form;
  NUIForm countryForm;
  bool validated;
  String originCountrySelected;

  @override
  void initState() {

    form = ProfileForm().myProfileForm(context, (id, value, form) {
      logNUI("IFormPreview", "Text changed on $id, cur value : $value");
      _getFormValue(id, value);
    }, (id, clickType, form) {
      logNUI("IFormPreview", "Button clicked on $id, click type : $clickType");
      validated = form.validate(false);
      final values = form.extractValues();
    }, (id, selection, selected, form) {
      logNUI("IFormPreview", "Item selected on $id, cur value : ${selection.value}, selected : $selected");
      _getFormValue(id, selection.value);
    });

    countryForm = countrySelectionForm(context, () {
    }, forProfile: true);

    NUIAsync.delay(Duration(milliseconds: 250), () async {
      form.fillInValues({
        "edtName": NUIFormValue(inpId: "edtName", value: NUIFormSelection(value: widget.userProfile.memberName)),
        "edtDateofBirth": NUIFormValue(inpId: "edtDateofBirth", value: NUIFormSelection(value: widget.userProfile.dateOfBirth)),
        "spnGender": NUIFormValue(inpId: "spnGender", value: NUIFormSelection(value: widget.userProfile.gender)),
      });
    });

    NUIAsync.delay(Duration(milliseconds: 250), () async {
      countryForm.fillInValues({
        "edtCountry": NUIFormValue(
            inpId: "edtCountry",
            value: NUIFormSelection(value: widget.userProfile.originCountry)
        )
      });
    });

    // fetch origin country based on country code from get user profile api
    super.initState();
  }

  Widget _renderForm(BuildContext context) {
    return NUIFormContent(form: form);
  }

  Widget _renderCountryForm(BuildContext context) {
    final comp = countryForm.updateItem<NUIFTextItem>("edtCountry", (item) {
      item.controller?.text = selectedCountry?.countryName;
      if(this.selectedCountry != null) {
        widget.updateBean.setOriginCountry(this.selectedCountry.countryName);
      }
      return item;
    });
    return NUIFormContent(form: countryForm);
  }

  void _getFormValue(String formId, String formValue) {
    if (formValue != null || formValue != "") {
      switch (formId) {
        case "edtName":
          widget.updateBean.setMemberName(formValue);
          break;
        case "edtDateofBirth":
          widget.updateBean.setDateOfBirth(formValue);
          break;
        case "spnGender":
          logNUI("MyProfileScreen", "Updating user gender : $formValue");
          widget.updateBean.setGender(formValue);
          break;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return NUIColumn(
      //mainAxisSize: MainAxisSize.max,
      //mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          width: double.infinity,
          padding: EdgeInsets.only(left: 32, right: 32, top: 24, bottom: 0),
          child: _renderForm(context),
        ),
        Divider(height: 12, color: NUIColors.NUITransparent),
        Padding(
          padding: EdgeInsets.only(left: 32, right: 32, bottom: 24),
          child: GestureDetector(
            child: _renderCountryForm(context),
            onTap: () async {
              final selectedCountry = await pushForResult<Country>(context, CountrySelectionScreen(false), transition: PageTransition.BOTTOM_TOP);
              if (selectedCountry.countryName != null) {
                this.selectedCountry = selectedCountry;
                form.fillInValues({
                  "edtCountry": NUIFormValue(inpId: "edtCountry", value: NUIFormSelection(value: this.selectedCountry.countryName))
                });
              }
            },
          ),
        ),
      ],
    );
  }
}


class MyProfileScreenBlank extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: getColor(context, backgroundWhite),
        body: Column(
          children: [
            Container(
                decoration: toolbarWhiteDecoration(context),
                child: MyProfileToolbar()
            ),
          ]
        )
    );
  }
}



