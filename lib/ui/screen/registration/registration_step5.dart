import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nesternship_flut/data/model/country_model.dart';
import 'package:nesternship_flut/ui/custom/dob_picker.dart';
import 'package:nesternship_flut/ui/custom/yellow_background_button.dart';
import 'package:nesternship_flut/ui/form/country_selection_form.dart';
import 'package:nesternship_flut/ui/form/registration_country_form.dart';
import 'package:nesternship_flut/ui/screen/registration/registration_step1.dart';
import 'package:nesternship_flut/ui/screen/registration_steps_screen.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_animation/nui_button_animation.dart';
import 'package:nui_core/nui_core.dart';
import 'package:nui_form/nuif_form.dart';
import 'package:nesternship_flut/datalayer/entity/registration/registration_detail.dart';
import '../../../bloc/registration_bloc.dart';
import '../country_selection_screen.dart';

class RegistrationStep5Screen extends StatefulWidget {
  RegistrationStep5Screen(this.registrationController, this.registrationDetail);
  final RegistrationController registrationController;
  final RegistrationDetail registrationDetail;
  @override
  _RegistrationStep5ScreenState createState() => _RegistrationStep5ScreenState();
}

class _RegistrationStep5ScreenState extends State<RegistrationStep5Screen> with TickerProviderStateMixin, RegistrationStepsAnimations {
  NUIForm form;
  Country selectedCountry = Country();
  RegistrationBloc regBloc = RegistrationBloc();
  String countryValue = "Select a Country";

  @override
  void initState() {
    super.initState();
    initAnimations(this);
    form = countrySelectionForm(context, () {
      // push(context)
    });
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: getColor(context, transparent),
      child: Container(
        width: double.infinity,
        height: double.infinity,
        padding: EdgeInsets.only(left: 32, right: 32),
        child: NUIColumn(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          divider: Divider(height: 35, color: NUIColors.NUITransparent),
          children: [
            WorldImage(),
            getFadingTitle(context, getString("questionOriginCountry")),
            GestureDetector(
              child: _renderForm(context, selectedCountry),
              onTap: () async {
                final selectedCountry = await pushForResult<Country>(
                    context, CountrySelectionScreen(false),
                    transition: PageTransition.BOTTOM_TOP);
                if (selectedCountry.countryName != null) {
                  this.selectedCountry = selectedCountry;
                  widget.registrationDetail.originCountry = selectedCountry.countryName;
                  widget.registrationController.setValue(selectedCountry);

                  form.fillInValues({
                    "edtCountry": NUIFormValue(
                        inpId: "edtCountry",
                        value: NUIFormSelection(
                            code: this.selectedCountry.countryName,
                            value: this.selectedCountry.countryName))
                  });

                  setState(() {});
                }
              },
            )
          ],
        ),
      ),
    );
  }

  Widget _renderForm(BuildContext context, Country theCountry) {
    final comp = form.updateItem<NUIFTextItem>("edtCountry", (item) {
      item.controller?.text = selectedCountry?.countryName;
      return item;
    });

    return NUIFormContent(form: form);
  }
}
