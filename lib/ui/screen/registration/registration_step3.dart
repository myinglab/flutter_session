import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nui_animation/nui_animation.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:nesternship_flut/ui/custom/dob_picker.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_core/nui_core.dart';
import 'package:nesternship_flut/datalayer/entity/registration/registration_detail.dart';
import '../registration_steps_screen.dart';

class RegistrationStep3Screen extends StatefulWidget {
  RegistrationStep3Screen(this.registrationController, this.registrationDetail);
  final RegistrationController registrationController;
  final RegistrationDetail registrationDetail;
  @override
  _RegistrationStep3ScreenState createState() =>
      _RegistrationStep3ScreenState();
}

class _RegistrationStep3ScreenState extends State<RegistrationStep3Screen> with TickerProviderStateMixin, RegistrationStepsAnimations {
  bool isMale;
  bool delayingClick = false;

  @override
  void initState(){
    super.initState();
    initAnimations(this);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      padding: EdgeInsets.only(left: 32, right: 32),
      child: NUIColumn(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        divider: Divider(height: 40, color: NUIColors.NUITransparent),
        children: [
          getFadingTitle(context, getString("registrationStep3Address")),
          genderLayout(context)
        ],
      ),
    );
  }

  Widget genderLayout(BuildContext context) {
    return NUIRow(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        divider: VerticalDivider(
          width: 12,
          color: NUIColors.NUITransparent,
        ),
        children: [
          genderButton(context, true, "assets/images/male.png",
              getString("registrationStep3Male")),
          genderButton(context, false, "assets/images/female.png",
              getString("registrationStep3Female"))
        ]);
  }

  Widget genderButton(BuildContext context, bool isMaleButton, String imagePath,
      String genderDescription) {
    return Expanded(
      child: NUIBouncingAnimation(
          onTap: () {
            setState(() {
              if(delayingClick == true) return;

              if (isMaleButton) {
                isMale = true;
              } else {
                isMale = false;
              }
              widget.registrationDetail.gender = isMale ? "M" : "F";
              widget.registrationController.setValue(isMale);
              delayingClick = true;

              NUIAsync.delay(Duration(milliseconds: 250), () {
                delayingClick = false;
                widget.registrationController.nextStep();
              });
            });
          },
          child: AspectRatio(
            aspectRatio: 1,
            child: Container(
                decoration: BoxDecoration(
                    border: Border.all(
                      color: ((isMaleButton && isMale == true) ||
                              (!isMaleButton && isMale == false))
                          ? getColor(context, accentBright)
                          : getColor(context, white),
                      width: ((isMaleButton && isMale == true) ||
                              (!isMaleButton && isMale == false))
                          ? 3
                          : 1,
                    ),
                    borderRadius: BorderRadius.circular(8)),
                child: Padding(
                    padding: EdgeInsets.only(
                        left: 25, right: 25, top: 15, bottom: 15),
                    child: NUIColumn(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Expanded(
                            child: Container(
                                alignment: Alignment.center,
                                padding: EdgeInsets.all(10),
                                child: Image.asset(imagePath))),
                        Text(genderDescription,
                            textAlign: TextAlign.center,
                            style: getFont(context, size: 16, color: white))
                      ],
                    ))),
          )),
    );
  }
}
