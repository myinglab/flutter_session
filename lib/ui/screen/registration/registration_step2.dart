import 'package:flutter/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nesternship_flut/ui/form/registration_name_form.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_core/nui_core.dart';
import 'package:nui_form/nuif_form.dart';
import '../registration_steps_screen.dart';
import 'package:nesternship_flut/datalayer/entity/registration/registration_detail.dart';

class RegistrationStep2Screen extends StatefulWidget {
  RegistrationStep2Screen(this.registrationController, this.registrationDetail);
  final RegistrationController registrationController;
  final RegistrationDetail registrationDetail;
  @override
  _RegistrationStep2ScreenState createState() =>
      _RegistrationStep2ScreenState();
}

class _RegistrationStep2ScreenState
    extends State<RegistrationStep2Screen> with TickerProviderStateMixin, RegistrationStepsAnimations {
  NUIForm form;
  bool updated = false;

  @override
  void initState(){
    super.initState();
    initAnimations(this);
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: getColor(context, transparent),
      child: Container(
        width: double.infinity,
        height: double.infinity,
        padding: EdgeInsets.only(left: 32, right: 32),
        child: Center(
          child: NUIColumn(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              divider: Divider(height: 47, color: NUIColors.NUITransparent),
              children: [
                getFadingTitle(context, getString('registrationStep2Title')),
                _renderForm(context),
                Divider(height: 50, color: NUIColors.NUITransparent),
              ],
          ),

        ),
      ),
    );
  }

  Widget _renderForm(BuildContext context) {
    form = RegistrationNameForm().registrationForm(context, (id, value, form) {
      logNUI("IFormPreview", "Text changed on $id, cur value : $value");
      if (value != null && value != "") {
        widget.registrationDetail.name = value;
        widget.registrationController.setValue(value);
      }
      else {
        widget.registrationController.setValue(null);
      }
    }, (id, clickType, form) {
      logNUI("IFormPreview", "Button clicked on $id, click type : $clickType");
      final validated = form.validate(false);
      final values = form.extractValues();
    }, (id, selection, selected, form) {
      logNUI("IFormPreview",
          "Item selected on $id, cur value : ${selection.value}, selected : $selected");
    }, (value){
      widget.registrationController.nextStep();
    });

    return NUIFormContent(form: form);
  }
}
