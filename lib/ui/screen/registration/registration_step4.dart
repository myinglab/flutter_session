import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nesternship_flut/ui/custom/dob_picker.dart';
import 'package:nesternship_flut/ui/screen/registration_steps_screen.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_core/nui_core.dart';
import 'package:nesternship_flut/datalayer/entity/registration/registration_detail.dart';

class RegistrationStep4Screen extends StatefulWidget {
  RegistrationStep4Screen(this.registrationController, this.registrationDetail);
  final RegistrationController registrationController;
  final RegistrationDetail registrationDetail;
  @override
  _RegistrationStep4ScreenState createState() =>
      _RegistrationStep4ScreenState();
}

class _RegistrationStep4ScreenState extends State<RegistrationStep4Screen> with TickerProviderStateMixin, RegistrationStepsAnimations {
  DOBPickerController controller;

  @override
  void initState() {
    super.initState();
    initAnimations(this);
    controller = DOBPickerController(valueChangedListener: () {
      final dob = controller.getDateOfBirth();
      widget.registrationDetail.dateOfBirth = dob.toString();
      widget.registrationController.setValue(dob);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      padding: EdgeInsets.only(left: 32, right: 32),
      child: NUIColumn(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        divider: Divider(height: 40, color: NUIColors.NUITransparent),
        children: [
          getFadingTitle(context, getString("registrationDOB")),
          DOBPicker(
            controller: controller,
          )
        ],
      ),
    );
  }
}
