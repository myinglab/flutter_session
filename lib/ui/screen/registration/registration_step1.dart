
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nesternship_flut/bloc/registration_bloc.dart';
import 'package:nesternship_flut/datalayer/entity/registration/registration_detail.dart';
import 'package:nesternship_flut/data/model/country_model.dart';
import 'package:nesternship_flut/ui/form/country_selection_form.dart';
import 'package:nesternship_flut/ui/screen/registration_steps_screen.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_animation/nui_animation.dart';
import 'package:nui_core/nui_core.dart';
import 'package:nui_form/nuif_form.dart';

import '../country_selection_screen.dart';

class RegistrationStep1Screen extends StatefulWidget {
  RegistrationStep1Screen(this.registrationController, this.registrationDetail);
  final RegistrationController registrationController;
  final RegistrationDetail registrationDetail;
  @override
  _RegistrationStep1ScreenState createState() =>
      _RegistrationStep1ScreenState();
}

class _RegistrationStep1ScreenState extends State<RegistrationStep1Screen> with TickerProviderStateMixin, RegistrationStepsAnimations{
  NUIForm form;
  Country selectedCountry = Country();
  RegistrationBloc regBloc = RegistrationBloc();
  String countryValue = "Select a Country";

  @override
  void initState() {
    super.initState();
    form = countrySelectionForm(context, () {
      // push(context)
    });

    initAnimations(this);
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Container(
        width: double.infinity,
        height: double.infinity,
        padding: EdgeInsets.only(left: 32, right: 32),
        child: NUIColumn(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          divider: Divider(height: 40, color: NUIColors.NUITransparent),
          children: [
            getFadingTitle(context, getString("questionInternCountry")),
            GestureDetector(
              child: _renderForm(context, selectedCountry),
              onTap: () async {
                final selectedCountry = await pushForResult<Country>(
                    context, CountrySelectionScreen(true),
                    transition: PageTransition.BOTTOM_TOP);
                if (selectedCountry.countryName != null) {
                  this.selectedCountry = selectedCountry;
                  widget.registrationDetail.internCountry = selectedCountry.countryName;
                  widget.registrationController.setValue(selectedCountry);
                  widget.registrationController.nextStep();

                  form.fillInValues({
                    "edtCountry": NUIFormValue(
                        inpId: "edtCountry",
                        value: NUIFormSelection(code: this.selectedCountry.countryName, value: this.selectedCountry.countryName))
                  });

                  setState(() {});
                }
              },
            ),
            WorldImage()
          ],
        ),
      ),
    );
  }

  Widget _renderForm(BuildContext context, Country theCountry) {
    final comp = form.updateItem<NUIFTextItem>("edtCountry", (item) {
      item.controller?.text = selectedCountry?.countryName;
      return item;
    });
    return NUIFormContent(form: form);
  }
}

class WorldImage extends StatefulWidget {
  @override
  _WorldImageState createState() => _WorldImageState();
}

class _WorldImageState extends State<WorldImage> with TickerProviderStateMixin {
  Future<bool> _delay;

  @override
  void initState() {
    super.initState();
    _delay = NUIAsync.delayForResult(Duration(milliseconds: 300), () async => true);
  }

  @override
  Widget build(BuildContext context) {
    return NUIFutureWidget<bool>(
      futureValue: _delay,
      callback: (state, data, msg){
        return AnimatedOpacity(
          duration: Duration(milliseconds: 1000),
          opacity: data == true ? 1 : 0,
          child: AspectRatio(
            aspectRatio: (351 / 142.83),
            child: loadSvg("map.svg", color: null),
          ),
        );
      },
    );
  }
}

