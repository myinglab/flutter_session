import 'package:flutter/material.dart';
import 'package:nesternship_flut/bloc/course_bloc.dart';
import 'package:nesternship_flut/bloc/search_course_bloc.dart';
import 'package:nesternship_flut/data/model/search_course.dart';
import 'package:nesternship_flut/data/model/search_criteria.dart';
import 'package:nesternship_flut/ui/custom/listing_placeholder.dart';
import 'package:nesternship_flut/ui/custom/no_internet_connection_holder.dart';
import 'package:nesternship_flut/ui/custom/search_toolbar.dart';
import 'package:nesternship_flut/ui/list/app_list_repo.dart';
import 'package:nesternship_flut/ui/list/skeletal/listitem_search_course_skeletal.dart';
import 'package:nesternship_flut/ui/screen/pdf_viewer_screen.dart';
import 'package:nesternship_flut/ui/screen/video_viewer_screen.dart';
import 'package:nesternship_flut/ui/screen/webview_screen.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_auth/ui/nui_auth_activity_screen.dart';
import 'package:nui_core/nui_core.dart';
import 'package:nui_core/ui/component/listview/nui_list_interface.dart';
import 'package:nui_core/ui/component/nui_search_text.dart';
import 'package:nesternship_flut/service/base_service.dart';

class MyCoursesScreen extends StatefulWidget {
  @override
  _MyCoursesScreenState createState() => _MyCoursesScreenState();
}

class _MyCoursesScreenState extends State<MyCoursesScreen> {
  final CourseBloc bloc = CourseBloc();
  NUIListController listController;
  SearchCriteria searchCriteria = SearchCriteria();

  @override
  void initState() {
    super.initState();
    listController = NUIListController(onLoadComplete: () {

    });
  }

  @override
  Widget build(BuildContext context) {
    return NUIAuthScreen(
      overlay: NUIOverlayStyle(darkStatusBarIcon: true),
      module: "My Courses",
      child: Scaffold(
          body: Container(
              color: getColor(context, white),
              child: NUIColumn(
                children: [
                  Toolbar(),
                  Expanded(
                      child: NoInternetConnectionChecker(
                        child: NUIAnimatedHeaderList(
                              repo: AppListRepo(),
                              enablePagination: true,
                              enableSwipeRefresh: false,
                              controller: listController,
                              placeHolder: () => ListingPlaceHolder(title: "No Courses", desc: "It seems like you do not have any courses yet!"),
                              errorHolder: (NUIListViewModel data) => ListingPlaceHolder(title: "Oops!", desc: "It seems like something went wrong, try refreshing the page!"),
                              skeletalCount: 2,
                              itemAnimation: NUIListItemAnimation.FADE,
                              paginationInflater: NUILoadingListItem(shadowRadius: 0, inflater: (context) => ListingPagination()),
                              skeletalInflater: NUILoadingListItem(shadowRadius: 16, inflater: (context) => LISearchCourseSkeletal()),
                              padding: const EdgeInsets.only(bottom: 5, top: 24),
                              initialList: [],
                              itemAnimationDuration: Duration(milliseconds: 0),
                              bloc: bloc.myCourses(),
                              clickListener: (data, position, clickType) {
                                if (data is SearchCourse) {
                                  switch (data.course.type.toLowerCase()) {
                                    case "video":
                                      {
                                        showVideoViewer(context, searchCourse: data.course);
                                      }
                                      break;

                                    case "pdf":
                                      {
                                        showPDFViewer(context, searchCourse: data.course);
                                      }
                                      break;

                                    case "module":
                                      {
                                        showWebViewer(context, searchCourse: data.course);
                                      }
                                      break;
                                  }
                                }
                              },
                            )
                        )
                      )
                ],
              ))),
    );
  }
}


class Toolbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: toolbarWhiteDecoration(context),
      child: Column(
        children: [
          NUIStatusBar(),
          Container(
            height: 48,
            width: double.infinity,
            child: Padding(
              padding: EdgeInsets.only(left: 8, right: 8),
              child: NUIRow(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                divider:
                VerticalDivider(width: 10, color: NUIColors.NUITransparent),
                children: [
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.only(left: 16),
                      child: Text(getString("toolbarMyCourses"),
                          style:
                          getFont(context, size: 16, color: textDarkGray)),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class MyCoursesScreenBlank extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: getColor(context, backgroundWhite),
        body: Column(
            children: [
              Container(
                  decoration: toolbarWhiteDecoration(context),
                  child: Toolbar()
              ),
            ]
        )
    );
  }
}
