import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:nesternship_flut/bloc/registration_bloc.dart';
import 'package:nesternship_flut/main.dart';
import 'package:nesternship_flut/ui/custom/yellow_background_button.dart';
import 'package:nesternship_flut/datalayer/entity/registration/registration_detail.dart';
import 'package:nesternship_flut/ui/screen/home_screen.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_core/color/nui_colors.dart';
import 'package:nui_core/nui_core.dart';
import 'package:nui_core/ui/component/nui_app.dart';
import 'package:nui_core/ui/component/nui_column.dart';
import 'package:nui_core/ui/component/nui_screen_widget.dart';
import 'package:nui_core/ui/nui_navigator.dart';

import '../screen_codes.dart';
import 'login_screen.dart';
import 'main_screen.dart';
import 'myprofile_screen.dart';

class RegistrationSuccessScreen extends StatelessWidget {
  RegistrationSuccessScreen(this.registrationDetail);
  final RegistrationDetail registrationDetail;
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: defaultGradientBackground(context),
        child: ContentBody(registrationDetail)
      )
    );
  }
}

class ContentBody extends StatefulWidget {
  ContentBody(this.registrationDetail);
  final RegistrationDetail registrationDetail;
  @override
  _ContentBodyState createState() => _ContentBodyState();
}

class _ContentBodyState extends State<ContentBody> {
  RegistrationBloc registrationBloc;
  MyProfileScreen profileScreen;
  bool registered = false;
  bool registrationFailure = false;

  @override
  void initState(){
    super.initState();
    registrationBloc = RegistrationBloc();
    _register();
  }

  void _register() async{
    final result = await registrationBloc.registerUserDetail(context, widget.registrationDetail);
    if(result == true) {
      setState(() {
        registered = true;
      });
    }
    else{
      //Todo: Something went wrong, do something
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: AnimatedSwitcher(
        duration: Duration(milliseconds: 250),
        child: registered ?  SuccessBody() : LoadingBody(),
      ),
    );
  }
}

class LoadingBody extends StatefulWidget {
  @override
  _LoadingBodyState createState() => _LoadingBodyState();
}

class _LoadingBodyState extends State<LoadingBody> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      alignment: Alignment.center,
      child: NUIShaderMask(
        shaderType: NUIShaderMaskType.BOTTOM_FADE,
        shadeOffset: 0.2,
        child: Lottie.asset(
           'assets/lottie/leave_drop_lottie.json',
            fit: BoxFit.contain,
         ),
      ),
    );
  }
}


class SuccessBody extends StatelessWidget {
  _thankYouWidget(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(16, 0, 16, 0),
      child: Text(
        getString("thankYouContent"),
        style: getFont(context, size: 22, color: white, isBold: false),
        textAlign: TextAlign.center,
      ),
    );
  }

  _successPageContentWidget(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(16, 0, 16, 0),
      child: Text(
        getString("successPageContent"),
        style: getFont(context, size: 16, color: white, isBold: false),
        textAlign: TextAlign.center,
      ),
    );
  }

  void _startDelayAndGoToMainScreen(BuildContext context){
    NUIAsync.delay(Duration(seconds: 3), () {
      popAllAndPush(context, MainScreen());
    });
  }

  @override
  Widget build(BuildContext context) {
    _startDelayAndGoToMainScreen(context);
    return Stack(alignment: Alignment.center, children: [
      NUIColumn(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: NUIColumn(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              divider: Divider(height: 16, color: NUIColors.NUITransparent),
              children: [
                loadSvg("success.svg", width: 64, height: 64, color: Colors.white),
                Divider(height: 72, color: getColor(context, transparent)),
                _thankYouWidget(context),
                _successPageContentWidget(context),
              ],
            ),
          ),
          YellowBackgroundButton(
            text: getString("closeContent"),
            onTap: () {
              popAllAndPush(context, MainScreen());
            }
          ),
          Divider(
            height: 88,
            color: NUIColors.NUITransparent,
          )
        ],
      ),
    ]);
  }
}



