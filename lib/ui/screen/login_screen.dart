import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:nesternship_flut/bloc/login_bloc.dart';
import 'package:nesternship_flut/bloc/user_bloc.dart';
import 'package:nesternship_flut/ui/custom/custom_checkbox.dart';
import 'package:nesternship_flut/ui/custom/screen_header.dart';
import 'package:nesternship_flut/ui/custom/yellow_background_button.dart';
import 'package:nesternship_flut/ui/form/forgot_password_form.dart';
import 'package:nesternship_flut/ui/form/login_form.dart';
import 'package:nesternship_flut/ui/screen_codes.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nesternship_flut/ui/custom/google_login_button.dart';
import 'package:nesternship_flut/ui/custom/apple_login_button.dart';
import 'package:nui_animation/nui_button_animation.dart';
import 'package:nui_auth/domain/nui_user_info.dart';
import 'package:nui_auth/nui_auth.dart';

import 'package:nui_core/nui_core.dart';
import 'package:nui_form/nuif_form.dart';

import 'main_screen.dart';

class LoginScreen extends StatelessWidget {
  final NUIBlocData<Map<String, bool>> backgroundImageBloc = NUIBlocData();
  final NUIBlocData<Map<String, String>> bottomSheetBloc = NUIBlocData();
  String mode = 'login';

  void changeModeToLogin(String mode) {
    //backgroundImageBloc.notify({'minimizeImage': true});
    this.mode = mode;
    if (mode == 'login') {
      //the reason why only login is notified is because forgot password sheet transition is handled in bottom white sheet
      //notifying it another time will make activate the transition twice
      bottomSheetBloc.notify({'mode': mode, "backToLogin": "true"});
    }
  }

  void handleBack(BuildContext context) {
    if (mode == 'login') {
      pop(context);
    } else {
      changeModeToLogin('login');
    }
  }

  @override
  Widget build(BuildContext context) {
    return NUIScreen(
        overlayStyle: NUIOverlayStyle(darkStatusBarIcon: true),
        content: (context) => Scaffold(
          body: SingleChildScrollView(
            child: WillPopScope(
              onWillPop: () async {
                handleBack(context);
                return false;
              },
              child: Container(
                  width: screenWidth(context),
                  height: screenHeight(context),
                  decoration: NUIBoxDecoration(
                      gradient: NUILinearGradient(colors: [
                    getColor(context, accentBlueDark),
                    getColor(context, accentBlueBright)
                  ], orientation: GradientOrientation.T_B)),
                  child: NUIColumn(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                          child: Container(
                        child: Stack(
                          children: [
                            NUIBlocStream<Map<String, bool>>(
                                bloc: backgroundImageBloc,
                                stream: (context, status, data) =>
                                    BackgroundImage(screenHeight(context), data ?? {'minimizeImage': true})
                            ),
                            Container(
                                child: NUIColumn(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                  ScreenHeader(
                                    true,
                                    context,
                                    onBackPressed: handleBack,
                                  ),
                                  Expanded(child: LoginPageTitle()),
                                  Stack(children: [
                                    NUIBlocStream<Map<String, String>>(
                                        bloc: bottomSheetBloc,
                                        stream: (context, status, data) =>
                                            BottomWhiteSheet(
                                                changeModeToLogin,
                                                data ?? {'mode': 'login'})),
                                  ])
                                ]))
                          ],
                        ),
                      )),
                    ],
                  )),
            ),
          ),
        ),
        screenCode: ScreenCodes.login);
  }
}

class BackgroundImage extends StatefulWidget {
  final double originalHeight;
  final Map<String, bool> toOriginalSize;

  BackgroundImage(this.originalHeight, this.toOriginalSize);

  @override
  _BackgroundImageState createState() => _BackgroundImageState();
}

class _BackgroundImageState extends State<BackgroundImage>
    with TickerProviderStateMixin {
  AnimationController controller;
  Animation<double> animation;

  @override
  void initState() {
    super.initState();
    controller = new AnimationController(
        duration: Duration(milliseconds: 250), vsync: this)
      ..addListener(() {
        setState(() {});
      })
      ..addStatusListener((status) {
        if (status == AnimationStatus.dismissed) {
          controller.forward();
        }
      });
    double shrunkHeight = widget.originalHeight * 0.8;
    animation = Tween(begin: widget.originalHeight, end: shrunkHeight)
        .animate(controller);
    Future.delayed(Duration(milliseconds: 1000), () {
      controller.forward();
    });
  }

  @override
  Widget build(BuildContext context) {
    return NUIShaderMask(
      shaderType: NUIShaderMaskType.BOTTOM_FADE,
      shadeOffset: 0.2,
      child: Container(
        width: double.infinity,
        height: animation.value,
        child: Container(
            color: getColor(context, backgroundGray),
            child: Image.asset(
              'assets/images/login_bg.png',
              fit: BoxFit.cover,
            )),
      ),
    );
  }
}

class LoginPageTitle extends StatefulWidget {
  @override
  _LoginPageTitleState createState() => _LoginPageTitleState();
}

class _LoginPageTitleState extends State<LoginPageTitle>
    with TickerProviderStateMixin {
  AnimationController controller;
  Animation<double> animation;

  @override
  void initState() {
    super.initState();
    controller = new AnimationController(
        duration: Duration(milliseconds: 250), vsync: this);
    animation = Tween(begin: 50.0, end: 0.0).animate(controller);
    Future.delayed(Duration(milliseconds: 1000), () {
      controller.forward();
    });
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: controller,
      builder: (_, child) {
        return Transform.translate(
          offset: Offset(0.0, animation.value),
          child: Container(
            alignment: Alignment.topCenter,
            child: Image.asset('assets/images/logo.png'),
          ),
        );
      },
    );
  }
}

class BottomWhiteSheet extends StatefulWidget {
  final Function mainModeChangeCallback;
  final Map<String, String> showLoginSheet;

  const BottomWhiteSheet(this.mainModeChangeCallback, this.showLoginSheet);

  @override
  _BottomWhiteSheetState createState() => _BottomWhiteSheetState();
}

class _BottomWhiteSheetState extends State<BottomWhiteSheet>
    with TickerProviderStateMixin {
  AnimationController slideUpAnimationController;
  Animation<double> slideUpAnimation;
  // bool alreadySwitched = false;
  String currentContent;
  Map<String, String> showLoginSheet;

  //this variable is used to determine whether going down animation has completed, and if so, change bottom sheet details while it's out of sight
  bool convertForgotPassword;

  @override
  void initState() {
    super.initState();
    showLoginSheet = widget.showLoginSheet;
    convertForgotPassword = false;
    slideUpAnimationController = new AnimationController(
        duration: Duration(milliseconds: 250), vsync: this)
      ..addStatusListener((AnimationStatus status) {
        // AnimationStatus.completed = animation.forward() listener
        // AnimationStatus.dismissed = animation.reverse() listener
        if (status == AnimationStatus.dismissed) {
          if (widget.showLoginSheet['mode'] == 'forgotPassword') {
            convertForgotPassword = true;
          } else {
            convertForgotPassword = false;
          }
          slideUpAnimationController.forward();
        }
      });
    slideUpAnimation = Tween(begin: 500.0, end: 0.0).animate(slideUpAnimationController);
    Future.delayed(Duration(milliseconds: 1000), () {
      slideUpAnimationController.forward();
    });
  }

  //this method is also passed to the login form callback,
  //whenever the 'forgot password' is tapped, this method will be called
  void switchToForgotPasswordSheet() {
    if (currentContent != "password") {
      showLoginSheet["mode"] = "forgotPassword";
      currentContent = "password";
      // alreadySwitched = true;
      widget.mainModeChangeCallback('forgotPassword');
      slideUpAnimationController.reverse();
      widget.showLoginSheet['mode'] = 'forgotPassword';
    }

  }

  //this method will be called from 2 place
  // 1. when the bloc stream from parent widget returns a new value for widget.showLoginSheet['mode'], and this value == 'login'
  // 2. when the 'back to login' button is pressed in forgot password page
  void switchToLoginSheet() {
    if (currentContent != "login") {
      showLoginSheet["mode"] = "login";
      currentContent = "login";
      // alreadySwitched = true;
      convertForgotPassword = false;
      slideUpAnimationController.reverse();
      widget.showLoginSheet['mode'] = 'login';
    }
  }

  Widget _loginContent(BuildContext context){
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        LoginPageFormBody(this.switchToForgotPasswordSheet),
//        Divider(height: 20, color: NUIColors.NUITransparent),
//        Padding(
//          padding: EdgeInsets.only(left: 16, right: 16),
//          child: NUIRow(
//            mainAxisSize: MainAxisSize.max,
//            crossAxisAlignment: CrossAxisAlignment.center,
//            children: [
//              Expanded(
//                child: Container(
//                  width: double.infinity,
//                  height: 1,
//                  color: getColor(context, textGray),
//                ),
//              ),
//              Text(
//                getString('loginPageOrText'),
//                style: getFont(context, size: 14, color: textBlack),
//              ),
//              Expanded(
//                child: Container(
//                  width: double.infinity,
//                  height: 1,
//                  color: getColor(context, textGray),
//                ),
//              ),
//            ],
//            divider: VerticalDivider(width: 15, color: NUIColors.NUITransparent)
//          ),
//        ),
//        Divider(height: 27, color: NUIColors.NUITransparent),
//        SocialLoginButtons(),
//        Divider(height: 32, color: NUIColors.NUITransparent)
          Divider(height: 80, color: Colors.transparent) //TODO: Comment this is social buttons are added
      ]
    );
  }

  Widget _forgotPasswordContent(BuildContext context){
    return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Divider(color: Colors.transparent, height: 24),
          Text(getString('forgotPasswordPageTitle'),
            textAlign: TextAlign.center,
            style: getFont(context, size: 18, color: textBlack, isBold: false)
          ),
          Divider(color: Colors.transparent, height: 16),
          Container(
            margin: EdgeInsets.fromLTRB(40, 0, 40, 0),
            child: Text(getString('forgotPasswordPageDesc'),
              textAlign: TextAlign.center,
              style: getFont(context, size: 16, color: textDarkGray, isBold: false)
            ),
          ),
          ForgotPasswordFormBody(),
          Divider(color: Colors.transparent, height: 20),
          NUIBouncingAnimation(
            onTap: () {
              //notify the main widget that the current bottom sheet mode is forgotPassword
              this.switchToLoginSheet();
            },
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Text(
                getString('forgotPasswordPageBackToLoginText'),
                style: getFont(context, size: 16, color: accentBright, isBold: true),
              ),
            ),
          ),
          Divider(color: Colors.transparent, height: 28),
        ]
    );
  }

  @override
  Widget build(BuildContext context) {
    if (showLoginSheet['mode'] == 'login' || widget.showLoginSheet["backToLogin"] == "true") {
      logNUI("LoginScreen", "Switching to login");
      widget.showLoginSheet["backToLogin"] = null;
      switchToLoginSheet();
    }
    return AnimatedBuilder(
      animation: slideUpAnimationController,
      builder: (_, child) {
        return Transform.translate(
          offset: Offset(0.0, slideUpAnimation.value),
          child: Container(
              decoration: NUIBoxDecoration(
                borderRadius: BorderRadius.only(topLeft: Radius.circular(20.0), topRight: Radius.circular(20.0)),
                shadowColor: getColor(context, toolbarShadow),
                shadowRadius: 5,
                shadowOffset: Offset(-1, -1),
                gradient: NUILinearGradient(
                  colors: [
                    getColor(context, lightBlue),
                    getColor(context, white)
                  ],
                  orientation: GradientOrientation.T_B
                )
              ),
              child: convertForgotPassword
                  ? _forgotPasswordContent(context)
                  : _loginContent(context)
          ),
        );
      },
      child: Placeholder(key: Key('animated')),
    );
  }
}

class LoginPageFormBody extends StatefulWidget {
  final Function forgotPasswordButtonCallback;
  const LoginPageFormBody(this.forgotPasswordButtonCallback);

  @override
  _LoginPageFormBodyState createState() => _LoginPageFormBodyState();
}

class _LoginPageFormBodyState extends State<LoginPageFormBody> with TickerProviderStateMixin{
  LoginBloc loginBloc;
  NUIForm form;

  int curValue = 3;
  int _currentValue = 15;
  YellowButtonController _buttonController;
  String _loginErrorMsg;
  bool _rememberMe = false;
  AuthEmailAcc _rememberedEmail;
  CheckBoxController checkBoxController;

  @override
  void initState() {
    super.initState();
    _loadRemembered();
    checkBoxController = CheckBoxController();
    _buttonController = YellowButtonController();
    loginBloc = LoginBloc();
    form = LoginForm().emailPasswordForm(context, (id, value, form) {
      logNUI("IFormPreview", "Text changed on $id, cur value : $value");
    }, (id, clickType, form) {
      logNUI("IFormPreview", "Button clicked on $id, click type : $clickType");
      final validated = form.validate(false);
      final values = form.extractValues();
    }, (id, selection, selected, form) {
      logNUI("IFormPreview", "Item selected on $id, cur value : ${selection.value}, selected : $selected");
    });
  }

  void _loadRemembered() async{
    final remembered = await NUIAuth.get().getRememberedEmail(context);
    _rememberedEmail = remembered;
    _rememberMe = remembered != null;
    logNUI("LoginScreen", "Has remembered email: ${_rememberedEmail != null}, rememberMe: $_rememberMe");
    setState(() {
      checkBoxController.setChecked(_rememberMe);
      if(_rememberMe) {
        form.fillInValues({
          "edtEmail": NUIFormValue(inpId: "edtEmail", value: NUIFormSelection(value: _rememberedEmail.email)),
          "edtPassword": NUIFormValue(inpId: "edtPassword", value: NUIFormSelection(value: _rememberedEmail.password.maskedPassword))
        });
      }
    });
  }

  Widget _renderForm(BuildContext context) {
    return NUIFormContent(form: form);
  }

  void _login() async{
    bool valid = form.validate(false);
    if (valid) {
      final values = form.extractValues();
      final email = values["edtEmail"].value.value;
      var password = values["edtPassword"].value.value;

      if(_rememberMe && _rememberedEmail != null){
        //Check if is using remembered email
        password = _rememberedEmail.password.getRealPassword(password);
      }

      _buttonController.updateState(true, null);
      final loginSuccess = await loginBloc.login(email: email, password: password);
      _buttonController.updateState(false, null);
      if (loginSuccess.success) {
        logNUI("LoginScreen", "Logged in, remember me: $_rememberMe");
        if(_rememberMe == true){
          logNUI("LoginScreen", "Remembering email: $email, password: $password");
          await NUIAuth.get().rememberEmail(email: email, password: password);
        }
        popAllAndPush(context, MainScreen());
        _loginErrorMsg = null;
      }
      else {
        setState(() {
          _loginErrorMsg = loginSuccess.message.isNullOrEmpty() ? "Failed to login, please try again" : loginSuccess.message;
        });
      }
    }
  }

  void _rememberMeCheckUpdate(bool checked){
    logNUI("LoginScreen", "Remember me: $checked");
    _rememberMe = checked;
    if(!checked){
      NUIAuth.get().clearRememberedAccount();
      return;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(16, 31, 16, 12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          _renderForm(context),
          Container(
            margin: EdgeInsets.fromLTRB(8, 18, 0, 18),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                NUIRow(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  divider: VerticalDivider(
                    color: Colors.transparent,
                    width: 14,
                  ),
                  children: [
                    CustomCheckbox(checked: _rememberMe, controller: checkBoxController, onCheckChange: (checked){
                      _rememberMeCheckUpdate(checked);
                    }),
                    Text(getString("loginPageRememberMe"), style: getFont(context, size: 12, color: accentBright, isBold: true))
                  ],
                ),
                NUIBouncingAnimation(
                  onTap: () {
                    widget.forgotPasswordButtonCallback();
                  },
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(8, 8, 0, 8),
                    child: Text(
                      getString('loginPageForgotPassword'),
                      style: getFont(context, size: 12, color: accentBright, isBold: true),
                    ),
                  ),
                )
              ],
            ),
          ),
          Divider(height: 30, color: Colors.transparent),
          YellowBackgroundButton(
            padding: EdgeInsets.all(0),
            text: getString('loginPageLoginButtonText'),
            controller: _buttonController,
            onTap: () {
              _login();
            },
          ),
          AnimatedSize(
            duration: Duration(milliseconds: 250),
            vsync: this,
            child: Visibility(
              visible: _loginErrorMsg != null,
              child: Padding(
                padding: EdgeInsets.only(top: 10),
                child: Text(_loginErrorMsg ?? "", style: getFont(context, size: 12, color: formErrorRed, isBold: true)),
              )
            ),
          )
        ],
      ),
    );
  }
}

class SocialLoginButtons extends StatelessWidget {

  void _loginWithGoogle(BuildContext context) async{
    final result = await NUIAuth.get().loginWithGoogle(context);
    logNUI("LoginScreen", "Login with google result, success : ${result.success}, message : ${result.message}");
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: NUIColumn(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        divider: Divider(height: 16, color: NUIColors.NUITransparent),
        children: [
          GoogleLoginButton(
              text: getString('loginPageContinueWithGoogle'), onTap: () {
                _loginWithGoogle(context);
          }),
          AppleLoginButton(
              text: getString('loginPageContinueWithAppleID'), onTap: () {}),
        ],
      ),
    );
  }
}

//this section belong to the forgot password content
class ForgotPasswordFormBody extends StatefulWidget {
  @override
  _ForgotPasswordFormBodyState createState() => _ForgotPasswordFormBodyState();
}

class _ForgotPasswordFormBodyState extends State<ForgotPasswordFormBody> with TickerProviderStateMixin{
  NUIForm form;
  int curValue = 3;
  int _currentValue = 15;
  YellowButtonController buttonController;
  String _errorMsg;
  String _successMessage;
  UserBloc userBloc;

  @override
  void initState(){
    super.initState();
    userBloc = UserBloc();
    buttonController = YellowButtonController();
    form = ForgotPasswordForm().emailForm(context, (id, value, form) {
      logNUI("IFormPreview", "Text changed on $id, cur value : $value");
    }, (id, clickType, form) {
      logNUI("IFormPreview", "Button clicked on $id, click type : $clickType");
      final validated = form.validate(false);
      final values = form.extractValues();
    }, (id, selection, selected, form) {
      logNUI("IFormPreview",
          "Item selected on $id, cur value : ${selection.value}, selected : $selected");
    });
  }

  Widget _renderForm(BuildContext context) {
    return NUIFormContent(form: form);
  }

  void _forgotPassword(BuildContext context) async{
    setState(() {
      _errorMsg = null;
      _successMessage = null;
    });

    final validated = form.validate(false);
    if(!validated) return;

    final values = form.extractValues();
    final email = values["edtEmail"];

    if(email == null || isNullOrEmpty(email.value?.value)) return;

    buttonController.updateState(true, null);
    final result = await userBloc.forgotPassword(context, email.value.value);
    buttonController.updateState(false, null);

    if(result.success){
      //Todo: Successful, show a dialog or something
      setState(() {
        _successMessage = result.message ?? "A reset password link has been sent to your email";
      });
      // toast(result.message ?? "A reset password link has been sent to your email", context: context, textStyle: getFont(context, size: 14, color: textWhite));
    }
    else{
      setState(() {
        _errorMsg = result.message.isNullOrEmpty() ? "Failed to reset password, please try again" : result.message;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.fromLTRB(16, 31, 16, 8),
              child: _renderForm(context),
            ),
            Divider(color: Colors.transparent, height: 129),
            Align(
              alignment: Alignment.bottomCenter,
              child: YellowBackgroundButton(
                text: getString('forgotPasswordSendEmail'),
                controller: buttonController,
                onTap: (){
                  _forgotPassword(context);
                },
              )
            ),
            AnimatedSize(
              duration: Duration(milliseconds: 250),
              vsync: this,
              child: Visibility(
                  visible: _errorMsg != null,
                  child: Padding(
                    padding: EdgeInsets.only(top: 10),
                    child: Text(_errorMsg ?? "", style: getFont(context, size: 12, color: formErrorRed, isBold: true)),
                  )
              ),
            ),
            AnimatedSize(
              duration: Duration(milliseconds: 250),
              vsync: this,
              child: Visibility(
                  visible: _successMessage != null,
                  child: Padding(
                    padding: EdgeInsets.only(top: 10),
                    child: Text(_successMessage ?? "", style: getFont(context, size: 12, color: textGray, isBold: true)),
                  )
              ),
            ),
          ],
        ),
      ),
    );
  }
}
