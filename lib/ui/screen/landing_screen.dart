import 'dart:developer';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_controller.dart';
import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:nesternship_flut/bloc/landing_page_bloc.dart';
import 'package:nesternship_flut/bloc/master_bloc.dart';
import 'package:nesternship_flut/bloc/notification_bloc.dart';
import 'package:nesternship_flut/bloc/profile_bloc.dart';
import 'package:nesternship_flut/bloc/registration_bloc.dart';
import 'package:nesternship_flut/ui/custom/apple_login_button.dart';
import 'package:nesternship_flut/ui/custom/apple_login_button.dart';
import 'package:nesternship_flut/ui/custom/fading_slider.dart';
import 'package:nesternship_flut/ui/custom/google_login_button.dart';
import 'package:nesternship_flut/ui/custom/yellow_background_button.dart';
import 'package:nesternship_flut/ui/screen/login_screen.dart';
import 'package:nesternship_flut/ui/screen/registration_steps_screen.dart';
import 'package:nui_core/nui_core.dart';
import 'package:visibility_detector/visibility_detector.dart';

import '../../bloc/training_course_bloc.dart';
import '../../data/model/banner.dart';
import '../custom/pageview_indicator.dart';
import '../screen_codes.dart';
import '../style/app_colors.dart';
import '../style/app_theme.dart';
import 'registration_main_screen.dart';

class LandingScreen extends StatelessWidget {
  final CarouselController buttonCarouselController = CarouselController();
  final LandingPageBloc _bloc = new LandingPageBloc();
  final RegistrationBloc registrationBloc = new RegistrationBloc();
  final ProfileBloc profileBloc = new ProfileBloc();
  final NotificationBloc notificationBloc = NotificationBloc();
  final MasterBloc masterBloc = MasterBloc();

  @override
  Widget build(BuildContext context) {
    NUIDisplayUtil.setStatusBarStyleAttr(NUIOverlayStyle(darkStatusBarIcon: false));
    NUIDisplayUtil.setStatusBarStyleAttr(NUIOverlayStyle(darkStatusBarIcon: false));
    //Uncomment to test api
    // registrationBloc.registerAccount();
    // profileBloc.getUserProfile("3");
    // notificationBloc.getNotificationCount("9");
    // notificationBloc.setNotificationRead();

    // profileBloc.getUserProfile("3");
    // notificationBloc.getNotificationCount("2");
    // notificationBloc.deleteNotification("8");
    // profileBloc.setMemberInactive("2");
    // notificationBloc.createNotification();
    masterBloc.syncMasterData();

    return NUIScreen(
        overlayStyle: NUIOverlayStyle(darkStatusBarIcon: false),
        content: (context) => Scaffold(
          body: Container(
            decoration: NUIBoxDecoration(
                gradient: NUILinearGradient(colors: [
              getColor(context, accentBlueDark),
              getColor(context, accentBlueBright),
            ], orientation: GradientOrientation.T_B)),
            width: double.infinity,
            height: double.infinity,
            child: NUIColumn(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                LandingPageSlider(),
              ],
            )
          ),
        ),
        screenCode: ScreenCodes.splash);
  }
}

class LandingPageSlider extends StatefulWidget {
  @override
  _LandingPageSliderState createState() => _LandingPageSliderState();
}

class _LandingPageSliderState extends State<LandingPageSlider> {
  int _currentIndex = 0;
  final LandingPageBloc _bloc = LandingPageBloc();
  List<BannerImage> imageList;
  bool firstTime = true;

  void callback() {
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    imageList = _bloc.getLandingPageSliderImages();

    NUIAsync.delay(Duration(milliseconds: 200), () {
      setState(() {
        firstTime = false;
      });
    });
  }

  Widget _getCarousel(BuildContext context){
    final width = screenWidth(context);
    final height = screenHeight(context);
    return FadingSlider(
      width: width,
      height: height,
      carouselOptions: CarouselOptions(
          height: height,
          viewportFraction: 1.0,
          enlargeCenterPage: false,
          autoPlay: true,
          autoPlayInterval: Duration(seconds: 8),
          autoPlayAnimationDuration: Duration(milliseconds: 500),
          autoPlayCurve: Curves.fastOutSlowIn,
          onPageChanged: (index, reason) {
            setState(() {
              _currentIndex = index;
            });
          }
      ),
      count: imageList.length,
      backgroundInflater: (context, position, fraction) {
        final e = imageList[position];
        return SliderImage(e, double.infinity, double.infinity);
      },
      foregroundInflater: (context, position, fraction) {
        final e = imageList[position];
        return SliderDetails(e, fraction);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Stack(children: [
        _getCarousel(context),
        SliderIndicatorAndBottomButtons(selectedIndex: _currentIndex)
      ]),
    );
  }
}

class SliderImage extends StatelessWidget {
  SliderImage(this.currentImage, this.sliderImageWidth, this.sliderImageHeight);

  final BannerImage currentImage;
  final double sliderImageWidth;
  final double sliderImageHeight;

  @override
  Widget build(BuildContext context) {
    return Image.asset(currentImage.bannerImage,
        fit: BoxFit.cover,
        height: sliderImageHeight,
        width: sliderImageWidth
    );
  }
}

class SliderDetails extends StatelessWidget {
  SliderDetails(this.image, this.visibilityFraction);
  final BannerImage image;
  final double visibilityFraction;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Divider(
          height: 28,
          color: Colors.transparent,
        ),
        Opacity(
          opacity: visibilityFraction,
          child: Text(image.title,
              style: getFont(context, size: 24, color: white, isBold: false)),
        ),
        Divider(
          height: 20,
          color: Colors.transparent,
        ),
        Opacity(
          opacity: visibilityFraction,
          child: Container(
            margin: EdgeInsets.fromLTRB(16, 0, 16, 0),
            child: Text(
              image.description,
              style: getFont(context, size: 16, color: white, isBold: false),
              maxLines: 4,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.center,
            ),
          ),
        ),
        Divider(
          height: 160,
          //Todo: Change back to 280 if social login buttons are enabled
          color: Colors.transparent,
        )
      ],
    );
  }
}

class SliderIndicatorAndBottomButtons extends StatefulWidget {
  final int selectedIndex;
  final LandingPageBloc _bloc = LandingPageBloc();

  SliderIndicatorAndBottomButtons({@required this.selectedIndex});

  @override
  _SliderIndicatorAndBottomButtonsState createState() =>
      _SliderIndicatorAndBottomButtonsState();
}

class _SliderIndicatorAndBottomButtonsState
    extends State<SliderIndicatorAndBottomButtons> {
  ValueNotifier<int> _currentPageNotifier;

  @override
  void initState() {
    super.initState();
    _currentPageNotifier = ValueNotifier<int>(widget.selectedIndex);
  }

  @override
  Widget build(BuildContext context) {
    _currentPageNotifier.value = widget.selectedIndex;

    return NUIColumn(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Expanded(
            child: Container(
          margin: EdgeInsets.fromLTRB(0, 30, 0, 0),
          alignment: Alignment.center,
          child: Image.asset(
            'assets/images/logo_white.png',
          ),
        )),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            PageViewIndicator(
              currentPageNotifier: _currentPageNotifier,
              dotSpacing: 16,
              itemCount: widget._bloc.getLandingPageSliderImages().length,
              size: 8,
              selectedSize: 12,
              borderColor: getColor(context, white),
              borderWidth: 1,
              dotColor: NUIColors.NUITransparent,
              selectedDotColor: getColor(context, white),
              selectedBorderColor: getColor(context, white),
            )
          ],
        ),
        Divider(
          height: 186,
          color: Colors.transparent,
        ),
        //TODO: Uncomment the social login buttons if enabled
//        GoogleLoginButton(text: "Continue with Google", onTap: () {}),
//        Divider(
//          height: 16,
//          color: Colors.transparent,
//        ),
//        AppleLoginButton(text: "Continue with Apple ID", onTap: () {}),
//        Divider(
//          height: 16,
//          color: Colors.transparent,
//        ),
        YellowBackgroundButton(
          text: getString("landingScreenButtonDesc"),
          onTap: () {
            push(context, RegistrationMainScreen(), transition: PageTransition.RIGHT_LEFT);
          },
        ),
        Divider(
          height: 10,
          color: Colors.transparent,
        ),
        FlatButton(
          onPressed: () {
            push(context, LoginScreen(), transition: PageTransition.FADE, duration: Duration(seconds: 1));
          },
          child: Text(
            getString('landingScreenToLoginButton'),
            style: getFont(context, size: 16, color: white, isBold: true),
          ),
          padding: EdgeInsets.all(10),
        ),
        Divider(
          height: 16,
          color: Colors.transparent,
        ),
      ],
    );
  }
}




