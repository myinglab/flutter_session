import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:nesternship_flut/bloc/notification_bloc.dart';
import 'package:nesternship_flut/bloc/user_bloc.dart';
import 'package:nesternship_flut/data/model/country_model.dart';
import 'package:nesternship_flut/data/model/notification.dart';
import 'package:nesternship_flut/data/model/search_criteria.dart';
import 'package:nesternship_flut/ui/custom/listing_placeholder.dart';
import 'package:nesternship_flut/ui/custom/no_internet_connection_holder.dart';
import 'package:nesternship_flut/ui/custom/notification_listing_toolbar.dart';
import 'package:nesternship_flut/ui/custom/search_toolbar.dart';
import 'package:nesternship_flut/ui/list/skeletal/listitem_lottie_skeletal.dart';
import 'package:nesternship_flut/ui/list/skeletal/listitem_notification_skeletal.dart';
import 'package:nesternship_flut/ui/list/skeletal/listitem_reg_country_skeletal.dart';
import 'package:nesternship_flut/ui/screen_codes.dart';
import 'package:nui_auth/ui/nui_auth_activity_screen.dart';
import 'package:nui_core/nui_core.dart';
import 'package:nui_core/ui/component/nui_search_text.dart';
import 'package:nesternship_flut/ui/list/app_list_repo.dart';
import '../../style/app_theme.dart';
import 'notification_detail_screen.dart';

void showNotifications(BuildContext context, NotificationBloc bloc) {
  push(context, NotificationListingScreen(bloc));
}

class NotificationListingScreen extends StatefulWidget {
  NotificationListingScreen(this.notiBloc);
  final NotificationBloc notiBloc;
  @override
  _NotificationListingScreenState createState() =>
      _NotificationListingScreenState();
}

class _NotificationListingScreenState extends State<NotificationListingScreen> {
  UserBloc user = UserBloc();
  NotificationBloc bloc;
  NUIListController listController;
  @override
  void initState() {
    super.initState();
    bloc = widget.notiBloc;
    listController = NUIListController(onLoadComplete: () {});
  }

  @override
  Widget build(BuildContext context) {
    return NUIAuthScreen(
      overlay: NUIOverlayStyle(darkStatusBarIcon: true),
      module: ScreenCodes.notificationList,
      child: Scaffold(
        body: Container(
          width: double.infinity,
          height: double.infinity,
          color: getColor(context, white),
          alignment: Alignment.center,
          child: NUIColumn(
            mainAxisSize: MainAxisSize.max,
            children: [
              NotificationListingToolbar(user.getUserId(), bloc, clearCallback: (){
                listController.reload();
              },),
              Expanded(
                  child: NoInternetConnectionChecker(
                    child: NUIAnimatedHeaderList(
                      repo: AppListRepo(),
                      enablePagination: true,
                      enableSwipeRefresh: true,
                      controller: listController,
                      skeletalCount: 3,
                      placeHolder: () => ListingPlaceHolder(title: "No Notifications Yet", desc: "It seems like you do not have any notifications at the moment!",),
                      paginationInflater: NUILoadingListItem(shadowRadius: 0, inflater: (context) => ListingPagination()),
                      errorHolder: (NUIListViewModel data) => ListingPlaceHolder(title: "Oops!", desc: "It seems like something went wrong, try refreshing the page!",),
                      itemAnimation: NUIListItemAnimation.FADE,
                      itemAnimationDuration: Duration(milliseconds: 50),
                      skeletalInflater: NUILoadingListItem(inflater: (context) => LINotificationSkeletal()),
                      padding: const EdgeInsets.only(bottom: 8, top: 8),
                      initialList: [],
                      bloc: bloc.userNotifications(user.getUserId()),
                      clickListener: (data, position, clickType) {
                          if (data is UserNotification) {
                            if(match(clickType, NUIListActions.REMOVE)){
                              bloc.deleteNotification(data.notificationId.toString());
                              listController.removeItemAt(position);
                            }
                            else {
                              markNotificationAsRead(context, data);
                              showNotificationDetail(context, data);
                            }
                          }
                      },
                    ),
                  ))
            ],
          ),
        ),
      ),
    );
  }

  void markNotificationAsRead(BuildContext context, UserNotification noti) async{
    final result = await bloc.setNotificationRead(noti.notificationId);
    noti.read = true;
    listController.refreshList();
  }
}



