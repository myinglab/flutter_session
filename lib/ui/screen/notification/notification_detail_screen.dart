import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:nesternship_flut/data/model/notification.dart';
import 'package:nesternship_flut/ui/custom/notification_toolbar.dart';
import 'package:nesternship_flut/ui/screen_codes.dart';
import 'package:nui_core/nui_core.dart';
import 'package:nui_form/nuif_form.dart';
import 'package:intl/intl.dart';
import '../../style/app_theme.dart';

void showNotificationDetail(BuildContext context, UserNotification notification){
  push(context, NotificationDetailScreen(notification));
}

class NotificationDetailScreen extends StatefulWidget {
  NotificationDetailScreen(this.notification);
  final UserNotification notification;
  @override
  _NotificationDetailScreenState  createState() => _NotificationDetailScreenState ();
}

class  _NotificationDetailScreenState  extends State<NotificationDetailScreen> {
  @override
  Widget build(BuildContext context){
    return NUIScreen(
      overlayStyle: NUIOverlayStyle(darkStatusBarIcon: true),
      screenCode: ScreenCodes.notificationDetail,
      content: (context) => Scaffold(
        body:Container(
          height: double.infinity,
          width: double.infinity,
          color: getColor(context, white),
          alignment: Alignment.center,
          child:NUIColumn(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children:[
              NotificationToolbar(widget.notification.title),
              Expanded(
                child:ListView(
                  padding: EdgeInsets.only(bottom:30, top: 16, left: 24, right: 24),
                  children: [
                    dateCalenderNotification(context, widget.notification.dateTime),
                    Divider(height: 16, color: NUIColors.NUITransparent),
                    notificationTile(context, widget.notification.title),
                    Divider(height: 16, color: NUIColors.NUITransparent),
                    notificationBody(context, widget.notification.description)
                  ],
                )
              ),
            ]
          )
        )
      ),
    );
  }

  Widget dateCalenderNotification(BuildContext context, DateTime date){
   String formattedDate =  NUIDTUtil.dateToString(date, format: 'dd MMM yyyy');
    return Container(
      child: NUIRow(
          children: [
            Icon(Icons.calendar_today, color: getColor(context, textDarkGray), size: 20,),
            SizedBox(width:19),
            Flexible(child: Text(formattedDate , style:getFont(context, size:14, color: textGray)))
          ],
        )
    );
  }

  Widget notificationTile(BuildContext context, String title){
    return Container(
        alignment: Alignment.centerLeft,
      child: Text(title, style:getFont(context, size:24, color: textDarkGray))
    );
  }

  Widget notificationBody(BuildContext context, String body){
    return Container(
      alignment: Alignment.centerLeft,
        child: Text(body, style:getFont(context, size:16, color: textDarkGray))
    );
  }
}