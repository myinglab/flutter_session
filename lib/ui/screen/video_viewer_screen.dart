import 'package:advance_pdf_viewer/advance_pdf_viewer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:nesternship_flut/bloc/course_bloc.dart';
import 'package:nesternship_flut/data/model/search_course.dart';
import 'package:nesternship_flut/datalayer/entity/course/course_response.dart';
import 'package:nesternship_flut/ui/custom/app_video_player.dart';
import 'package:nesternship_flut/ui/screen_codes.dart';
import 'package:nui_animation/nui_button_animation.dart';
import 'package:nui_core/nui_core.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import '../style/app_theme.dart';
import 'package:nesternship_flut/ui/custom/no_internet_connection_holder.dart';
import 'package:flutter/services.dart';

void showVideoViewer(BuildContext context, {@required CourseResponse searchCourse, bool isYoutube: false}) {
  push(context, VideoViewerScreen(searchCourse, isYoutube: isYoutube), transition: PageTransition.BOTTOM_TOP);
}

//void testYoutubeVideo(BuildContext context, {@required CourseResponse searchCourse}) {
//  searchCourse.sourceUrl = "https://www.youtube.com/watch?v=dRc1xBHt6is";
//  push(context, VideoViewerScreen(searchCourse), transition: PageTransition.BOTTOM_TOP);
//}

class VideoViewerScreen extends StatefulWidget {
  VideoViewerScreen(this.searchCourse, {this.isYoutube:false});
  final CourseResponse searchCourse;
  final bool isYoutube;
  @override
  _VideoViewerScreenState createState() => _VideoViewerScreenState();
}

class _VideoViewerScreenState extends State<VideoViewerScreen> {
  final CourseBloc _courseBloc = CourseBloc();
  Future<bool> isCompleted;

  void getCourseStatus() async {
    _courseBloc.updateCourseStatus(widget.searchCourse.id);
    this.isCompleted = _courseBloc.getCourseStatus(widget.searchCourse.id);
  }

  @override
  void initState() {
    super.initState();
    getCourseStatus();
  }

  @override
  Widget build(BuildContext context) {
    return NUIScreen(
      overlayStyle: NUIOverlayStyle(darkStatusBarIcon: true),
      screenCode: ScreenCodes.videoViewer,
      content: (context) => Scaffold(
          body: Container(
              color: getColor(context, white),
              alignment: Alignment.center,
              child: NUIColumn(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    NUIFutureWidget<bool>(
                        futureValue: isCompleted,
                        callback: (state, data, msg) {
                          if (state == FutureState.SUCCESS && data != null) {
                            return CourseToolBar(
                                key: Key("Loaded"),
                                searchCourse: widget.searchCourse,
                                isCompleted: data);
                          } else {
                            return CourseToolBar(
                                key: Key("Loading"),
                                searchCourse: widget.searchCourse,
                                isCompleted: false);
                          }
                        }),
                    Expanded(
                      child: Container(
                      height: double.infinity,
                      width: double.infinity,
                      child: Stack(
                        children: [
                          NoInternetConnectionChecker(
                            child: widget.isYoutube != true ? VideoContentBody(widget.searchCourse) : YoutubeVideoContentBody(widget.searchCourse),
                          ),
                          Container(
                            height: 10,
                            width: double.infinity,
                            decoration: NUIBoxDecoration(
                                gradient: NUILinearGradient(colors: [
                              getColor(context, toolbarShadow),
                              NUIColors.NUITransparent
                            ], orientation: GradientOrientation.T_B)),
                          )
                        ],
                      ),
                    )),
                  ]
              )
          )
      ),
    );
  }
}

class VideoContentBody extends StatelessWidget {
  VideoContentBody(this.searchCourse);
  final CourseResponse searchCourse;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: NUIColumn(children: [
        AppVideoPlayer(searchCourse.sourceUrl),
        Container(
            padding: EdgeInsets.only(left: 15, right: 15, top: 22, bottom: 22),
            child: NUIColumn(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(searchCourse.description,
                      style: getFont(context, size: 16, color: textBlack)),
                  Divider(height: 12, color: NUIColors.NUITransparent),
                  Text(searchCourse.description,
                      style: getFont(context, size: 14, color: textLightGray2)),
                ]))
      ]),
    );
  }
}


class YoutubeVideoContentBody extends StatelessWidget {
  YoutubeVideoContentBody(this.searchCourse);
  final CourseResponse searchCourse;

  String _youtubeIdFromUrl(String url){
    String videoId = YoutubePlayer.convertUrlToId(url);
    return videoId;
  }

  @override
  Widget build(BuildContext context) {
    final YoutubePlayerController _controller = YoutubePlayerController(
      initialVideoId: _youtubeIdFromUrl(searchCourse.sourceUrl),
      flags: YoutubePlayerFlags(
        autoPlay: false,
        mute: true,
      ),
    );

    return Container(
      width: double.infinity,
      height: double.infinity,
      child: YoutubePlayerBuilder(
        player: YoutubePlayer(
        controller: _controller),
        builder: (context, player){
          return Column(
            children: [
              player,
              Container(
                  padding: EdgeInsets.only(left: 15, right: 15, top: 22, bottom: 22),
                  child: NUIColumn(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(searchCourse.description, style: getFont(context, size: 16, color: textBlack)),
                        Divider(height: 12, color: NUIColors.NUITransparent),
                        Text(searchCourse.description,
                            style: getFont(context, size: 14, color: textLightGray2)),
                      ]
                  )
              )
            ],
          );
        },
      )
    );
  }
}

class CourseToolBar extends StatefulWidget {
  CourseToolBar({Key key, this.searchCourse, this.isCompleted})
      : super(key: key);
  final CourseResponse searchCourse;
  final bool isCompleted;

  @override
  CourseToolBarState createState() => CourseToolBarState();
}

class CourseToolBarState extends State<CourseToolBar> {
  final CourseBloc _courseBloc = CourseBloc();
  bool isCompleted;

  @override
  void initState() {
    super.initState();
    isCompleted = widget.isCompleted;
  }

  void updateCourseStatus() async {
    final DataResult result = await _courseBloc.updateCourseStatus(widget.searchCourse.id, courseCompleted: true);
    if (result.success == true) {
      setState(() {
        isCompleted = true;
      });
    } else {
      toast("Something went wrong, please try again",
          context: context,
          textStyle: getFont(context, size: 14, color: textWhite));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: toolbarWhiteDecoration(context),
      child: Column(
        children: [
          NUIStatusBar(),
          Container(
            height: 48,
            width: double.infinity,
            child: Padding(
              padding: EdgeInsets.only(left: 8, right: 8),
              child: NUIRow(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                divider:
                    VerticalDivider(width: 10, color: NUIColors.NUITransparent),
                children: [
                  NUIBouncingAnimation(
                      child: Padding(
                          padding: EdgeInsets.all(10),
                          child: loadSvg("back_ui.svg",
                              width: 28,
                              height: 28,
                              color: getColor(context, accentBright))),
                      onTap: () {
                        pop(context);
                      }),
                  Expanded(
                    child: Text(
                      widget.searchCourse.description,
                      style: getFont(context, size: 16, color: textDarkGray),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                    ),
                  ),
                  isCompleted
                      ? Align(
                        alignment: Alignment.centerRight,
                        child: Padding(
                        padding: const EdgeInsets.only(left: 10, right: 10, top: 13, bottom: 13),
                        child: Container(
                            width: 22,
                            height: 22,
                            alignment: Alignment.center,
                            decoration: NUIBoxDecoration(
                                color: getColor(context, greenHighlight2),
                                shadowOffset: Offset(1, 1),
                                shadowRadius: 3,
                                borderRadius: BorderRadius.circular(12),
                                shadowColor: getColor(context, greenHighlight2).withOpacity(0.2)
                            ),
                            child: Icon(Icons.check, color: Colors.white, size: 16)
                        )
                  ),
                      )
                      : NUIBouncingAnimation(
                    onTap: () {
                      this.updateCourseStatus();
                    },
                    child: Padding(
                        padding: EdgeInsets.only(top: 13, bottom: 13, left: 8, right: 8),
                        child: Text(getString("markCompleted"),
                            style: getFont(context,
                                isBold: true,
                                size: 12,
                                color: accentBright))),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
