import 'package:flutter/material.dart';
import 'package:nesternship_flut/bloc/course_bloc.dart';
import 'package:nesternship_flut/bloc/search_course_bloc.dart';
import 'package:nesternship_flut/data/model/search_course.dart';
import 'package:nesternship_flut/data/model/search_criteria.dart';
import 'package:nesternship_flut/ui/custom/listing_placeholder.dart';
import 'package:nesternship_flut/ui/custom/no_internet_connection_holder.dart';
import 'package:nesternship_flut/ui/custom/search_toolbar.dart';
import 'package:nesternship_flut/ui/list/app_list_repo.dart';
import 'package:nesternship_flut/ui/list/skeletal/listitem_search_course_skeletal.dart';
import 'package:nesternship_flut/ui/screen/pdf_viewer_screen.dart';
import 'package:nesternship_flut/ui/screen/video_viewer_screen.dart';
import 'package:nesternship_flut/ui/screen/webview_screen.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_auth/ui/nui_auth_activity_screen.dart';
import 'package:nui_core/nui_core.dart';
import 'package:nui_core/ui/component/listview/nui_list_interface.dart';
import 'package:nui_core/ui/component/nui_search_text.dart';

class SearchScreen extends StatefulWidget {
  SearchScreen({this.searchCriteria, this.listController, this.searchController});
  final SearchCriteria searchCriteria;
  final NUIListController listController;
  final NUISearchTextController searchController;
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  final CourseBloc bloc = CourseBloc();
  SearchCriteria searchCriteria;
  NUIListController listController;
  NUISearchTextController searchController;

  @override
  void initState() {
    super.initState();

    searchCriteria = widget.searchCriteria;
    listController = widget.listController;
    searchController = widget.searchController;

//    listController = NUIListController(onLoadComplete: () {
//      searchController.endSearching();
//    });
//
//    searchController = NUISearchTextController(searchTrigger: (keyword) {
//      searchCriteria.keyword = keyword;
//      listController.reload();
//    });
//
//    NUIBusEvent.get().listenInvoke(randomUUID(), listener: (payload) async{
//      if(match(payload.code, "100")) {
//        setState(() {
//          searchCriteria.keyword = payload.data;
//        });
//      }
//    });
  }

  @override
  Widget build(BuildContext context) {
    return NUIAuthScreen(
      overlay: NUIOverlayStyle(darkStatusBarIcon: true),
      module: "Search",
      child: Scaffold(
          body: Container(
              color: getColor(context, white),
              child: NUIColumn(
                children: [
                  SearchToolbar(searchController, getString("searchCourseHint"),
                      true, false, searchCriteria.keyword ?? ""),
                  Expanded(
                      child: NoInternetConnectionChecker(
                        child: NUIAnimatedHeaderList(
                          repo: AppListRepo(),
                          enablePagination: true,
                          enableSwipeRefresh: false,
                          controller: listController,
                          skeletalCount: 2,
                          itemAnimation: NUIListItemAnimation.FADE,
                          skeletalInflater: NUILoadingListItem(shadowRadius: 16, inflater: (context) => LISearchCourseSkeletal()),
                          paginationInflater: NUILoadingListItem(shadowRadius: 0, inflater: (context) => ListingPagination()),
                          padding: const EdgeInsets.only(bottom: 5, top: 24),
                          placeHolder: () => ListingPlaceHolder(title: "No Results", desc: "It seems no courses matched your search criteria, try another one!",),
                          errorHolder: (NUIListViewModel data) => ListingPlaceHolder(title: "Oops!", desc: "It seems like something went wrong, try refreshing the page!",),
                          initialList: [],
                          itemAnimationDuration: Duration(milliseconds: 0),
                          bloc: bloc.coursesByKeyword(null, searchCriteria),
                          clickListener: (data, position, clickType) {
                              if (data is SearchCourse) {
                                bloc.redirectCourseToDetail(context, data.course);
                              }
                          },
                        ),
                      )
                  )
                ],
              ))),
    );
  }
}

class SearchScreenBlank extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            color: getColor(context, white),
            child: NUIColumn(
              children: [
                SearchToolbar(NUISearchTextController(searchTrigger: (value){}), getString("searchCourseHint"),
                    true, false, ""
                ),
              ],
            ))
    );
  }
}
