import 'package:better_player/better_player.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nesternship_flut/data/model/country_model.dart';
import 'package:nesternship_flut/data/model/search_criteria.dart';
import 'package:nesternship_flut/ui/custom/search_toolbar.dart';
import 'package:nesternship_flut/ui/list/skeletal/listitem_reg_country_skeletal.dart';
import 'package:nesternship_flut/ui/screen_codes.dart';
import 'package:nui_core/nui_core.dart';
import 'package:nui_core/ui/component/nui_search_text.dart';
import 'package:nesternship_flut/ui/list/app_list_repo.dart';
import '../../bloc/registration_bloc.dart';
import '../style/app_theme.dart';

class CountrySelectionScreen extends StatefulWidget {

  final bool getNesternshipCountryOnly;

  const CountrySelectionScreen(this.getNesternshipCountryOnly);

  @override
  _CountrySelectionScreenState createState() => _CountrySelectionScreenState();
}

class _CountrySelectionScreenState extends State<CountrySelectionScreen> {
  final RegistrationBloc bloc = RegistrationBloc();
  SearchCriteria searchCriteria = SearchCriteria();
  NUIListController listController;
  NUISearchTextController searchController;

  @override
  void initState() {
    super.initState();
    listController = NUIListController(onLoadComplete: () {
      searchController.endSearching();
    });

    searchController = NUISearchTextController(searchTrigger: (keyword) {
      logNUI("CountrySelectionScreen", "Keyword : $keyword");
      searchCriteria.keyword = keyword;
      listController.reload();
    });
  }

  @override
  Widget build(BuildContext context) {
    return NUIScreen(
      screenCode: ScreenCodes.countrySelection,
      overlayStyle: NUIOverlayStyle(darkStatusBarIcon: true),
      content: (context) => Scaffold(
        body: Container(
          color: getColor(context, white),
          alignment: Alignment.center,
          child: NUIColumn(
            children: [
              SearchToolbar(searchController, "Select a Country", false, true, ""),
              Expanded(
                child: NUIAnimatedHeaderList(
                repo: AppListRepo(),
                enablePagination: false,
                enableSwipeRefresh: true,
                controller: listController,
                skeletalCount: 2,
                itemAnimation: NUIListItemAnimation.FADE,
                itemAnimationDuration: Duration(milliseconds: 0),
                skeletalInflater: NUILoadingListItem(shadowRadius: 16, inflater: (context) => LIRegistrationCountrySkeletal()),
                padding: const EdgeInsets.only(bottom: 5, top: 5),
                initialList: [],
                bloc: bloc.registrationCountryBloc(searchCriteria, widget.getNesternshipCountryOnly),
                clickListener: (data, position, clickType) {
                  if (data is Country) {
                    popWithResult<Country>(context, data);
                  }
                },
              ))
            ],
          ),
        ),
      ),
    );
  }
}
