import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:nesternship_flut/datalayer/entity/registration/registration_detail.dart';
import 'package:nesternship_flut/ui/custom/yellow_background_button.dart';
import 'package:nesternship_flut/ui/screen/registration/registration_step1.dart';
import 'package:nesternship_flut/ui/screen/registration/registration_step5.dart';
import 'package:nesternship_flut/ui/screen/registration/registration_step3.dart';
import 'package:nesternship_flut/ui/screen/registration/registration_step2.dart';
import 'package:nesternship_flut/ui/screen/registration_success_screen.dart';
import 'package:nesternship_flut/ui/screen_codes.dart';
import 'package:nesternship_flut/ui/screen/registration/registration_step4.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nesternship_flut/ui/custom/stepper_line_indicator.dart';
import 'package:nui_animation/nui_animation.dart';
import 'package:nui_core/nui_core.dart';
import 'package:nui_toolbox/nui_header_tab_widget.dart';

//Validation controller on each screen before the user can continue to next step
class RegistrationController {
  RegistrationController({this.optional: false});
  dynamic _value;
  bool optional;
  Function _valueUpdateListener;
  Function _nextStepListener;

  void setValue(dynamic value) {
    _value = value;
    print(_value);
    _valueUpdateListener();
  }

  void nextStep(){
    if(_nextStepListener != null) _nextStepListener();
  }

  dynamic getValue() {
    return _value;
  }

  bool hasEntered() {
    return _value != null;
  }
}

class RegistrationStepsScreen extends StatelessWidget {
  RegistrationStepsScreen(this.registrationBean);
  final RegistrationDetail registrationBean;

  @override
  Widget build(BuildContext context) {
    return NUIScreen(
      overlayStyle: NUIOverlayStyle(darkStatusBarIcon: false),
      content: (context) => ContentBody(registrationBean),
      screenCode: ScreenCodes.registrationSteps);
  }
}

class ContentBodyController {
  Function onBackPressCallback;
  void onBackPress() {
    if (onBackPressCallback != null) onBackPressCallback();
  }
}

class ContentBody extends StatefulWidget {
  ContentBody(this.registrationDetail);
  final RegistrationDetail registrationDetail;
  @override
  _ContentBodyState createState() => _ContentBodyState();
}

class _ContentBodyState extends State<ContentBody> {
  int curPage = 0;
  PageController pageController = PageController();
  final _currentPageNotifier = ValueNotifier<int>(0);
  final NUIBlocData<int> buttonNotifier = NUIBlocData();
  final NUIBlocData<int> _pageTextNotifier = NUIBlocData();
  final NUIBlocData<int> _skipPageNotifier = NUIBlocData();
  ContentBodyController controller = ContentBodyController();

  RegistrationController controllerStep1 = RegistrationController();
  RegistrationController controllerStep2 = RegistrationController();
  RegistrationController controllerStep3 = RegistrationController(optional: true);
  RegistrationController controllerStep4 = RegistrationController(optional: true);
  RegistrationController controllerStep5 = RegistrationController(optional: true);

  List<RegistrationController> getControllers() {
    return [
      controllerStep1,
      controllerStep2,
      controllerStep3,
      controllerStep4,
      controllerStep5
    ];
  }

  void _updateStepValidation() {
    buttonNotifier.notify(curPage);
  }

  @override
  void initState() {
    super.initState();
    for (RegistrationController cont in getControllers()) {
      cont._valueUpdateListener = () {
        _updateStepValidation();
      };
      cont._nextStepListener = (){
        nextPage();
      };
    }
    controller?.onBackPressCallback = () {
      previousPage();
    };
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: NUIBoxDecoration(
          gradient: NUILinearGradient(colors: [
            getColor(context, accentBlueDark),
            getColor(context, accentBlueBright)
          ],
          orientation: GradientOrientation.T_B)
        ),
        child: SingleChildScrollView(
          child: SizedBox(
            height: screenHeight(context),
            child: NUIColumn(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                NUIStatusBar(),
                Divider(height: 15, color: NUIColors.NUITransparent),
                StepperHeader(
                  widget.registrationDetail,
                  currentPageNotifier: _currentPageNotifier,
                  pageTextNotifier: _pageTextNotifier,
                  bodyController: controller,
                  skipPageNotifier: _skipPageNotifier,
                ),
                Divider(height: 15, color: NUIColors.NUITransparent),
                Expanded(
                  child: PageView(
                    physics: NeverScrollableScrollPhysics(),
                    controller: pageController,
                    children: [
                      NUITabViewContent(child: RegistrationStep1Screen(controllerStep1, widget.registrationDetail)),
                      NUITabViewContent(child: RegistrationStep2Screen(controllerStep2, widget.registrationDetail)),
                      NUITabViewContent(child: RegistrationStep3Screen(controllerStep3, widget.registrationDetail)),
                      NUITabViewContent(child: RegistrationStep4Screen(controllerStep4, widget.registrationDetail)),
                      NUITabViewContent(child: RegistrationStep5Screen(controllerStep5, widget.registrationDetail)),
                    ],
                  )
                ),
                Divider(height: 15, color: NUIColors.NUITransparent),
                NUIBlocStream<int>(
                  bloc: buttonNotifier,
                  stream: (context, state, data) => NUIColumn(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      AnimatedOpacity(
                        duration: Duration(milliseconds: 250),
                        opacity: validatePage(curPage) == true ? 1.0 : 0,
                        child: YellowBackgroundButton(
                            text: getString('registrationScreenContinueButton'),
                            onTap: () {
                              if (validatePage(curPage)) {
                                nextPage();
                              }
                            }),
                      ),
                      AnimatedOpacity(
                        duration: Duration(milliseconds: 250),
                        opacity: curPage > 1 ? 1 : 0,
                        child : NUIBouncingAnimation(
                          onTap: (){
                            if(curPage > 1){
                              skipPage(curPage);
                            }
                          },
                          child: Container(
                            child: Text(
                              getString('registrationStepScreenSkipButton'),
                              style: getFont(context, size: 16, color: white, isBold: true),
                            ),
                            padding: EdgeInsets.fromLTRB(30, 20, 30, 20),
                          ),
                        )
                      )
                    ],
                  ),
                ),
                Divider(height: 32, color: NUIColors.NUITransparent)
              ],
            ),
          ),
        ),
      ),
    );
  }

  bool optionalPage(int position) {
    return getControllers()[position].optional == true;
  }

  bool validatePage(int position) {
    return getControllers()[position].hasEntered();
  }

  void skipPage(int position) {
    FocusScope.of(context).unfocus();
    getControllers()[position].setValue(null);
    nextPage(validate: false);
  }

  void nextPage({bool validate: true}) {
    FocusScope.of(context).unfocus();
    final validated = validatePage(curPage);

    if (validate && !validated) return;

    if (curPage != 4) {
      //Not the last page yet
      curPage += 1;
      buttonNotifier.notify(curPage);
      pageController.jumpToPage(curPage);
      _pageTextNotifier.notify(curPage + 1);
      _skipPageNotifier.notify(curPage + 1);
      _currentPageNotifier.value = curPage;
    } else {
      //Last page
      logNUI("RegistrationStepsScreen", "Registration detail : ${widget.registrationDetail}");
      popAndPush(context, RegistrationSuccessScreen(widget.registrationDetail), transition: PageTransition.FADE, duration: Duration(milliseconds: 250));
    }
  }

  void previousPage() {
    if (curPage != 0) {
      //Not the last page yet
      curPage -= 1;
      buttonNotifier.notify(curPage);
      pageController.jumpToPage(curPage);
      _pageTextNotifier.notify(curPage + 1);
      _skipPageNotifier.notify(curPage + 1);
      _currentPageNotifier.value = curPage;
    } else {
      pop(context);
    }
  }
}

class BottomButtonNavigation extends StatefulWidget {
  @override
  _BottomButtonNavigationState createState() => _BottomButtonNavigationState();
}

class _BottomButtonNavigationState extends State<BottomButtonNavigation> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class StepperHeader extends StatefulWidget {
  StepperHeader(
      this.registrationDetail,
      {this.currentPageNotifier, this.pageTextNotifier, this.bodyController, this.skipPageNotifier});
  final ValueNotifier<int> currentPageNotifier;
  final NUIBlocData<int> pageTextNotifier;
  final NUIBlocData<int> skipPageNotifier;
  final ContentBodyController bodyController;
  final RegistrationDetail registrationDetail;

  @override
  _StepperHeaderState createState() => _StepperHeaderState();
}

class _StepperHeaderState extends State<StepperHeader> {
  int currentStep = 1;
  int totalStep = 5;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(left: 10, right: 10, bottom: 15),
      child: NUIColumn(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        divider: Divider(
          height: 8,
          color: NUIColors.NUITransparent,
        ),
        children: [
          NUIRow(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            divider:
                VerticalDivider(width: 10, color: NUIColors.NUITransparent),
            children: [
              NUIBouncingAnimation(
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(6, 0, 10, 0),
                    child: loadSvg("arrow_backward.svg", color: getColor(context, accentBright), width: 28, height: 28)
                  ),
                  onTap: () {
                    widget.bodyController?.onBackPress();
                  }),
              NUIBlocStream<int>(
                bloc: widget.pageTextNotifier,
                stream: (context, state, data) => RichText(
                  textAlign: TextAlign.start,
                  text: TextSpan(
                      style: getFont(context, size: 14.0, color: textWhite),
                      children: [
                        TextSpan(text: "Step "),
                        TextSpan(text: (data ?? 1).toString(), style: getFont(context, isBold: true, color: textWhite, size: 14)),
                        TextSpan(text: " out of "),
                        TextSpan(text: totalStep.toString(), style: getFont(context, isBold: true, color: textWhite, size: 14)),
                      ]),
                ),
              ),
              Expanded(
                  child: SizedBox(height: 2,)
              ),
              NUIBlocStream<int>(
                bloc: widget.skipPageNotifier,
                stream: (context, state, data) =>
                    //if is page 3 and beyond
                    data != null && data >= 3 && data < totalStep
                        ? NUIBouncingAnimation(
                            onTap: () {
                              popAllAndPush(
                                  context,
                                  RegistrationSuccessScreen(
                                      widget.registrationDetail));
                            },
                            child: Padding(
                              padding: EdgeInsets.all(10),
                              child: Text(
                                getString('registrationScreenSkipAllButton'),
                                style: getFont(context,
                                    size: 14,
                                    color: accentBright,
                                    isBold: true),
                              ),
                            ),
                          )
                        :
                        //material codebase uses this to represent empty widget
                        SizedBox.shrink(),
              )
            ],
          ),
          Container(
            width: double.infinity,
            padding: EdgeInsets.only(left: 58, right: 10),
            child: StepperLineIndicator(
              currentPageNotifier: widget.currentPageNotifier,
              size: 4,
              selectedSize: 8,
              itemCount: 5,
              color: getColor(context, white).withOpacity(0.6),
              selectedColor: getColor(context, white),
            ),
          )
        ],
      ),
    );
  }
}


abstract class RegistrationStepsAnimations{
  AnimationController _controller;
  Animation<Offset> _animation;
  Animation<double> _fadeAnimation;

  Widget getFadingTitle(BuildContext context, String title){
    return FadeTransition(
      opacity: _fadeAnimation,
      child: SlideTransition(
          position: _animation,
          child: Text(title, style: getFont(context, size: 21, color: textWhite))
      ),
    );
  }

  void initAnimations(TickerProviderStateMixin vsync){
    _controller = AnimationController(
      duration: const Duration(milliseconds: 800),
      vsync: vsync,
    )..forward();
    _animation = Tween<Offset>(
      begin: const Offset(1, 0.0),
      end: const Offset(0.0, 0.0),
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.linearToEaseOut,
    ));
    _fadeAnimation = Tween<double>(
      begin: 0,
      end: 1,
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Curves.linear,
    ));
  }
}