import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nesternship_flut/ui/custom/back_toolbar.dart';
import 'package:nesternship_flut/ui/custom/yellow_background_button.dart';
import 'package:nui_core/nui_core.dart';
import '../style/app_theme.dart';

class NoConnectionScreen extends StatefulWidget {
  @override
  _NoConnectionScreenState createState() => _NoConnectionScreenState();
}

class _NoConnectionScreenState extends State<NoConnectionScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      color: getColor(context, white),
      alignment: Alignment.center,
      child: NUIColumn(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          BackToolbar("Lorem Ipsum dolor sit... "),
          Expanded(
              child: Container(
            width: double.infinity,
            alignment: Alignment.center,
            child: NUIColumn(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    child: NUIColumn(
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        AspectRatio(
                            aspectRatio: 1,
                            child: Expanded(
                              child: Image.asset("assets/images/no_wifi.png"),
                            )),
                        Text(getString("noInternetConnection"),
                            style:
                                getFont(context, size: 18, color: textBlack)),
                        Padding(
                            padding: EdgeInsets.only(top: 16),
                            child: Flexible(
                                child: Text(
                                    getString("cantFindConnection"),
                                    style: getFont(context,
                                        size: 16, color: textDarkGray)))),
                        Flexible(
                            child: Text(
                                getString("checkConnection"),
                                style: getFont(context,
                                    size: 16, color: textDarkGray))),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 64),
                    child: YellowBackgroundButton(
                      text:  getString("noInternetConnectionRetryButton"),
                    ),
                  )
                ]),
          )),
        ],
      ),
    );
  }
}
