import 'package:flutter/cupertino.dart';

class AppColors {
  AppColors._builder();
  //Todo : Add your color values here

  static const AppleIcon = Color(0xFFD8D8D8);
  static const ToolbarGradientStart = Color(0xFF01847E);
  static const ToolbarGradientEnd = Color(0xFF33B3A9);
  static const AccentDark = Color(0xFF01847E);
  static const AccentBright = Color(0xFFFFB15D);
  static const AccentBright2 = Color(0xFFEEFFFE);
  static const YellowHighlight = Color(0xFFFFE996);
  static const GreenHighlight = Color(0xFF30B38A);
  static const GreenLight = Color(0xFF30B38A);
  static const Background = Color(0xFFF7FBF6);
  static const GrayOutline = Color(0xFFCCCCCC);
  static const GrayLine = Color(0xFFF5F5F5);
  static const AccentOutline = Color(0xFF60C0B9);
  static const Transparent = Color(0x00FFFFFF);
  static const ToolbarShadow = Color(0xFF7A7A7A);
  static const White = Color(0xFFFFFFFF);
  static const Black = Color(0xFF000000);
  static const BackgroundGray = Color(0xFFE4F0EF);
  static const BackgroundWhite = Color(0xFFFFFFFF);
  static const AccentBlueDark = Color(0xFF686DDB);
  static const AccentBlueBright = Color(0xFF007FBF);
  static const LightBlue = Color(0xFFE1EBF4);
  static const FormErrorRed = Color(0xFFFF8E8E);
  static const ShadowGrayLight = Color(0xFFBDB0B0);
  static const GreenHighlight2 = Color(0xFF24D28D);

  //Text
  static const TextWhite = Color(0xFFFFFFFF);
  static const TextAccent = Color(0xFF36827C);
  static const TextBlue = Color(0xFF007BBA);
  static const TextAccentBright = Color(0xFF60C0B9);
  static const TextBrown = Color(0xFF63513D);
  static const TextAccentBright2 = Color(0xFFA1F0EA);
  static const TextLightGray = Color(0xFFB0A8A8);
  static const TextLightGray2 = Color(0xFF6C6A73);
  static const TextGray = Color(0xFF92929D);
  static const TextDarkGray = Color(0xFF6C6A73);
  static const TextBlack = Color(0xFF3C3B40);
  static const TextYellow = Color(0xFFDEC66B);

  static const ButtonAccent = Color(0xFF30B38A);
  static const ButtonAccentGradientStart = Color(0xFF1BB2AB);
  static const ButtonAccentGradientEnd = Color(0xFF3DC5AC);
  static const ButtonAccentBright = Color(0xFFD4F2F0);
}
