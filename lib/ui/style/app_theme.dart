import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nesternship_flut/ui/style/app_colors.dart';
import 'package:nui_core/model/nui_theme.dart';
import 'package:nui_core/nui_core.dart';
import 'package:nui_core/ui/styling/nui_box_decoration.dart';
import 'package:nui_core/ui/styling/nui_linear_gradient.dart';

//Style color pairs
NUIThemePair<Color> toolbarGradientStart = NUIThemePair(light: AppColors.ToolbarGradientStart, dark: AppColors.ToolbarGradientStart);
NUIThemePair<Color> toolbarGradientEnd = NUIThemePair(light: AppColors.ToolbarGradientEnd, dark: AppColors.ToolbarGradientEnd);
NUIThemePair<Color> transparent = NUIThemePair(light: AppColors.Transparent, dark: AppColors.Transparent);
NUIThemePair<Color> background = NUIThemePair(light: AppColors.Background, dark: AppColors.Background);
NUIThemePair<Color> backgroundGray = NUIThemePair(light: AppColors.BackgroundGray, dark: AppColors.BackgroundGray);
NUIThemePair<Color> backgroundWhite = NUIThemePair(light: AppColors.BackgroundWhite, dark: AppColors.BackgroundWhite);
NUIThemePair<Color> accentDark = NUIThemePair(light: AppColors.AccentDark, dark: AppColors.AccentDark);
NUIThemePair<Color> accentBright = NUIThemePair(light: AppColors.AccentBright, dark: AppColors.AccentBright);
NUIThemePair<Color> accentBright2 = NUIThemePair(light: AppColors.AccentBright2, dark: AppColors.AccentBright2);
NUIThemePair<Color> accentBlueDark = NUIThemePair(light: AppColors.AccentBlueDark, dark: AppColors.AccentBlueDark);
NUIThemePair<Color> accentBlueBright = NUIThemePair(light: AppColors.AccentBlueBright, dark: AppColors.AccentBlueBright);
NUIThemePair<Color> lightBlue = NUIThemePair(light: AppColors.LightBlue, dark: AppColors.LightBlue);
NUIThemePair<Color> yellowHighlight = NUIThemePair(light: AppColors.YellowHighlight, dark: AppColors.YellowHighlight);
NUIThemePair<Color> greenHighlight = NUIThemePair(light: AppColors.GreenHighlight, dark: AppColors.GreenHighlight);
NUIThemePair<Color> greenLight = NUIThemePair(light: AppColors.GreenLight, dark: AppColors.GreenLight);
NUIThemePair<Color> toolbarShadow = NUIThemePair(light: AppColors.ToolbarShadow.withOpacity(0.2), dark: AppColors.ToolbarShadow.withOpacity(0.2));
NUIThemePair<Color> white = NUIThemePair(light: AppColors.White, dark: AppColors.White);
NUIThemePair<Color> black = NUIThemePair(light: AppColors.Black, dark: AppColors.Black);
NUIThemePair<Color> formErrorRed = NUIThemePair(light: AppColors.FormErrorRed, dark: AppColors.FormErrorRed);
NUIThemePair<Color> shadowGrayLight = NUIThemePair(light: AppColors.ShadowGrayLight.withOpacity(0.5), dark: AppColors.ShadowGrayLight.withOpacity(0.5));
NUIThemePair<Color> greenHighlight2 = NUIThemePair(light: AppColors.GreenHighlight2, dark: AppColors.GreenHighlight2);
//Text color pairs
NUIThemePair<Color> textBlack = NUIThemePair(light: AppColors.TextBlack, dark: AppColors.TextBlack);
NUIThemePair<Color> textWhite = NUIThemePair(light: AppColors.TextWhite, dark: AppColors.TextWhite);
NUIThemePair<Color> textBlue = NUIThemePair(light: AppColors.TextBlue, dark: AppColors.TextBlue);
NUIThemePair<Color> textLightGray = NUIThemePair(light: AppColors.TextLightGray, dark: AppColors.TextLightGray);
NUIThemePair<Color> textLightGray2 = NUIThemePair(light: AppColors.TextLightGray2, dark: AppColors.TextLightGray2);
NUIThemePair<Color> textBrown = NUIThemePair(light: AppColors.TextBrown, dark: AppColors.TextBrown);
NUIThemePair<Color> textGray = NUIThemePair(light: AppColors.TextGray, dark: AppColors.TextGray);
NUIThemePair<Color> textDarkGray = NUIThemePair(light: AppColors.TextDarkGray, dark: AppColors.TextDarkGray);
NUIThemePair<Color> textAccent = NUIThemePair(light: AppColors.TextAccent, dark: AppColors.TextAccent);
NUIThemePair<Color> textAccentBright = NUIThemePair(light: AppColors.TextAccentBright, dark: AppColors.TextAccentBright);
NUIThemePair<Color> textAccentBright2 = NUIThemePair(light: AppColors.TextAccentBright2, dark: AppColors.TextAccentBright2);
NUIThemePair<Color> textYellow = NUIThemePair(light: AppColors.TextYellow, dark: AppColors.TextYellow);
NUIThemePair<Color> grayOutline = NUIThemePair(light: AppColors.GrayOutline, dark: AppColors.GrayOutline);
NUIThemePair<Color> grayLine = NUIThemePair(light: AppColors.GrayLine, dark: AppColors.GrayLine);
NUIThemePair<Color> buttonAccentBright = NUIThemePair(light: AppColors.ButtonAccentBright, dark: AppColors.ButtonAccentBright);
NUIThemePair<Color> buttonGradientStart = NUIThemePair(light: AppColors.ButtonAccentGradientStart, dark: AppColors.ButtonAccentGradientStart);
NUIThemePair<Color> buttonGradientEnd = NUIThemePair(light: AppColors.ButtonAccentGradientEnd, dark: AppColors.ButtonAccentGradientEnd);

final toolbarWhiteDecoration = (BuildContext context) => NUIBoxDecoration(
    color: getColor(context, white),
    shadowColor: getColor(context, toolbarShadow),
    shadowRadius: 10,
    shadowOffset: Offset(0, 2));

final defaultGradientBackground = (BuildContext context) => NUIBoxDecoration(
        gradient: NUILinearGradient(colors: [
      getColor(context, accentBlueDark),
      getColor(context, accentBlueBright),
    ], orientation: GradientOrientation.T_B));

Map<int, Color> getPrimaryColor() {
  Map<int, Color> color = {
    50: Color.fromRGBO(0, 0, 0, .1),
    100: Color.fromRGBO(0, 0, 0, .2),
    200: Color.fromRGBO(0, 0, 0, .3),
    300: Color.fromRGBO(0, 0, 0, .4),
    400: Color.fromRGBO(0, 0, 0, .5),
    500: Color.fromRGBO(0, 0, 0, .6),
    600: Color.fromRGBO(0, 0, 0, .7),
    700: Color.fromRGBO(0, 0, 0, .8),
    800: Color.fromRGBO(0, 0, 0, .9),
    900: Color.fromRGBO(0, 0, 0, 1),
  };
  return color;
}

Map<int, Color> getPrimaryColorDark() {
  Map<int, Color> color = {
    50: Color.fromRGBO(0, 0, 0, .1),
    100: Color.fromRGBO(0, 0, 0, .2),
    200: Color.fromRGBO(0, 0, 0, .3),
    300: Color.fromRGBO(0, 0, 0, .4),
    400: Color.fromRGBO(0, 0, 0, .5),
    500: Color.fromRGBO(0, 0, 0, .6),
    600: Color.fromRGBO(0, 0, 0, .7),
    700: Color.fromRGBO(0, 0, 0, .8),
    800: Color.fromRGBO(0, 0, 0, .9),
    900: Color.fromRGBO(0, 0, 0, 1),
  };
  return color;
}

NUIThemeData theme(BuildContext context) => NUIThemeData(
    fontFamily: "Lato",
    primaryColor: NUIThemePair<Color>(
        light: AppColors.Background, dark: AppColors.Background),
    primarySwatch: NUIThemePair<MaterialColor>(
        light: MaterialColor(0xFFFFB15D, getPrimaryColor()),
        dark: MaterialColor(0xFFFFB15D, getPrimaryColorDark())),
    canvasColor: NUIThemePair<Color>(
        light: AppColors.Background, dark: AppColors.Background),
    backgroundColor: NUIThemePair<Color>(
        light: AppColors.Background, dark: AppColors.Background),
    accentColor: accentBright,
    defaultTheme:
        NUIThemeStyle.LIGHT, //Light or Dark theme as the default theme
    colors: [],
    colorScheme: ColorScheme(
        primary: AppColors.AccentBright,
        primaryVariant: AppColors.AccentBright,
        secondary: AppColors.AccentBright,
        secondaryVariant: AppColors.AccentBright,
        surface: AppColors.Background,
        background: AppColors.Background,
        error: AppColors.FormErrorRed,
        onPrimary: AppColors.TextWhite,
        onSecondary: AppColors.TextWhite,
        onSurface: AppColors.TextLightGray,
        onBackground: AppColors.TextLightGray,
        onError: AppColors.TextWhite,
        brightness: Brightness.light));
