import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:nesternship_flut/ui/style/app_colors.dart';
import 'package:nesternship_flut/ui/form/form_styles.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_core/ui/component/nui_app.dart';
import 'package:nui_form/nuif_form.dart';

class ForgotPasswordForm{

  FormInitiator emailForm = (BuildContext context, FormTextChangeListener textChangeListener, FormButtonClickListener buttonClickListener, FormItemSelectListener itemSelectListener) {
    NUIForm form;
    form = NUIForm(
        allButtonClickListener: buttonClickListener,
        allTextChangeListener: textChangeListener,
        allItemSelectListener: itemSelectListener,
        item: NUIFColumnItem(
            inpId: "lytEmailPassword",
            divider: 12,
            items: [
              NUIFTextItem(
                  controller: TextEditingController(text: ""),
                  inpId: "edtEmail",
                  hint: getString('formEmailAddressTextFieldHint'),
                  label: getString('formEmailAddressTextFieldLabel'),
                  maskedInputType: NUIFTextInputType.EMAIL,
                  minLength: 5,
                  validateOnEditComplete: true,
                  validateOnSubmit: true,
                  prefixIcon: Icon(LineAwesomeIcons.envelope, color: getColor(context, textGray)),
                  style: loginPageFormStyle(context)
              ),
            ]
        )
    );

    return form;
  };
}