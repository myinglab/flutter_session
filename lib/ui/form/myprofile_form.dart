import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:nesternship_flut/ui/style/app_colors.dart';
import 'package:nesternship_flut/ui/form/form_styles.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_core/ui/component/nui_app.dart';
import 'package:nui_form/nuif_form.dart';

class ProfileForm {
    FormInitiator myProfileForm = (BuildContext context,
        FormTextChangeListener textChangeListener,
        FormButtonClickListener buttonClickListener,
        FormItemSelectListener itemSelectListener) {
      NUIForm form;
      form = NUIForm(
          allButtonClickListener: buttonClickListener,
          allTextChangeListener: textChangeListener,
          allItemSelectListener: itemSelectListener,
          item: NUIFColumnItem(inpId: "lytMyProfile", divider: 20, items: [
            NUIFTextItem(
              controller: TextEditingController(text: ""),
              inpId: "edtName",
              label: getString('formProfileNameLabel'),
              maskedInputType: NUIFTextInputType.NAME,
              minLength: 5,
              style: loginPageFormStyle(context),
            ),
            NUIFTextItem(
              controller: TextEditingController(text: ""),
              inpId: "edtDateofBirth",
              hint: "Date of birth",
              label: getString('formProfileDateofBirthLabel'),
              style: loginPageFormStyle(context),
              validateOnChange: true,
              maskedInputType: NUIFTextInputType.DATE,
              suffixIcon:
              Icon(Icons.event_note, color: getColor(context, accentBright)),
            ),
            NUIFDropdownItem(
              inpId: "spnGender",
              selections: [GenderSelection("Male"), GenderSelection("Female")],
              label: "Gender",
              style: loginPageFormStyle(context),
            ),
          ]));
      return form;
    };
}

class GenderSelection extends NUIFSelection {
  GenderSelection(this.value);
  String value;
  @override
  String getCaption() => value;

  @override
  String getId() => value;

  @override
  String getValue() => value;
}

class NationalitySelection extends NUIFSelection {
  NationalitySelection(this.value);
  String value;
  @override
  String getCaption() => value;

  @override
  String getId() => value;

  @override
  String getValue() => value;
}
