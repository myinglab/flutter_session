import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nesternship_flut/ui/style/app_colors.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_core/ui/component/nui_app.dart';
import 'package:nui_form/component/nuif_text.dart';

NUIFTextStyle inputTextStyle14(BuildContext context) => NUIFTextStyle(
  labelStyle: getFont(context, size: 14, color: textWhite, isBold: false),
  helperStyle: getFont(context, size: 12, color: textWhite, isBold: false),
  errorHelperStyle: getFont(context, size: 12, color: formErrorRed, isBold: false),
  errorLabelStyle: getFont(context, size: 14, color: textWhite, isBold: false),
  border: OutlineInputBorder(borderSide: BorderSide(color: getColor(context, white).withOpacity(0.6), width: 1)),
  focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: getColor(context, accentBright), width: 2)),
  design: NUIFTextDesign.BORDER,
  dense: true,
  cursor: NUIFTextCursor(color: getColor(context, textWhite), cursorWidth: 2),
  textStyle: getFont(context, size: 16, color: textWhite, isBold: false),
  hintStyle: getFont(context, size: 16, singleColor: AppColors.TextWhite.withOpacity(0.6), isBold: false),
);

NUIFTextStyle loginPageFormStyle(BuildContext context) => NUIFTextStyle(
  labelStyle: getFont(context, size: 16, color: accentBright, isBold: false),
  helperStyle: getFont(context, size: 12, color: accentBright, isBold: false),
  errorHelperStyle: getFont(context, size: 12, color: formErrorRed, isBold: false),
  border: OutlineInputBorder(borderSide: BorderSide(color: getColor(context, textGray), width: 1)),
  focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: getColor(context, accentBright), width: 2)),
  errorBorder: OutlineInputBorder(borderSide: BorderSide(color: getColor(context, formErrorRed), width: 1)),
  errorFocusedBorder: OutlineInputBorder(borderSide: BorderSide(color: getColor(context, formErrorRed), width: 2)),
  design: NUIFTextDesign.BORDER,
  dense: true,
  cursor: NUIFTextCursor(color: getColor(context, accentBright), cursorWidth: 2),
  textStyle: getFont(context, size: 16, color: textDarkGray, isBold: false),
  hintStyle: getFont(context, size: 16, singleColor: getColor(context, textLightGray), isBold: false),
);
