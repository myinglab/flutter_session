import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:nesternship_flut/ui/style/app_colors.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_core/ui/component/nui_app.dart';
import 'package:nui_form/nuif_form.dart';

import 'form_styles.dart';

NUIForm countrySelectionForm(BuildContext context, Function onMoreClick, {bool forProfile: false}) {
  NUIForm form;
  form = NUIForm(
      item: NUIFColumnItem(inpId: "lytCountry", divider: 20, items: [
    NUIFTextItem(
        controller: TextEditingController(text: ""),
        inpId: "edtCountry",
        hint: "Select a country", 
        label: forProfile != true ? "Select a Country" : "Nationality",
        maskedInputType: NUIFTextInputType.NAME,
        enabled: false,
        minLength: 5,
        validateOnChange: true,
        suffixIcon: Icon(LineAwesomeIcons.angle_down, color: getColor(context, white)),
        style: forProfile != true ? inputTextStyle14(context) : loginPageFormStyle(context),
        suffixOnTap: () {
          if (onMoreClick != null) onMoreClick();
        }),
  ]));
  return form;
}
