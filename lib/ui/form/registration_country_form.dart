import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nesternship_flut/ui/style/app_colors.dart';
import 'package:nesternship_flut/ui/form/form_styles.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_core/ui/component/nui_app.dart';
import 'package:nui_form/nuif_form.dart';

class RegistrationCountryForm {
  FormInitiator selectCountryForm = (BuildContext context, FormTextChangeListener textChangeListener, FormButtonClickListener buttonClickListener, FormItemSelectListener itemSelectListener) {
    NUIForm form;
    form = NUIForm(
        allButtonClickListener: buttonClickListener,
        allTextChangeListener: textChangeListener,
        allItemSelectListener: itemSelectListener,
        item: NUIFTextItem(
            controller: TextEditingController(text: ""),
            inpId: "spnCountry",
            hint: "Select a Country",
            label: "Email",
            maskedInputType: NUIFTextInputType.EMAIL,
            minLength: 5,
            validateOnChange: true,
            prefixIcon: Icon(Icons.email, color: getColor(context, white)),
            style: inputTextStyle14(context)
        ),
    );

    return form;
  };
}