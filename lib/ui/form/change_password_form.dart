import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:nesternship_flut/ui/style/app_colors.dart';
import 'package:nesternship_flut/ui/form/form_styles.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_core/ui/component/nui_app.dart';
import 'package:nui_form/nuif_form.dart';

class ChangePasswordForm{
  FormInitiator changePasswordForm = (BuildContext context, FormTextChangeListener textChangeListener, FormButtonClickListener buttonClickListener, FormItemSelectListener itemSelectListener) {
    NUIForm form;
    form = NUIForm(
        allButtonClickListener: buttonClickListener,
        allTextChangeListener: textChangeListener,
        allItemSelectListener: itemSelectListener,
        item: NUIFColumnItem(
            inpId: "lytChangePassword",
            divider: 24,
            items: [
              NUIFTextItem(
                  controller: TextEditingController(text: ""),
                  inpId: "currentPassword",
                  hint: getString('formCurrentPasswordTextFieldHint'),
                  label: getString('formCurrentPasswordTextFieldLabel'),
                  maskedInputType: NUIFTextInputType.PASSWORD,
                  minLength: 8,
                  validateOnChange: false,
                  validateOnEditComplete: true,
                  validateOnSubmit: true,
                  iconPasswordVisible: Icon(LineAwesomeIcons.eye, color: getColor(context, textGray)),
                  iconPasswordHide: Icon(LineAwesomeIcons.eye_slash, color: getColor(context, textGray)),
                  suffixIcon: Icon(LineAwesomeIcons.eye, color: getColor(context, textGray)),
                  style: loginPageFormStyle(context)
              ),
              NUIFTextItem(
                  controller: TextEditingController(text: ""),
                  inpId: "newPassword",
                  hint: getString('formNewPasswordTextFieldHint'),
                  label: getString('formNewPasswordTextFieldLabel'),
                  maskedInputType: NUIFTextInputType.PASSWORD_NEW,
                  minLength: 8,
                  validateOnChange: false,
                  validateOnEditComplete: true,
                  validateOnSubmit: true,
                  iconPasswordVisible: Icon(LineAwesomeIcons.eye, color: getColor(context, textGray)),
                  iconPasswordHide: Icon(LineAwesomeIcons.eye_slash, color: getColor(context, textGray)),
                  suffixIcon: Icon(LineAwesomeIcons.eye, color: getColor(context, textGray)),
                  style: loginPageFormStyle(context)
              ),
            ]
        )
    );
    return form;
  };
}
