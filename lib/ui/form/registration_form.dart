import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:nesternship_flut/ui/style/app_colors.dart';
import 'package:nesternship_flut/ui/form/form_styles.dart';
import 'package:nesternship_flut/ui/style/app_theme.dart';
import 'package:nui_core/ui/component/nui_app.dart';
import 'package:nui_form/nuif_form.dart';

class RegistrationForm {
  FormInitiator emailPasswordForm = (BuildContext context,
      FormTextChangeListener textChangeListener,
      FormButtonClickListener buttonClickListener,
      FormItemSelectListener itemSelectListener) {
    NUIForm form;
    form = NUIForm(
        allButtonClickListener: buttonClickListener,
        allTextChangeListener: textChangeListener,
        allItemSelectListener: itemSelectListener,
        item: NUIFColumnItem(inpId: "lytEmailPassword", divider: 20, items: [
          NUIFTextItem(
              controller: TextEditingController(text: ""),
              inpId: "edtEmail",
              hint: getString('formEmailAddressTextFieldHint'),
              label: getString('formEmailAddressTextFieldLabel'),
              maskedInputType: NUIFTextInputType.EMAIL,
              minLength: 5,
              validateOnEditComplete: true,
              validateOnSubmit: true,
              compulsory: true,
              prefixIcon: Icon(Icons.email, color: getColor(context, white)),
              style: inputTextStyle14(context)),
          NUIFTextItem(
              controller: TextEditingController(text: ""),
              inpId: "edtPassword",
              hint: getString('formPasswordTextFieldHint'),
              label: getString('formPasswordTextFieldLabel'),
              style: inputTextStyle14(context),
              validateOnChange: false,
              validateOnEditComplete: true,
              validateOnSubmit: true,
              compulsory: true,
              maskedInputType: NUIFTextInputType.PASSWORD_NEW,
              iconPasswordVisible: Icon(LineAwesomeIcons.eye, color: getColor(context, textWhite)),
              iconPasswordHide: Icon(LineAwesomeIcons.eye_slash, color: getColor(context, textWhite)),
              prefixIcon: Icon(Icons.lock, color: getColor(context, white)),
              suffixIcon: Icon(
                Icons.remove_red_eye,
                color: getColor(context, white),
              ),
              obscure: true),
        ]));

    return form;
  };
}
