import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:nesternship_flut/ui/style/app_colors.dart';
import 'package:nui_form/nuif_form.dart';
import 'form_styles.dart';

class RegistrationNameForm {
  NUIForm registrationForm(BuildContext context,
      FormTextChangeListener textChangeListener,
      FormButtonClickListener buttonClickListener,
      FormItemSelectListener itemSelectListener, Function(String) onSubmitted){

    return NUIForm(
          allButtonClickListener: buttonClickListener,
          allTextChangeListener: textChangeListener,
          allItemSelectListener: itemSelectListener,
          item: NUIFColumnItem(inpId: "lytName", divider: 20, items: [
            NUIFTextItem(
              controller: TextEditingController(text: ""),
              inpId: "edtName",
              hint: "Your Name",
              label: "Name",
              maskedInputType: NUIFTextInputType.NAME,
              enabled: true,
              minLength: 2,
              validateOnEditComplete: true,
              validateOnSubmit: true,
              onSubmitted: (text){
                if(onSubmitted != null) onSubmitted(text);
              },
              style: inputTextStyle14(context),
            )
          ]));
  }

}
