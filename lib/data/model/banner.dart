import 'dart:ffi';

import 'package:flutter/cupertino.dart';

class BannerImage {
  int bannerId;
  String bannerUrl;
  String actionUrl;
  String bannerTitle;
  String bannerDescription;
  int sequence;
  int status;
  BannerImage({this.bannerId , this.bannerUrl , this.actionUrl , this.bannerTitle, this.bannerDescription, this.sequence , this.status});
  String title = "Nestle Live Leadership Talk";
  String description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ultrices, augue ut condimentum maximus, ex eros commodo arcu, vel imperdiet lacus metus quis est";
  String bannerImage;
  double transparency = 0.0;
  BannerImage.landing(this.title, this.description, this.bannerImage);
}