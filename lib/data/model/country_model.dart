import 'package:flutter/material.dart';

class Country {
  Country({this.countryId, this.countryName, this.countryCode ,this.isNestleCountry});
  int countryId;
  String countryName;
  String countryCode;
  bool isNestleCountry;
}
