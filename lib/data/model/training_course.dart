import 'package:nui_core/math/nui_math_util.dart';

class TrainingCourse {

  static const String COVER1 = "https://firebasestorage.googleapis.com/v0/b/xplore-cbd14.appspot.com/o/uat%2Fimages%2Ftests%2Fbookcover1.jpg?alt=media&token=4ec63482-91c5-463f-a6ad-4850d9eed9e8";
  static const String COVER2 = "https://firebasestorage.googleapis.com/v0/b/xplore-cbd14.appspot.com/o/uat%2Fimages%2Ftests%2Fbookcover2.jpg?alt=media&token=ff8462f6-ce3e-4117-8a44-f78b482b1788";
  static const String COVER3 = "https://firebasestorage.googleapis.com/v0/b/xplore-cbd14.appspot.com/o/uat%2Fimages%2Ftests%2Fbookcover3.jpg?alt=media&token=5d5e2fe5-07b6-4ecc-82a9-2e274c0cd99d";
  static const String COVER4 = "https://firebasestorage.googleapis.com/v0/b/xplore-cbd14.appspot.com/o/uat%2Fimages%2Ftests%2Fbookcover4.jpg?alt=media&token=3cffa73a-0d8c-425e-873d-b4050c06426a";

  String _imageUrl;
  String get imageUrl {
    if(_imageUrl != null) return _imageUrl;

    final covers = [COVER1, COVER2, COVER3, COVER4];
    final randomNum = NUIMathUtil.random(0, covers.length - 1);
    _imageUrl = covers[randomNum];
    return _imageUrl;
  }

  String trainingType = "Mandatory training";
  String title = "Nestle Purpose and Values";
  String type = "Video";
  String duration = "02:05min";
  String contentUrl = "https://vod-progressive.akamaized.net/exp=1608207379~acl=%2A%2F1075861707.mp4%2A~hmac=56374ca4d83aaa8c74aeaae64867436b7193dc9f878c348225a63aee9473bd02/vimeo-prod-skyfire-std-us/01/2097/11/285489976/1075861707.mp4?download=1&filename=Pexels+Videos+1333384.mp4";
  String description = "Praesent sit amet leo dictum, tincidunt sapien sit amet, laoreet justo. Phasellus in sapien in mi pellentesque mollis gravida id sem. ";
}
