import 'package:nui_core/data/nui_data_util.dart';

class UserProfile{
  String id;
  String memberEmail;
  String memberName;
  String userId;
  String avatar;
  String dateOfBirth;
  String gender;
  String originCountry;
  String nestleCountry;

  UserProfile({this.id,
    this.memberEmail,
    this.memberName,
    this.userId,
    this.gender,
    this.dateOfBirth,
    this.nestleCountry,
    this.originCountry,
    this.avatar,
  });

  String getUpdateProfileSteps(){
    int totalCount = 1;
    if(!isNullOrEmpty(dateOfBirth)) totalCount++;
    if(!isNullOrEmpty(gender)) totalCount++;
    if(!isNullOrEmpty(originCountry)) totalCount++;

    if(totalCount == 4) return null;
    return "Update Profile now. $totalCount/4 completed";
  }

  /*static UserProfile getDummy(){
    final user = UserProfile();
    user.name = "Neoh Wei";
    user.dateOfBirth = "03/03/1997";
    user.gender = "Male";
    user.nationality = "Malaysia";
    user.profileImage = "https://pic.17qq.com/uploads/mpnkskqmsy.jpeg";
    return user;
  }*/
}