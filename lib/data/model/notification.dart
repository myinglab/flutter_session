class UserNotification{
  UserNotification({
    this.notificationId,
    this.userId,
    this.notificationContentId,
    this.title,
    this.description,
    this.dateTime,
    this.bannerUrl,
    this.read});

  int notificationId;
  int userId;
  int notificationContentId;
  String title;
  String description;
  DateTime dateTime;
  String bannerUrl;
  bool read;
}