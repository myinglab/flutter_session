import 'package:nui_database/models/nui_database_model.dart';
import 'package:nui_database/nui_database.dart';

class AppDatabase extends NUIDatabaseModel{
  static const String name = "nesternshipdb.db";
  static NUIDatabase get(){
    return NUIDatabase.get(name);
  }

  String databaseName() => name;

  List<NUIDBEntityMapper> entities() => [

  ];
}